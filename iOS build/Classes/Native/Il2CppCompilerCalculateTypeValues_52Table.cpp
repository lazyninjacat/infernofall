﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector
struct Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable[]
struct Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString
struct Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence
struct Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set
struct Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser
struct Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DefiniteLengthInputStream
struct DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString
struct DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean
struct DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated[]
struct DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime
struct DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger
struct DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier[]
struct DerObjectIdentifierU5BU5D_tC595A7256A12DE98032B8267A8795C3EC8F3D6B9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IAsn1String
struct IAsn1String_t7C8CA245B8AF08115FC8EC5EB355F8FAF48B3DBE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AccessDescription[]
struct AccessDescriptionU5BU5D_t684A1B002EAB096C84A881888BDDAE8536F2050B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertIssuer
struct AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertValidityPeriod
struct AttCertValidityPeriod_tE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo
struct AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DistributionPointName
struct DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName
struct GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName[]
struct GeneralNameU5BU5D_t8C8E87C7B1F2854AD266B704A0BA432004D2DC73;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames
struct GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Holder
struct Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial
struct IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ObjectDigestInfo
struct ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyInformation[]
struct PolicyInformationU5BU5D_t6B99B0474D52C0BDF1D00BAEB3C65DD792091D5D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ReasonFlags
struct ReasonFlags_tBB7FD42D692CC7182B9F721F5558208A7EBF1C3A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList
struct TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure
struct X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions
struct X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHValidationParms
struct DHValidationParms_t63346AFA3FBC424FF674B40E14C3E078B994BF10;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.KeySpecificInfo
struct KeySpecificInfo_t1D036F45CEDCB08508825FC00CA64E1C4EC96D1B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters
struct X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECPoint
struct X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9FieldID
struct X9FieldID_t87179DEE274AEDFB62026D21D8059D527DF43814;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve
struct ECCurve_t6071986B64066FBF97315592BE971355FA584A39;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement
struct ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint
struct ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#define ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct  Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifndef ASN1GENERATOR_T56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A_H
#define ASN1GENERATOR_T56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Generator
struct  Asn1Generator_t56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A  : public RuntimeObject
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Generator::_out
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____out_0;

public:
	inline static int32_t get_offset_of__out_0() { return static_cast<int32_t>(offsetof(Asn1Generator_t56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A, ____out_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__out_0() const { return ____out_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__out_0() { return &____out_0; }
	inline void set__out_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____out_0 = value;
		Il2CppCodeGenWriteBarrier((&____out_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1GENERATOR_T56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A_H
#ifndef DEROCTETSTRINGPARSER_T79DFE7483E35FB0A440C25C98C7DD3E95E32341C_H
#define DEROCTETSTRINGPARSER_T79DFE7483E35FB0A440C25C98C7DD3E95E32341C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerOctetStringParser
struct  DerOctetStringParser_t79DFE7483E35FB0A440C25C98C7DD3E95E32341C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DefiniteLengthInputStream BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerOctetStringParser::stream
	DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141 * ___stream_0;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(DerOctetStringParser_t79DFE7483E35FB0A440C25C98C7DD3E95E32341C, ___stream_0)); }
	inline DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141 * get_stream_0() const { return ___stream_0; }
	inline DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEROCTETSTRINGPARSER_T79DFE7483E35FB0A440C25C98C7DD3E95E32341C_H
#ifndef DERSEQUENCEPARSER_T9487D03AE4F6C2923CB5FB92B384B7448CE0311E_H
#define DERSEQUENCEPARSER_T9487D03AE4F6C2923CB5FB92B384B7448CE0311E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSequenceParser
struct  DerSequenceParser_t9487D03AE4F6C2923CB5FB92B384B7448CE0311E  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSequenceParser::_parser
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * ____parser_0;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(DerSequenceParser_t9487D03AE4F6C2923CB5FB92B384B7448CE0311E, ____parser_0)); }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * get__parser_0() const { return ____parser_0; }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSEQUENCEPARSER_T9487D03AE4F6C2923CB5FB92B384B7448CE0311E_H
#ifndef DERSETPARSER_T6BEA112499DB4FF0CA867CA454F4B8E8F759DC50_H
#define DERSETPARSER_T6BEA112499DB4FF0CA867CA454F4B8E8F759DC50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSetParser
struct  DerSetParser_t6BEA112499DB4FF0CA867CA454F4B8E8F759DC50  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSetParser::_parser
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * ____parser_0;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(DerSetParser_t6BEA112499DB4FF0CA867CA454F4B8E8F759DC50, ____parser_0)); }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * get__parser_0() const { return ____parser_0; }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSETPARSER_T6BEA112499DB4FF0CA867CA454F4B8E8F759DC50_H
#ifndef OIDTOKENIZER_TA4B8515FFAF2CA7E83D8322B929C889649577199_H
#define OIDTOKENIZER_TA4B8515FFAF2CA7E83D8322B929C889649577199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.OidTokenizer
struct  OidTokenizer_tA4B8515FFAF2CA7E83D8322B929C889649577199  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.OidTokenizer::oid
	String_t* ___oid_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.OidTokenizer::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_oid_0() { return static_cast<int32_t>(offsetof(OidTokenizer_tA4B8515FFAF2CA7E83D8322B929C889649577199, ___oid_0)); }
	inline String_t* get_oid_0() const { return ___oid_0; }
	inline String_t** get_address_of_oid_0() { return &___oid_0; }
	inline void set_oid_0(String_t* value)
	{
		___oid_0 = value;
		Il2CppCodeGenWriteBarrier((&___oid_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(OidTokenizer_tA4B8515FFAF2CA7E83D8322B929C889649577199, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDTOKENIZER_TA4B8515FFAF2CA7E83D8322B929C889649577199_H
#ifndef ATTRIBUTETABLE_T098C85EA280D051F2AB13FCF83240A14BB5921D0_H
#define ATTRIBUTETABLE_T098C85EA280D051F2AB13FCF83240A14BB5921D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeTable
struct  AttributeTable_t098C85EA280D051F2AB13FCF83240A14BB5921D0  : public RuntimeObject
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeTable::attributes
	RuntimeObject* ___attributes_0;

public:
	inline static int32_t get_offset_of_attributes_0() { return static_cast<int32_t>(offsetof(AttributeTable_t098C85EA280D051F2AB13FCF83240A14BB5921D0, ___attributes_0)); }
	inline RuntimeObject* get_attributes_0() const { return ___attributes_0; }
	inline RuntimeObject** get_address_of_attributes_0() { return &___attributes_0; }
	inline void set_attributes_0(RuntimeObject* value)
	{
		___attributes_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETABLE_T098C85EA280D051F2AB13FCF83240A14BB5921D0_H
#ifndef ECNAMEDCURVETABLE_T731CDB9CDFC0570FBDA70A17A198E014401715B2_H
#define ECNAMEDCURVETABLE_T731CDB9CDFC0570FBDA70A17A198E014401715B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.ECNamedCurveTable
struct  ECNamedCurveTable_t731CDB9CDFC0570FBDA70A17A198E014401715B2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECNAMEDCURVETABLE_T731CDB9CDFC0570FBDA70A17A198E014401715B2_H
#ifndef X962NAMEDCURVES_TC5338F1681E7B1DA60CCDCB58294991C7FAC49D1_H
#define X962NAMEDCURVES_TC5338F1681E7B1DA60CCDCB58294991C7FAC49D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves
struct  X962NamedCurves_tC5338F1681E7B1DA60CCDCB58294991C7FAC49D1  : public RuntimeObject
{
public:

public:
};

struct X962NamedCurves_tC5338F1681E7B1DA60CCDCB58294991C7FAC49D1_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves::curves
	RuntimeObject* ___curves_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves::names
	RuntimeObject* ___names_2;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(X962NamedCurves_tC5338F1681E7B1DA60CCDCB58294991C7FAC49D1_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_curves_1() { return static_cast<int32_t>(offsetof(X962NamedCurves_tC5338F1681E7B1DA60CCDCB58294991C7FAC49D1_StaticFields, ___curves_1)); }
	inline RuntimeObject* get_curves_1() const { return ___curves_1; }
	inline RuntimeObject** get_address_of_curves_1() { return &___curves_1; }
	inline void set_curves_1(RuntimeObject* value)
	{
		___curves_1 = value;
		Il2CppCodeGenWriteBarrier((&___curves_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(X962NamedCurves_tC5338F1681E7B1DA60CCDCB58294991C7FAC49D1_StaticFields, ___names_2)); }
	inline RuntimeObject* get_names_2() const { return ___names_2; }
	inline RuntimeObject** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(RuntimeObject* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X962NAMEDCURVES_TC5338F1681E7B1DA60CCDCB58294991C7FAC49D1_H
#ifndef X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#define X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct  X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder::parameters
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * ___parameters_0;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB, ___parameters_0)); }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * get_parameters_0() const { return ___parameters_0; }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifndef X9INTEGERCONVERTER_T1BCF1565B0B650B877BF3AD1F8B4C22C83F65616_H
#define X9INTEGERCONVERTER_T1BCF1565B0B650B877BF3AD1F8B4C22C83F65616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9IntegerConverter
struct  X9IntegerConverter_t1BCF1565B0B650B877BF3AD1F8B4C22C83F65616  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9INTEGERCONVERTER_T1BCF1565B0B650B877BF3AD1F8B4C22C83F65616_H
#ifndef X9OBJECTIDENTIFIERS_TA801960EE331C7E21CEA8A62855FA8520B297EB6_H
#define X9OBJECTIDENTIFIERS_TA801960EE331C7E21CEA8A62855FA8520B297EB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers
struct  X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6  : public RuntimeObject
{
public:

public:
};

struct X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ansi_X9_62
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ansi_X9_62_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::IdFieldType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdFieldType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::PrimeField
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PrimeField_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::CharacteristicTwoField
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CharacteristicTwoField_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::GNBasis
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GNBasis_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::TPBasis
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___TPBasis_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::PPBasis
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PPBasis_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::id_ecSigType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_ecSigType_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ECDsaWithSha1_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::id_publicKeyType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_publicKeyType_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::IdECPublicKey
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdECPublicKey_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ECDsaWithSha2_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ECDsaWithSha224_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ECDsaWithSha256_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ECDsaWithSha384_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ECDsaWithSha512_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::EllipticCurve
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EllipticCurve_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::CTwoCurve
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CTwoCurve_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb163v1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Pnb163v1_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb163v2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Pnb163v2_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb163v3
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Pnb163v3_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb176w1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Pnb176w1_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb191v1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Tnb191v1_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb191v2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Tnb191v2_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb191v3
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Tnb191v3_27;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Onb191v4
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Onb191v4_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Onb191v5
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Onb191v5_29;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb208w1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Pnb208w1_30;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb239v1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Tnb239v1_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb239v2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Tnb239v2_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb239v3
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Tnb239v3_33;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Onb239v4
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Onb239v4_34;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Onb239v5
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Onb239v5_35;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb272w1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Pnb272w1_36;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb304w1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Pnb304w1_37;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb359v1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Tnb359v1_38;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb368w1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Pnb368w1_39;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb431r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C2Tnb431r1_40;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::PrimeCurve
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PrimeCurve_41;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime192v1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Prime192v1_42;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime192v2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Prime192v2_43;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime192v3
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Prime192v3_44;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime239v1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Prime239v1_45;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime239v2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Prime239v2_46;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime239v3
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Prime239v3_47;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime256v1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Prime256v1_48;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::IdDsa
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdDsa_49;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::IdDsaWithSha1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdDsaWithSha1_50;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::X9x63Scheme
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___X9x63Scheme_51;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHSinglePassStdDHSha1KdfScheme
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DHSinglePassStdDHSha1KdfScheme_52;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHSinglePassCofactorDHSha1KdfScheme
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DHSinglePassCofactorDHSha1KdfScheme_53;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::MqvSinglePassSha1KdfScheme
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MqvSinglePassSha1KdfScheme_54;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ansi_x9_42
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ansi_x9_42_55;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHPublicNumber
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DHPublicNumber_56;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::X9x42Schemes
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___X9x42Schemes_57;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHStatic
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DHStatic_58;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHEphem
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DHEphem_59;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHOneFlow
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DHOneFlow_60;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHHybrid1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DHHybrid1_61;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHHybrid2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DHHybrid2_62;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHHybridOneFlow
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DHHybridOneFlow_63;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Mqv2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Mqv2_64;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Mqv1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Mqv1_65;

public:
	inline static int32_t get_offset_of_ansi_X9_62_1() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___ansi_X9_62_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ansi_X9_62_1() const { return ___ansi_X9_62_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ansi_X9_62_1() { return &___ansi_X9_62_1; }
	inline void set_ansi_X9_62_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ansi_X9_62_1 = value;
		Il2CppCodeGenWriteBarrier((&___ansi_X9_62_1), value);
	}

	inline static int32_t get_offset_of_IdFieldType_2() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___IdFieldType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdFieldType_2() const { return ___IdFieldType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdFieldType_2() { return &___IdFieldType_2; }
	inline void set_IdFieldType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdFieldType_2 = value;
		Il2CppCodeGenWriteBarrier((&___IdFieldType_2), value);
	}

	inline static int32_t get_offset_of_PrimeField_3() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___PrimeField_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PrimeField_3() const { return ___PrimeField_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PrimeField_3() { return &___PrimeField_3; }
	inline void set_PrimeField_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PrimeField_3 = value;
		Il2CppCodeGenWriteBarrier((&___PrimeField_3), value);
	}

	inline static int32_t get_offset_of_CharacteristicTwoField_4() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___CharacteristicTwoField_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CharacteristicTwoField_4() const { return ___CharacteristicTwoField_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CharacteristicTwoField_4() { return &___CharacteristicTwoField_4; }
	inline void set_CharacteristicTwoField_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CharacteristicTwoField_4 = value;
		Il2CppCodeGenWriteBarrier((&___CharacteristicTwoField_4), value);
	}

	inline static int32_t get_offset_of_GNBasis_5() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___GNBasis_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GNBasis_5() const { return ___GNBasis_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GNBasis_5() { return &___GNBasis_5; }
	inline void set_GNBasis_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GNBasis_5 = value;
		Il2CppCodeGenWriteBarrier((&___GNBasis_5), value);
	}

	inline static int32_t get_offset_of_TPBasis_6() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___TPBasis_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_TPBasis_6() const { return ___TPBasis_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_TPBasis_6() { return &___TPBasis_6; }
	inline void set_TPBasis_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___TPBasis_6 = value;
		Il2CppCodeGenWriteBarrier((&___TPBasis_6), value);
	}

	inline static int32_t get_offset_of_PPBasis_7() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___PPBasis_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PPBasis_7() const { return ___PPBasis_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PPBasis_7() { return &___PPBasis_7; }
	inline void set_PPBasis_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PPBasis_7 = value;
		Il2CppCodeGenWriteBarrier((&___PPBasis_7), value);
	}

	inline static int32_t get_offset_of_id_ecSigType_9() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___id_ecSigType_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_ecSigType_9() const { return ___id_ecSigType_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_ecSigType_9() { return &___id_ecSigType_9; }
	inline void set_id_ecSigType_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_ecSigType_9 = value;
		Il2CppCodeGenWriteBarrier((&___id_ecSigType_9), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha1_10() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___ECDsaWithSha1_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ECDsaWithSha1_10() const { return ___ECDsaWithSha1_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ECDsaWithSha1_10() { return &___ECDsaWithSha1_10; }
	inline void set_ECDsaWithSha1_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ECDsaWithSha1_10 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha1_10), value);
	}

	inline static int32_t get_offset_of_id_publicKeyType_12() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___id_publicKeyType_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_publicKeyType_12() const { return ___id_publicKeyType_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_publicKeyType_12() { return &___id_publicKeyType_12; }
	inline void set_id_publicKeyType_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_publicKeyType_12 = value;
		Il2CppCodeGenWriteBarrier((&___id_publicKeyType_12), value);
	}

	inline static int32_t get_offset_of_IdECPublicKey_13() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___IdECPublicKey_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdECPublicKey_13() const { return ___IdECPublicKey_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdECPublicKey_13() { return &___IdECPublicKey_13; }
	inline void set_IdECPublicKey_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdECPublicKey_13 = value;
		Il2CppCodeGenWriteBarrier((&___IdECPublicKey_13), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha2_14() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___ECDsaWithSha2_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ECDsaWithSha2_14() const { return ___ECDsaWithSha2_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ECDsaWithSha2_14() { return &___ECDsaWithSha2_14; }
	inline void set_ECDsaWithSha2_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ECDsaWithSha2_14 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha2_14), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha224_15() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___ECDsaWithSha224_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ECDsaWithSha224_15() const { return ___ECDsaWithSha224_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ECDsaWithSha224_15() { return &___ECDsaWithSha224_15; }
	inline void set_ECDsaWithSha224_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ECDsaWithSha224_15 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha224_15), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha256_16() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___ECDsaWithSha256_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ECDsaWithSha256_16() const { return ___ECDsaWithSha256_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ECDsaWithSha256_16() { return &___ECDsaWithSha256_16; }
	inline void set_ECDsaWithSha256_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ECDsaWithSha256_16 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha256_16), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha384_17() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___ECDsaWithSha384_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ECDsaWithSha384_17() const { return ___ECDsaWithSha384_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ECDsaWithSha384_17() { return &___ECDsaWithSha384_17; }
	inline void set_ECDsaWithSha384_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ECDsaWithSha384_17 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha384_17), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha512_18() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___ECDsaWithSha512_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ECDsaWithSha512_18() const { return ___ECDsaWithSha512_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ECDsaWithSha512_18() { return &___ECDsaWithSha512_18; }
	inline void set_ECDsaWithSha512_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ECDsaWithSha512_18 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha512_18), value);
	}

	inline static int32_t get_offset_of_EllipticCurve_19() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___EllipticCurve_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EllipticCurve_19() const { return ___EllipticCurve_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EllipticCurve_19() { return &___EllipticCurve_19; }
	inline void set_EllipticCurve_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EllipticCurve_19 = value;
		Il2CppCodeGenWriteBarrier((&___EllipticCurve_19), value);
	}

	inline static int32_t get_offset_of_CTwoCurve_20() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___CTwoCurve_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CTwoCurve_20() const { return ___CTwoCurve_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CTwoCurve_20() { return &___CTwoCurve_20; }
	inline void set_CTwoCurve_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CTwoCurve_20 = value;
		Il2CppCodeGenWriteBarrier((&___CTwoCurve_20), value);
	}

	inline static int32_t get_offset_of_C2Pnb163v1_21() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Pnb163v1_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Pnb163v1_21() const { return ___C2Pnb163v1_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Pnb163v1_21() { return &___C2Pnb163v1_21; }
	inline void set_C2Pnb163v1_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Pnb163v1_21 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb163v1_21), value);
	}

	inline static int32_t get_offset_of_C2Pnb163v2_22() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Pnb163v2_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Pnb163v2_22() const { return ___C2Pnb163v2_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Pnb163v2_22() { return &___C2Pnb163v2_22; }
	inline void set_C2Pnb163v2_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Pnb163v2_22 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb163v2_22), value);
	}

	inline static int32_t get_offset_of_C2Pnb163v3_23() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Pnb163v3_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Pnb163v3_23() const { return ___C2Pnb163v3_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Pnb163v3_23() { return &___C2Pnb163v3_23; }
	inline void set_C2Pnb163v3_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Pnb163v3_23 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb163v3_23), value);
	}

	inline static int32_t get_offset_of_C2Pnb176w1_24() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Pnb176w1_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Pnb176w1_24() const { return ___C2Pnb176w1_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Pnb176w1_24() { return &___C2Pnb176w1_24; }
	inline void set_C2Pnb176w1_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Pnb176w1_24 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb176w1_24), value);
	}

	inline static int32_t get_offset_of_C2Tnb191v1_25() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Tnb191v1_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Tnb191v1_25() const { return ___C2Tnb191v1_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Tnb191v1_25() { return &___C2Tnb191v1_25; }
	inline void set_C2Tnb191v1_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Tnb191v1_25 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb191v1_25), value);
	}

	inline static int32_t get_offset_of_C2Tnb191v2_26() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Tnb191v2_26)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Tnb191v2_26() const { return ___C2Tnb191v2_26; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Tnb191v2_26() { return &___C2Tnb191v2_26; }
	inline void set_C2Tnb191v2_26(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Tnb191v2_26 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb191v2_26), value);
	}

	inline static int32_t get_offset_of_C2Tnb191v3_27() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Tnb191v3_27)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Tnb191v3_27() const { return ___C2Tnb191v3_27; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Tnb191v3_27() { return &___C2Tnb191v3_27; }
	inline void set_C2Tnb191v3_27(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Tnb191v3_27 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb191v3_27), value);
	}

	inline static int32_t get_offset_of_C2Onb191v4_28() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Onb191v4_28)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Onb191v4_28() const { return ___C2Onb191v4_28; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Onb191v4_28() { return &___C2Onb191v4_28; }
	inline void set_C2Onb191v4_28(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Onb191v4_28 = value;
		Il2CppCodeGenWriteBarrier((&___C2Onb191v4_28), value);
	}

	inline static int32_t get_offset_of_C2Onb191v5_29() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Onb191v5_29)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Onb191v5_29() const { return ___C2Onb191v5_29; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Onb191v5_29() { return &___C2Onb191v5_29; }
	inline void set_C2Onb191v5_29(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Onb191v5_29 = value;
		Il2CppCodeGenWriteBarrier((&___C2Onb191v5_29), value);
	}

	inline static int32_t get_offset_of_C2Pnb208w1_30() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Pnb208w1_30)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Pnb208w1_30() const { return ___C2Pnb208w1_30; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Pnb208w1_30() { return &___C2Pnb208w1_30; }
	inline void set_C2Pnb208w1_30(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Pnb208w1_30 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb208w1_30), value);
	}

	inline static int32_t get_offset_of_C2Tnb239v1_31() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Tnb239v1_31)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Tnb239v1_31() const { return ___C2Tnb239v1_31; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Tnb239v1_31() { return &___C2Tnb239v1_31; }
	inline void set_C2Tnb239v1_31(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Tnb239v1_31 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb239v1_31), value);
	}

	inline static int32_t get_offset_of_C2Tnb239v2_32() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Tnb239v2_32)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Tnb239v2_32() const { return ___C2Tnb239v2_32; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Tnb239v2_32() { return &___C2Tnb239v2_32; }
	inline void set_C2Tnb239v2_32(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Tnb239v2_32 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb239v2_32), value);
	}

	inline static int32_t get_offset_of_C2Tnb239v3_33() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Tnb239v3_33)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Tnb239v3_33() const { return ___C2Tnb239v3_33; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Tnb239v3_33() { return &___C2Tnb239v3_33; }
	inline void set_C2Tnb239v3_33(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Tnb239v3_33 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb239v3_33), value);
	}

	inline static int32_t get_offset_of_C2Onb239v4_34() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Onb239v4_34)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Onb239v4_34() const { return ___C2Onb239v4_34; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Onb239v4_34() { return &___C2Onb239v4_34; }
	inline void set_C2Onb239v4_34(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Onb239v4_34 = value;
		Il2CppCodeGenWriteBarrier((&___C2Onb239v4_34), value);
	}

	inline static int32_t get_offset_of_C2Onb239v5_35() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Onb239v5_35)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Onb239v5_35() const { return ___C2Onb239v5_35; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Onb239v5_35() { return &___C2Onb239v5_35; }
	inline void set_C2Onb239v5_35(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Onb239v5_35 = value;
		Il2CppCodeGenWriteBarrier((&___C2Onb239v5_35), value);
	}

	inline static int32_t get_offset_of_C2Pnb272w1_36() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Pnb272w1_36)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Pnb272w1_36() const { return ___C2Pnb272w1_36; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Pnb272w1_36() { return &___C2Pnb272w1_36; }
	inline void set_C2Pnb272w1_36(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Pnb272w1_36 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb272w1_36), value);
	}

	inline static int32_t get_offset_of_C2Pnb304w1_37() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Pnb304w1_37)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Pnb304w1_37() const { return ___C2Pnb304w1_37; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Pnb304w1_37() { return &___C2Pnb304w1_37; }
	inline void set_C2Pnb304w1_37(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Pnb304w1_37 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb304w1_37), value);
	}

	inline static int32_t get_offset_of_C2Tnb359v1_38() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Tnb359v1_38)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Tnb359v1_38() const { return ___C2Tnb359v1_38; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Tnb359v1_38() { return &___C2Tnb359v1_38; }
	inline void set_C2Tnb359v1_38(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Tnb359v1_38 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb359v1_38), value);
	}

	inline static int32_t get_offset_of_C2Pnb368w1_39() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Pnb368w1_39)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Pnb368w1_39() const { return ___C2Pnb368w1_39; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Pnb368w1_39() { return &___C2Pnb368w1_39; }
	inline void set_C2Pnb368w1_39(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Pnb368w1_39 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb368w1_39), value);
	}

	inline static int32_t get_offset_of_C2Tnb431r1_40() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___C2Tnb431r1_40)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C2Tnb431r1_40() const { return ___C2Tnb431r1_40; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C2Tnb431r1_40() { return &___C2Tnb431r1_40; }
	inline void set_C2Tnb431r1_40(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C2Tnb431r1_40 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb431r1_40), value);
	}

	inline static int32_t get_offset_of_PrimeCurve_41() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___PrimeCurve_41)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PrimeCurve_41() const { return ___PrimeCurve_41; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PrimeCurve_41() { return &___PrimeCurve_41; }
	inline void set_PrimeCurve_41(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PrimeCurve_41 = value;
		Il2CppCodeGenWriteBarrier((&___PrimeCurve_41), value);
	}

	inline static int32_t get_offset_of_Prime192v1_42() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___Prime192v1_42)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Prime192v1_42() const { return ___Prime192v1_42; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Prime192v1_42() { return &___Prime192v1_42; }
	inline void set_Prime192v1_42(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Prime192v1_42 = value;
		Il2CppCodeGenWriteBarrier((&___Prime192v1_42), value);
	}

	inline static int32_t get_offset_of_Prime192v2_43() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___Prime192v2_43)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Prime192v2_43() const { return ___Prime192v2_43; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Prime192v2_43() { return &___Prime192v2_43; }
	inline void set_Prime192v2_43(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Prime192v2_43 = value;
		Il2CppCodeGenWriteBarrier((&___Prime192v2_43), value);
	}

	inline static int32_t get_offset_of_Prime192v3_44() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___Prime192v3_44)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Prime192v3_44() const { return ___Prime192v3_44; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Prime192v3_44() { return &___Prime192v3_44; }
	inline void set_Prime192v3_44(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Prime192v3_44 = value;
		Il2CppCodeGenWriteBarrier((&___Prime192v3_44), value);
	}

	inline static int32_t get_offset_of_Prime239v1_45() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___Prime239v1_45)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Prime239v1_45() const { return ___Prime239v1_45; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Prime239v1_45() { return &___Prime239v1_45; }
	inline void set_Prime239v1_45(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Prime239v1_45 = value;
		Il2CppCodeGenWriteBarrier((&___Prime239v1_45), value);
	}

	inline static int32_t get_offset_of_Prime239v2_46() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___Prime239v2_46)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Prime239v2_46() const { return ___Prime239v2_46; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Prime239v2_46() { return &___Prime239v2_46; }
	inline void set_Prime239v2_46(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Prime239v2_46 = value;
		Il2CppCodeGenWriteBarrier((&___Prime239v2_46), value);
	}

	inline static int32_t get_offset_of_Prime239v3_47() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___Prime239v3_47)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Prime239v3_47() const { return ___Prime239v3_47; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Prime239v3_47() { return &___Prime239v3_47; }
	inline void set_Prime239v3_47(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Prime239v3_47 = value;
		Il2CppCodeGenWriteBarrier((&___Prime239v3_47), value);
	}

	inline static int32_t get_offset_of_Prime256v1_48() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___Prime256v1_48)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Prime256v1_48() const { return ___Prime256v1_48; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Prime256v1_48() { return &___Prime256v1_48; }
	inline void set_Prime256v1_48(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Prime256v1_48 = value;
		Il2CppCodeGenWriteBarrier((&___Prime256v1_48), value);
	}

	inline static int32_t get_offset_of_IdDsa_49() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___IdDsa_49)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdDsa_49() const { return ___IdDsa_49; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdDsa_49() { return &___IdDsa_49; }
	inline void set_IdDsa_49(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdDsa_49 = value;
		Il2CppCodeGenWriteBarrier((&___IdDsa_49), value);
	}

	inline static int32_t get_offset_of_IdDsaWithSha1_50() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___IdDsaWithSha1_50)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdDsaWithSha1_50() const { return ___IdDsaWithSha1_50; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdDsaWithSha1_50() { return &___IdDsaWithSha1_50; }
	inline void set_IdDsaWithSha1_50(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdDsaWithSha1_50 = value;
		Il2CppCodeGenWriteBarrier((&___IdDsaWithSha1_50), value);
	}

	inline static int32_t get_offset_of_X9x63Scheme_51() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___X9x63Scheme_51)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_X9x63Scheme_51() const { return ___X9x63Scheme_51; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_X9x63Scheme_51() { return &___X9x63Scheme_51; }
	inline void set_X9x63Scheme_51(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___X9x63Scheme_51 = value;
		Il2CppCodeGenWriteBarrier((&___X9x63Scheme_51), value);
	}

	inline static int32_t get_offset_of_DHSinglePassStdDHSha1KdfScheme_52() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___DHSinglePassStdDHSha1KdfScheme_52)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DHSinglePassStdDHSha1KdfScheme_52() const { return ___DHSinglePassStdDHSha1KdfScheme_52; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DHSinglePassStdDHSha1KdfScheme_52() { return &___DHSinglePassStdDHSha1KdfScheme_52; }
	inline void set_DHSinglePassStdDHSha1KdfScheme_52(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DHSinglePassStdDHSha1KdfScheme_52 = value;
		Il2CppCodeGenWriteBarrier((&___DHSinglePassStdDHSha1KdfScheme_52), value);
	}

	inline static int32_t get_offset_of_DHSinglePassCofactorDHSha1KdfScheme_53() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___DHSinglePassCofactorDHSha1KdfScheme_53)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DHSinglePassCofactorDHSha1KdfScheme_53() const { return ___DHSinglePassCofactorDHSha1KdfScheme_53; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DHSinglePassCofactorDHSha1KdfScheme_53() { return &___DHSinglePassCofactorDHSha1KdfScheme_53; }
	inline void set_DHSinglePassCofactorDHSha1KdfScheme_53(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DHSinglePassCofactorDHSha1KdfScheme_53 = value;
		Il2CppCodeGenWriteBarrier((&___DHSinglePassCofactorDHSha1KdfScheme_53), value);
	}

	inline static int32_t get_offset_of_MqvSinglePassSha1KdfScheme_54() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___MqvSinglePassSha1KdfScheme_54)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MqvSinglePassSha1KdfScheme_54() const { return ___MqvSinglePassSha1KdfScheme_54; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MqvSinglePassSha1KdfScheme_54() { return &___MqvSinglePassSha1KdfScheme_54; }
	inline void set_MqvSinglePassSha1KdfScheme_54(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MqvSinglePassSha1KdfScheme_54 = value;
		Il2CppCodeGenWriteBarrier((&___MqvSinglePassSha1KdfScheme_54), value);
	}

	inline static int32_t get_offset_of_ansi_x9_42_55() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___ansi_x9_42_55)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ansi_x9_42_55() const { return ___ansi_x9_42_55; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ansi_x9_42_55() { return &___ansi_x9_42_55; }
	inline void set_ansi_x9_42_55(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ansi_x9_42_55 = value;
		Il2CppCodeGenWriteBarrier((&___ansi_x9_42_55), value);
	}

	inline static int32_t get_offset_of_DHPublicNumber_56() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___DHPublicNumber_56)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DHPublicNumber_56() const { return ___DHPublicNumber_56; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DHPublicNumber_56() { return &___DHPublicNumber_56; }
	inline void set_DHPublicNumber_56(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DHPublicNumber_56 = value;
		Il2CppCodeGenWriteBarrier((&___DHPublicNumber_56), value);
	}

	inline static int32_t get_offset_of_X9x42Schemes_57() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___X9x42Schemes_57)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_X9x42Schemes_57() const { return ___X9x42Schemes_57; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_X9x42Schemes_57() { return &___X9x42Schemes_57; }
	inline void set_X9x42Schemes_57(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___X9x42Schemes_57 = value;
		Il2CppCodeGenWriteBarrier((&___X9x42Schemes_57), value);
	}

	inline static int32_t get_offset_of_DHStatic_58() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___DHStatic_58)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DHStatic_58() const { return ___DHStatic_58; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DHStatic_58() { return &___DHStatic_58; }
	inline void set_DHStatic_58(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DHStatic_58 = value;
		Il2CppCodeGenWriteBarrier((&___DHStatic_58), value);
	}

	inline static int32_t get_offset_of_DHEphem_59() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___DHEphem_59)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DHEphem_59() const { return ___DHEphem_59; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DHEphem_59() { return &___DHEphem_59; }
	inline void set_DHEphem_59(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DHEphem_59 = value;
		Il2CppCodeGenWriteBarrier((&___DHEphem_59), value);
	}

	inline static int32_t get_offset_of_DHOneFlow_60() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___DHOneFlow_60)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DHOneFlow_60() const { return ___DHOneFlow_60; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DHOneFlow_60() { return &___DHOneFlow_60; }
	inline void set_DHOneFlow_60(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DHOneFlow_60 = value;
		Il2CppCodeGenWriteBarrier((&___DHOneFlow_60), value);
	}

	inline static int32_t get_offset_of_DHHybrid1_61() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___DHHybrid1_61)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DHHybrid1_61() const { return ___DHHybrid1_61; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DHHybrid1_61() { return &___DHHybrid1_61; }
	inline void set_DHHybrid1_61(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DHHybrid1_61 = value;
		Il2CppCodeGenWriteBarrier((&___DHHybrid1_61), value);
	}

	inline static int32_t get_offset_of_DHHybrid2_62() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___DHHybrid2_62)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DHHybrid2_62() const { return ___DHHybrid2_62; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DHHybrid2_62() { return &___DHHybrid2_62; }
	inline void set_DHHybrid2_62(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DHHybrid2_62 = value;
		Il2CppCodeGenWriteBarrier((&___DHHybrid2_62), value);
	}

	inline static int32_t get_offset_of_DHHybridOneFlow_63() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___DHHybridOneFlow_63)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DHHybridOneFlow_63() const { return ___DHHybridOneFlow_63; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DHHybridOneFlow_63() { return &___DHHybridOneFlow_63; }
	inline void set_DHHybridOneFlow_63(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DHHybridOneFlow_63 = value;
		Il2CppCodeGenWriteBarrier((&___DHHybridOneFlow_63), value);
	}

	inline static int32_t get_offset_of_Mqv2_64() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___Mqv2_64)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Mqv2_64() const { return ___Mqv2_64; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Mqv2_64() { return &___Mqv2_64; }
	inline void set_Mqv2_64(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Mqv2_64 = value;
		Il2CppCodeGenWriteBarrier((&___Mqv2_64), value);
	}

	inline static int32_t get_offset_of_Mqv1_65() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields, ___Mqv1_65)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Mqv1_65() const { return ___Mqv1_65; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Mqv1_65() { return &___Mqv1_65; }
	inline void set_Mqv1_65(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Mqv1_65 = value;
		Il2CppCodeGenWriteBarrier((&___Mqv1_65), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9OBJECTIDENTIFIERS_TA801960EE331C7E21CEA8A62855FA8520B297EB6_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#define ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct  Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifndef DERGENERATOR_T61583277391BC057473563CDC35A6DE1070E078E_H
#define DERGENERATOR_T61583277391BC057473563CDC35A6DE1070E078E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGenerator
struct  DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E  : public Asn1Generator_t56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGenerator::_tagged
	bool ____tagged_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGenerator::_isExplicit
	bool ____isExplicit_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGenerator::_tagNo
	int32_t ____tagNo_3;

public:
	inline static int32_t get_offset_of__tagged_1() { return static_cast<int32_t>(offsetof(DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E, ____tagged_1)); }
	inline bool get__tagged_1() const { return ____tagged_1; }
	inline bool* get_address_of__tagged_1() { return &____tagged_1; }
	inline void set__tagged_1(bool value)
	{
		____tagged_1 = value;
	}

	inline static int32_t get_offset_of__isExplicit_2() { return static_cast<int32_t>(offsetof(DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E, ____isExplicit_2)); }
	inline bool get__isExplicit_2() const { return ____isExplicit_2; }
	inline bool* get_address_of__isExplicit_2() { return &____isExplicit_2; }
	inline void set__isExplicit_2(bool value)
	{
		____isExplicit_2 = value;
	}

	inline static int32_t get_offset_of__tagNo_3() { return static_cast<int32_t>(offsetof(DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E, ____tagNo_3)); }
	inline int32_t get__tagNo_3() const { return ____tagNo_3; }
	inline int32_t* get_address_of__tagNo_3() { return &____tagNo_3; }
	inline void set__tagNo_3(int32_t value)
	{
		____tagNo_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERGENERATOR_T61583277391BC057473563CDC35A6DE1070E078E_H
#ifndef ACCESSDESCRIPTION_T483969979FA65EBC413ECA1DA206AF9813389538_H
#define ACCESSDESCRIPTION_T483969979FA65EBC413ECA1DA206AF9813389538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AccessDescription
struct  AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AccessDescription::accessMethod
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___accessMethod_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AccessDescription::accessLocation
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___accessLocation_5;

public:
	inline static int32_t get_offset_of_accessMethod_4() { return static_cast<int32_t>(offsetof(AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538, ___accessMethod_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_accessMethod_4() const { return ___accessMethod_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_accessMethod_4() { return &___accessMethod_4; }
	inline void set_accessMethod_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___accessMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&___accessMethod_4), value);
	}

	inline static int32_t get_offset_of_accessLocation_5() { return static_cast<int32_t>(offsetof(AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538, ___accessLocation_5)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_accessLocation_5() const { return ___accessLocation_5; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_accessLocation_5() { return &___accessLocation_5; }
	inline void set_accessLocation_5(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___accessLocation_5 = value;
		Il2CppCodeGenWriteBarrier((&___accessLocation_5), value);
	}
};

struct AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AccessDescription::IdADCAIssuers
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdADCAIssuers_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AccessDescription::IdADOcsp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdADOcsp_3;

public:
	inline static int32_t get_offset_of_IdADCAIssuers_2() { return static_cast<int32_t>(offsetof(AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538_StaticFields, ___IdADCAIssuers_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdADCAIssuers_2() const { return ___IdADCAIssuers_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdADCAIssuers_2() { return &___IdADCAIssuers_2; }
	inline void set_IdADCAIssuers_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdADCAIssuers_2 = value;
		Il2CppCodeGenWriteBarrier((&___IdADCAIssuers_2), value);
	}

	inline static int32_t get_offset_of_IdADOcsp_3() { return static_cast<int32_t>(offsetof(AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538_StaticFields, ___IdADOcsp_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdADOcsp_3() const { return ___IdADOcsp_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdADOcsp_3() { return &___IdADOcsp_3; }
	inline void set_IdADOcsp_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdADOcsp_3 = value;
		Il2CppCodeGenWriteBarrier((&___IdADOcsp_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSDESCRIPTION_T483969979FA65EBC413ECA1DA206AF9813389538_H
#ifndef ALGORITHMIDENTIFIER_TCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3_H
#define ALGORITHMIDENTIFIER_TCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct  AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier::algorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___algorithm_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier::parameters
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___parameters_3;

public:
	inline static int32_t get_offset_of_algorithm_2() { return static_cast<int32_t>(offsetof(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3, ___algorithm_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_algorithm_2() const { return ___algorithm_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_algorithm_2() { return &___algorithm_2; }
	inline void set_algorithm_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___algorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_2), value);
	}

	inline static int32_t get_offset_of_parameters_3() { return static_cast<int32_t>(offsetof(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3, ___parameters_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_parameters_3() const { return ___parameters_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_parameters_3() { return &___parameters_3; }
	inline void set_parameters_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALGORITHMIDENTIFIER_TCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3_H
#ifndef ATTCERTISSUER_T09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF_H
#define ATTCERTISSUER_T09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertIssuer
struct  AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertIssuer::obj
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___obj_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertIssuer::choiceObj
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___choiceObj_3;

public:
	inline static int32_t get_offset_of_obj_2() { return static_cast<int32_t>(offsetof(AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF, ___obj_2)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_obj_2() const { return ___obj_2; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_obj_2() { return &___obj_2; }
	inline void set_obj_2(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___obj_2 = value;
		Il2CppCodeGenWriteBarrier((&___obj_2), value);
	}

	inline static int32_t get_offset_of_choiceObj_3() { return static_cast<int32_t>(offsetof(AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF, ___choiceObj_3)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_choiceObj_3() const { return ___choiceObj_3; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_choiceObj_3() { return &___choiceObj_3; }
	inline void set_choiceObj_3(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___choiceObj_3 = value;
		Il2CppCodeGenWriteBarrier((&___choiceObj_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTCERTISSUER_T09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF_H
#ifndef ATTCERTVALIDITYPERIOD_TE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D_H
#define ATTCERTVALIDITYPERIOD_TE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertValidityPeriod
struct  AttCertValidityPeriod_tE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertValidityPeriod::notBeforeTime
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___notBeforeTime_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertValidityPeriod::notAfterTime
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___notAfterTime_3;

public:
	inline static int32_t get_offset_of_notBeforeTime_2() { return static_cast<int32_t>(offsetof(AttCertValidityPeriod_tE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D, ___notBeforeTime_2)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_notBeforeTime_2() const { return ___notBeforeTime_2; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_notBeforeTime_2() { return &___notBeforeTime_2; }
	inline void set_notBeforeTime_2(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___notBeforeTime_2 = value;
		Il2CppCodeGenWriteBarrier((&___notBeforeTime_2), value);
	}

	inline static int32_t get_offset_of_notAfterTime_3() { return static_cast<int32_t>(offsetof(AttCertValidityPeriod_tE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D, ___notAfterTime_3)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_notAfterTime_3() const { return ___notAfterTime_3; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_notAfterTime_3() { return &___notAfterTime_3; }
	inline void set_notAfterTime_3(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___notAfterTime_3 = value;
		Il2CppCodeGenWriteBarrier((&___notAfterTime_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTCERTVALIDITYPERIOD_TE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D_H
#ifndef ATTRIBUTECERTIFICATE_T0DD6CD134E59614B589B667A1033ABCE2F43AD7B_H
#define ATTRIBUTECERTIFICATE_T0DD6CD134E59614B589B667A1033ABCE2F43AD7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificate
struct  AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificate::acinfo
	AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA * ___acinfo_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificate::signatureAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signatureAlgorithm_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificate::signatureValue
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___signatureValue_4;

public:
	inline static int32_t get_offset_of_acinfo_2() { return static_cast<int32_t>(offsetof(AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B, ___acinfo_2)); }
	inline AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA * get_acinfo_2() const { return ___acinfo_2; }
	inline AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA ** get_address_of_acinfo_2() { return &___acinfo_2; }
	inline void set_acinfo_2(AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA * value)
	{
		___acinfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___acinfo_2), value);
	}

	inline static int32_t get_offset_of_signatureAlgorithm_3() { return static_cast<int32_t>(offsetof(AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B, ___signatureAlgorithm_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signatureAlgorithm_3() const { return ___signatureAlgorithm_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signatureAlgorithm_3() { return &___signatureAlgorithm_3; }
	inline void set_signatureAlgorithm_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signatureAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___signatureAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_signatureValue_4() { return static_cast<int32_t>(offsetof(AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B, ___signatureValue_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_signatureValue_4() const { return ___signatureValue_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_signatureValue_4() { return &___signatureValue_4; }
	inline void set_signatureValue_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___signatureValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___signatureValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTECERTIFICATE_T0DD6CD134E59614B589B667A1033ABCE2F43AD7B_H
#ifndef ATTRIBUTECERTIFICATEINFO_T85F5B73C43432A8DEE6B1D0317899A862E98C8CA_H
#define ATTRIBUTECERTIFICATEINFO_T85F5B73C43432A8DEE6B1D0317899A862E98C8CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo
struct  AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Holder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo::holder
	Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D * ___holder_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertIssuer BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo::issuer
	AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF * ___issuer_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo::signature
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signature_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo::serialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serialNumber_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertValidityPeriod BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo::attrCertValidityPeriod
	AttCertValidityPeriod_tE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D * ___attrCertValidityPeriod_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo::attributes
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___attributes_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo::issuerUniqueID
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___issuerUniqueID_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificateInfo::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_10;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_holder_3() { return static_cast<int32_t>(offsetof(AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA, ___holder_3)); }
	inline Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D * get_holder_3() const { return ___holder_3; }
	inline Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D ** get_address_of_holder_3() { return &___holder_3; }
	inline void set_holder_3(Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D * value)
	{
		___holder_3 = value;
		Il2CppCodeGenWriteBarrier((&___holder_3), value);
	}

	inline static int32_t get_offset_of_issuer_4() { return static_cast<int32_t>(offsetof(AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA, ___issuer_4)); }
	inline AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF * get_issuer_4() const { return ___issuer_4; }
	inline AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF ** get_address_of_issuer_4() { return &___issuer_4; }
	inline void set_issuer_4(AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF * value)
	{
		___issuer_4 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_4), value);
	}

	inline static int32_t get_offset_of_signature_5() { return static_cast<int32_t>(offsetof(AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA, ___signature_5)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signature_5() const { return ___signature_5; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signature_5() { return &___signature_5; }
	inline void set_signature_5(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signature_5 = value;
		Il2CppCodeGenWriteBarrier((&___signature_5), value);
	}

	inline static int32_t get_offset_of_serialNumber_6() { return static_cast<int32_t>(offsetof(AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA, ___serialNumber_6)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serialNumber_6() const { return ___serialNumber_6; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serialNumber_6() { return &___serialNumber_6; }
	inline void set_serialNumber_6(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serialNumber_6 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_6), value);
	}

	inline static int32_t get_offset_of_attrCertValidityPeriod_7() { return static_cast<int32_t>(offsetof(AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA, ___attrCertValidityPeriod_7)); }
	inline AttCertValidityPeriod_tE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D * get_attrCertValidityPeriod_7() const { return ___attrCertValidityPeriod_7; }
	inline AttCertValidityPeriod_tE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D ** get_address_of_attrCertValidityPeriod_7() { return &___attrCertValidityPeriod_7; }
	inline void set_attrCertValidityPeriod_7(AttCertValidityPeriod_tE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D * value)
	{
		___attrCertValidityPeriod_7 = value;
		Il2CppCodeGenWriteBarrier((&___attrCertValidityPeriod_7), value);
	}

	inline static int32_t get_offset_of_attributes_8() { return static_cast<int32_t>(offsetof(AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA, ___attributes_8)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_attributes_8() const { return ___attributes_8; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_attributes_8() { return &___attributes_8; }
	inline void set_attributes_8(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___attributes_8 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_8), value);
	}

	inline static int32_t get_offset_of_issuerUniqueID_9() { return static_cast<int32_t>(offsetof(AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA, ___issuerUniqueID_9)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_issuerUniqueID_9() const { return ___issuerUniqueID_9; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_issuerUniqueID_9() { return &___issuerUniqueID_9; }
	inline void set_issuerUniqueID_9(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___issuerUniqueID_9 = value;
		Il2CppCodeGenWriteBarrier((&___issuerUniqueID_9), value);
	}

	inline static int32_t get_offset_of_extensions_10() { return static_cast<int32_t>(offsetof(AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA, ___extensions_10)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_10() const { return ___extensions_10; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_10() { return &___extensions_10; }
	inline void set_extensions_10(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_10 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTECERTIFICATEINFO_T85F5B73C43432A8DEE6B1D0317899A862E98C8CA_H
#ifndef ATTRIBUTEX509_T27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76_H
#define ATTRIBUTEX509_T27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeX509
struct  AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeX509::attrType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___attrType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeX509::attrValues
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___attrValues_3;

public:
	inline static int32_t get_offset_of_attrType_2() { return static_cast<int32_t>(offsetof(AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76, ___attrType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_attrType_2() const { return ___attrType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_attrType_2() { return &___attrType_2; }
	inline void set_attrType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___attrType_2 = value;
		Il2CppCodeGenWriteBarrier((&___attrType_2), value);
	}

	inline static int32_t get_offset_of_attrValues_3() { return static_cast<int32_t>(offsetof(AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76, ___attrValues_3)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_attrValues_3() const { return ___attrValues_3; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_attrValues_3() { return &___attrValues_3; }
	inline void set_attrValues_3(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___attrValues_3 = value;
		Il2CppCodeGenWriteBarrier((&___attrValues_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEX509_T27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76_H
#ifndef AUTHORITYINFORMATIONACCESS_T980F87C7F67F387659ED9FB5A588FD75B04D6656_H
#define AUTHORITYINFORMATIONACCESS_T980F87C7F67F387659ED9FB5A588FD75B04D6656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AuthorityInformationAccess
struct  AuthorityInformationAccess_t980F87C7F67F387659ED9FB5A588FD75B04D6656  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AccessDescription[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AuthorityInformationAccess::descriptions
	AccessDescriptionU5BU5D_t684A1B002EAB096C84A881888BDDAE8536F2050B* ___descriptions_2;

public:
	inline static int32_t get_offset_of_descriptions_2() { return static_cast<int32_t>(offsetof(AuthorityInformationAccess_t980F87C7F67F387659ED9FB5A588FD75B04D6656, ___descriptions_2)); }
	inline AccessDescriptionU5BU5D_t684A1B002EAB096C84A881888BDDAE8536F2050B* get_descriptions_2() const { return ___descriptions_2; }
	inline AccessDescriptionU5BU5D_t684A1B002EAB096C84A881888BDDAE8536F2050B** get_address_of_descriptions_2() { return &___descriptions_2; }
	inline void set_descriptions_2(AccessDescriptionU5BU5D_t684A1B002EAB096C84A881888BDDAE8536F2050B* value)
	{
		___descriptions_2 = value;
		Il2CppCodeGenWriteBarrier((&___descriptions_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORITYINFORMATIONACCESS_T980F87C7F67F387659ED9FB5A588FD75B04D6656_H
#ifndef AUTHORITYKEYIDENTIFIER_T720D49360890539809676FFD0BE6209BA9BC93EA_H
#define AUTHORITYKEYIDENTIFIER_T720D49360890539809676FFD0BE6209BA9BC93EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AuthorityKeyIdentifier
struct  AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AuthorityKeyIdentifier::keyidentifier
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___keyidentifier_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AuthorityKeyIdentifier::certissuer
	GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * ___certissuer_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AuthorityKeyIdentifier::certserno
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___certserno_4;

public:
	inline static int32_t get_offset_of_keyidentifier_2() { return static_cast<int32_t>(offsetof(AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA, ___keyidentifier_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_keyidentifier_2() const { return ___keyidentifier_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_keyidentifier_2() { return &___keyidentifier_2; }
	inline void set_keyidentifier_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___keyidentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyidentifier_2), value);
	}

	inline static int32_t get_offset_of_certissuer_3() { return static_cast<int32_t>(offsetof(AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA, ___certissuer_3)); }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * get_certissuer_3() const { return ___certissuer_3; }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD ** get_address_of_certissuer_3() { return &___certissuer_3; }
	inline void set_certissuer_3(GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * value)
	{
		___certissuer_3 = value;
		Il2CppCodeGenWriteBarrier((&___certissuer_3), value);
	}

	inline static int32_t get_offset_of_certserno_4() { return static_cast<int32_t>(offsetof(AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA, ___certserno_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_certserno_4() const { return ___certserno_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_certserno_4() { return &___certserno_4; }
	inline void set_certserno_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___certserno_4 = value;
		Il2CppCodeGenWriteBarrier((&___certserno_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORITYKEYIDENTIFIER_T720D49360890539809676FFD0BE6209BA9BC93EA_H
#ifndef BASICCONSTRAINTS_T1F4EBA54689CD8E1522DAC83DC632226F189D217_H
#define BASICCONSTRAINTS_T1F4EBA54689CD8E1522DAC83DC632226F189D217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.BasicConstraints
struct  BasicConstraints_t1F4EBA54689CD8E1522DAC83DC632226F189D217  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.BasicConstraints::cA
	DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * ___cA_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.BasicConstraints::pathLenConstraint
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___pathLenConstraint_3;

public:
	inline static int32_t get_offset_of_cA_2() { return static_cast<int32_t>(offsetof(BasicConstraints_t1F4EBA54689CD8E1522DAC83DC632226F189D217, ___cA_2)); }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * get_cA_2() const { return ___cA_2; }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C ** get_address_of_cA_2() { return &___cA_2; }
	inline void set_cA_2(DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * value)
	{
		___cA_2 = value;
		Il2CppCodeGenWriteBarrier((&___cA_2), value);
	}

	inline static int32_t get_offset_of_pathLenConstraint_3() { return static_cast<int32_t>(offsetof(BasicConstraints_t1F4EBA54689CD8E1522DAC83DC632226F189D217, ___pathLenConstraint_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_pathLenConstraint_3() const { return ___pathLenConstraint_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_pathLenConstraint_3() { return &___pathLenConstraint_3; }
	inline void set_pathLenConstraint_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___pathLenConstraint_3 = value;
		Il2CppCodeGenWriteBarrier((&___pathLenConstraint_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCONSTRAINTS_T1F4EBA54689CD8E1522DAC83DC632226F189D217_H
#ifndef CERTIFICATELIST_T6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8_H
#define CERTIFICATELIST_T6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificateList
struct  CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificateList::tbsCertList
	TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC * ___tbsCertList_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificateList::sigAlgID
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___sigAlgID_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificateList::sig
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___sig_4;

public:
	inline static int32_t get_offset_of_tbsCertList_2() { return static_cast<int32_t>(offsetof(CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8, ___tbsCertList_2)); }
	inline TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC * get_tbsCertList_2() const { return ___tbsCertList_2; }
	inline TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC ** get_address_of_tbsCertList_2() { return &___tbsCertList_2; }
	inline void set_tbsCertList_2(TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC * value)
	{
		___tbsCertList_2 = value;
		Il2CppCodeGenWriteBarrier((&___tbsCertList_2), value);
	}

	inline static int32_t get_offset_of_sigAlgID_3() { return static_cast<int32_t>(offsetof(CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8, ___sigAlgID_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_sigAlgID_3() const { return ___sigAlgID_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_sigAlgID_3() { return &___sigAlgID_3; }
	inline void set_sigAlgID_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___sigAlgID_3 = value;
		Il2CppCodeGenWriteBarrier((&___sigAlgID_3), value);
	}

	inline static int32_t get_offset_of_sig_4() { return static_cast<int32_t>(offsetof(CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8, ___sig_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_sig_4() const { return ___sig_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_sig_4() { return &___sig_4; }
	inline void set_sig_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___sig_4 = value;
		Il2CppCodeGenWriteBarrier((&___sig_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATELIST_T6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8_H
#ifndef CERTIFICATEPAIR_TE8DB62307CA376BABBB15994D484DC6E4914F933_H
#define CERTIFICATEPAIR_TE8DB62307CA376BABBB15994D484DC6E4914F933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificatePair
struct  CertificatePair_tE8DB62307CA376BABBB15994D484DC6E4914F933  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificatePair::forward
	X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * ___forward_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificatePair::reverse
	X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * ___reverse_3;

public:
	inline static int32_t get_offset_of_forward_2() { return static_cast<int32_t>(offsetof(CertificatePair_tE8DB62307CA376BABBB15994D484DC6E4914F933, ___forward_2)); }
	inline X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * get_forward_2() const { return ___forward_2; }
	inline X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 ** get_address_of_forward_2() { return &___forward_2; }
	inline void set_forward_2(X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * value)
	{
		___forward_2 = value;
		Il2CppCodeGenWriteBarrier((&___forward_2), value);
	}

	inline static int32_t get_offset_of_reverse_3() { return static_cast<int32_t>(offsetof(CertificatePair_tE8DB62307CA376BABBB15994D484DC6E4914F933, ___reverse_3)); }
	inline X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * get_reverse_3() const { return ___reverse_3; }
	inline X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 ** get_address_of_reverse_3() { return &___reverse_3; }
	inline void set_reverse_3(X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * value)
	{
		___reverse_3 = value;
		Il2CppCodeGenWriteBarrier((&___reverse_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEPAIR_TE8DB62307CA376BABBB15994D484DC6E4914F933_H
#ifndef CERTIFICATEPOLICIES_T6065C56C8EC321C5D8E4ADF0552C36A62266491B_H
#define CERTIFICATEPOLICIES_T6065C56C8EC321C5D8E4ADF0552C36A62266491B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificatePolicies
struct  CertificatePolicies_t6065C56C8EC321C5D8E4ADF0552C36A62266491B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyInformation[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificatePolicies::policyInformation
	PolicyInformationU5BU5D_t6B99B0474D52C0BDF1D00BAEB3C65DD792091D5D* ___policyInformation_2;

public:
	inline static int32_t get_offset_of_policyInformation_2() { return static_cast<int32_t>(offsetof(CertificatePolicies_t6065C56C8EC321C5D8E4ADF0552C36A62266491B, ___policyInformation_2)); }
	inline PolicyInformationU5BU5D_t6B99B0474D52C0BDF1D00BAEB3C65DD792091D5D* get_policyInformation_2() const { return ___policyInformation_2; }
	inline PolicyInformationU5BU5D_t6B99B0474D52C0BDF1D00BAEB3C65DD792091D5D** get_address_of_policyInformation_2() { return &___policyInformation_2; }
	inline void set_policyInformation_2(PolicyInformationU5BU5D_t6B99B0474D52C0BDF1D00BAEB3C65DD792091D5D* value)
	{
		___policyInformation_2 = value;
		Il2CppCodeGenWriteBarrier((&___policyInformation_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEPOLICIES_T6065C56C8EC321C5D8E4ADF0552C36A62266491B_H
#ifndef CRLDISTPOINT_T91207A59A47177800952A0A16145D883799C1271_H
#define CRLDISTPOINT_T91207A59A47177800952A0A16145D883799C1271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlDistPoint
struct  CrlDistPoint_t91207A59A47177800952A0A16145D883799C1271  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlDistPoint::seq
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___seq_2;

public:
	inline static int32_t get_offset_of_seq_2() { return static_cast<int32_t>(offsetof(CrlDistPoint_t91207A59A47177800952A0A16145D883799C1271, ___seq_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_seq_2() const { return ___seq_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_seq_2() { return &___seq_2; }
	inline void set_seq_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___seq_2 = value;
		Il2CppCodeGenWriteBarrier((&___seq_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRLDISTPOINT_T91207A59A47177800952A0A16145D883799C1271_H
#ifndef DIGESTINFO_TB45D2AE982ABCE52832F57B2D89F996C42354D70_H
#define DIGESTINFO_TB45D2AE982ABCE52832F57B2D89F996C42354D70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DigestInfo
struct  DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DigestInfo::digest
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___digest_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DigestInfo::algID
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algID_3;

public:
	inline static int32_t get_offset_of_digest_2() { return static_cast<int32_t>(offsetof(DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70, ___digest_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_digest_2() const { return ___digest_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_digest_2() { return &___digest_2; }
	inline void set_digest_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___digest_2 = value;
		Il2CppCodeGenWriteBarrier((&___digest_2), value);
	}

	inline static int32_t get_offset_of_algID_3() { return static_cast<int32_t>(offsetof(DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70, ___algID_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algID_3() const { return ___algID_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algID_3() { return &___algID_3; }
	inline void set_algID_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algID_3 = value;
		Il2CppCodeGenWriteBarrier((&___algID_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTINFO_TB45D2AE982ABCE52832F57B2D89F996C42354D70_H
#ifndef DISPLAYTEXT_T35D77F340949841C4236212DDA1957F0BC4BD4CB_H
#define DISPLAYTEXT_T35D77F340949841C4236212DDA1957F0BC4BD4CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DisplayText
struct  DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DisplayText::contentType
	int32_t ___contentType_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IAsn1String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DisplayText::contents
	RuntimeObject* ___contents_8;

public:
	inline static int32_t get_offset_of_contentType_7() { return static_cast<int32_t>(offsetof(DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB, ___contentType_7)); }
	inline int32_t get_contentType_7() const { return ___contentType_7; }
	inline int32_t* get_address_of_contentType_7() { return &___contentType_7; }
	inline void set_contentType_7(int32_t value)
	{
		___contentType_7 = value;
	}

	inline static int32_t get_offset_of_contents_8() { return static_cast<int32_t>(offsetof(DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB, ___contents_8)); }
	inline RuntimeObject* get_contents_8() const { return ___contents_8; }
	inline RuntimeObject** get_address_of_contents_8() { return &___contents_8; }
	inline void set_contents_8(RuntimeObject* value)
	{
		___contents_8 = value;
		Il2CppCodeGenWriteBarrier((&___contents_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYTEXT_T35D77F340949841C4236212DDA1957F0BC4BD4CB_H
#ifndef DISTRIBUTIONPOINT_TD337742D8A4356AF7EE04AA132632B3DC4FDD950_H
#define DISTRIBUTIONPOINT_TD337742D8A4356AF7EE04AA132632B3DC4FDD950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DistributionPoint
struct  DistributionPoint_tD337742D8A4356AF7EE04AA132632B3DC4FDD950  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DistributionPointName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DistributionPoint::distributionPoint
	DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5 * ___distributionPoint_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ReasonFlags BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DistributionPoint::reasons
	ReasonFlags_tBB7FD42D692CC7182B9F721F5558208A7EBF1C3A * ___reasons_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DistributionPoint::cRLIssuer
	GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * ___cRLIssuer_4;

public:
	inline static int32_t get_offset_of_distributionPoint_2() { return static_cast<int32_t>(offsetof(DistributionPoint_tD337742D8A4356AF7EE04AA132632B3DC4FDD950, ___distributionPoint_2)); }
	inline DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5 * get_distributionPoint_2() const { return ___distributionPoint_2; }
	inline DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5 ** get_address_of_distributionPoint_2() { return &___distributionPoint_2; }
	inline void set_distributionPoint_2(DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5 * value)
	{
		___distributionPoint_2 = value;
		Il2CppCodeGenWriteBarrier((&___distributionPoint_2), value);
	}

	inline static int32_t get_offset_of_reasons_3() { return static_cast<int32_t>(offsetof(DistributionPoint_tD337742D8A4356AF7EE04AA132632B3DC4FDD950, ___reasons_3)); }
	inline ReasonFlags_tBB7FD42D692CC7182B9F721F5558208A7EBF1C3A * get_reasons_3() const { return ___reasons_3; }
	inline ReasonFlags_tBB7FD42D692CC7182B9F721F5558208A7EBF1C3A ** get_address_of_reasons_3() { return &___reasons_3; }
	inline void set_reasons_3(ReasonFlags_tBB7FD42D692CC7182B9F721F5558208A7EBF1C3A * value)
	{
		___reasons_3 = value;
		Il2CppCodeGenWriteBarrier((&___reasons_3), value);
	}

	inline static int32_t get_offset_of_cRLIssuer_4() { return static_cast<int32_t>(offsetof(DistributionPoint_tD337742D8A4356AF7EE04AA132632B3DC4FDD950, ___cRLIssuer_4)); }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * get_cRLIssuer_4() const { return ___cRLIssuer_4; }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD ** get_address_of_cRLIssuer_4() { return &___cRLIssuer_4; }
	inline void set_cRLIssuer_4(GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * value)
	{
		___cRLIssuer_4 = value;
		Il2CppCodeGenWriteBarrier((&___cRLIssuer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTRIBUTIONPOINT_TD337742D8A4356AF7EE04AA132632B3DC4FDD950_H
#ifndef DISTRIBUTIONPOINTNAME_T856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5_H
#define DISTRIBUTIONPOINTNAME_T856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DistributionPointName
struct  DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DistributionPointName::name
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___name_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DistributionPointName::type
	int32_t ___type_3;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5, ___name_2)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_name_2() const { return ___name_2; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTRIBUTIONPOINTNAME_T856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5_H
#ifndef DSAPARAMETER_TF0E4517BEB4D03D5160584964A7D745F61D741F9_H
#define DSAPARAMETER_TF0E4517BEB4D03D5160584964A7D745F61D741F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DsaParameter
struct  DsaParameter_tF0E4517BEB4D03D5160584964A7D745F61D741F9  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DsaParameter::p
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___p_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DsaParameter::q
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___q_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DsaParameter::g
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___g_4;

public:
	inline static int32_t get_offset_of_p_2() { return static_cast<int32_t>(offsetof(DsaParameter_tF0E4517BEB4D03D5160584964A7D745F61D741F9, ___p_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_p_2() const { return ___p_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_p_2() { return &___p_2; }
	inline void set_p_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___p_2 = value;
		Il2CppCodeGenWriteBarrier((&___p_2), value);
	}

	inline static int32_t get_offset_of_q_3() { return static_cast<int32_t>(offsetof(DsaParameter_tF0E4517BEB4D03D5160584964A7D745F61D741F9, ___q_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_q_3() const { return ___q_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_q_3() { return &___q_3; }
	inline void set_q_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___q_3 = value;
		Il2CppCodeGenWriteBarrier((&___q_3), value);
	}

	inline static int32_t get_offset_of_g_4() { return static_cast<int32_t>(offsetof(DsaParameter_tF0E4517BEB4D03D5160584964A7D745F61D741F9, ___g_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_g_4() const { return ___g_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_g_4() { return &___g_4; }
	inline void set_g_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___g_4 = value;
		Il2CppCodeGenWriteBarrier((&___g_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAPARAMETER_TF0E4517BEB4D03D5160584964A7D745F61D741F9_H
#ifndef EXTENDEDKEYUSAGE_T7A5FAFC0A1BD83FB701067A50EC6DCDA7C6B08D7_H
#define EXTENDEDKEYUSAGE_T7A5FAFC0A1BD83FB701067A50EC6DCDA7C6B08D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ExtendedKeyUsage
struct  ExtendedKeyUsage_t7A5FAFC0A1BD83FB701067A50EC6DCDA7C6B08D7  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ExtendedKeyUsage::usageTable
	RuntimeObject* ___usageTable_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ExtendedKeyUsage::seq
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___seq_3;

public:
	inline static int32_t get_offset_of_usageTable_2() { return static_cast<int32_t>(offsetof(ExtendedKeyUsage_t7A5FAFC0A1BD83FB701067A50EC6DCDA7C6B08D7, ___usageTable_2)); }
	inline RuntimeObject* get_usageTable_2() const { return ___usageTable_2; }
	inline RuntimeObject** get_address_of_usageTable_2() { return &___usageTable_2; }
	inline void set_usageTable_2(RuntimeObject* value)
	{
		___usageTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___usageTable_2), value);
	}

	inline static int32_t get_offset_of_seq_3() { return static_cast<int32_t>(offsetof(ExtendedKeyUsage_t7A5FAFC0A1BD83FB701067A50EC6DCDA7C6B08D7, ___seq_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_seq_3() const { return ___seq_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_seq_3() { return &___seq_3; }
	inline void set_seq_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___seq_3 = value;
		Il2CppCodeGenWriteBarrier((&___seq_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDEDKEYUSAGE_T7A5FAFC0A1BD83FB701067A50EC6DCDA7C6B08D7_H
#ifndef GENERALNAME_T613B894A93E71463E938C46C4F1A2EDDA72BAF7B_H
#define GENERALNAME_T613B894A93E71463E938C46C4F1A2EDDA72BAF7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName
struct  GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName::obj
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___obj_11;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName::tag
	int32_t ___tag_12;

public:
	inline static int32_t get_offset_of_obj_11() { return static_cast<int32_t>(offsetof(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B, ___obj_11)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_obj_11() const { return ___obj_11; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_obj_11() { return &___obj_11; }
	inline void set_obj_11(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___obj_11 = value;
		Il2CppCodeGenWriteBarrier((&___obj_11), value);
	}

	inline static int32_t get_offset_of_tag_12() { return static_cast<int32_t>(offsetof(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B, ___tag_12)); }
	inline int32_t get_tag_12() const { return ___tag_12; }
	inline int32_t* get_address_of_tag_12() { return &___tag_12; }
	inline void set_tag_12(int32_t value)
	{
		___tag_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALNAME_T613B894A93E71463E938C46C4F1A2EDDA72BAF7B_H
#ifndef GENERALNAMES_T8FC331E0DBB7C3881577692B6083FA269A8BE8FD_H
#define GENERALNAMES_T8FC331E0DBB7C3881577692B6083FA269A8BE8FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames
struct  GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames::names
	GeneralNameU5BU5D_t8C8E87C7B1F2854AD266B704A0BA432004D2DC73* ___names_2;

public:
	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD, ___names_2)); }
	inline GeneralNameU5BU5D_t8C8E87C7B1F2854AD266B704A0BA432004D2DC73* get_names_2() const { return ___names_2; }
	inline GeneralNameU5BU5D_t8C8E87C7B1F2854AD266B704A0BA432004D2DC73** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(GeneralNameU5BU5D_t8C8E87C7B1F2854AD266B704A0BA432004D2DC73* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALNAMES_T8FC331E0DBB7C3881577692B6083FA269A8BE8FD_H
#ifndef GENERALSUBTREE_T677DE5FC24696439438A34C0D61D26E907D13A13_H
#define GENERALSUBTREE_T677DE5FC24696439438A34C0D61D26E907D13A13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralSubtree
struct  GeneralSubtree_t677DE5FC24696439438A34C0D61D26E907D13A13  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralSubtree::baseName
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___baseName_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralSubtree::minimum
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___minimum_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralSubtree::maximum
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___maximum_4;

public:
	inline static int32_t get_offset_of_baseName_2() { return static_cast<int32_t>(offsetof(GeneralSubtree_t677DE5FC24696439438A34C0D61D26E907D13A13, ___baseName_2)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_baseName_2() const { return ___baseName_2; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_baseName_2() { return &___baseName_2; }
	inline void set_baseName_2(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___baseName_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseName_2), value);
	}

	inline static int32_t get_offset_of_minimum_3() { return static_cast<int32_t>(offsetof(GeneralSubtree_t677DE5FC24696439438A34C0D61D26E907D13A13, ___minimum_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_minimum_3() const { return ___minimum_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_minimum_3() { return &___minimum_3; }
	inline void set_minimum_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___minimum_3 = value;
		Il2CppCodeGenWriteBarrier((&___minimum_3), value);
	}

	inline static int32_t get_offset_of_maximum_4() { return static_cast<int32_t>(offsetof(GeneralSubtree_t677DE5FC24696439438A34C0D61D26E907D13A13, ___maximum_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_maximum_4() const { return ___maximum_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_maximum_4() { return &___maximum_4; }
	inline void set_maximum_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___maximum_4 = value;
		Il2CppCodeGenWriteBarrier((&___maximum_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALSUBTREE_T677DE5FC24696439438A34C0D61D26E907D13A13_H
#ifndef HOLDER_T79EC89066F0307269E8B7D8EB8504506DF03F13D_H
#define HOLDER_T79EC89066F0307269E8B7D8EB8504506DF03F13D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Holder
struct  Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Holder::baseCertificateID
	IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * ___baseCertificateID_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Holder::entityName
	GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * ___entityName_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ObjectDigestInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Holder::objectDigestInfo
	ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F * ___objectDigestInfo_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Holder::version
	int32_t ___version_5;

public:
	inline static int32_t get_offset_of_baseCertificateID_2() { return static_cast<int32_t>(offsetof(Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D, ___baseCertificateID_2)); }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * get_baseCertificateID_2() const { return ___baseCertificateID_2; }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C ** get_address_of_baseCertificateID_2() { return &___baseCertificateID_2; }
	inline void set_baseCertificateID_2(IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * value)
	{
		___baseCertificateID_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseCertificateID_2), value);
	}

	inline static int32_t get_offset_of_entityName_3() { return static_cast<int32_t>(offsetof(Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D, ___entityName_3)); }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * get_entityName_3() const { return ___entityName_3; }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD ** get_address_of_entityName_3() { return &___entityName_3; }
	inline void set_entityName_3(GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * value)
	{
		___entityName_3 = value;
		Il2CppCodeGenWriteBarrier((&___entityName_3), value);
	}

	inline static int32_t get_offset_of_objectDigestInfo_4() { return static_cast<int32_t>(offsetof(Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D, ___objectDigestInfo_4)); }
	inline ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F * get_objectDigestInfo_4() const { return ___objectDigestInfo_4; }
	inline ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F ** get_address_of_objectDigestInfo_4() { return &___objectDigestInfo_4; }
	inline void set_objectDigestInfo_4(ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F * value)
	{
		___objectDigestInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___objectDigestInfo_4), value);
	}

	inline static int32_t get_offset_of_version_5() { return static_cast<int32_t>(offsetof(Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D, ___version_5)); }
	inline int32_t get_version_5() const { return ___version_5; }
	inline int32_t* get_address_of_version_5() { return &___version_5; }
	inline void set_version_5(int32_t value)
	{
		___version_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLDER_T79EC89066F0307269E8B7D8EB8504506DF03F13D_H
#ifndef IETFATTRSYNTAX_TDE34B40913FF0FC4BAE966515B60D2F3C7DB588B_H
#define IETFATTRSYNTAX_TDE34B40913FF0FC4BAE966515B60D2F3C7DB588B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IetfAttrSyntax
struct  IetfAttrSyntax_tDE34B40913FF0FC4BAE966515B60D2F3C7DB588B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IetfAttrSyntax::policyAuthority
	GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * ___policyAuthority_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IetfAttrSyntax::values
	Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * ___values_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IetfAttrSyntax::valueChoice
	int32_t ___valueChoice_7;

public:
	inline static int32_t get_offset_of_policyAuthority_5() { return static_cast<int32_t>(offsetof(IetfAttrSyntax_tDE34B40913FF0FC4BAE966515B60D2F3C7DB588B, ___policyAuthority_5)); }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * get_policyAuthority_5() const { return ___policyAuthority_5; }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD ** get_address_of_policyAuthority_5() { return &___policyAuthority_5; }
	inline void set_policyAuthority_5(GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * value)
	{
		___policyAuthority_5 = value;
		Il2CppCodeGenWriteBarrier((&___policyAuthority_5), value);
	}

	inline static int32_t get_offset_of_values_6() { return static_cast<int32_t>(offsetof(IetfAttrSyntax_tDE34B40913FF0FC4BAE966515B60D2F3C7DB588B, ___values_6)); }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * get_values_6() const { return ___values_6; }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 ** get_address_of_values_6() { return &___values_6; }
	inline void set_values_6(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * value)
	{
		___values_6 = value;
		Il2CppCodeGenWriteBarrier((&___values_6), value);
	}

	inline static int32_t get_offset_of_valueChoice_7() { return static_cast<int32_t>(offsetof(IetfAttrSyntax_tDE34B40913FF0FC4BAE966515B60D2F3C7DB588B, ___valueChoice_7)); }
	inline int32_t get_valueChoice_7() const { return ___valueChoice_7; }
	inline int32_t* get_address_of_valueChoice_7() { return &___valueChoice_7; }
	inline void set_valueChoice_7(int32_t value)
	{
		___valueChoice_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IETFATTRSYNTAX_TDE34B40913FF0FC4BAE966515B60D2F3C7DB588B_H
#ifndef ISSUERSERIAL_T317A1FBECBF989427611290BC189C88AD6BEDA5C_H
#define ISSUERSERIAL_T317A1FBECBF989427611290BC189C88AD6BEDA5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial
struct  IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial::issuer
	GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * ___issuer_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial::serial
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serial_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial::issuerUid
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___issuerUid_4;

public:
	inline static int32_t get_offset_of_issuer_2() { return static_cast<int32_t>(offsetof(IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C, ___issuer_2)); }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * get_issuer_2() const { return ___issuer_2; }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD ** get_address_of_issuer_2() { return &___issuer_2; }
	inline void set_issuer_2(GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * value)
	{
		___issuer_2 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_2), value);
	}

	inline static int32_t get_offset_of_serial_3() { return static_cast<int32_t>(offsetof(IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C, ___serial_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serial_3() const { return ___serial_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serial_3() { return &___serial_3; }
	inline void set_serial_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serial_3 = value;
		Il2CppCodeGenWriteBarrier((&___serial_3), value);
	}

	inline static int32_t get_offset_of_issuerUid_4() { return static_cast<int32_t>(offsetof(IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C, ___issuerUid_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_issuerUid_4() const { return ___issuerUid_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_issuerUid_4() { return &___issuerUid_4; }
	inline void set_issuerUid_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___issuerUid_4 = value;
		Il2CppCodeGenWriteBarrier((&___issuerUid_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISSUERSERIAL_T317A1FBECBF989427611290BC189C88AD6BEDA5C_H
#ifndef ISSUINGDISTRIBUTIONPOINT_T12AB715F1439394A85CC76FC3578A6E76A4BE3E3_H
#define ISSUINGDISTRIBUTIONPOINT_T12AB715F1439394A85CC76FC3578A6E76A4BE3E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuingDistributionPoint
struct  IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DistributionPointName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuingDistributionPoint::_distributionPoint
	DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5 * ____distributionPoint_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuingDistributionPoint::_onlyContainsUserCerts
	bool ____onlyContainsUserCerts_3;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuingDistributionPoint::_onlyContainsCACerts
	bool ____onlyContainsCACerts_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ReasonFlags BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuingDistributionPoint::_onlySomeReasons
	ReasonFlags_tBB7FD42D692CC7182B9F721F5558208A7EBF1C3A * ____onlySomeReasons_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuingDistributionPoint::_indirectCRL
	bool ____indirectCRL_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuingDistributionPoint::_onlyContainsAttributeCerts
	bool ____onlyContainsAttributeCerts_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuingDistributionPoint::seq
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___seq_8;

public:
	inline static int32_t get_offset_of__distributionPoint_2() { return static_cast<int32_t>(offsetof(IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3, ____distributionPoint_2)); }
	inline DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5 * get__distributionPoint_2() const { return ____distributionPoint_2; }
	inline DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5 ** get_address_of__distributionPoint_2() { return &____distributionPoint_2; }
	inline void set__distributionPoint_2(DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5 * value)
	{
		____distributionPoint_2 = value;
		Il2CppCodeGenWriteBarrier((&____distributionPoint_2), value);
	}

	inline static int32_t get_offset_of__onlyContainsUserCerts_3() { return static_cast<int32_t>(offsetof(IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3, ____onlyContainsUserCerts_3)); }
	inline bool get__onlyContainsUserCerts_3() const { return ____onlyContainsUserCerts_3; }
	inline bool* get_address_of__onlyContainsUserCerts_3() { return &____onlyContainsUserCerts_3; }
	inline void set__onlyContainsUserCerts_3(bool value)
	{
		____onlyContainsUserCerts_3 = value;
	}

	inline static int32_t get_offset_of__onlyContainsCACerts_4() { return static_cast<int32_t>(offsetof(IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3, ____onlyContainsCACerts_4)); }
	inline bool get__onlyContainsCACerts_4() const { return ____onlyContainsCACerts_4; }
	inline bool* get_address_of__onlyContainsCACerts_4() { return &____onlyContainsCACerts_4; }
	inline void set__onlyContainsCACerts_4(bool value)
	{
		____onlyContainsCACerts_4 = value;
	}

	inline static int32_t get_offset_of__onlySomeReasons_5() { return static_cast<int32_t>(offsetof(IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3, ____onlySomeReasons_5)); }
	inline ReasonFlags_tBB7FD42D692CC7182B9F721F5558208A7EBF1C3A * get__onlySomeReasons_5() const { return ____onlySomeReasons_5; }
	inline ReasonFlags_tBB7FD42D692CC7182B9F721F5558208A7EBF1C3A ** get_address_of__onlySomeReasons_5() { return &____onlySomeReasons_5; }
	inline void set__onlySomeReasons_5(ReasonFlags_tBB7FD42D692CC7182B9F721F5558208A7EBF1C3A * value)
	{
		____onlySomeReasons_5 = value;
		Il2CppCodeGenWriteBarrier((&____onlySomeReasons_5), value);
	}

	inline static int32_t get_offset_of__indirectCRL_6() { return static_cast<int32_t>(offsetof(IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3, ____indirectCRL_6)); }
	inline bool get__indirectCRL_6() const { return ____indirectCRL_6; }
	inline bool* get_address_of__indirectCRL_6() { return &____indirectCRL_6; }
	inline void set__indirectCRL_6(bool value)
	{
		____indirectCRL_6 = value;
	}

	inline static int32_t get_offset_of__onlyContainsAttributeCerts_7() { return static_cast<int32_t>(offsetof(IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3, ____onlyContainsAttributeCerts_7)); }
	inline bool get__onlyContainsAttributeCerts_7() const { return ____onlyContainsAttributeCerts_7; }
	inline bool* get_address_of__onlyContainsAttributeCerts_7() { return &____onlyContainsAttributeCerts_7; }
	inline void set__onlyContainsAttributeCerts_7(bool value)
	{
		____onlyContainsAttributeCerts_7 = value;
	}

	inline static int32_t get_offset_of_seq_8() { return static_cast<int32_t>(offsetof(IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3, ___seq_8)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_seq_8() const { return ___seq_8; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_seq_8() { return &___seq_8; }
	inline void set_seq_8(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___seq_8 = value;
		Il2CppCodeGenWriteBarrier((&___seq_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISSUINGDISTRIBUTIONPOINT_T12AB715F1439394A85CC76FC3578A6E76A4BE3E3_H
#ifndef DHDOMAINPARAMETERS_T5962030617F28C31FDD95768A1FA99BCDBCC4416_H
#define DHDOMAINPARAMETERS_T5962030617F28C31FDD95768A1FA99BCDBCC4416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHDomainParameters
struct  DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHDomainParameters::p
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___p_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHDomainParameters::g
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___g_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHDomainParameters::q
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___q_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHDomainParameters::j
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___j_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHValidationParms BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHDomainParameters::validationParms
	DHValidationParms_t63346AFA3FBC424FF674B40E14C3E078B994BF10 * ___validationParms_6;

public:
	inline static int32_t get_offset_of_p_2() { return static_cast<int32_t>(offsetof(DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416, ___p_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_p_2() const { return ___p_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_p_2() { return &___p_2; }
	inline void set_p_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___p_2 = value;
		Il2CppCodeGenWriteBarrier((&___p_2), value);
	}

	inline static int32_t get_offset_of_g_3() { return static_cast<int32_t>(offsetof(DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416, ___g_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_g_3() const { return ___g_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_g_3() { return &___g_3; }
	inline void set_g_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___g_3 = value;
		Il2CppCodeGenWriteBarrier((&___g_3), value);
	}

	inline static int32_t get_offset_of_q_4() { return static_cast<int32_t>(offsetof(DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416, ___q_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_q_4() const { return ___q_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_q_4() { return &___q_4; }
	inline void set_q_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___q_4 = value;
		Il2CppCodeGenWriteBarrier((&___q_4), value);
	}

	inline static int32_t get_offset_of_j_5() { return static_cast<int32_t>(offsetof(DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416, ___j_5)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_j_5() const { return ___j_5; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_j_5() { return &___j_5; }
	inline void set_j_5(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___j_5 = value;
		Il2CppCodeGenWriteBarrier((&___j_5), value);
	}

	inline static int32_t get_offset_of_validationParms_6() { return static_cast<int32_t>(offsetof(DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416, ___validationParms_6)); }
	inline DHValidationParms_t63346AFA3FBC424FF674B40E14C3E078B994BF10 * get_validationParms_6() const { return ___validationParms_6; }
	inline DHValidationParms_t63346AFA3FBC424FF674B40E14C3E078B994BF10 ** get_address_of_validationParms_6() { return &___validationParms_6; }
	inline void set_validationParms_6(DHValidationParms_t63346AFA3FBC424FF674B40E14C3E078B994BF10 * value)
	{
		___validationParms_6 = value;
		Il2CppCodeGenWriteBarrier((&___validationParms_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHDOMAINPARAMETERS_T5962030617F28C31FDD95768A1FA99BCDBCC4416_H
#ifndef DHPUBLICKEY_T1BC64FCC586E30DF63E8F82EAE2708243A4D8739_H
#define DHPUBLICKEY_T1BC64FCC586E30DF63E8F82EAE2708243A4D8739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHPublicKey
struct  DHPublicKey_t1BC64FCC586E30DF63E8F82EAE2708243A4D8739  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHPublicKey::y
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___y_2;

public:
	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(DHPublicKey_t1BC64FCC586E30DF63E8F82EAE2708243A4D8739, ___y_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_y_2() const { return ___y_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___y_2 = value;
		Il2CppCodeGenWriteBarrier((&___y_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPUBLICKEY_T1BC64FCC586E30DF63E8F82EAE2708243A4D8739_H
#ifndef DHVALIDATIONPARMS_T63346AFA3FBC424FF674B40E14C3E078B994BF10_H
#define DHVALIDATIONPARMS_T63346AFA3FBC424FF674B40E14C3E078B994BF10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHValidationParms
struct  DHValidationParms_t63346AFA3FBC424FF674B40E14C3E078B994BF10  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHValidationParms::seed
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___seed_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.DHValidationParms::pgenCounter
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___pgenCounter_3;

public:
	inline static int32_t get_offset_of_seed_2() { return static_cast<int32_t>(offsetof(DHValidationParms_t63346AFA3FBC424FF674B40E14C3E078B994BF10, ___seed_2)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_seed_2() const { return ___seed_2; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_seed_2() { return &___seed_2; }
	inline void set_seed_2(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___seed_2 = value;
		Il2CppCodeGenWriteBarrier((&___seed_2), value);
	}

	inline static int32_t get_offset_of_pgenCounter_3() { return static_cast<int32_t>(offsetof(DHValidationParms_t63346AFA3FBC424FF674B40E14C3E078B994BF10, ___pgenCounter_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_pgenCounter_3() const { return ___pgenCounter_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_pgenCounter_3() { return &___pgenCounter_3; }
	inline void set_pgenCounter_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___pgenCounter_3 = value;
		Il2CppCodeGenWriteBarrier((&___pgenCounter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHVALIDATIONPARMS_T63346AFA3FBC424FF674B40E14C3E078B994BF10_H
#ifndef KEYSPECIFICINFO_T1D036F45CEDCB08508825FC00CA64E1C4EC96D1B_H
#define KEYSPECIFICINFO_T1D036F45CEDCB08508825FC00CA64E1C4EC96D1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.KeySpecificInfo
struct  KeySpecificInfo_t1D036F45CEDCB08508825FC00CA64E1C4EC96D1B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.KeySpecificInfo::algorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___algorithm_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.KeySpecificInfo::counter
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___counter_3;

public:
	inline static int32_t get_offset_of_algorithm_2() { return static_cast<int32_t>(offsetof(KeySpecificInfo_t1D036F45CEDCB08508825FC00CA64E1C4EC96D1B, ___algorithm_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_algorithm_2() const { return ___algorithm_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_algorithm_2() { return &___algorithm_2; }
	inline void set_algorithm_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___algorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_2), value);
	}

	inline static int32_t get_offset_of_counter_3() { return static_cast<int32_t>(offsetof(KeySpecificInfo_t1D036F45CEDCB08508825FC00CA64E1C4EC96D1B, ___counter_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_counter_3() const { return ___counter_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_counter_3() { return &___counter_3; }
	inline void set_counter_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___counter_3 = value;
		Il2CppCodeGenWriteBarrier((&___counter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYSPECIFICINFO_T1D036F45CEDCB08508825FC00CA64E1C4EC96D1B_H
#ifndef OTHERINFO_TDCD42C62FFE1800A60E6B27E171AE3DE620D9D7B_H
#define OTHERINFO_TDCD42C62FFE1800A60E6B27E171AE3DE620D9D7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.OtherInfo
struct  OtherInfo_tDCD42C62FFE1800A60E6B27E171AE3DE620D9D7B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.KeySpecificInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.OtherInfo::keyInfo
	KeySpecificInfo_t1D036F45CEDCB08508825FC00CA64E1C4EC96D1B * ___keyInfo_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.OtherInfo::partyAInfo
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___partyAInfo_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.OtherInfo::suppPubInfo
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___suppPubInfo_4;

public:
	inline static int32_t get_offset_of_keyInfo_2() { return static_cast<int32_t>(offsetof(OtherInfo_tDCD42C62FFE1800A60E6B27E171AE3DE620D9D7B, ___keyInfo_2)); }
	inline KeySpecificInfo_t1D036F45CEDCB08508825FC00CA64E1C4EC96D1B * get_keyInfo_2() const { return ___keyInfo_2; }
	inline KeySpecificInfo_t1D036F45CEDCB08508825FC00CA64E1C4EC96D1B ** get_address_of_keyInfo_2() { return &___keyInfo_2; }
	inline void set_keyInfo_2(KeySpecificInfo_t1D036F45CEDCB08508825FC00CA64E1C4EC96D1B * value)
	{
		___keyInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyInfo_2), value);
	}

	inline static int32_t get_offset_of_partyAInfo_3() { return static_cast<int32_t>(offsetof(OtherInfo_tDCD42C62FFE1800A60E6B27E171AE3DE620D9D7B, ___partyAInfo_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_partyAInfo_3() const { return ___partyAInfo_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_partyAInfo_3() { return &___partyAInfo_3; }
	inline void set_partyAInfo_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___partyAInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___partyAInfo_3), value);
	}

	inline static int32_t get_offset_of_suppPubInfo_4() { return static_cast<int32_t>(offsetof(OtherInfo_tDCD42C62FFE1800A60E6B27E171AE3DE620D9D7B, ___suppPubInfo_4)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_suppPubInfo_4() const { return ___suppPubInfo_4; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_suppPubInfo_4() { return &___suppPubInfo_4; }
	inline void set_suppPubInfo_4(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___suppPubInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___suppPubInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERINFO_TDCD42C62FFE1800A60E6B27E171AE3DE620D9D7B_H
#ifndef C2PNB163V1HOLDER_T8A0B28EE96C579DFDE022611429B476CFA43756A_H
#define C2PNB163V1HOLDER_T8A0B28EE96C579DFDE022611429B476CFA43756A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v1Holder
struct  C2pnb163v1Holder_t8A0B28EE96C579DFDE022611429B476CFA43756A  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2pnb163v1Holder_t8A0B28EE96C579DFDE022611429B476CFA43756A_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb163v1Holder_t8A0B28EE96C579DFDE022611429B476CFA43756A_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB163V1HOLDER_T8A0B28EE96C579DFDE022611429B476CFA43756A_H
#ifndef C2PNB163V2HOLDER_T33D854A61D6F03C5C5359508AF8019792F4B1C4D_H
#define C2PNB163V2HOLDER_T33D854A61D6F03C5C5359508AF8019792F4B1C4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v2Holder
struct  C2pnb163v2Holder_t33D854A61D6F03C5C5359508AF8019792F4B1C4D  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2pnb163v2Holder_t33D854A61D6F03C5C5359508AF8019792F4B1C4D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb163v2Holder_t33D854A61D6F03C5C5359508AF8019792F4B1C4D_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB163V2HOLDER_T33D854A61D6F03C5C5359508AF8019792F4B1C4D_H
#ifndef C2PNB163V3HOLDER_TC31D04E359E806F900D9E1818E1FE97FCBB4B479_H
#define C2PNB163V3HOLDER_TC31D04E359E806F900D9E1818E1FE97FCBB4B479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v3Holder
struct  C2pnb163v3Holder_tC31D04E359E806F900D9E1818E1FE97FCBB4B479  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2pnb163v3Holder_tC31D04E359E806F900D9E1818E1FE97FCBB4B479_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v3Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb163v3Holder_tC31D04E359E806F900D9E1818E1FE97FCBB4B479_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB163V3HOLDER_TC31D04E359E806F900D9E1818E1FE97FCBB4B479_H
#ifndef C2PNB176W1HOLDER_TF413BB3F184D410A3AD1B7514C191DC77C4B1F27_H
#define C2PNB176W1HOLDER_TF413BB3F184D410A3AD1B7514C191DC77C4B1F27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb176w1Holder
struct  C2pnb176w1Holder_tF413BB3F184D410A3AD1B7514C191DC77C4B1F27  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2pnb176w1Holder_tF413BB3F184D410A3AD1B7514C191DC77C4B1F27_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb176w1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb176w1Holder_tF413BB3F184D410A3AD1B7514C191DC77C4B1F27_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB176W1HOLDER_TF413BB3F184D410A3AD1B7514C191DC77C4B1F27_H
#ifndef C2PNB208W1HOLDER_T8DAD438F485B42E97AAD49A661A02FFE4E3B41E3_H
#define C2PNB208W1HOLDER_T8DAD438F485B42E97AAD49A661A02FFE4E3B41E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb208w1Holder
struct  C2pnb208w1Holder_t8DAD438F485B42E97AAD49A661A02FFE4E3B41E3  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2pnb208w1Holder_t8DAD438F485B42E97AAD49A661A02FFE4E3B41E3_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb208w1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb208w1Holder_t8DAD438F485B42E97AAD49A661A02FFE4E3B41E3_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB208W1HOLDER_T8DAD438F485B42E97AAD49A661A02FFE4E3B41E3_H
#ifndef C2PNB272W1HOLDER_T01601F54760AA1D6329BCF0E5332DB920C398F38_H
#define C2PNB272W1HOLDER_T01601F54760AA1D6329BCF0E5332DB920C398F38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb272w1Holder
struct  C2pnb272w1Holder_t01601F54760AA1D6329BCF0E5332DB920C398F38  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2pnb272w1Holder_t01601F54760AA1D6329BCF0E5332DB920C398F38_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb272w1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb272w1Holder_t01601F54760AA1D6329BCF0E5332DB920C398F38_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB272W1HOLDER_T01601F54760AA1D6329BCF0E5332DB920C398F38_H
#ifndef C2PNB304W1HOLDER_T9CB420C938E8A40549F738BA3187BC9BF6061985_H
#define C2PNB304W1HOLDER_T9CB420C938E8A40549F738BA3187BC9BF6061985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb304w1Holder
struct  C2pnb304w1Holder_t9CB420C938E8A40549F738BA3187BC9BF6061985  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2pnb304w1Holder_t9CB420C938E8A40549F738BA3187BC9BF6061985_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb304w1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb304w1Holder_t9CB420C938E8A40549F738BA3187BC9BF6061985_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB304W1HOLDER_T9CB420C938E8A40549F738BA3187BC9BF6061985_H
#ifndef C2PNB368W1HOLDER_T18E9C7B2C6C25C8B731F5DDDC6545DAD9CDF00EB_H
#define C2PNB368W1HOLDER_T18E9C7B2C6C25C8B731F5DDDC6545DAD9CDF00EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb368w1Holder
struct  C2pnb368w1Holder_t18E9C7B2C6C25C8B731F5DDDC6545DAD9CDF00EB  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2pnb368w1Holder_t18E9C7B2C6C25C8B731F5DDDC6545DAD9CDF00EB_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb368w1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb368w1Holder_t18E9C7B2C6C25C8B731F5DDDC6545DAD9CDF00EB_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB368W1HOLDER_T18E9C7B2C6C25C8B731F5DDDC6545DAD9CDF00EB_H
#ifndef C2TNB191V1HOLDER_T1367882EB8F05E213721278F2C759E17EA6D5E31_H
#define C2TNB191V1HOLDER_T1367882EB8F05E213721278F2C759E17EA6D5E31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v1Holder
struct  C2tnb191v1Holder_t1367882EB8F05E213721278F2C759E17EA6D5E31  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2tnb191v1Holder_t1367882EB8F05E213721278F2C759E17EA6D5E31_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb191v1Holder_t1367882EB8F05E213721278F2C759E17EA6D5E31_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB191V1HOLDER_T1367882EB8F05E213721278F2C759E17EA6D5E31_H
#ifndef C2TNB191V2HOLDER_TCB413ABBE11C9447C2F7045F99707167706C8835_H
#define C2TNB191V2HOLDER_TCB413ABBE11C9447C2F7045F99707167706C8835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v2Holder
struct  C2tnb191v2Holder_tCB413ABBE11C9447C2F7045F99707167706C8835  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2tnb191v2Holder_tCB413ABBE11C9447C2F7045F99707167706C8835_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb191v2Holder_tCB413ABBE11C9447C2F7045F99707167706C8835_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB191V2HOLDER_TCB413ABBE11C9447C2F7045F99707167706C8835_H
#ifndef C2TNB191V3HOLDER_TD40950CD2FC2F29F3420E7F61690E88A8D4AA943_H
#define C2TNB191V3HOLDER_TD40950CD2FC2F29F3420E7F61690E88A8D4AA943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v3Holder
struct  C2tnb191v3Holder_tD40950CD2FC2F29F3420E7F61690E88A8D4AA943  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2tnb191v3Holder_tD40950CD2FC2F29F3420E7F61690E88A8D4AA943_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v3Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb191v3Holder_tD40950CD2FC2F29F3420E7F61690E88A8D4AA943_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB191V3HOLDER_TD40950CD2FC2F29F3420E7F61690E88A8D4AA943_H
#ifndef C2TNB239V1HOLDER_T1377D091458C021AA9A49407C956CF8518C99337_H
#define C2TNB239V1HOLDER_T1377D091458C021AA9A49407C956CF8518C99337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v1Holder
struct  C2tnb239v1Holder_t1377D091458C021AA9A49407C956CF8518C99337  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2tnb239v1Holder_t1377D091458C021AA9A49407C956CF8518C99337_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb239v1Holder_t1377D091458C021AA9A49407C956CF8518C99337_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB239V1HOLDER_T1377D091458C021AA9A49407C956CF8518C99337_H
#ifndef C2TNB239V2HOLDER_T9322B934ED1BA165E81B26A1939F7310A69F8143_H
#define C2TNB239V2HOLDER_T9322B934ED1BA165E81B26A1939F7310A69F8143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v2Holder
struct  C2tnb239v2Holder_t9322B934ED1BA165E81B26A1939F7310A69F8143  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2tnb239v2Holder_t9322B934ED1BA165E81B26A1939F7310A69F8143_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb239v2Holder_t9322B934ED1BA165E81B26A1939F7310A69F8143_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB239V2HOLDER_T9322B934ED1BA165E81B26A1939F7310A69F8143_H
#ifndef C2TNB239V3HOLDER_TFFAD0931B458F5F0FE38776B868AE5A001F55FEF_H
#define C2TNB239V3HOLDER_TFFAD0931B458F5F0FE38776B868AE5A001F55FEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v3Holder
struct  C2tnb239v3Holder_tFFAD0931B458F5F0FE38776B868AE5A001F55FEF  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2tnb239v3Holder_tFFAD0931B458F5F0FE38776B868AE5A001F55FEF_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v3Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb239v3Holder_tFFAD0931B458F5F0FE38776B868AE5A001F55FEF_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB239V3HOLDER_TFFAD0931B458F5F0FE38776B868AE5A001F55FEF_H
#ifndef C2TNB359V1HOLDER_T7F14F0C705583EE4B756E8D0E33D86F0199709DC_H
#define C2TNB359V1HOLDER_T7F14F0C705583EE4B756E8D0E33D86F0199709DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb359v1Holder
struct  C2tnb359v1Holder_t7F14F0C705583EE4B756E8D0E33D86F0199709DC  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2tnb359v1Holder_t7F14F0C705583EE4B756E8D0E33D86F0199709DC_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb359v1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb359v1Holder_t7F14F0C705583EE4B756E8D0E33D86F0199709DC_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB359V1HOLDER_T7F14F0C705583EE4B756E8D0E33D86F0199709DC_H
#ifndef C2TNB431R1HOLDER_T6A9AC360F567C118B22A04422B670E2C8ABE36E6_H
#define C2TNB431R1HOLDER_T6A9AC360F567C118B22A04422B670E2C8ABE36E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb431r1Holder
struct  C2tnb431r1Holder_t6A9AC360F567C118B22A04422B670E2C8ABE36E6  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct C2tnb431r1Holder_t6A9AC360F567C118B22A04422B670E2C8ABE36E6_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb431r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb431r1Holder_t6A9AC360F567C118B22A04422B670E2C8ABE36E6_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB431R1HOLDER_T6A9AC360F567C118B22A04422B670E2C8ABE36E6_H
#ifndef PRIME192V1HOLDER_T4AD410E77E85E21C26A453D22FDF0B9B7FE11059_H
#define PRIME192V1HOLDER_T4AD410E77E85E21C26A453D22FDF0B9B7FE11059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v1Holder
struct  Prime192v1Holder_t4AD410E77E85E21C26A453D22FDF0B9B7FE11059  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Prime192v1Holder_t4AD410E77E85E21C26A453D22FDF0B9B7FE11059_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime192v1Holder_t4AD410E77E85E21C26A453D22FDF0B9B7FE11059_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME192V1HOLDER_T4AD410E77E85E21C26A453D22FDF0B9B7FE11059_H
#ifndef PRIME192V2HOLDER_T103640B4583E4F1250A72703A0F32F609505EF51_H
#define PRIME192V2HOLDER_T103640B4583E4F1250A72703A0F32F609505EF51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v2Holder
struct  Prime192v2Holder_t103640B4583E4F1250A72703A0F32F609505EF51  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Prime192v2Holder_t103640B4583E4F1250A72703A0F32F609505EF51_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime192v2Holder_t103640B4583E4F1250A72703A0F32F609505EF51_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME192V2HOLDER_T103640B4583E4F1250A72703A0F32F609505EF51_H
#ifndef PRIME192V3HOLDER_T1CDBBF7B8D0CA24AB0E17EEAE30BBF3210D9E302_H
#define PRIME192V3HOLDER_T1CDBBF7B8D0CA24AB0E17EEAE30BBF3210D9E302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v3Holder
struct  Prime192v3Holder_t1CDBBF7B8D0CA24AB0E17EEAE30BBF3210D9E302  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Prime192v3Holder_t1CDBBF7B8D0CA24AB0E17EEAE30BBF3210D9E302_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v3Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime192v3Holder_t1CDBBF7B8D0CA24AB0E17EEAE30BBF3210D9E302_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME192V3HOLDER_T1CDBBF7B8D0CA24AB0E17EEAE30BBF3210D9E302_H
#ifndef PRIME239V1HOLDER_T40AF4FF5F2DB318E127CC3572BFD3CE225BBD074_H
#define PRIME239V1HOLDER_T40AF4FF5F2DB318E127CC3572BFD3CE225BBD074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v1Holder
struct  Prime239v1Holder_t40AF4FF5F2DB318E127CC3572BFD3CE225BBD074  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Prime239v1Holder_t40AF4FF5F2DB318E127CC3572BFD3CE225BBD074_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime239v1Holder_t40AF4FF5F2DB318E127CC3572BFD3CE225BBD074_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME239V1HOLDER_T40AF4FF5F2DB318E127CC3572BFD3CE225BBD074_H
#ifndef PRIME239V2HOLDER_T569AA76E2C8B74C52BDDAB0FD22F87CA71FB5E11_H
#define PRIME239V2HOLDER_T569AA76E2C8B74C52BDDAB0FD22F87CA71FB5E11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v2Holder
struct  Prime239v2Holder_t569AA76E2C8B74C52BDDAB0FD22F87CA71FB5E11  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Prime239v2Holder_t569AA76E2C8B74C52BDDAB0FD22F87CA71FB5E11_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime239v2Holder_t569AA76E2C8B74C52BDDAB0FD22F87CA71FB5E11_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME239V2HOLDER_T569AA76E2C8B74C52BDDAB0FD22F87CA71FB5E11_H
#ifndef PRIME239V3HOLDER_T001CE85343E7FC5A220845BF6F0293A0FE2D27FD_H
#define PRIME239V3HOLDER_T001CE85343E7FC5A220845BF6F0293A0FE2D27FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v3Holder
struct  Prime239v3Holder_t001CE85343E7FC5A220845BF6F0293A0FE2D27FD  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Prime239v3Holder_t001CE85343E7FC5A220845BF6F0293A0FE2D27FD_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v3Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime239v3Holder_t001CE85343E7FC5A220845BF6F0293A0FE2D27FD_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME239V3HOLDER_T001CE85343E7FC5A220845BF6F0293A0FE2D27FD_H
#ifndef PRIME256V1HOLDER_TBCC6190E0BD85411C4B7A84C91FEF677FC428506_H
#define PRIME256V1HOLDER_TBCC6190E0BD85411C4B7A84C91FEF677FC428506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime256v1Holder
struct  Prime256v1Holder_tBCC6190E0BD85411C4B7A84C91FEF677FC428506  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Prime256v1Holder_tBCC6190E0BD85411C4B7A84C91FEF677FC428506_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime256v1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime256v1Holder_tBCC6190E0BD85411C4B7A84C91FEF677FC428506_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME256V1HOLDER_TBCC6190E0BD85411C4B7A84C91FEF677FC428506_H
#ifndef X962PARAMETERS_T9F1FD8DEA6DCEEFEF6CE1F9B3151F5624DBFEE84_H
#define X962PARAMETERS_T9F1FD8DEA6DCEEFEF6CE1F9B3151F5624DBFEE84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962Parameters
struct  X962Parameters_t9F1FD8DEA6DCEEFEF6CE1F9B3151F5624DBFEE84  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X962Parameters::_params
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ____params_2;

public:
	inline static int32_t get_offset_of__params_2() { return static_cast<int32_t>(offsetof(X962Parameters_t9F1FD8DEA6DCEEFEF6CE1F9B3151F5624DBFEE84, ____params_2)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get__params_2() const { return ____params_2; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of__params_2() { return &____params_2; }
	inline void set__params_2(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		____params_2 = value;
		Il2CppCodeGenWriteBarrier((&____params_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X962PARAMETERS_T9F1FD8DEA6DCEEFEF6CE1F9B3151F5624DBFEE84_H
#ifndef X9CURVE_TFBE5C777274C2EF80A4BF71CF358A8553F699894_H
#define X9CURVE_TFBE5C777274C2EF80A4BF71CF358A8553F699894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9Curve
struct  X9Curve_tFBE5C777274C2EF80A4BF71CF358A8553F699894  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9Curve::curve
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * ___curve_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9Curve::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9Curve::fieldIdentifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___fieldIdentifier_4;

public:
	inline static int32_t get_offset_of_curve_2() { return static_cast<int32_t>(offsetof(X9Curve_tFBE5C777274C2EF80A4BF71CF358A8553F699894, ___curve_2)); }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * get_curve_2() const { return ___curve_2; }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 ** get_address_of_curve_2() { return &___curve_2; }
	inline void set_curve_2(ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * value)
	{
		___curve_2 = value;
		Il2CppCodeGenWriteBarrier((&___curve_2), value);
	}

	inline static int32_t get_offset_of_seed_3() { return static_cast<int32_t>(offsetof(X9Curve_tFBE5C777274C2EF80A4BF71CF358A8553F699894, ___seed_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_3() const { return ___seed_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_3() { return &___seed_3; }
	inline void set_seed_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_3 = value;
		Il2CppCodeGenWriteBarrier((&___seed_3), value);
	}

	inline static int32_t get_offset_of_fieldIdentifier_4() { return static_cast<int32_t>(offsetof(X9Curve_tFBE5C777274C2EF80A4BF71CF358A8553F699894, ___fieldIdentifier_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_fieldIdentifier_4() const { return ___fieldIdentifier_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_fieldIdentifier_4() { return &___fieldIdentifier_4; }
	inline void set_fieldIdentifier_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___fieldIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___fieldIdentifier_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9CURVE_TFBE5C777274C2EF80A4BF71CF358A8553F699894_H
#ifndef X9ECPARAMETERS_T87A644D587E32F7498181513E6DADDB7F7AC4A86_H
#define X9ECPARAMETERS_T87A644D587E32F7498181513E6DADDB7F7AC4A86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters
struct  X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9FieldID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters::fieldID
	X9FieldID_t87179DEE274AEDFB62026D21D8059D527DF43814 * ___fieldID_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters::curve
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * ___curve_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters::g
	X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D * ___g_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters::n
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___n_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters::h
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___h_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_7;

public:
	inline static int32_t get_offset_of_fieldID_2() { return static_cast<int32_t>(offsetof(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86, ___fieldID_2)); }
	inline X9FieldID_t87179DEE274AEDFB62026D21D8059D527DF43814 * get_fieldID_2() const { return ___fieldID_2; }
	inline X9FieldID_t87179DEE274AEDFB62026D21D8059D527DF43814 ** get_address_of_fieldID_2() { return &___fieldID_2; }
	inline void set_fieldID_2(X9FieldID_t87179DEE274AEDFB62026D21D8059D527DF43814 * value)
	{
		___fieldID_2 = value;
		Il2CppCodeGenWriteBarrier((&___fieldID_2), value);
	}

	inline static int32_t get_offset_of_curve_3() { return static_cast<int32_t>(offsetof(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86, ___curve_3)); }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * get_curve_3() const { return ___curve_3; }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 ** get_address_of_curve_3() { return &___curve_3; }
	inline void set_curve_3(ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * value)
	{
		___curve_3 = value;
		Il2CppCodeGenWriteBarrier((&___curve_3), value);
	}

	inline static int32_t get_offset_of_g_4() { return static_cast<int32_t>(offsetof(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86, ___g_4)); }
	inline X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D * get_g_4() const { return ___g_4; }
	inline X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D ** get_address_of_g_4() { return &___g_4; }
	inline void set_g_4(X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D * value)
	{
		___g_4 = value;
		Il2CppCodeGenWriteBarrier((&___g_4), value);
	}

	inline static int32_t get_offset_of_n_5() { return static_cast<int32_t>(offsetof(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86, ___n_5)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_n_5() const { return ___n_5; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_n_5() { return &___n_5; }
	inline void set_n_5(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___n_5 = value;
		Il2CppCodeGenWriteBarrier((&___n_5), value);
	}

	inline static int32_t get_offset_of_h_6() { return static_cast<int32_t>(offsetof(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86, ___h_6)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_h_6() const { return ___h_6; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_h_6() { return &___h_6; }
	inline void set_h_6(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___h_6 = value;
		Il2CppCodeGenWriteBarrier((&___h_6), value);
	}

	inline static int32_t get_offset_of_seed_7() { return static_cast<int32_t>(offsetof(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86, ___seed_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_7() const { return ___seed_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_7() { return &___seed_7; }
	inline void set_seed_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_7 = value;
		Il2CppCodeGenWriteBarrier((&___seed_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPARAMETERS_T87A644D587E32F7498181513E6DADDB7F7AC4A86_H
#ifndef X9ECPOINT_TA0D997DA6AD7402CD6944CAA87A160062FC33C8D_H
#define X9ECPOINT_TA0D997DA6AD7402CD6944CAA87A160062FC33C8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECPoint
struct  X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECPoint::encoding
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___encoding_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECPoint::c
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * ___c_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECPoint::p
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___p_4;

public:
	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D, ___encoding_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_encoding_2() const { return ___encoding_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_2), value);
	}

	inline static int32_t get_offset_of_c_3() { return static_cast<int32_t>(offsetof(X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D, ___c_3)); }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * get_c_3() const { return ___c_3; }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 ** get_address_of_c_3() { return &___c_3; }
	inline void set_c_3(ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * value)
	{
		___c_3 = value;
		Il2CppCodeGenWriteBarrier((&___c_3), value);
	}

	inline static int32_t get_offset_of_p_4() { return static_cast<int32_t>(offsetof(X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D, ___p_4)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_p_4() const { return ___p_4; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_p_4() { return &___p_4; }
	inline void set_p_4(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___p_4 = value;
		Il2CppCodeGenWriteBarrier((&___p_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPOINT_TA0D997DA6AD7402CD6944CAA87A160062FC33C8D_H
#ifndef X9FIELDELEMENT_T377B32CDCEEE302DE2BEA58581F9F99F161E555F_H
#define X9FIELDELEMENT_T377B32CDCEEE302DE2BEA58581F9F99F161E555F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9FieldElement
struct  X9FieldElement_t377B32CDCEEE302DE2BEA58581F9F99F161E555F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9FieldElement::f
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___f_2;

public:
	inline static int32_t get_offset_of_f_2() { return static_cast<int32_t>(offsetof(X9FieldElement_t377B32CDCEEE302DE2BEA58581F9F99F161E555F, ___f_2)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_f_2() const { return ___f_2; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_f_2() { return &___f_2; }
	inline void set_f_2(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___f_2 = value;
		Il2CppCodeGenWriteBarrier((&___f_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9FIELDELEMENT_T377B32CDCEEE302DE2BEA58581F9F99F161E555F_H
#ifndef X9FIELDID_T87179DEE274AEDFB62026D21D8059D527DF43814_H
#define X9FIELDID_T87179DEE274AEDFB62026D21D8059D527DF43814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9FieldID
struct  X9FieldID_t87179DEE274AEDFB62026D21D8059D527DF43814  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9FieldID::id
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9FieldID::parameters
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___parameters_3;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(X9FieldID_t87179DEE274AEDFB62026D21D8059D527DF43814, ___id_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_2() const { return ___id_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_2), value);
	}

	inline static int32_t get_offset_of_parameters_3() { return static_cast<int32_t>(offsetof(X9FieldID_t87179DEE274AEDFB62026D21D8059D527DF43814, ___parameters_3)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_parameters_3() const { return ___parameters_3; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_parameters_3() { return &___parameters_3; }
	inline void set_parameters_3(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9FIELDID_T87179DEE274AEDFB62026D21D8059D527DF43814_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef ASN1OCTETSTRING_T013D79AEF2791863689C38F8305C12D7F540024B_H
#define ASN1OCTETSTRING_T013D79AEF2791863689C38F8305C12D7F540024B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString
struct  Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString::str
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B, ___str_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_str_2() const { return ___str_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OCTETSTRING_T013D79AEF2791863689C38F8305C12D7F540024B_H
#ifndef ASN1SEQUENCE_T8D18DC0674425C28D79ACDD26FD19D10BD62C205_H
#define ASN1SEQUENCE_T8D18DC0674425C28D79ACDD26FD19D10BD62C205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence
struct  Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence::elements
	Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* ___elements_2;

public:
	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205, ___elements_2)); }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* get_elements_2() const { return ___elements_2; }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1SEQUENCE_T8D18DC0674425C28D79ACDD26FD19D10BD62C205_H
#ifndef ASN1SET_TB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29_H
#define ASN1SET_TB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set
struct  Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set::elements
	Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* ___elements_2;

public:
	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29, ___elements_2)); }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* get_elements_2() const { return ___elements_2; }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1SET_TB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29_H
#ifndef ASN1TAGGEDOBJECT_T02C4D2D88510F17ED8F969894CA89B565BA3F0FD_H
#define ASN1TAGGEDOBJECT_T02C4D2D88510F17ED8F969894CA89B565BA3F0FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObject
struct  Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObject::tagNo
	int32_t ___tagNo_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObject::explicitly
	bool ___explicitly_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObject::obj
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___obj_4;

public:
	inline static int32_t get_offset_of_tagNo_2() { return static_cast<int32_t>(offsetof(Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD, ___tagNo_2)); }
	inline int32_t get_tagNo_2() const { return ___tagNo_2; }
	inline int32_t* get_address_of_tagNo_2() { return &___tagNo_2; }
	inline void set_tagNo_2(int32_t value)
	{
		___tagNo_2 = value;
	}

	inline static int32_t get_offset_of_explicitly_3() { return static_cast<int32_t>(offsetof(Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD, ___explicitly_3)); }
	inline bool get_explicitly_3() const { return ___explicitly_3; }
	inline bool* get_address_of_explicitly_3() { return &___explicitly_3; }
	inline void set_explicitly_3(bool value)
	{
		___explicitly_3 = value;
	}

	inline static int32_t get_offset_of_obj_4() { return static_cast<int32_t>(offsetof(Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD, ___obj_4)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_obj_4() const { return ___obj_4; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_obj_4() { return &___obj_4; }
	inline void set_obj_4(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___obj_4 = value;
		Il2CppCodeGenWriteBarrier((&___obj_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1TAGGEDOBJECT_T02C4D2D88510F17ED8F969894CA89B565BA3F0FD_H
#ifndef DERENUMERATED_TD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_H
#define DERENUMERATED_TD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated
struct  DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated::start
	int32_t ___start_3;

public:
	inline static int32_t get_offset_of_bytes_2() { return static_cast<int32_t>(offsetof(DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F, ___bytes_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_2() const { return ___bytes_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_2() { return &___bytes_2; }
	inline void set_bytes_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_2), value);
	}

	inline static int32_t get_offset_of_start_3() { return static_cast<int32_t>(offsetof(DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F, ___start_3)); }
	inline int32_t get_start_3() const { return ___start_3; }
	inline int32_t* get_address_of_start_3() { return &___start_3; }
	inline void set_start_3(int32_t value)
	{
		___start_3 = value;
	}
};

struct DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated::cache
	DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD* ___cache_4;

public:
	inline static int32_t get_offset_of_cache_4() { return static_cast<int32_t>(offsetof(DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_StaticFields, ___cache_4)); }
	inline DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD* get_cache_4() const { return ___cache_4; }
	inline DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD** get_address_of_cache_4() { return &___cache_4; }
	inline void set_cache_4(DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD* value)
	{
		___cache_4 = value;
		Il2CppCodeGenWriteBarrier((&___cache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERENUMERATED_TD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_H
#ifndef DERINTEGER_TD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96_H
#define DERINTEGER_TD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger
struct  DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger::start
	int32_t ___start_6;

public:
	inline static int32_t get_offset_of_bytes_5() { return static_cast<int32_t>(offsetof(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96, ___bytes_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_5() const { return ___bytes_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_5() { return &___bytes_5; }
	inline void set_bytes_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_5 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_5), value);
	}

	inline static int32_t get_offset_of_start_6() { return static_cast<int32_t>(offsetof(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96, ___start_6)); }
	inline int32_t get_start_6() const { return ___start_6; }
	inline int32_t* get_address_of_start_6() { return &___start_6; }
	inline void set_start_6(int32_t value)
	{
		___start_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERINTEGER_TD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96_H
#ifndef DEROBJECTIDENTIFIER_T6D50C7785FE0773A3C6CFF7A550A85885C2D2676_H
#define DEROBJECTIDENTIFIER_T6D50C7785FE0773A3C6CFF7A550A85885C2D2676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct  DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier::identifier
	String_t* ___identifier_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier::body
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___body_3;

public:
	inline static int32_t get_offset_of_identifier_2() { return static_cast<int32_t>(offsetof(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676, ___identifier_2)); }
	inline String_t* get_identifier_2() const { return ___identifier_2; }
	inline String_t** get_address_of_identifier_2() { return &___identifier_2; }
	inline void set_identifier_2(String_t* value)
	{
		___identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_2), value);
	}

	inline static int32_t get_offset_of_body_3() { return static_cast<int32_t>(offsetof(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676, ___body_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_body_3() const { return ___body_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_body_3() { return &___body_3; }
	inline void set_body_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___body_3 = value;
		Il2CppCodeGenWriteBarrier((&___body_3), value);
	}
};

struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier::cache
	DerObjectIdentifierU5BU5D_tC595A7256A12DE98032B8267A8795C3EC8F3D6B9* ___cache_5;

public:
	inline static int32_t get_offset_of_cache_5() { return static_cast<int32_t>(offsetof(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676_StaticFields, ___cache_5)); }
	inline DerObjectIdentifierU5BU5D_tC595A7256A12DE98032B8267A8795C3EC8F3D6B9* get_cache_5() const { return ___cache_5; }
	inline DerObjectIdentifierU5BU5D_tC595A7256A12DE98032B8267A8795C3EC8F3D6B9** get_address_of_cache_5() { return &___cache_5; }
	inline void set_cache_5(DerObjectIdentifierU5BU5D_tC595A7256A12DE98032B8267A8795C3EC8F3D6B9* value)
	{
		___cache_5 = value;
		Il2CppCodeGenWriteBarrier((&___cache_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEROBJECTIDENTIFIER_T6D50C7785FE0773A3C6CFF7A550A85885C2D2676_H
#ifndef DERSEQUENCEGENERATOR_T99E21CA5C65F2C0A0554A36D91212AEAA7EEBC24_H
#define DERSEQUENCEGENERATOR_T99E21CA5C65F2C0A0554A36D91212AEAA7EEBC24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSequenceGenerator
struct  DerSequenceGenerator_t99E21CA5C65F2C0A0554A36D91212AEAA7EEBC24  : public DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E
{
public:
	// System.IO.MemoryStream BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSequenceGenerator::_bOut
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ____bOut_4;

public:
	inline static int32_t get_offset_of__bOut_4() { return static_cast<int32_t>(offsetof(DerSequenceGenerator_t99E21CA5C65F2C0A0554A36D91212AEAA7EEBC24, ____bOut_4)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get__bOut_4() const { return ____bOut_4; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of__bOut_4() { return &____bOut_4; }
	inline void set__bOut_4(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		____bOut_4 = value;
		Il2CppCodeGenWriteBarrier((&____bOut_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSEQUENCEGENERATOR_T99E21CA5C65F2C0A0554A36D91212AEAA7EEBC24_H
#ifndef DERSETGENERATOR_TAC577F827FFB462A8EED476A99C94F018E5A1818_H
#define DERSETGENERATOR_TAC577F827FFB462A8EED476A99C94F018E5A1818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSetGenerator
struct  DerSetGenerator_tAC577F827FFB462A8EED476A99C94F018E5A1818  : public DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E
{
public:
	// System.IO.MemoryStream BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSetGenerator::_bOut
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ____bOut_4;

public:
	inline static int32_t get_offset_of__bOut_4() { return static_cast<int32_t>(offsetof(DerSetGenerator_tAC577F827FFB462A8EED476A99C94F018E5A1818, ____bOut_4)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get__bOut_4() const { return ____bOut_4; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of__bOut_4() { return &____bOut_4; }
	inline void set__bOut_4(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		____bOut_4 = value;
		Il2CppCodeGenWriteBarrier((&____bOut_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSETGENERATOR_TAC577F827FFB462A8EED476A99C94F018E5A1818_H
#ifndef DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#define DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerStringBase
struct  DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifndef DERUTCTIME_TA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E_H
#define DERUTCTIME_TA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtcTime
struct  DerUtcTime_tA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtcTime::time
	String_t* ___time_2;

public:
	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(DerUtcTime_tA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E, ___time_2)); }
	inline String_t* get_time_2() const { return ___time_2; }
	inline String_t** get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(String_t* value)
	{
		___time_2 = value;
		Il2CppCodeGenWriteBarrier((&___time_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERUTCTIME_TA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E_H
#ifndef BASEINPUTSTREAM_TFB934C38CB5B81842A543CE8228DDE5235F1E2E4_H
#define BASEINPUTSTREAM_TFB934C38CB5B81842A543CE8228DDE5235F1E2E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseInputStream
struct  BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseInputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTSTREAM_TFB934C38CB5B81842A543CE8228DDE5235F1E2E4_H
#ifndef FILTERSTREAM_T45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF_H
#define FILTERSTREAM_T45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.FilterStream
struct  FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.FilterStream::s
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___s_5;

public:
	inline static int32_t get_offset_of_s_5() { return static_cast<int32_t>(offsetof(FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF, ___s_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_s_5() const { return ___s_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_s_5() { return &___s_5; }
	inline void set_s_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___s_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERSTREAM_T45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF_H
#ifndef ASN1INPUTSTREAM_T41FB5C019456D8C95B07D0B145B80E4952061D33_H
#define ASN1INPUTSTREAM_T41FB5C019456D8C95B07D0B145B80E4952061D33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1InputStream
struct  Asn1InputStream_t41FB5C019456D8C95B07D0B145B80E4952061D33  : public FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1InputStream::limit
	int32_t ___limit_6;
	// System.Byte[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1InputStream::tmpBuffers
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___tmpBuffers_7;

public:
	inline static int32_t get_offset_of_limit_6() { return static_cast<int32_t>(offsetof(Asn1InputStream_t41FB5C019456D8C95B07D0B145B80E4952061D33, ___limit_6)); }
	inline int32_t get_limit_6() const { return ___limit_6; }
	inline int32_t* get_address_of_limit_6() { return &___limit_6; }
	inline void set_limit_6(int32_t value)
	{
		___limit_6 = value;
	}

	inline static int32_t get_offset_of_tmpBuffers_7() { return static_cast<int32_t>(offsetof(Asn1InputStream_t41FB5C019456D8C95B07D0B145B80E4952061D33, ___tmpBuffers_7)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_tmpBuffers_7() const { return ___tmpBuffers_7; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_tmpBuffers_7() { return &___tmpBuffers_7; }
	inline void set_tmpBuffers_7(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___tmpBuffers_7 = value;
		Il2CppCodeGenWriteBarrier((&___tmpBuffers_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1INPUTSTREAM_T41FB5C019456D8C95B07D0B145B80E4952061D33_H
#ifndef DEROCTETSTRING_TB3FE7FC789AF57240C7AAC64F625F962F499050A_H
#define DEROCTETSTRING_TB3FE7FC789AF57240C7AAC64F625F962F499050A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerOctetString
struct  DerOctetString_tB3FE7FC789AF57240C7AAC64F625F962F499050A  : public Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEROCTETSTRING_TB3FE7FC789AF57240C7AAC64F625F962F499050A_H
#ifndef DEROUTPUTSTREAM_TF14E89982B98990995F8610F800B024DD05ADFA1_H
#define DEROUTPUTSTREAM_TF14E89982B98990995F8610F800B024DD05ADFA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerOutputStream
struct  DerOutputStream_tF14E89982B98990995F8610F800B024DD05ADFA1  : public FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEROUTPUTSTREAM_TF14E89982B98990995F8610F800B024DD05ADFA1_H
#ifndef DERPRINTABLESTRING_TCA859465A18F31F2332B5409C7976B16738B74F0_H
#define DERPRINTABLESTRING_TCA859465A18F31F2332B5409C7976B16738B74F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerPrintableString
struct  DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerPrintableString::str
	String_t* ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERPRINTABLESTRING_TCA859465A18F31F2332B5409C7976B16738B74F0_H
#ifndef DERSEQUENCE_T28172634167ADE42A32B1361A26C6469E70B66BD_H
#define DERSEQUENCE_T28172634167ADE42A32B1361A26C6469E70B66BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSequence
struct  DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD  : public Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205
{
public:

public:
};

struct DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSequence::Empty
	DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD * ___Empty_3;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD_StaticFields, ___Empty_3)); }
	inline DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD * get_Empty_3() const { return ___Empty_3; }
	inline DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSEQUENCE_T28172634167ADE42A32B1361A26C6469E70B66BD_H
#ifndef DERSET_T0EF6821F7F0694159B02B3C79CEB9915176B219C_H
#define DERSET_T0EF6821F7F0694159B02B3C79CEB9915176B219C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSet
struct  DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C  : public Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29
{
public:

public:
};

struct DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSet BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSet::Empty
	DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C * ___Empty_3;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C_StaticFields, ___Empty_3)); }
	inline DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C * get_Empty_3() const { return ___Empty_3; }
	inline DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSET_T0EF6821F7F0694159B02B3C79CEB9915176B219C_H
#ifndef DERT61STRING_T18209D1F0F70408A82A5D829E160872FFEB74EB7_H
#define DERT61STRING_T18209D1F0F70408A82A5D829E160872FFEB74EB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerT61String
struct  DerT61String_t18209D1F0F70408A82A5D829E160872FFEB74EB7  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerT61String::str
	String_t* ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(DerT61String_t18209D1F0F70408A82A5D829E160872FFEB74EB7, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERT61STRING_T18209D1F0F70408A82A5D829E160872FFEB74EB7_H
#ifndef DERTAGGEDOBJECT_T96F301FC6AA54E94C6FF065F9FE47ABC4918FF89_H
#define DERTAGGEDOBJECT_T96F301FC6AA54E94C6FF065F9FE47ABC4918FF89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerTaggedObject
struct  DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89  : public Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERTAGGEDOBJECT_T96F301FC6AA54E94C6FF065F9FE47ABC4918FF89_H
#ifndef DERUNIVERSALSTRING_TFBE8201E676C1E71838E76950D3575DD93CAC6BA_H
#define DERUNIVERSALSTRING_TFBE8201E676C1E71838E76950D3575DD93CAC6BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUniversalString
struct  DerUniversalString_tFBE8201E676C1E71838E76950D3575DD93CAC6BA  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUniversalString::str
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___str_3;

public:
	inline static int32_t get_offset_of_str_3() { return static_cast<int32_t>(offsetof(DerUniversalString_tFBE8201E676C1E71838E76950D3575DD93CAC6BA, ___str_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_str_3() const { return ___str_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_str_3() { return &___str_3; }
	inline void set_str_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___str_3 = value;
		Il2CppCodeGenWriteBarrier((&___str_3), value);
	}
};

struct DerUniversalString_tFBE8201E676C1E71838E76950D3575DD93CAC6BA_StaticFields
{
public:
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUniversalString::table
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___table_2;

public:
	inline static int32_t get_offset_of_table_2() { return static_cast<int32_t>(offsetof(DerUniversalString_tFBE8201E676C1E71838E76950D3575DD93CAC6BA_StaticFields, ___table_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_table_2() const { return ___table_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_table_2() { return &___table_2; }
	inline void set_table_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___table_2 = value;
		Il2CppCodeGenWriteBarrier((&___table_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERUNIVERSALSTRING_TFBE8201E676C1E71838E76950D3575DD93CAC6BA_H
#ifndef DERUTF8STRING_TB210D5A249CA8CB39DF88A0F5B064601FF1ECF15_H
#define DERUTF8STRING_TB210D5A249CA8CB39DF88A0F5B064601FF1ECF15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtf8String
struct  DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtf8String::str
	String_t* ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERUTF8STRING_TB210D5A249CA8CB39DF88A0F5B064601FF1ECF15_H
#ifndef DERVIDEOTEXSTRING_T563824BD35F31D6793DFE7A755AA11E9D6312DDC_H
#define DERVIDEOTEXSTRING_T563824BD35F31D6793DFE7A755AA11E9D6312DDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerVideotexString
struct  DerVideotexString_t563824BD35F31D6793DFE7A755AA11E9D6312DDC  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerVideotexString::mString
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mString_2;

public:
	inline static int32_t get_offset_of_mString_2() { return static_cast<int32_t>(offsetof(DerVideotexString_t563824BD35F31D6793DFE7A755AA11E9D6312DDC, ___mString_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mString_2() const { return ___mString_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mString_2() { return &___mString_2; }
	inline void set_mString_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mString_2 = value;
		Il2CppCodeGenWriteBarrier((&___mString_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERVIDEOTEXSTRING_T563824BD35F31D6793DFE7A755AA11E9D6312DDC_H
#ifndef DERVISIBLESTRING_T768DDD05BFF705BCAE47CAA87999BB86B921ECE5_H
#define DERVISIBLESTRING_T768DDD05BFF705BCAE47CAA87999BB86B921ECE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerVisibleString
struct  DerVisibleString_t768DDD05BFF705BCAE47CAA87999BB86B921ECE5  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerVisibleString::str
	String_t* ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(DerVisibleString_t768DDD05BFF705BCAE47CAA87999BB86B921ECE5, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERVISIBLESTRING_T768DDD05BFF705BCAE47CAA87999BB86B921ECE5_H
#ifndef LIMITEDINPUTSTREAM_TC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76_H
#define LIMITEDINPUTSTREAM_TC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.LimitedInputStream
struct  LimitedInputStream_tC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76  : public BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.LimitedInputStream::_in
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____in_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.LimitedInputStream::_limit
	int32_t ____limit_7;

public:
	inline static int32_t get_offset_of__in_6() { return static_cast<int32_t>(offsetof(LimitedInputStream_tC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76, ____in_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__in_6() const { return ____in_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__in_6() { return &____in_6; }
	inline void set__in_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____in_6 = value;
		Il2CppCodeGenWriteBarrier((&____in_6), value);
	}

	inline static int32_t get_offset_of__limit_7() { return static_cast<int32_t>(offsetof(LimitedInputStream_tC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76, ____limit_7)); }
	inline int32_t get__limit_7() const { return ____limit_7; }
	inline int32_t* get_address_of__limit_7() { return &____limit_7; }
	inline void set__limit_7(int32_t value)
	{
		____limit_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIMITEDINPUTSTREAM_TC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76_H
#ifndef CERTPOLICYID_T7C079F8022D54FA0DF0A535335D5D57D21DCC655_H
#define CERTPOLICYID_T7C079F8022D54FA0DF0A535335D5D57D21DCC655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertPolicyID
struct  CertPolicyID_t7C079F8022D54FA0DF0A535335D5D57D21DCC655  : public DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTPOLICYID_T7C079F8022D54FA0DF0A535335D5D57D21DCC655_H
#ifndef CRLNUMBER_T9F10A6E65ACD2137A04ABF8DC8E15B16A83934D5_H
#define CRLNUMBER_T9F10A6E65ACD2137A04ABF8DC8E15B16A83934D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlNumber
struct  CrlNumber_t9F10A6E65ACD2137A04ABF8DC8E15B16A83934D5  : public DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRLNUMBER_T9F10A6E65ACD2137A04ABF8DC8E15B16A83934D5_H
#ifndef CRLREASON_T483113851B3620EA05D3EB258A0476FAF88BCD08_H
#define CRLREASON_T483113851B3620EA05D3EB258A0476FAF88BCD08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlReason
struct  CrlReason_t483113851B3620EA05D3EB258A0476FAF88BCD08  : public DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F
{
public:

public:
};

struct CrlReason_t483113851B3620EA05D3EB258A0476FAF88BCD08_StaticFields
{
public:
	// System.String[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlReason::ReasonString
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___ReasonString_15;

public:
	inline static int32_t get_offset_of_ReasonString_15() { return static_cast<int32_t>(offsetof(CrlReason_t483113851B3620EA05D3EB258A0476FAF88BCD08_StaticFields, ___ReasonString_15)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_ReasonString_15() const { return ___ReasonString_15; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_ReasonString_15() { return &___ReasonString_15; }
	inline void set_ReasonString_15(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___ReasonString_15 = value;
		Il2CppCodeGenWriteBarrier((&___ReasonString_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRLREASON_T483113851B3620EA05D3EB258A0476FAF88BCD08_H
#ifndef KEYPURPOSEID_T2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_H
#define KEYPURPOSEID_T2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID
struct  KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3  : public DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676
{
public:

public:
};

struct KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::AnyExtendedKeyUsage
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___AnyExtendedKeyUsage_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::IdKPServerAuth
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___IdKPServerAuth_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::IdKPClientAuth
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___IdKPClientAuth_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::IdKPCodeSigning
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___IdKPCodeSigning_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::IdKPEmailProtection
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___IdKPEmailProtection_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::IdKPIpsecEndSystem
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___IdKPIpsecEndSystem_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::IdKPIpsecTunnel
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___IdKPIpsecTunnel_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::IdKPIpsecUser
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___IdKPIpsecUser_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::IdKPTimeStamping
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___IdKPTimeStamping_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::IdKPOcspSigning
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___IdKPOcspSigning_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::IdKPSmartCardLogon
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___IdKPSmartCardLogon_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyPurposeID::IdKPMacAddress
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * ___IdKPMacAddress_18;

public:
	inline static int32_t get_offset_of_AnyExtendedKeyUsage_7() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___AnyExtendedKeyUsage_7)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_AnyExtendedKeyUsage_7() const { return ___AnyExtendedKeyUsage_7; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_AnyExtendedKeyUsage_7() { return &___AnyExtendedKeyUsage_7; }
	inline void set_AnyExtendedKeyUsage_7(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___AnyExtendedKeyUsage_7 = value;
		Il2CppCodeGenWriteBarrier((&___AnyExtendedKeyUsage_7), value);
	}

	inline static int32_t get_offset_of_IdKPServerAuth_8() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___IdKPServerAuth_8)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_IdKPServerAuth_8() const { return ___IdKPServerAuth_8; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_IdKPServerAuth_8() { return &___IdKPServerAuth_8; }
	inline void set_IdKPServerAuth_8(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___IdKPServerAuth_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdKPServerAuth_8), value);
	}

	inline static int32_t get_offset_of_IdKPClientAuth_9() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___IdKPClientAuth_9)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_IdKPClientAuth_9() const { return ___IdKPClientAuth_9; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_IdKPClientAuth_9() { return &___IdKPClientAuth_9; }
	inline void set_IdKPClientAuth_9(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___IdKPClientAuth_9 = value;
		Il2CppCodeGenWriteBarrier((&___IdKPClientAuth_9), value);
	}

	inline static int32_t get_offset_of_IdKPCodeSigning_10() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___IdKPCodeSigning_10)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_IdKPCodeSigning_10() const { return ___IdKPCodeSigning_10; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_IdKPCodeSigning_10() { return &___IdKPCodeSigning_10; }
	inline void set_IdKPCodeSigning_10(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___IdKPCodeSigning_10 = value;
		Il2CppCodeGenWriteBarrier((&___IdKPCodeSigning_10), value);
	}

	inline static int32_t get_offset_of_IdKPEmailProtection_11() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___IdKPEmailProtection_11)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_IdKPEmailProtection_11() const { return ___IdKPEmailProtection_11; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_IdKPEmailProtection_11() { return &___IdKPEmailProtection_11; }
	inline void set_IdKPEmailProtection_11(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___IdKPEmailProtection_11 = value;
		Il2CppCodeGenWriteBarrier((&___IdKPEmailProtection_11), value);
	}

	inline static int32_t get_offset_of_IdKPIpsecEndSystem_12() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___IdKPIpsecEndSystem_12)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_IdKPIpsecEndSystem_12() const { return ___IdKPIpsecEndSystem_12; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_IdKPIpsecEndSystem_12() { return &___IdKPIpsecEndSystem_12; }
	inline void set_IdKPIpsecEndSystem_12(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___IdKPIpsecEndSystem_12 = value;
		Il2CppCodeGenWriteBarrier((&___IdKPIpsecEndSystem_12), value);
	}

	inline static int32_t get_offset_of_IdKPIpsecTunnel_13() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___IdKPIpsecTunnel_13)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_IdKPIpsecTunnel_13() const { return ___IdKPIpsecTunnel_13; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_IdKPIpsecTunnel_13() { return &___IdKPIpsecTunnel_13; }
	inline void set_IdKPIpsecTunnel_13(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___IdKPIpsecTunnel_13 = value;
		Il2CppCodeGenWriteBarrier((&___IdKPIpsecTunnel_13), value);
	}

	inline static int32_t get_offset_of_IdKPIpsecUser_14() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___IdKPIpsecUser_14)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_IdKPIpsecUser_14() const { return ___IdKPIpsecUser_14; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_IdKPIpsecUser_14() { return &___IdKPIpsecUser_14; }
	inline void set_IdKPIpsecUser_14(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___IdKPIpsecUser_14 = value;
		Il2CppCodeGenWriteBarrier((&___IdKPIpsecUser_14), value);
	}

	inline static int32_t get_offset_of_IdKPTimeStamping_15() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___IdKPTimeStamping_15)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_IdKPTimeStamping_15() const { return ___IdKPTimeStamping_15; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_IdKPTimeStamping_15() { return &___IdKPTimeStamping_15; }
	inline void set_IdKPTimeStamping_15(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___IdKPTimeStamping_15 = value;
		Il2CppCodeGenWriteBarrier((&___IdKPTimeStamping_15), value);
	}

	inline static int32_t get_offset_of_IdKPOcspSigning_16() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___IdKPOcspSigning_16)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_IdKPOcspSigning_16() const { return ___IdKPOcspSigning_16; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_IdKPOcspSigning_16() { return &___IdKPOcspSigning_16; }
	inline void set_IdKPOcspSigning_16(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___IdKPOcspSigning_16 = value;
		Il2CppCodeGenWriteBarrier((&___IdKPOcspSigning_16), value);
	}

	inline static int32_t get_offset_of_IdKPSmartCardLogon_17() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___IdKPSmartCardLogon_17)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_IdKPSmartCardLogon_17() const { return ___IdKPSmartCardLogon_17; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_IdKPSmartCardLogon_17() { return &___IdKPSmartCardLogon_17; }
	inline void set_IdKPSmartCardLogon_17(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___IdKPSmartCardLogon_17 = value;
		Il2CppCodeGenWriteBarrier((&___IdKPSmartCardLogon_17), value);
	}

	inline static int32_t get_offset_of_IdKPMacAddress_18() { return static_cast<int32_t>(offsetof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields, ___IdKPMacAddress_18)); }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * get_IdKPMacAddress_18() const { return ___IdKPMacAddress_18; }
	inline KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 ** get_address_of_IdKPMacAddress_18() { return &___IdKPMacAddress_18; }
	inline void set_IdKPMacAddress_18(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3 * value)
	{
		___IdKPMacAddress_18 = value;
		Il2CppCodeGenWriteBarrier((&___IdKPMacAddress_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYPURPOSEID_T2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_H
#ifndef INDEFINITELENGTHINPUTSTREAM_T78574C641176D5AE91E8E960BC03534E0118AD60_H
#define INDEFINITELENGTHINPUTSTREAM_T78574C641176D5AE91E8E960BC03534E0118AD60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IndefiniteLengthInputStream
struct  IndefiniteLengthInputStream_t78574C641176D5AE91E8E960BC03534E0118AD60  : public LimitedInputStream_tC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IndefiniteLengthInputStream::_lookAhead
	int32_t ____lookAhead_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IndefiniteLengthInputStream::_eofOn00
	bool ____eofOn00_9;

public:
	inline static int32_t get_offset_of__lookAhead_8() { return static_cast<int32_t>(offsetof(IndefiniteLengthInputStream_t78574C641176D5AE91E8E960BC03534E0118AD60, ____lookAhead_8)); }
	inline int32_t get__lookAhead_8() const { return ____lookAhead_8; }
	inline int32_t* get_address_of__lookAhead_8() { return &____lookAhead_8; }
	inline void set__lookAhead_8(int32_t value)
	{
		____lookAhead_8 = value;
	}

	inline static int32_t get_offset_of__eofOn00_9() { return static_cast<int32_t>(offsetof(IndefiniteLengthInputStream_t78574C641176D5AE91E8E960BC03534E0118AD60, ____eofOn00_9)); }
	inline bool get__eofOn00_9() const { return ____eofOn00_9; }
	inline bool* get_address_of__eofOn00_9() { return &____eofOn00_9; }
	inline void set__eofOn00_9(bool value)
	{
		____eofOn00_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEFINITELENGTHINPUTSTREAM_T78574C641176D5AE91E8E960BC03534E0118AD60_H
#ifndef LAZYASN1INPUTSTREAM_T93ED05B1CDA7E05EBB6E4523081092D6C157D975_H
#define LAZYASN1INPUTSTREAM_T93ED05B1CDA7E05EBB6E4523081092D6C157D975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.LazyAsn1InputStream
struct  LazyAsn1InputStream_t93ED05B1CDA7E05EBB6E4523081092D6C157D975  : public Asn1InputStream_t41FB5C019456D8C95B07D0B145B80E4952061D33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYASN1INPUTSTREAM_T93ED05B1CDA7E05EBB6E4523081092D6C157D975_H
#ifndef LAZYDERSEQUENCE_T3BF5407E9974044C642DA390F7C13E4A0AEF1CD8_H
#define LAZYDERSEQUENCE_T3BF5407E9974044C642DA390F7C13E4A0AEF1CD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.LazyDerSequence
struct  LazyDerSequence_t3BF5407E9974044C642DA390F7C13E4A0AEF1CD8  : public DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.LazyDerSequence::encoded
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___encoded_4;

public:
	inline static int32_t get_offset_of_encoded_4() { return static_cast<int32_t>(offsetof(LazyDerSequence_t3BF5407E9974044C642DA390F7C13E4A0AEF1CD8, ___encoded_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_encoded_4() const { return ___encoded_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_encoded_4() { return &___encoded_4; }
	inline void set_encoded_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___encoded_4 = value;
		Il2CppCodeGenWriteBarrier((&___encoded_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYDERSEQUENCE_T3BF5407E9974044C642DA390F7C13E4A0AEF1CD8_H
#ifndef LAZYDERSET_T3F23EBC0B037C25238E3F2D3BDB8417113D9F4BD_H
#define LAZYDERSET_T3F23EBC0B037C25238E3F2D3BDB8417113D9F4BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.LazyDerSet
struct  LazyDerSet_t3F23EBC0B037C25238E3F2D3BDB8417113D9F4BD  : public DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.LazyDerSet::encoded
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___encoded_4;

public:
	inline static int32_t get_offset_of_encoded_4() { return static_cast<int32_t>(offsetof(LazyDerSet_t3F23EBC0B037C25238E3F2D3BDB8417113D9F4BD, ___encoded_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_encoded_4() const { return ___encoded_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_encoded_4() { return &___encoded_4; }
	inline void set_encoded_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___encoded_4 = value;
		Il2CppCodeGenWriteBarrier((&___encoded_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYDERSET_T3F23EBC0B037C25238E3F2D3BDB8417113D9F4BD_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5200 = { sizeof (DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676), -1, sizeof(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5200[4] = 
{
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676::get_offset_of_identifier_2(),
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676::get_offset_of_body_3(),
	0,
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676_StaticFields::get_offset_of_cache_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5201 = { sizeof (DerOctetString_tB3FE7FC789AF57240C7AAC64F625F962F499050A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5202 = { sizeof (DerOctetStringParser_t79DFE7483E35FB0A440C25C98C7DD3E95E32341C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5202[1] = 
{
	DerOctetStringParser_t79DFE7483E35FB0A440C25C98C7DD3E95E32341C::get_offset_of_stream_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5203 = { sizeof (DerOutputStream_tF14E89982B98990995F8610F800B024DD05ADFA1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5204 = { sizeof (DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5204[1] = 
{
	DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5205 = { sizeof (DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD), -1, sizeof(DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5205[1] = 
{
	DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD_StaticFields::get_offset_of_Empty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5206 = { sizeof (DerSequenceGenerator_t99E21CA5C65F2C0A0554A36D91212AEAA7EEBC24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5206[1] = 
{
	DerSequenceGenerator_t99E21CA5C65F2C0A0554A36D91212AEAA7EEBC24::get_offset_of__bOut_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5207 = { sizeof (DerSequenceParser_t9487D03AE4F6C2923CB5FB92B384B7448CE0311E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5207[1] = 
{
	DerSequenceParser_t9487D03AE4F6C2923CB5FB92B384B7448CE0311E::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5208 = { sizeof (DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C), -1, sizeof(DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5208[1] = 
{
	DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C_StaticFields::get_offset_of_Empty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5209 = { sizeof (DerSetGenerator_tAC577F827FFB462A8EED476A99C94F018E5A1818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5209[1] = 
{
	DerSetGenerator_tAC577F827FFB462A8EED476A99C94F018E5A1818::get_offset_of__bOut_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5210 = { sizeof (DerSetParser_t6BEA112499DB4FF0CA867CA454F4B8E8F759DC50), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5210[1] = 
{
	DerSetParser_t6BEA112499DB4FF0CA867CA454F4B8E8F759DC50::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5211 = { sizeof (DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5212 = { sizeof (DerT61String_t18209D1F0F70408A82A5D829E160872FFEB74EB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5212[1] = 
{
	DerT61String_t18209D1F0F70408A82A5D829E160872FFEB74EB7::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5213 = { sizeof (DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5214 = { sizeof (DerUniversalString_tFBE8201E676C1E71838E76950D3575DD93CAC6BA), -1, sizeof(DerUniversalString_tFBE8201E676C1E71838E76950D3575DD93CAC6BA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5214[2] = 
{
	DerUniversalString_tFBE8201E676C1E71838E76950D3575DD93CAC6BA_StaticFields::get_offset_of_table_2(),
	DerUniversalString_tFBE8201E676C1E71838E76950D3575DD93CAC6BA::get_offset_of_str_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5215 = { sizeof (DerUtcTime_tA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5215[1] = 
{
	DerUtcTime_tA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5216 = { sizeof (DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5216[1] = 
{
	DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5217 = { sizeof (DerVideotexString_t563824BD35F31D6793DFE7A755AA11E9D6312DDC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5217[1] = 
{
	DerVideotexString_t563824BD35F31D6793DFE7A755AA11E9D6312DDC::get_offset_of_mString_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5218 = { sizeof (DerVisibleString_t768DDD05BFF705BCAE47CAA87999BB86B921ECE5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5218[1] = 
{
	DerVisibleString_t768DDD05BFF705BCAE47CAA87999BB86B921ECE5::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5219 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5220 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5221 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5222 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5223 = { sizeof (IndefiniteLengthInputStream_t78574C641176D5AE91E8E960BC03534E0118AD60), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5223[2] = 
{
	IndefiniteLengthInputStream_t78574C641176D5AE91E8E960BC03534E0118AD60::get_offset_of__lookAhead_8(),
	IndefiniteLengthInputStream_t78574C641176D5AE91E8E960BC03534E0118AD60::get_offset_of__eofOn00_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5224 = { sizeof (LazyAsn1InputStream_t93ED05B1CDA7E05EBB6E4523081092D6C157D975), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5225 = { sizeof (LazyDerSequence_t3BF5407E9974044C642DA390F7C13E4A0AEF1CD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5225[1] = 
{
	LazyDerSequence_t3BF5407E9974044C642DA390F7C13E4A0AEF1CD8::get_offset_of_encoded_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5226 = { sizeof (LazyDerSet_t3F23EBC0B037C25238E3F2D3BDB8417113D9F4BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5226[1] = 
{
	LazyDerSet_t3F23EBC0B037C25238E3F2D3BDB8417113D9F4BD::get_offset_of_encoded_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5227 = { sizeof (LimitedInputStream_tC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5227[2] = 
{
	LimitedInputStream_tC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76::get_offset_of__in_6(),
	LimitedInputStream_tC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76::get_offset_of__limit_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5228 = { sizeof (OidTokenizer_tA4B8515FFAF2CA7E83D8322B929C889649577199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5228[2] = 
{
	OidTokenizer_tA4B8515FFAF2CA7E83D8322B929C889649577199::get_offset_of_oid_0(),
	OidTokenizer_tA4B8515FFAF2CA7E83D8322B929C889649577199::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5229 = { sizeof (DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5229[5] = 
{
	DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416::get_offset_of_p_2(),
	DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416::get_offset_of_g_3(),
	DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416::get_offset_of_q_4(),
	DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416::get_offset_of_j_5(),
	DHDomainParameters_t5962030617F28C31FDD95768A1FA99BCDBCC4416::get_offset_of_validationParms_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5230 = { sizeof (DHPublicKey_t1BC64FCC586E30DF63E8F82EAE2708243A4D8739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5230[1] = 
{
	DHPublicKey_t1BC64FCC586E30DF63E8F82EAE2708243A4D8739::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5231 = { sizeof (DHValidationParms_t63346AFA3FBC424FF674B40E14C3E078B994BF10), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5231[2] = 
{
	DHValidationParms_t63346AFA3FBC424FF674B40E14C3E078B994BF10::get_offset_of_seed_2(),
	DHValidationParms_t63346AFA3FBC424FF674B40E14C3E078B994BF10::get_offset_of_pgenCounter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5232 = { sizeof (ECNamedCurveTable_t731CDB9CDFC0570FBDA70A17A198E014401715B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5233 = { sizeof (KeySpecificInfo_t1D036F45CEDCB08508825FC00CA64E1C4EC96D1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5233[2] = 
{
	KeySpecificInfo_t1D036F45CEDCB08508825FC00CA64E1C4EC96D1B::get_offset_of_algorithm_2(),
	KeySpecificInfo_t1D036F45CEDCB08508825FC00CA64E1C4EC96D1B::get_offset_of_counter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5234 = { sizeof (OtherInfo_tDCD42C62FFE1800A60E6B27E171AE3DE620D9D7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5234[3] = 
{
	OtherInfo_tDCD42C62FFE1800A60E6B27E171AE3DE620D9D7B::get_offset_of_keyInfo_2(),
	OtherInfo_tDCD42C62FFE1800A60E6B27E171AE3DE620D9D7B::get_offset_of_partyAInfo_3(),
	OtherInfo_tDCD42C62FFE1800A60E6B27E171AE3DE620D9D7B::get_offset_of_suppPubInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5235 = { sizeof (X962NamedCurves_tC5338F1681E7B1DA60CCDCB58294991C7FAC49D1), -1, sizeof(X962NamedCurves_tC5338F1681E7B1DA60CCDCB58294991C7FAC49D1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5235[3] = 
{
	X962NamedCurves_tC5338F1681E7B1DA60CCDCB58294991C7FAC49D1_StaticFields::get_offset_of_objIds_0(),
	X962NamedCurves_tC5338F1681E7B1DA60CCDCB58294991C7FAC49D1_StaticFields::get_offset_of_curves_1(),
	X962NamedCurves_tC5338F1681E7B1DA60CCDCB58294991C7FAC49D1_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5236 = { sizeof (Prime192v1Holder_t4AD410E77E85E21C26A453D22FDF0B9B7FE11059), -1, sizeof(Prime192v1Holder_t4AD410E77E85E21C26A453D22FDF0B9B7FE11059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5236[1] = 
{
	Prime192v1Holder_t4AD410E77E85E21C26A453D22FDF0B9B7FE11059_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5237 = { sizeof (Prime192v2Holder_t103640B4583E4F1250A72703A0F32F609505EF51), -1, sizeof(Prime192v2Holder_t103640B4583E4F1250A72703A0F32F609505EF51_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5237[1] = 
{
	Prime192v2Holder_t103640B4583E4F1250A72703A0F32F609505EF51_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5238 = { sizeof (Prime192v3Holder_t1CDBBF7B8D0CA24AB0E17EEAE30BBF3210D9E302), -1, sizeof(Prime192v3Holder_t1CDBBF7B8D0CA24AB0E17EEAE30BBF3210D9E302_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5238[1] = 
{
	Prime192v3Holder_t1CDBBF7B8D0CA24AB0E17EEAE30BBF3210D9E302_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5239 = { sizeof (Prime239v1Holder_t40AF4FF5F2DB318E127CC3572BFD3CE225BBD074), -1, sizeof(Prime239v1Holder_t40AF4FF5F2DB318E127CC3572BFD3CE225BBD074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5239[1] = 
{
	Prime239v1Holder_t40AF4FF5F2DB318E127CC3572BFD3CE225BBD074_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5240 = { sizeof (Prime239v2Holder_t569AA76E2C8B74C52BDDAB0FD22F87CA71FB5E11), -1, sizeof(Prime239v2Holder_t569AA76E2C8B74C52BDDAB0FD22F87CA71FB5E11_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5240[1] = 
{
	Prime239v2Holder_t569AA76E2C8B74C52BDDAB0FD22F87CA71FB5E11_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5241 = { sizeof (Prime239v3Holder_t001CE85343E7FC5A220845BF6F0293A0FE2D27FD), -1, sizeof(Prime239v3Holder_t001CE85343E7FC5A220845BF6F0293A0FE2D27FD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5241[1] = 
{
	Prime239v3Holder_t001CE85343E7FC5A220845BF6F0293A0FE2D27FD_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5242 = { sizeof (Prime256v1Holder_tBCC6190E0BD85411C4B7A84C91FEF677FC428506), -1, sizeof(Prime256v1Holder_tBCC6190E0BD85411C4B7A84C91FEF677FC428506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5242[1] = 
{
	Prime256v1Holder_tBCC6190E0BD85411C4B7A84C91FEF677FC428506_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5243 = { sizeof (C2pnb163v1Holder_t8A0B28EE96C579DFDE022611429B476CFA43756A), -1, sizeof(C2pnb163v1Holder_t8A0B28EE96C579DFDE022611429B476CFA43756A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5243[1] = 
{
	C2pnb163v1Holder_t8A0B28EE96C579DFDE022611429B476CFA43756A_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5244 = { sizeof (C2pnb163v2Holder_t33D854A61D6F03C5C5359508AF8019792F4B1C4D), -1, sizeof(C2pnb163v2Holder_t33D854A61D6F03C5C5359508AF8019792F4B1C4D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5244[1] = 
{
	C2pnb163v2Holder_t33D854A61D6F03C5C5359508AF8019792F4B1C4D_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5245 = { sizeof (C2pnb163v3Holder_tC31D04E359E806F900D9E1818E1FE97FCBB4B479), -1, sizeof(C2pnb163v3Holder_tC31D04E359E806F900D9E1818E1FE97FCBB4B479_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5245[1] = 
{
	C2pnb163v3Holder_tC31D04E359E806F900D9E1818E1FE97FCBB4B479_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5246 = { sizeof (C2pnb176w1Holder_tF413BB3F184D410A3AD1B7514C191DC77C4B1F27), -1, sizeof(C2pnb176w1Holder_tF413BB3F184D410A3AD1B7514C191DC77C4B1F27_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5246[1] = 
{
	C2pnb176w1Holder_tF413BB3F184D410A3AD1B7514C191DC77C4B1F27_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5247 = { sizeof (C2tnb191v1Holder_t1367882EB8F05E213721278F2C759E17EA6D5E31), -1, sizeof(C2tnb191v1Holder_t1367882EB8F05E213721278F2C759E17EA6D5E31_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5247[1] = 
{
	C2tnb191v1Holder_t1367882EB8F05E213721278F2C759E17EA6D5E31_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5248 = { sizeof (C2tnb191v2Holder_tCB413ABBE11C9447C2F7045F99707167706C8835), -1, sizeof(C2tnb191v2Holder_tCB413ABBE11C9447C2F7045F99707167706C8835_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5248[1] = 
{
	C2tnb191v2Holder_tCB413ABBE11C9447C2F7045F99707167706C8835_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5249 = { sizeof (C2tnb191v3Holder_tD40950CD2FC2F29F3420E7F61690E88A8D4AA943), -1, sizeof(C2tnb191v3Holder_tD40950CD2FC2F29F3420E7F61690E88A8D4AA943_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5249[1] = 
{
	C2tnb191v3Holder_tD40950CD2FC2F29F3420E7F61690E88A8D4AA943_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5250 = { sizeof (C2pnb208w1Holder_t8DAD438F485B42E97AAD49A661A02FFE4E3B41E3), -1, sizeof(C2pnb208w1Holder_t8DAD438F485B42E97AAD49A661A02FFE4E3B41E3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5250[1] = 
{
	C2pnb208w1Holder_t8DAD438F485B42E97AAD49A661A02FFE4E3B41E3_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5251 = { sizeof (C2tnb239v1Holder_t1377D091458C021AA9A49407C956CF8518C99337), -1, sizeof(C2tnb239v1Holder_t1377D091458C021AA9A49407C956CF8518C99337_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5251[1] = 
{
	C2tnb239v1Holder_t1377D091458C021AA9A49407C956CF8518C99337_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5252 = { sizeof (C2tnb239v2Holder_t9322B934ED1BA165E81B26A1939F7310A69F8143), -1, sizeof(C2tnb239v2Holder_t9322B934ED1BA165E81B26A1939F7310A69F8143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5252[1] = 
{
	C2tnb239v2Holder_t9322B934ED1BA165E81B26A1939F7310A69F8143_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5253 = { sizeof (C2tnb239v3Holder_tFFAD0931B458F5F0FE38776B868AE5A001F55FEF), -1, sizeof(C2tnb239v3Holder_tFFAD0931B458F5F0FE38776B868AE5A001F55FEF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5253[1] = 
{
	C2tnb239v3Holder_tFFAD0931B458F5F0FE38776B868AE5A001F55FEF_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5254 = { sizeof (C2pnb272w1Holder_t01601F54760AA1D6329BCF0E5332DB920C398F38), -1, sizeof(C2pnb272w1Holder_t01601F54760AA1D6329BCF0E5332DB920C398F38_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5254[1] = 
{
	C2pnb272w1Holder_t01601F54760AA1D6329BCF0E5332DB920C398F38_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5255 = { sizeof (C2pnb304w1Holder_t9CB420C938E8A40549F738BA3187BC9BF6061985), -1, sizeof(C2pnb304w1Holder_t9CB420C938E8A40549F738BA3187BC9BF6061985_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5255[1] = 
{
	C2pnb304w1Holder_t9CB420C938E8A40549F738BA3187BC9BF6061985_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5256 = { sizeof (C2tnb359v1Holder_t7F14F0C705583EE4B756E8D0E33D86F0199709DC), -1, sizeof(C2tnb359v1Holder_t7F14F0C705583EE4B756E8D0E33D86F0199709DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5256[1] = 
{
	C2tnb359v1Holder_t7F14F0C705583EE4B756E8D0E33D86F0199709DC_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5257 = { sizeof (C2pnb368w1Holder_t18E9C7B2C6C25C8B731F5DDDC6545DAD9CDF00EB), -1, sizeof(C2pnb368w1Holder_t18E9C7B2C6C25C8B731F5DDDC6545DAD9CDF00EB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5257[1] = 
{
	C2pnb368w1Holder_t18E9C7B2C6C25C8B731F5DDDC6545DAD9CDF00EB_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5258 = { sizeof (C2tnb431r1Holder_t6A9AC360F567C118B22A04422B670E2C8ABE36E6), -1, sizeof(C2tnb431r1Holder_t6A9AC360F567C118B22A04422B670E2C8ABE36E6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5258[1] = 
{
	C2tnb431r1Holder_t6A9AC360F567C118B22A04422B670E2C8ABE36E6_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5259 = { sizeof (X962Parameters_t9F1FD8DEA6DCEEFEF6CE1F9B3151F5624DBFEE84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5259[1] = 
{
	X962Parameters_t9F1FD8DEA6DCEEFEF6CE1F9B3151F5624DBFEE84::get_offset_of__params_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5260 = { sizeof (X9Curve_tFBE5C777274C2EF80A4BF71CF358A8553F699894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5260[3] = 
{
	X9Curve_tFBE5C777274C2EF80A4BF71CF358A8553F699894::get_offset_of_curve_2(),
	X9Curve_tFBE5C777274C2EF80A4BF71CF358A8553F699894::get_offset_of_seed_3(),
	X9Curve_tFBE5C777274C2EF80A4BF71CF358A8553F699894::get_offset_of_fieldIdentifier_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5261 = { sizeof (X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5261[6] = 
{
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86::get_offset_of_fieldID_2(),
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86::get_offset_of_curve_3(),
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86::get_offset_of_g_4(),
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86::get_offset_of_n_5(),
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86::get_offset_of_h_6(),
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86::get_offset_of_seed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5262 = { sizeof (X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5262[1] = 
{
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB::get_offset_of_parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5263 = { sizeof (X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5263[3] = 
{
	X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D::get_offset_of_encoding_2(),
	X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D::get_offset_of_c_3(),
	X9ECPoint_tA0D997DA6AD7402CD6944CAA87A160062FC33C8D::get_offset_of_p_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5264 = { sizeof (X9FieldElement_t377B32CDCEEE302DE2BEA58581F9F99F161E555F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5264[1] = 
{
	X9FieldElement_t377B32CDCEEE302DE2BEA58581F9F99F161E555F::get_offset_of_f_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5265 = { sizeof (X9FieldID_t87179DEE274AEDFB62026D21D8059D527DF43814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5265[2] = 
{
	X9FieldID_t87179DEE274AEDFB62026D21D8059D527DF43814::get_offset_of_id_2(),
	X9FieldID_t87179DEE274AEDFB62026D21D8059D527DF43814::get_offset_of_parameters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5266 = { sizeof (X9IntegerConverter_t1BCF1565B0B650B877BF3AD1F8B4C22C83F65616), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5267 = { sizeof (X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6), -1, sizeof(X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5267[66] = 
{
	0,
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_ansi_X9_62_1(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_IdFieldType_2(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_PrimeField_3(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_CharacteristicTwoField_4(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_GNBasis_5(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_TPBasis_6(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_PPBasis_7(),
	0,
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_id_ecSigType_9(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_ECDsaWithSha1_10(),
	0,
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_id_publicKeyType_12(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_IdECPublicKey_13(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_ECDsaWithSha2_14(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_ECDsaWithSha224_15(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_ECDsaWithSha256_16(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_ECDsaWithSha384_17(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_ECDsaWithSha512_18(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_EllipticCurve_19(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_CTwoCurve_20(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Pnb163v1_21(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Pnb163v2_22(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Pnb163v3_23(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Pnb176w1_24(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Tnb191v1_25(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Tnb191v2_26(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Tnb191v3_27(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Onb191v4_28(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Onb191v5_29(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Pnb208w1_30(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Tnb239v1_31(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Tnb239v2_32(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Tnb239v3_33(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Onb239v4_34(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Onb239v5_35(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Pnb272w1_36(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Pnb304w1_37(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Tnb359v1_38(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Pnb368w1_39(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_C2Tnb431r1_40(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_PrimeCurve_41(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_Prime192v1_42(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_Prime192v2_43(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_Prime192v3_44(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_Prime239v1_45(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_Prime239v2_46(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_Prime239v3_47(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_Prime256v1_48(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_IdDsa_49(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_IdDsaWithSha1_50(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_X9x63Scheme_51(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_DHSinglePassStdDHSha1KdfScheme_52(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_DHSinglePassCofactorDHSha1KdfScheme_53(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_MqvSinglePassSha1KdfScheme_54(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_ansi_x9_42_55(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_DHPublicNumber_56(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_X9x42Schemes_57(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_DHStatic_58(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_DHEphem_59(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_DHOneFlow_60(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_DHHybrid1_61(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_DHHybrid2_62(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_DHHybridOneFlow_63(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_Mqv2_64(),
	X9ObjectIdentifiers_tA801960EE331C7E21CEA8A62855FA8520B297EB6_StaticFields::get_offset_of_Mqv1_65(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5268 = { sizeof (AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538), -1, sizeof(AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5268[4] = 
{
	AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538_StaticFields::get_offset_of_IdADCAIssuers_2(),
	AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538_StaticFields::get_offset_of_IdADOcsp_3(),
	AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538::get_offset_of_accessMethod_4(),
	AccessDescription_t483969979FA65EBC413ECA1DA206AF9813389538::get_offset_of_accessLocation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5269 = { sizeof (AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5269[2] = 
{
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3::get_offset_of_algorithm_2(),
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3::get_offset_of_parameters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5270 = { sizeof (AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5270[2] = 
{
	AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF::get_offset_of_obj_2(),
	AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF::get_offset_of_choiceObj_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5271 = { sizeof (AttCertValidityPeriod_tE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5271[2] = 
{
	AttCertValidityPeriod_tE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D::get_offset_of_notBeforeTime_2(),
	AttCertValidityPeriod_tE0AA4948B38B1F42E8AC8BFCDD115D5129B73F3D::get_offset_of_notAfterTime_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5272 = { sizeof (AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5272[2] = 
{
	AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76::get_offset_of_attrType_2(),
	AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76::get_offset_of_attrValues_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5273 = { sizeof (AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5273[3] = 
{
	AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B::get_offset_of_acinfo_2(),
	AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B::get_offset_of_signatureAlgorithm_3(),
	AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B::get_offset_of_signatureValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5274 = { sizeof (AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5274[9] = 
{
	AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA::get_offset_of_version_2(),
	AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA::get_offset_of_holder_3(),
	AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA::get_offset_of_issuer_4(),
	AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA::get_offset_of_signature_5(),
	AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA::get_offset_of_serialNumber_6(),
	AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA::get_offset_of_attrCertValidityPeriod_7(),
	AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA::get_offset_of_attributes_8(),
	AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA::get_offset_of_issuerUniqueID_9(),
	AttributeCertificateInfo_t85F5B73C43432A8DEE6B1D0317899A862E98C8CA::get_offset_of_extensions_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5275 = { sizeof (AttributeTable_t098C85EA280D051F2AB13FCF83240A14BB5921D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5275[1] = 
{
	AttributeTable_t098C85EA280D051F2AB13FCF83240A14BB5921D0::get_offset_of_attributes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5276 = { sizeof (AuthorityInformationAccess_t980F87C7F67F387659ED9FB5A588FD75B04D6656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5276[1] = 
{
	AuthorityInformationAccess_t980F87C7F67F387659ED9FB5A588FD75B04D6656::get_offset_of_descriptions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5277 = { sizeof (AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5277[3] = 
{
	AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA::get_offset_of_keyidentifier_2(),
	AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA::get_offset_of_certissuer_3(),
	AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA::get_offset_of_certserno_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5278 = { sizeof (BasicConstraints_t1F4EBA54689CD8E1522DAC83DC632226F189D217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5278[2] = 
{
	BasicConstraints_t1F4EBA54689CD8E1522DAC83DC632226F189D217::get_offset_of_cA_2(),
	BasicConstraints_t1F4EBA54689CD8E1522DAC83DC632226F189D217::get_offset_of_pathLenConstraint_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5279 = { sizeof (CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5279[3] = 
{
	CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8::get_offset_of_tbsCertList_2(),
	CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8::get_offset_of_sigAlgID_3(),
	CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8::get_offset_of_sig_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5280 = { sizeof (CertificatePair_tE8DB62307CA376BABBB15994D484DC6E4914F933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5280[2] = 
{
	CertificatePair_tE8DB62307CA376BABBB15994D484DC6E4914F933::get_offset_of_forward_2(),
	CertificatePair_tE8DB62307CA376BABBB15994D484DC6E4914F933::get_offset_of_reverse_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5281 = { sizeof (CertificatePolicies_t6065C56C8EC321C5D8E4ADF0552C36A62266491B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5281[1] = 
{
	CertificatePolicies_t6065C56C8EC321C5D8E4ADF0552C36A62266491B::get_offset_of_policyInformation_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5282 = { sizeof (CertPolicyID_t7C079F8022D54FA0DF0A535335D5D57D21DCC655), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5283 = { sizeof (CrlDistPoint_t91207A59A47177800952A0A16145D883799C1271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5283[1] = 
{
	CrlDistPoint_t91207A59A47177800952A0A16145D883799C1271::get_offset_of_seq_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5284 = { sizeof (CrlNumber_t9F10A6E65ACD2137A04ABF8DC8E15B16A83934D5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5285 = { sizeof (CrlReason_t483113851B3620EA05D3EB258A0476FAF88BCD08), -1, sizeof(CrlReason_t483113851B3620EA05D3EB258A0476FAF88BCD08_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5285[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	CrlReason_t483113851B3620EA05D3EB258A0476FAF88BCD08_StaticFields::get_offset_of_ReasonString_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5286 = { sizeof (DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5286[2] = 
{
	DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70::get_offset_of_digest_2(),
	DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70::get_offset_of_algID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5287 = { sizeof (DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5287[7] = 
{
	0,
	0,
	0,
	0,
	0,
	DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB::get_offset_of_contentType_7(),
	DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB::get_offset_of_contents_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5288 = { sizeof (DistributionPoint_tD337742D8A4356AF7EE04AA132632B3DC4FDD950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5288[3] = 
{
	DistributionPoint_tD337742D8A4356AF7EE04AA132632B3DC4FDD950::get_offset_of_distributionPoint_2(),
	DistributionPoint_tD337742D8A4356AF7EE04AA132632B3DC4FDD950::get_offset_of_reasons_3(),
	DistributionPoint_tD337742D8A4356AF7EE04AA132632B3DC4FDD950::get_offset_of_cRLIssuer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5289 = { sizeof (DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5289[4] = 
{
	DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5::get_offset_of_name_2(),
	DistributionPointName_t856C40324BAB89A7FF8488CD1C7F40FE8AFBBBE5::get_offset_of_type_3(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5290 = { sizeof (DsaParameter_tF0E4517BEB4D03D5160584964A7D745F61D741F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5290[3] = 
{
	DsaParameter_tF0E4517BEB4D03D5160584964A7D745F61D741F9::get_offset_of_p_2(),
	DsaParameter_tF0E4517BEB4D03D5160584964A7D745F61D741F9::get_offset_of_q_3(),
	DsaParameter_tF0E4517BEB4D03D5160584964A7D745F61D741F9::get_offset_of_g_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5291 = { sizeof (ExtendedKeyUsage_t7A5FAFC0A1BD83FB701067A50EC6DCDA7C6B08D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5291[2] = 
{
	ExtendedKeyUsage_t7A5FAFC0A1BD83FB701067A50EC6DCDA7C6B08D7::get_offset_of_usageTable_2(),
	ExtendedKeyUsage_t7A5FAFC0A1BD83FB701067A50EC6DCDA7C6B08D7::get_offset_of_seq_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5292 = { sizeof (GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5292[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B::get_offset_of_obj_11(),
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B::get_offset_of_tag_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5293 = { sizeof (GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5293[1] = 
{
	GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5294 = { sizeof (GeneralSubtree_t677DE5FC24696439438A34C0D61D26E907D13A13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5294[3] = 
{
	GeneralSubtree_t677DE5FC24696439438A34C0D61D26E907D13A13::get_offset_of_baseName_2(),
	GeneralSubtree_t677DE5FC24696439438A34C0D61D26E907D13A13::get_offset_of_minimum_3(),
	GeneralSubtree_t677DE5FC24696439438A34C0D61D26E907D13A13::get_offset_of_maximum_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5295 = { sizeof (Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5295[4] = 
{
	Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D::get_offset_of_baseCertificateID_2(),
	Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D::get_offset_of_entityName_3(),
	Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D::get_offset_of_objectDigestInfo_4(),
	Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D::get_offset_of_version_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5296 = { sizeof (IetfAttrSyntax_tDE34B40913FF0FC4BAE966515B60D2F3C7DB588B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5296[6] = 
{
	0,
	0,
	0,
	IetfAttrSyntax_tDE34B40913FF0FC4BAE966515B60D2F3C7DB588B::get_offset_of_policyAuthority_5(),
	IetfAttrSyntax_tDE34B40913FF0FC4BAE966515B60D2F3C7DB588B::get_offset_of_values_6(),
	IetfAttrSyntax_tDE34B40913FF0FC4BAE966515B60D2F3C7DB588B::get_offset_of_valueChoice_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5297 = { sizeof (IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5297[3] = 
{
	IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C::get_offset_of_issuer_2(),
	IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C::get_offset_of_serial_3(),
	IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C::get_offset_of_issuerUid_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5298 = { sizeof (IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5298[7] = 
{
	IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3::get_offset_of__distributionPoint_2(),
	IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3::get_offset_of__onlyContainsUserCerts_3(),
	IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3::get_offset_of__onlyContainsCACerts_4(),
	IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3::get_offset_of__onlySomeReasons_5(),
	IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3::get_offset_of__indirectCRL_6(),
	IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3::get_offset_of__onlyContainsAttributeCerts_7(),
	IssuingDistributionPoint_t12AB715F1439394A85CC76FC3578A6E76A4BE3E3::get_offset_of_seq_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5299 = { sizeof (KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3), -1, sizeof(KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5299[13] = 
{
	0,
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_AnyExtendedKeyUsage_7(),
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_IdKPServerAuth_8(),
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_IdKPClientAuth_9(),
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_IdKPCodeSigning_10(),
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_IdKPEmailProtection_11(),
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_IdKPIpsecEndSystem_12(),
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_IdKPIpsecTunnel_13(),
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_IdKPIpsecUser_14(),
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_IdKPTimeStamping_15(),
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_IdKPOcspSigning_16(),
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_IdKPSmartCardLogon_17(),
	KeyPurposeID_t2DF7D28359152A5627E0A7AFF75E428CCC94BBD3_StaticFields::get_offset_of_IdKPMacAddress_18(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
