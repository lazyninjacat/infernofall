﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector
struct Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString
struct Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetStringParser
struct Asn1OctetStringParser_t0A124E335C33E945227F8072A0AEC4CA345A5C43;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence
struct Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1SequenceParser
struct Asn1SequenceParser_t77615613D1D55866EDAB2563A8321E678C0FF76D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set
struct Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertOrEncCert
struct CertOrEncCert_t7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertifiedKeyPair
struct CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpCertificate
struct CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiBody
struct PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiFreeText
struct PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader
struct PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable
struct PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusInfo
struct PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo
struct ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Evidence
struct Evidence_t6897C267406A516689941A0FF07964FF20B44F07;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientIdentifier
struct KeyAgreeRecipientIdentifier_tE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.MetaData
struct MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorPublicKey
struct OriginatorPublicKey_t860ECE14956B5E7E4A98C44017B2529C39C8F209;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherKeyAttribute
struct OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerIdentifier
struct SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampAndCrl[]
struct TimeStampAndCrlU5BU5D_tB819CF986C00FD6731A39C2E86A78F6C473E5A1D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertId
struct CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate
struct CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue
struct EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PkiPublicationInfo
struct PkiPublicationInfo_t61EE698B09F3C95DFE0520755D3F4FA2CB059950;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString
struct DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime
struct DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String
struct DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger
struct DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificate
struct AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificateList
struct CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DigestInfo
struct DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName
struct GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames
struct GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure
struct X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions
struct X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name
struct X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters
struct X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<BestHTTP.Forms.HTTPFieldData>
struct List_1_t858CB6B1A25B247C14089648534E35574CE355D6;
// System.Collections.Generic.List`1<BestHTTP.PlatformSupport.Memory.BufferDesc>
struct List_1_t8EC08E7C8DC1966BECAA901BF0B9F2CA9CCA6153;
// System.Collections.Generic.List`1<BestHTTP.PlatformSupport.Memory.BufferSegment>
struct List_1_t39F8A268286B4DC546055C5521B47EB5B8833BDD;
// System.Collections.Generic.List`1<BestHTTP.PlatformSupport.Memory.BufferStore>
struct List_1_t9933B44A0BC696C4070E4E1148A24563517BC43E;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Exception
struct Exception_t;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Net.Sockets.LingerOption
struct LingerOption_tC6A8E9C30F48D9C07C38B2730012ECA6067723C7;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA;
// System.Net.Sockets.Socket
struct Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408;
// System.Threading.ReaderWriterLockSlim
struct ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef HTTPFIELDDATA_T94B41FD45B6B2D78BB6D3A9C9813108249662F41_H
#define HTTPFIELDDATA_T94B41FD45B6B2D78BB6D3A9C9813108249662F41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Forms.HTTPFieldData
struct  HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41  : public RuntimeObject
{
public:
	// System.String BestHTTP.Forms.HTTPFieldData::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.String BestHTTP.Forms.HTTPFieldData::<FileName>k__BackingField
	String_t* ___U3CFileNameU3Ek__BackingField_1;
	// System.String BestHTTP.Forms.HTTPFieldData::<MimeType>k__BackingField
	String_t* ___U3CMimeTypeU3Ek__BackingField_2;
	// System.Text.Encoding BestHTTP.Forms.HTTPFieldData::<Encoding>k__BackingField
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___U3CEncodingU3Ek__BackingField_3;
	// System.String BestHTTP.Forms.HTTPFieldData::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_4;
	// System.Byte[] BestHTTP.Forms.HTTPFieldData::<Binary>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CBinaryU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CFileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41, ___U3CFileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CFileNameU3Ek__BackingField_1() const { return ___U3CFileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFileNameU3Ek__BackingField_1() { return &___U3CFileNameU3Ek__BackingField_1; }
	inline void set_U3CFileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CFileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMimeTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41, ___U3CMimeTypeU3Ek__BackingField_2)); }
	inline String_t* get_U3CMimeTypeU3Ek__BackingField_2() const { return ___U3CMimeTypeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMimeTypeU3Ek__BackingField_2() { return &___U3CMimeTypeU3Ek__BackingField_2; }
	inline void set_U3CMimeTypeU3Ek__BackingField_2(String_t* value)
	{
		___U3CMimeTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMimeTypeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CEncodingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41, ___U3CEncodingU3Ek__BackingField_3)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_U3CEncodingU3Ek__BackingField_3() const { return ___U3CEncodingU3Ek__BackingField_3; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_U3CEncodingU3Ek__BackingField_3() { return &___U3CEncodingU3Ek__BackingField_3; }
	inline void set_U3CEncodingU3Ek__BackingField_3(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___U3CEncodingU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEncodingU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41, ___U3CTextU3Ek__BackingField_4)); }
	inline String_t* get_U3CTextU3Ek__BackingField_4() const { return ___U3CTextU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_4() { return &___U3CTextU3Ek__BackingField_4; }
	inline void set_U3CTextU3Ek__BackingField_4(String_t* value)
	{
		___U3CTextU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CBinaryU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41, ___U3CBinaryU3Ek__BackingField_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CBinaryU3Ek__BackingField_5() const { return ___U3CBinaryU3Ek__BackingField_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CBinaryU3Ek__BackingField_5() { return &___U3CBinaryU3Ek__BackingField_5; }
	inline void set_U3CBinaryU3Ek__BackingField_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CBinaryU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBinaryU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPFIELDDATA_T94B41FD45B6B2D78BB6D3A9C9813108249662F41_H
#ifndef HTTPFORMBASE_T175DC4359F6B8A66520DCA85B0F0C0A0F6064B21_H
#define HTTPFORMBASE_T175DC4359F6B8A66520DCA85B0F0C0A0F6064B21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Forms.HTTPFormBase
struct  HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<BestHTTP.Forms.HTTPFieldData> BestHTTP.Forms.HTTPFormBase::<Fields>k__BackingField
	List_1_t858CB6B1A25B247C14089648534E35574CE355D6 * ___U3CFieldsU3Ek__BackingField_1;
	// System.Boolean BestHTTP.Forms.HTTPFormBase::<IsChanged>k__BackingField
	bool ___U3CIsChangedU3Ek__BackingField_2;
	// System.Boolean BestHTTP.Forms.HTTPFormBase::<HasBinary>k__BackingField
	bool ___U3CHasBinaryU3Ek__BackingField_3;
	// System.Boolean BestHTTP.Forms.HTTPFormBase::<HasLongValue>k__BackingField
	bool ___U3CHasLongValueU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CFieldsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21, ___U3CFieldsU3Ek__BackingField_1)); }
	inline List_1_t858CB6B1A25B247C14089648534E35574CE355D6 * get_U3CFieldsU3Ek__BackingField_1() const { return ___U3CFieldsU3Ek__BackingField_1; }
	inline List_1_t858CB6B1A25B247C14089648534E35574CE355D6 ** get_address_of_U3CFieldsU3Ek__BackingField_1() { return &___U3CFieldsU3Ek__BackingField_1; }
	inline void set_U3CFieldsU3Ek__BackingField_1(List_1_t858CB6B1A25B247C14089648534E35574CE355D6 * value)
	{
		___U3CFieldsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFieldsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CIsChangedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21, ___U3CIsChangedU3Ek__BackingField_2)); }
	inline bool get_U3CIsChangedU3Ek__BackingField_2() const { return ___U3CIsChangedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsChangedU3Ek__BackingField_2() { return &___U3CIsChangedU3Ek__BackingField_2; }
	inline void set_U3CIsChangedU3Ek__BackingField_2(bool value)
	{
		___U3CIsChangedU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CHasBinaryU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21, ___U3CHasBinaryU3Ek__BackingField_3)); }
	inline bool get_U3CHasBinaryU3Ek__BackingField_3() const { return ___U3CHasBinaryU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CHasBinaryU3Ek__BackingField_3() { return &___U3CHasBinaryU3Ek__BackingField_3; }
	inline void set_U3CHasBinaryU3Ek__BackingField_3(bool value)
	{
		___U3CHasBinaryU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CHasLongValueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21, ___U3CHasLongValueU3Ek__BackingField_4)); }
	inline bool get_U3CHasLongValueU3Ek__BackingField_4() const { return ___U3CHasLongValueU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CHasLongValueU3Ek__BackingField_4() { return &___U3CHasLongValueU3Ek__BackingField_4; }
	inline void set_U3CHasLongValueU3Ek__BackingField_4(bool value)
	{
		___U3CHasLongValueU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPFORMBASE_T175DC4359F6B8A66520DCA85B0F0C0A0F6064B21_H
#ifndef JSON_T9AFDDCC782A208C5DB6BB6B24029377E4098DD6B_H
#define JSON_T9AFDDCC782A208C5DB6BB6B24029377E4098DD6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.JSON.Json
struct  Json_t9AFDDCC782A208C5DB6BB6B24029377E4098DD6B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T9AFDDCC782A208C5DB6BB6B24029377E4098DD6B_H
#ifndef DEFAULTIOSERVICE_T0BAFDF604B0344628BA3D68470F4DBA32DD8BF6A_H
#define DEFAULTIOSERVICE_T0BAFDF604B0344628BA3D68470F4DBA32DD8BF6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.FileSystem.DefaultIOService
struct  DefaultIOService_t0BAFDF604B0344628BA3D68470F4DBA32DD8BF6A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTIOSERVICE_T0BAFDF604B0344628BA3D68470F4DBA32DD8BF6A_H
#ifndef U3CU3EC__DISPLAYCLASS55_0_T73216E9A10FD4E95DA16CDD4DFF1924644BD2694_H
#define U3CU3EC__DISPLAYCLASS55_0_T73216E9A10FD4E95DA16CDD4DFF1924644BD2694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.TcpClient.General.TcpClient_<>c__DisplayClass55_0
struct  U3CU3Ec__DisplayClass55_0_t73216E9A10FD4E95DA16CDD4DFF1924644BD2694  : public RuntimeObject
{
public:
	// System.Threading.ManualResetEvent BestHTTP.PlatformSupport.TcpClient.General.TcpClient_<>c__DisplayClass55_0::mre
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___mre_0;

public:
	inline static int32_t get_offset_of_mre_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass55_0_t73216E9A10FD4E95DA16CDD4DFF1924644BD2694, ___mre_0)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_mre_0() const { return ___mre_0; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_mre_0() { return &___mre_0; }
	inline void set_mre_0(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___mre_0 = value;
		Il2CppCodeGenWriteBarrier((&___mre_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS55_0_T73216E9A10FD4E95DA16CDD4DFF1924644BD2694_H
#ifndef U3CU3EC__DISPLAYCLASS58_0_T0F1818FB4BDC3D89ECCC6185DFDCDBA5A462CB45_H
#define U3CU3EC__DISPLAYCLASS58_0_T0F1818FB4BDC3D89ECCC6185DFDCDBA5A462CB45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.TcpClient.General.TcpClient_<>c__DisplayClass58_0
struct  U3CU3Ec__DisplayClass58_0_t0F1818FB4BDC3D89ECCC6185DFDCDBA5A462CB45  : public RuntimeObject
{
public:
	// System.Threading.ManualResetEvent BestHTTP.PlatformSupport.TcpClient.General.TcpClient_<>c__DisplayClass58_0::mre
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___mre_0;

public:
	inline static int32_t get_offset_of_mre_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass58_0_t0F1818FB4BDC3D89ECCC6185DFDCDBA5A462CB45, ___mre_0)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_mre_0() const { return ___mre_0; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_mre_0() { return &___mre_0; }
	inline void set_mre_0(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___mre_0 = value;
		Il2CppCodeGenWriteBarrier((&___mre_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS58_0_T0F1818FB4BDC3D89ECCC6185DFDCDBA5A462CB45_H
#ifndef THREADEDRUNNER_TCB8A3AC54158CC5E982493914F21D804C39B4819_H
#define THREADEDRUNNER_TCB8A3AC54158CC5E982493914F21D804C39B4819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.Threading.ThreadedRunner
struct  ThreadedRunner_tCB8A3AC54158CC5E982493914F21D804C39B4819  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADEDRUNNER_TCB8A3AC54158CC5E982493914F21D804C39B4819_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T84E818B624F332BBC7C4EA6C0F3671FF8F9B8617_H
#define U3CU3EC__DISPLAYCLASS3_0_T84E818B624F332BBC7C4EA6C0F3671FF8F9B8617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.Threading.ThreadedRunner_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t84E818B624F332BBC7C4EA6C0F3671FF8F9B8617  : public RuntimeObject
{
public:
	// System.Action BestHTTP.PlatformSupport.Threading.ThreadedRunner_<>c__DisplayClass3_0::job
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___job_0;

public:
	inline static int32_t get_offset_of_job_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t84E818B624F332BBC7C4EA6C0F3671FF8F9B8617, ___job_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_job_0() const { return ___job_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_job_0() { return &___job_0; }
	inline void set_job_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___job_0 = value;
		Il2CppCodeGenWriteBarrier((&___job_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T84E818B624F332BBC7C4EA6C0F3671FF8F9B8617_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_TD31C8D5F1828F6B02D2B1EC9356CD85669FE4E0A_H
#define U3CU3EC__DISPLAYCLASS4_0_TD31C8D5F1828F6B02D2B1EC9356CD85669FE4E0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.Threading.ThreadedRunner_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tD31C8D5F1828F6B02D2B1EC9356CD85669FE4E0A  : public RuntimeObject
{
public:
	// System.Action BestHTTP.PlatformSupport.Threading.ThreadedRunner_<>c__DisplayClass4_0::job
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___job_0;

public:
	inline static int32_t get_offset_of_job_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tD31C8D5F1828F6B02D2B1EC9356CD85669FE4E0A, ___job_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_job_0() const { return ___job_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_job_0() { return &___job_0; }
	inline void set_job_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___job_0 = value;
		Il2CppCodeGenWriteBarrier((&___job_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_TD31C8D5F1828F6B02D2B1EC9356CD85669FE4E0A_H
#ifndef ANSSINAMEDCURVES_T08E79A91F495D6C67ACACB026701848EE1DC068F_H
#define ANSSINAMEDCURVES_T08E79A91F495D6C67ACACB026701848EE1DC068F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves
struct  AnssiNamedCurves_t08E79A91F495D6C67ACACB026701848EE1DC068F  : public RuntimeObject
{
public:

public:
};

struct AnssiNamedCurves_t08E79A91F495D6C67ACACB026701848EE1DC068F_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves::curves
	RuntimeObject* ___curves_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves::names
	RuntimeObject* ___names_2;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(AnssiNamedCurves_t08E79A91F495D6C67ACACB026701848EE1DC068F_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_curves_1() { return static_cast<int32_t>(offsetof(AnssiNamedCurves_t08E79A91F495D6C67ACACB026701848EE1DC068F_StaticFields, ___curves_1)); }
	inline RuntimeObject* get_curves_1() const { return ___curves_1; }
	inline RuntimeObject** get_address_of_curves_1() { return &___curves_1; }
	inline void set_curves_1(RuntimeObject* value)
	{
		___curves_1 = value;
		Il2CppCodeGenWriteBarrier((&___curves_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(AnssiNamedCurves_t08E79A91F495D6C67ACACB026701848EE1DC068F_StaticFields, ___names_2)); }
	inline RuntimeObject* get_names_2() const { return ___names_2; }
	inline RuntimeObject** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(RuntimeObject* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANSSINAMEDCURVES_T08E79A91F495D6C67ACACB026701848EE1DC068F_H
#ifndef ANSSIOBJECTIDENTIFIERS_TC0B69E7BE4ACE5C0CDE87079A28DB68D3EC55B96_H
#define ANSSIOBJECTIDENTIFIERS_TC0B69E7BE4ACE5C0CDE87079A28DB68D3EC55B96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Anssi.AnssiObjectIdentifiers
struct  AnssiObjectIdentifiers_tC0B69E7BE4ACE5C0CDE87079A28DB68D3EC55B96  : public RuntimeObject
{
public:

public:
};

struct AnssiObjectIdentifiers_tC0B69E7BE4ACE5C0CDE87079A28DB68D3EC55B96_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Anssi.AnssiObjectIdentifiers::FRP256v1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___FRP256v1_0;

public:
	inline static int32_t get_offset_of_FRP256v1_0() { return static_cast<int32_t>(offsetof(AnssiObjectIdentifiers_tC0B69E7BE4ACE5C0CDE87079A28DB68D3EC55B96_StaticFields, ___FRP256v1_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_FRP256v1_0() const { return ___FRP256v1_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_FRP256v1_0() { return &___FRP256v1_0; }
	inline void set_FRP256v1_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___FRP256v1_0 = value;
		Il2CppCodeGenWriteBarrier((&___FRP256v1_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANSSIOBJECTIDENTIFIERS_TC0B69E7BE4ACE5C0CDE87079A28DB68D3EC55B96_H
#ifndef ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#define ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct  Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifndef BCOBJECTIDENTIFIERS_TBD0133B809D97EB246678D2384E81C37B4E0829E_H
#define BCOBJECTIDENTIFIERS_TBD0133B809D97EB246678D2384E81C37B4E0829E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers
struct  BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E  : public RuntimeObject
{
public:

public:
};

struct BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha1_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha256_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha384_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha512_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha224_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha1_pkcs5
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha1_pkcs5_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha1_pkcs12
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha1_pkcs12_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha256_pkcs5
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha256_pkcs5_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha256_pkcs12
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha256_pkcs12_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha1_pkcs12_aes128_cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha1_pkcs12_aes128_cbc_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha1_pkcs12_aes192_cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha1_pkcs12_aes192_cbc_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha1_pkcs12_aes256_cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha1_pkcs12_aes256_cbc_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha256_pkcs12_aes128_cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha256_pkcs12_aes128_cbc_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha256_pkcs12_aes192_cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha256_pkcs12_aes192_cbc_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_pbe_sha256_pkcs12_aes256_cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_pbe_sha256_pkcs12_aes256_cbc_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_sig
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_sig_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::sphincs256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sphincs256_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::sphincs256_with_BLAKE512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sphincs256_with_BLAKE512_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::sphincs256_with_SHA512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sphincs256_with_SHA512_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::sphincs256_with_SHA3_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sphincs256_with_SHA3_512_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::xmss
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___xmss_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::xmss_with_SHA256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___xmss_with_SHA256_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::xmss_with_SHA512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___xmss_with_SHA512_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::xmss_with_SHAKE128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___xmss_with_SHAKE128_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::xmss_with_SHAKE256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___xmss_with_SHAKE256_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::xmss_mt
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___xmss_mt_27;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::xmss_mt_with_SHA256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___xmss_mt_with_SHA256_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::xmss_mt_with_SHA512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___xmss_mt_with_SHA512_29;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::xmss_mt_with_SHAKE128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___xmss_mt_with_SHAKE128_30;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::xmss_mt_with_SHAKE256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___xmss_mt_with_SHAKE256_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_exch
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_exch_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::newHope
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___newHope_33;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::bc_ext
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bc_ext_34;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.BCObjectIdentifiers::linkedCertificate
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___linkedCertificate_35;

public:
	inline static int32_t get_offset_of_bc_0() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_0() const { return ___bc_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_0() { return &___bc_0; }
	inline void set_bc_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_0 = value;
		Il2CppCodeGenWriteBarrier((&___bc_0), value);
	}

	inline static int32_t get_offset_of_bc_pbe_1() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_1() const { return ___bc_pbe_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_1() { return &___bc_pbe_1; }
	inline void set_bc_pbe_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_1 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_1), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha1_2() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha1_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha1_2() const { return ___bc_pbe_sha1_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha1_2() { return &___bc_pbe_sha1_2; }
	inline void set_bc_pbe_sha1_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha1_2 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha1_2), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha256_3() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha256_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha256_3() const { return ___bc_pbe_sha256_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha256_3() { return &___bc_pbe_sha256_3; }
	inline void set_bc_pbe_sha256_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha256_3 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha256_3), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha384_4() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha384_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha384_4() const { return ___bc_pbe_sha384_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha384_4() { return &___bc_pbe_sha384_4; }
	inline void set_bc_pbe_sha384_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha384_4 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha384_4), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha512_5() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha512_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha512_5() const { return ___bc_pbe_sha512_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha512_5() { return &___bc_pbe_sha512_5; }
	inline void set_bc_pbe_sha512_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha512_5 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha512_5), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha224_6() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha224_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha224_6() const { return ___bc_pbe_sha224_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha224_6() { return &___bc_pbe_sha224_6; }
	inline void set_bc_pbe_sha224_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha224_6 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha224_6), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha1_pkcs5_7() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha1_pkcs5_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha1_pkcs5_7() const { return ___bc_pbe_sha1_pkcs5_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha1_pkcs5_7() { return &___bc_pbe_sha1_pkcs5_7; }
	inline void set_bc_pbe_sha1_pkcs5_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha1_pkcs5_7 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha1_pkcs5_7), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha1_pkcs12_8() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha1_pkcs12_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha1_pkcs12_8() const { return ___bc_pbe_sha1_pkcs12_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha1_pkcs12_8() { return &___bc_pbe_sha1_pkcs12_8; }
	inline void set_bc_pbe_sha1_pkcs12_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha1_pkcs12_8 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha1_pkcs12_8), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha256_pkcs5_9() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha256_pkcs5_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha256_pkcs5_9() const { return ___bc_pbe_sha256_pkcs5_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha256_pkcs5_9() { return &___bc_pbe_sha256_pkcs5_9; }
	inline void set_bc_pbe_sha256_pkcs5_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha256_pkcs5_9 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha256_pkcs5_9), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha256_pkcs12_10() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha256_pkcs12_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha256_pkcs12_10() const { return ___bc_pbe_sha256_pkcs12_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha256_pkcs12_10() { return &___bc_pbe_sha256_pkcs12_10; }
	inline void set_bc_pbe_sha256_pkcs12_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha256_pkcs12_10 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha256_pkcs12_10), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha1_pkcs12_aes128_cbc_11() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha1_pkcs12_aes128_cbc_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha1_pkcs12_aes128_cbc_11() const { return ___bc_pbe_sha1_pkcs12_aes128_cbc_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha1_pkcs12_aes128_cbc_11() { return &___bc_pbe_sha1_pkcs12_aes128_cbc_11; }
	inline void set_bc_pbe_sha1_pkcs12_aes128_cbc_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha1_pkcs12_aes128_cbc_11 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha1_pkcs12_aes128_cbc_11), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha1_pkcs12_aes192_cbc_12() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha1_pkcs12_aes192_cbc_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha1_pkcs12_aes192_cbc_12() const { return ___bc_pbe_sha1_pkcs12_aes192_cbc_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha1_pkcs12_aes192_cbc_12() { return &___bc_pbe_sha1_pkcs12_aes192_cbc_12; }
	inline void set_bc_pbe_sha1_pkcs12_aes192_cbc_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha1_pkcs12_aes192_cbc_12 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha1_pkcs12_aes192_cbc_12), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha1_pkcs12_aes256_cbc_13() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha1_pkcs12_aes256_cbc_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha1_pkcs12_aes256_cbc_13() const { return ___bc_pbe_sha1_pkcs12_aes256_cbc_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha1_pkcs12_aes256_cbc_13() { return &___bc_pbe_sha1_pkcs12_aes256_cbc_13; }
	inline void set_bc_pbe_sha1_pkcs12_aes256_cbc_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha1_pkcs12_aes256_cbc_13 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha1_pkcs12_aes256_cbc_13), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha256_pkcs12_aes128_cbc_14() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha256_pkcs12_aes128_cbc_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha256_pkcs12_aes128_cbc_14() const { return ___bc_pbe_sha256_pkcs12_aes128_cbc_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha256_pkcs12_aes128_cbc_14() { return &___bc_pbe_sha256_pkcs12_aes128_cbc_14; }
	inline void set_bc_pbe_sha256_pkcs12_aes128_cbc_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha256_pkcs12_aes128_cbc_14 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha256_pkcs12_aes128_cbc_14), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha256_pkcs12_aes192_cbc_15() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha256_pkcs12_aes192_cbc_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha256_pkcs12_aes192_cbc_15() const { return ___bc_pbe_sha256_pkcs12_aes192_cbc_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha256_pkcs12_aes192_cbc_15() { return &___bc_pbe_sha256_pkcs12_aes192_cbc_15; }
	inline void set_bc_pbe_sha256_pkcs12_aes192_cbc_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha256_pkcs12_aes192_cbc_15 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha256_pkcs12_aes192_cbc_15), value);
	}

	inline static int32_t get_offset_of_bc_pbe_sha256_pkcs12_aes256_cbc_16() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_pbe_sha256_pkcs12_aes256_cbc_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_pbe_sha256_pkcs12_aes256_cbc_16() const { return ___bc_pbe_sha256_pkcs12_aes256_cbc_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_pbe_sha256_pkcs12_aes256_cbc_16() { return &___bc_pbe_sha256_pkcs12_aes256_cbc_16; }
	inline void set_bc_pbe_sha256_pkcs12_aes256_cbc_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_pbe_sha256_pkcs12_aes256_cbc_16 = value;
		Il2CppCodeGenWriteBarrier((&___bc_pbe_sha256_pkcs12_aes256_cbc_16), value);
	}

	inline static int32_t get_offset_of_bc_sig_17() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_sig_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_sig_17() const { return ___bc_sig_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_sig_17() { return &___bc_sig_17; }
	inline void set_bc_sig_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_sig_17 = value;
		Il2CppCodeGenWriteBarrier((&___bc_sig_17), value);
	}

	inline static int32_t get_offset_of_sphincs256_18() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___sphincs256_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sphincs256_18() const { return ___sphincs256_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sphincs256_18() { return &___sphincs256_18; }
	inline void set_sphincs256_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sphincs256_18 = value;
		Il2CppCodeGenWriteBarrier((&___sphincs256_18), value);
	}

	inline static int32_t get_offset_of_sphincs256_with_BLAKE512_19() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___sphincs256_with_BLAKE512_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sphincs256_with_BLAKE512_19() const { return ___sphincs256_with_BLAKE512_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sphincs256_with_BLAKE512_19() { return &___sphincs256_with_BLAKE512_19; }
	inline void set_sphincs256_with_BLAKE512_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sphincs256_with_BLAKE512_19 = value;
		Il2CppCodeGenWriteBarrier((&___sphincs256_with_BLAKE512_19), value);
	}

	inline static int32_t get_offset_of_sphincs256_with_SHA512_20() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___sphincs256_with_SHA512_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sphincs256_with_SHA512_20() const { return ___sphincs256_with_SHA512_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sphincs256_with_SHA512_20() { return &___sphincs256_with_SHA512_20; }
	inline void set_sphincs256_with_SHA512_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sphincs256_with_SHA512_20 = value;
		Il2CppCodeGenWriteBarrier((&___sphincs256_with_SHA512_20), value);
	}

	inline static int32_t get_offset_of_sphincs256_with_SHA3_512_21() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___sphincs256_with_SHA3_512_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sphincs256_with_SHA3_512_21() const { return ___sphincs256_with_SHA3_512_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sphincs256_with_SHA3_512_21() { return &___sphincs256_with_SHA3_512_21; }
	inline void set_sphincs256_with_SHA3_512_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sphincs256_with_SHA3_512_21 = value;
		Il2CppCodeGenWriteBarrier((&___sphincs256_with_SHA3_512_21), value);
	}

	inline static int32_t get_offset_of_xmss_22() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___xmss_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_xmss_22() const { return ___xmss_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_xmss_22() { return &___xmss_22; }
	inline void set_xmss_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___xmss_22 = value;
		Il2CppCodeGenWriteBarrier((&___xmss_22), value);
	}

	inline static int32_t get_offset_of_xmss_with_SHA256_23() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___xmss_with_SHA256_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_xmss_with_SHA256_23() const { return ___xmss_with_SHA256_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_xmss_with_SHA256_23() { return &___xmss_with_SHA256_23; }
	inline void set_xmss_with_SHA256_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___xmss_with_SHA256_23 = value;
		Il2CppCodeGenWriteBarrier((&___xmss_with_SHA256_23), value);
	}

	inline static int32_t get_offset_of_xmss_with_SHA512_24() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___xmss_with_SHA512_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_xmss_with_SHA512_24() const { return ___xmss_with_SHA512_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_xmss_with_SHA512_24() { return &___xmss_with_SHA512_24; }
	inline void set_xmss_with_SHA512_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___xmss_with_SHA512_24 = value;
		Il2CppCodeGenWriteBarrier((&___xmss_with_SHA512_24), value);
	}

	inline static int32_t get_offset_of_xmss_with_SHAKE128_25() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___xmss_with_SHAKE128_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_xmss_with_SHAKE128_25() const { return ___xmss_with_SHAKE128_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_xmss_with_SHAKE128_25() { return &___xmss_with_SHAKE128_25; }
	inline void set_xmss_with_SHAKE128_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___xmss_with_SHAKE128_25 = value;
		Il2CppCodeGenWriteBarrier((&___xmss_with_SHAKE128_25), value);
	}

	inline static int32_t get_offset_of_xmss_with_SHAKE256_26() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___xmss_with_SHAKE256_26)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_xmss_with_SHAKE256_26() const { return ___xmss_with_SHAKE256_26; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_xmss_with_SHAKE256_26() { return &___xmss_with_SHAKE256_26; }
	inline void set_xmss_with_SHAKE256_26(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___xmss_with_SHAKE256_26 = value;
		Il2CppCodeGenWriteBarrier((&___xmss_with_SHAKE256_26), value);
	}

	inline static int32_t get_offset_of_xmss_mt_27() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___xmss_mt_27)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_xmss_mt_27() const { return ___xmss_mt_27; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_xmss_mt_27() { return &___xmss_mt_27; }
	inline void set_xmss_mt_27(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___xmss_mt_27 = value;
		Il2CppCodeGenWriteBarrier((&___xmss_mt_27), value);
	}

	inline static int32_t get_offset_of_xmss_mt_with_SHA256_28() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___xmss_mt_with_SHA256_28)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_xmss_mt_with_SHA256_28() const { return ___xmss_mt_with_SHA256_28; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_xmss_mt_with_SHA256_28() { return &___xmss_mt_with_SHA256_28; }
	inline void set_xmss_mt_with_SHA256_28(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___xmss_mt_with_SHA256_28 = value;
		Il2CppCodeGenWriteBarrier((&___xmss_mt_with_SHA256_28), value);
	}

	inline static int32_t get_offset_of_xmss_mt_with_SHA512_29() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___xmss_mt_with_SHA512_29)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_xmss_mt_with_SHA512_29() const { return ___xmss_mt_with_SHA512_29; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_xmss_mt_with_SHA512_29() { return &___xmss_mt_with_SHA512_29; }
	inline void set_xmss_mt_with_SHA512_29(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___xmss_mt_with_SHA512_29 = value;
		Il2CppCodeGenWriteBarrier((&___xmss_mt_with_SHA512_29), value);
	}

	inline static int32_t get_offset_of_xmss_mt_with_SHAKE128_30() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___xmss_mt_with_SHAKE128_30)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_xmss_mt_with_SHAKE128_30() const { return ___xmss_mt_with_SHAKE128_30; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_xmss_mt_with_SHAKE128_30() { return &___xmss_mt_with_SHAKE128_30; }
	inline void set_xmss_mt_with_SHAKE128_30(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___xmss_mt_with_SHAKE128_30 = value;
		Il2CppCodeGenWriteBarrier((&___xmss_mt_with_SHAKE128_30), value);
	}

	inline static int32_t get_offset_of_xmss_mt_with_SHAKE256_31() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___xmss_mt_with_SHAKE256_31)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_xmss_mt_with_SHAKE256_31() const { return ___xmss_mt_with_SHAKE256_31; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_xmss_mt_with_SHAKE256_31() { return &___xmss_mt_with_SHAKE256_31; }
	inline void set_xmss_mt_with_SHAKE256_31(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___xmss_mt_with_SHAKE256_31 = value;
		Il2CppCodeGenWriteBarrier((&___xmss_mt_with_SHAKE256_31), value);
	}

	inline static int32_t get_offset_of_bc_exch_32() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_exch_32)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_exch_32() const { return ___bc_exch_32; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_exch_32() { return &___bc_exch_32; }
	inline void set_bc_exch_32(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_exch_32 = value;
		Il2CppCodeGenWriteBarrier((&___bc_exch_32), value);
	}

	inline static int32_t get_offset_of_newHope_33() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___newHope_33)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_newHope_33() const { return ___newHope_33; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_newHope_33() { return &___newHope_33; }
	inline void set_newHope_33(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___newHope_33 = value;
		Il2CppCodeGenWriteBarrier((&___newHope_33), value);
	}

	inline static int32_t get_offset_of_bc_ext_34() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___bc_ext_34)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bc_ext_34() const { return ___bc_ext_34; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bc_ext_34() { return &___bc_ext_34; }
	inline void set_bc_ext_34(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bc_ext_34 = value;
		Il2CppCodeGenWriteBarrier((&___bc_ext_34), value);
	}

	inline static int32_t get_offset_of_linkedCertificate_35() { return static_cast<int32_t>(offsetof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields, ___linkedCertificate_35)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_linkedCertificate_35() const { return ___linkedCertificate_35; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_linkedCertificate_35() { return &___linkedCertificate_35; }
	inline void set_linkedCertificate_35(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___linkedCertificate_35 = value;
		Il2CppCodeGenWriteBarrier((&___linkedCertificate_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BCOBJECTIDENTIFIERS_TBD0133B809D97EB246678D2384E81C37B4E0829E_H
#ifndef BSIOBJECTIDENTIFIERS_TAD93234865D9F12245F1DC377E8364C2451E4866_H
#define BSIOBJECTIDENTIFIERS_TAD93234865D9F12245F1DC377E8364C2451E4866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers
struct  BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866  : public RuntimeObject
{
public:

public:
};

struct BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::bsi_de
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bsi_de_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::id_ecc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_ecc_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecdsa_plain_signatures
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecdsa_plain_signatures_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecdsa_plain_SHA1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecdsa_plain_SHA1_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecdsa_plain_SHA224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecdsa_plain_SHA224_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecdsa_plain_SHA256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecdsa_plain_SHA256_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecdsa_plain_SHA384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecdsa_plain_SHA384_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecdsa_plain_SHA512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecdsa_plain_SHA512_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecdsa_plain_RIPEMD160
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecdsa_plain_RIPEMD160_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::algorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___algorithm_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_X963kdf
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_X963kdf_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_X963kdf_SHA1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_X963kdf_SHA1_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_X963kdf_SHA224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_X963kdf_SHA224_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_X963kdf_SHA256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_X963kdf_SHA256_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_X963kdf_SHA384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_X963kdf_SHA384_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_X963kdf_SHA512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_X963kdf_SHA512_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_X963kdf_RIPEMD160
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_X963kdf_RIPEMD160_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_SessionKDF
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_SessionKDF_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_SessionKDF_3DES
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_SessionKDF_3DES_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_SessionKDF_AES128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_SessionKDF_AES128_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_SessionKDF_AES192
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_SessionKDF_AES192_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Bsi.BsiObjectIdentifiers::ecka_eg_SessionKDF_AES256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ecka_eg_SessionKDF_AES256_22;

public:
	inline static int32_t get_offset_of_bsi_de_0() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___bsi_de_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bsi_de_0() const { return ___bsi_de_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bsi_de_0() { return &___bsi_de_0; }
	inline void set_bsi_de_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bsi_de_0 = value;
		Il2CppCodeGenWriteBarrier((&___bsi_de_0), value);
	}

	inline static int32_t get_offset_of_id_ecc_1() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___id_ecc_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_ecc_1() const { return ___id_ecc_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_ecc_1() { return &___id_ecc_1; }
	inline void set_id_ecc_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_ecc_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_ecc_1), value);
	}

	inline static int32_t get_offset_of_ecdsa_plain_signatures_2() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecdsa_plain_signatures_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecdsa_plain_signatures_2() const { return ___ecdsa_plain_signatures_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecdsa_plain_signatures_2() { return &___ecdsa_plain_signatures_2; }
	inline void set_ecdsa_plain_signatures_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecdsa_plain_signatures_2 = value;
		Il2CppCodeGenWriteBarrier((&___ecdsa_plain_signatures_2), value);
	}

	inline static int32_t get_offset_of_ecdsa_plain_SHA1_3() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecdsa_plain_SHA1_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecdsa_plain_SHA1_3() const { return ___ecdsa_plain_SHA1_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecdsa_plain_SHA1_3() { return &___ecdsa_plain_SHA1_3; }
	inline void set_ecdsa_plain_SHA1_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecdsa_plain_SHA1_3 = value;
		Il2CppCodeGenWriteBarrier((&___ecdsa_plain_SHA1_3), value);
	}

	inline static int32_t get_offset_of_ecdsa_plain_SHA224_4() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecdsa_plain_SHA224_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecdsa_plain_SHA224_4() const { return ___ecdsa_plain_SHA224_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecdsa_plain_SHA224_4() { return &___ecdsa_plain_SHA224_4; }
	inline void set_ecdsa_plain_SHA224_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecdsa_plain_SHA224_4 = value;
		Il2CppCodeGenWriteBarrier((&___ecdsa_plain_SHA224_4), value);
	}

	inline static int32_t get_offset_of_ecdsa_plain_SHA256_5() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecdsa_plain_SHA256_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecdsa_plain_SHA256_5() const { return ___ecdsa_plain_SHA256_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecdsa_plain_SHA256_5() { return &___ecdsa_plain_SHA256_5; }
	inline void set_ecdsa_plain_SHA256_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecdsa_plain_SHA256_5 = value;
		Il2CppCodeGenWriteBarrier((&___ecdsa_plain_SHA256_5), value);
	}

	inline static int32_t get_offset_of_ecdsa_plain_SHA384_6() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecdsa_plain_SHA384_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecdsa_plain_SHA384_6() const { return ___ecdsa_plain_SHA384_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecdsa_plain_SHA384_6() { return &___ecdsa_plain_SHA384_6; }
	inline void set_ecdsa_plain_SHA384_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecdsa_plain_SHA384_6 = value;
		Il2CppCodeGenWriteBarrier((&___ecdsa_plain_SHA384_6), value);
	}

	inline static int32_t get_offset_of_ecdsa_plain_SHA512_7() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecdsa_plain_SHA512_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecdsa_plain_SHA512_7() const { return ___ecdsa_plain_SHA512_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecdsa_plain_SHA512_7() { return &___ecdsa_plain_SHA512_7; }
	inline void set_ecdsa_plain_SHA512_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecdsa_plain_SHA512_7 = value;
		Il2CppCodeGenWriteBarrier((&___ecdsa_plain_SHA512_7), value);
	}

	inline static int32_t get_offset_of_ecdsa_plain_RIPEMD160_8() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecdsa_plain_RIPEMD160_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecdsa_plain_RIPEMD160_8() const { return ___ecdsa_plain_RIPEMD160_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecdsa_plain_RIPEMD160_8() { return &___ecdsa_plain_RIPEMD160_8; }
	inline void set_ecdsa_plain_RIPEMD160_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecdsa_plain_RIPEMD160_8 = value;
		Il2CppCodeGenWriteBarrier((&___ecdsa_plain_RIPEMD160_8), value);
	}

	inline static int32_t get_offset_of_algorithm_9() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___algorithm_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_algorithm_9() const { return ___algorithm_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_algorithm_9() { return &___algorithm_9; }
	inline void set_algorithm_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___algorithm_9 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_9), value);
	}

	inline static int32_t get_offset_of_ecka_eg_10() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_10() const { return ___ecka_eg_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_10() { return &___ecka_eg_10; }
	inline void set_ecka_eg_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_10 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_10), value);
	}

	inline static int32_t get_offset_of_ecka_eg_X963kdf_11() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_X963kdf_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_X963kdf_11() const { return ___ecka_eg_X963kdf_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_X963kdf_11() { return &___ecka_eg_X963kdf_11; }
	inline void set_ecka_eg_X963kdf_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_X963kdf_11 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_X963kdf_11), value);
	}

	inline static int32_t get_offset_of_ecka_eg_X963kdf_SHA1_12() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_X963kdf_SHA1_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_X963kdf_SHA1_12() const { return ___ecka_eg_X963kdf_SHA1_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_X963kdf_SHA1_12() { return &___ecka_eg_X963kdf_SHA1_12; }
	inline void set_ecka_eg_X963kdf_SHA1_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_X963kdf_SHA1_12 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_X963kdf_SHA1_12), value);
	}

	inline static int32_t get_offset_of_ecka_eg_X963kdf_SHA224_13() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_X963kdf_SHA224_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_X963kdf_SHA224_13() const { return ___ecka_eg_X963kdf_SHA224_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_X963kdf_SHA224_13() { return &___ecka_eg_X963kdf_SHA224_13; }
	inline void set_ecka_eg_X963kdf_SHA224_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_X963kdf_SHA224_13 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_X963kdf_SHA224_13), value);
	}

	inline static int32_t get_offset_of_ecka_eg_X963kdf_SHA256_14() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_X963kdf_SHA256_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_X963kdf_SHA256_14() const { return ___ecka_eg_X963kdf_SHA256_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_X963kdf_SHA256_14() { return &___ecka_eg_X963kdf_SHA256_14; }
	inline void set_ecka_eg_X963kdf_SHA256_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_X963kdf_SHA256_14 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_X963kdf_SHA256_14), value);
	}

	inline static int32_t get_offset_of_ecka_eg_X963kdf_SHA384_15() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_X963kdf_SHA384_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_X963kdf_SHA384_15() const { return ___ecka_eg_X963kdf_SHA384_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_X963kdf_SHA384_15() { return &___ecka_eg_X963kdf_SHA384_15; }
	inline void set_ecka_eg_X963kdf_SHA384_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_X963kdf_SHA384_15 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_X963kdf_SHA384_15), value);
	}

	inline static int32_t get_offset_of_ecka_eg_X963kdf_SHA512_16() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_X963kdf_SHA512_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_X963kdf_SHA512_16() const { return ___ecka_eg_X963kdf_SHA512_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_X963kdf_SHA512_16() { return &___ecka_eg_X963kdf_SHA512_16; }
	inline void set_ecka_eg_X963kdf_SHA512_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_X963kdf_SHA512_16 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_X963kdf_SHA512_16), value);
	}

	inline static int32_t get_offset_of_ecka_eg_X963kdf_RIPEMD160_17() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_X963kdf_RIPEMD160_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_X963kdf_RIPEMD160_17() const { return ___ecka_eg_X963kdf_RIPEMD160_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_X963kdf_RIPEMD160_17() { return &___ecka_eg_X963kdf_RIPEMD160_17; }
	inline void set_ecka_eg_X963kdf_RIPEMD160_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_X963kdf_RIPEMD160_17 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_X963kdf_RIPEMD160_17), value);
	}

	inline static int32_t get_offset_of_ecka_eg_SessionKDF_18() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_SessionKDF_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_SessionKDF_18() const { return ___ecka_eg_SessionKDF_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_SessionKDF_18() { return &___ecka_eg_SessionKDF_18; }
	inline void set_ecka_eg_SessionKDF_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_SessionKDF_18 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_SessionKDF_18), value);
	}

	inline static int32_t get_offset_of_ecka_eg_SessionKDF_3DES_19() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_SessionKDF_3DES_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_SessionKDF_3DES_19() const { return ___ecka_eg_SessionKDF_3DES_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_SessionKDF_3DES_19() { return &___ecka_eg_SessionKDF_3DES_19; }
	inline void set_ecka_eg_SessionKDF_3DES_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_SessionKDF_3DES_19 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_SessionKDF_3DES_19), value);
	}

	inline static int32_t get_offset_of_ecka_eg_SessionKDF_AES128_20() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_SessionKDF_AES128_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_SessionKDF_AES128_20() const { return ___ecka_eg_SessionKDF_AES128_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_SessionKDF_AES128_20() { return &___ecka_eg_SessionKDF_AES128_20; }
	inline void set_ecka_eg_SessionKDF_AES128_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_SessionKDF_AES128_20 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_SessionKDF_AES128_20), value);
	}

	inline static int32_t get_offset_of_ecka_eg_SessionKDF_AES192_21() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_SessionKDF_AES192_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_SessionKDF_AES192_21() const { return ___ecka_eg_SessionKDF_AES192_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_SessionKDF_AES192_21() { return &___ecka_eg_SessionKDF_AES192_21; }
	inline void set_ecka_eg_SessionKDF_AES192_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_SessionKDF_AES192_21 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_SessionKDF_AES192_21), value);
	}

	inline static int32_t get_offset_of_ecka_eg_SessionKDF_AES256_22() { return static_cast<int32_t>(offsetof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields, ___ecka_eg_SessionKDF_AES256_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ecka_eg_SessionKDF_AES256_22() const { return ___ecka_eg_SessionKDF_AES256_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ecka_eg_SessionKDF_AES256_22() { return &___ecka_eg_SessionKDF_AES256_22; }
	inline void set_ecka_eg_SessionKDF_AES256_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ecka_eg_SessionKDF_AES256_22 = value;
		Il2CppCodeGenWriteBarrier((&___ecka_eg_SessionKDF_AES256_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSIOBJECTIDENTIFIERS_TAD93234865D9F12245F1DC377E8364C2451E4866_H
#ifndef CMPOBJECTIDENTIFIERS_T54E2668FFB58249273B5B953BDFC3ED62DE0B49E_H
#define CMPOBJECTIDENTIFIERS_T54E2668FFB58249273B5B953BDFC3ED62DE0B49E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers
struct  CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E  : public RuntimeObject
{
public:

public:
};

struct CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::passwordBasedMac
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___passwordBasedMac_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::dhBasedMac
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dhBasedMac_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_caProtEncCert
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_caProtEncCert_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_signKeyPairTypes
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_signKeyPairTypes_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_encKeyPairTypes
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_encKeyPairTypes_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_preferredSymAlg
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_preferredSymAlg_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_caKeyUpdateInfo
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_caKeyUpdateInfo_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_currentCRL
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_currentCRL_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_unsupportedOIDs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_unsupportedOIDs_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_keyPairParamReq
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_keyPairParamReq_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_keyPairParamRep
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_keyPairParamRep_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_revPassphrase
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_revPassphrase_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_implicitConfirm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_implicitConfirm_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_confirmWaitTime
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_confirmWaitTime_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_origPKIMessage
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_origPKIMessage_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::it_suppLangTags
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___it_suppLangTags_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::regCtrl_regToken
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___regCtrl_regToken_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::regCtrl_authenticator
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___regCtrl_authenticator_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::regCtrl_pkiPublicationInfo
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___regCtrl_pkiPublicationInfo_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::regCtrl_pkiArchiveOptions
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___regCtrl_pkiArchiveOptions_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::regCtrl_oldCertID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___regCtrl_oldCertID_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::regCtrl_protocolEncrKey
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___regCtrl_protocolEncrKey_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::regCtrl_altCertTemplate
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___regCtrl_altCertTemplate_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::regInfo_utf8Pairs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___regInfo_utf8Pairs_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::regInfo_certReq
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___regInfo_certReq_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpObjectIdentifiers::ct_encKeyWithID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ct_encKeyWithID_25;

public:
	inline static int32_t get_offset_of_passwordBasedMac_0() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___passwordBasedMac_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_passwordBasedMac_0() const { return ___passwordBasedMac_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_passwordBasedMac_0() { return &___passwordBasedMac_0; }
	inline void set_passwordBasedMac_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___passwordBasedMac_0 = value;
		Il2CppCodeGenWriteBarrier((&___passwordBasedMac_0), value);
	}

	inline static int32_t get_offset_of_dhBasedMac_1() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___dhBasedMac_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dhBasedMac_1() const { return ___dhBasedMac_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dhBasedMac_1() { return &___dhBasedMac_1; }
	inline void set_dhBasedMac_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dhBasedMac_1 = value;
		Il2CppCodeGenWriteBarrier((&___dhBasedMac_1), value);
	}

	inline static int32_t get_offset_of_it_caProtEncCert_2() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_caProtEncCert_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_caProtEncCert_2() const { return ___it_caProtEncCert_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_caProtEncCert_2() { return &___it_caProtEncCert_2; }
	inline void set_it_caProtEncCert_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_caProtEncCert_2 = value;
		Il2CppCodeGenWriteBarrier((&___it_caProtEncCert_2), value);
	}

	inline static int32_t get_offset_of_it_signKeyPairTypes_3() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_signKeyPairTypes_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_signKeyPairTypes_3() const { return ___it_signKeyPairTypes_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_signKeyPairTypes_3() { return &___it_signKeyPairTypes_3; }
	inline void set_it_signKeyPairTypes_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_signKeyPairTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___it_signKeyPairTypes_3), value);
	}

	inline static int32_t get_offset_of_it_encKeyPairTypes_4() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_encKeyPairTypes_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_encKeyPairTypes_4() const { return ___it_encKeyPairTypes_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_encKeyPairTypes_4() { return &___it_encKeyPairTypes_4; }
	inline void set_it_encKeyPairTypes_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_encKeyPairTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___it_encKeyPairTypes_4), value);
	}

	inline static int32_t get_offset_of_it_preferredSymAlg_5() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_preferredSymAlg_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_preferredSymAlg_5() const { return ___it_preferredSymAlg_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_preferredSymAlg_5() { return &___it_preferredSymAlg_5; }
	inline void set_it_preferredSymAlg_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_preferredSymAlg_5 = value;
		Il2CppCodeGenWriteBarrier((&___it_preferredSymAlg_5), value);
	}

	inline static int32_t get_offset_of_it_caKeyUpdateInfo_6() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_caKeyUpdateInfo_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_caKeyUpdateInfo_6() const { return ___it_caKeyUpdateInfo_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_caKeyUpdateInfo_6() { return &___it_caKeyUpdateInfo_6; }
	inline void set_it_caKeyUpdateInfo_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_caKeyUpdateInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___it_caKeyUpdateInfo_6), value);
	}

	inline static int32_t get_offset_of_it_currentCRL_7() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_currentCRL_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_currentCRL_7() const { return ___it_currentCRL_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_currentCRL_7() { return &___it_currentCRL_7; }
	inline void set_it_currentCRL_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_currentCRL_7 = value;
		Il2CppCodeGenWriteBarrier((&___it_currentCRL_7), value);
	}

	inline static int32_t get_offset_of_it_unsupportedOIDs_8() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_unsupportedOIDs_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_unsupportedOIDs_8() const { return ___it_unsupportedOIDs_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_unsupportedOIDs_8() { return &___it_unsupportedOIDs_8; }
	inline void set_it_unsupportedOIDs_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_unsupportedOIDs_8 = value;
		Il2CppCodeGenWriteBarrier((&___it_unsupportedOIDs_8), value);
	}

	inline static int32_t get_offset_of_it_keyPairParamReq_9() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_keyPairParamReq_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_keyPairParamReq_9() const { return ___it_keyPairParamReq_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_keyPairParamReq_9() { return &___it_keyPairParamReq_9; }
	inline void set_it_keyPairParamReq_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_keyPairParamReq_9 = value;
		Il2CppCodeGenWriteBarrier((&___it_keyPairParamReq_9), value);
	}

	inline static int32_t get_offset_of_it_keyPairParamRep_10() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_keyPairParamRep_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_keyPairParamRep_10() const { return ___it_keyPairParamRep_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_keyPairParamRep_10() { return &___it_keyPairParamRep_10; }
	inline void set_it_keyPairParamRep_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_keyPairParamRep_10 = value;
		Il2CppCodeGenWriteBarrier((&___it_keyPairParamRep_10), value);
	}

	inline static int32_t get_offset_of_it_revPassphrase_11() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_revPassphrase_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_revPassphrase_11() const { return ___it_revPassphrase_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_revPassphrase_11() { return &___it_revPassphrase_11; }
	inline void set_it_revPassphrase_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_revPassphrase_11 = value;
		Il2CppCodeGenWriteBarrier((&___it_revPassphrase_11), value);
	}

	inline static int32_t get_offset_of_it_implicitConfirm_12() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_implicitConfirm_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_implicitConfirm_12() const { return ___it_implicitConfirm_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_implicitConfirm_12() { return &___it_implicitConfirm_12; }
	inline void set_it_implicitConfirm_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_implicitConfirm_12 = value;
		Il2CppCodeGenWriteBarrier((&___it_implicitConfirm_12), value);
	}

	inline static int32_t get_offset_of_it_confirmWaitTime_13() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_confirmWaitTime_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_confirmWaitTime_13() const { return ___it_confirmWaitTime_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_confirmWaitTime_13() { return &___it_confirmWaitTime_13; }
	inline void set_it_confirmWaitTime_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_confirmWaitTime_13 = value;
		Il2CppCodeGenWriteBarrier((&___it_confirmWaitTime_13), value);
	}

	inline static int32_t get_offset_of_it_origPKIMessage_14() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_origPKIMessage_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_origPKIMessage_14() const { return ___it_origPKIMessage_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_origPKIMessage_14() { return &___it_origPKIMessage_14; }
	inline void set_it_origPKIMessage_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_origPKIMessage_14 = value;
		Il2CppCodeGenWriteBarrier((&___it_origPKIMessage_14), value);
	}

	inline static int32_t get_offset_of_it_suppLangTags_15() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___it_suppLangTags_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_it_suppLangTags_15() const { return ___it_suppLangTags_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_it_suppLangTags_15() { return &___it_suppLangTags_15; }
	inline void set_it_suppLangTags_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___it_suppLangTags_15 = value;
		Il2CppCodeGenWriteBarrier((&___it_suppLangTags_15), value);
	}

	inline static int32_t get_offset_of_regCtrl_regToken_16() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___regCtrl_regToken_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_regCtrl_regToken_16() const { return ___regCtrl_regToken_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_regCtrl_regToken_16() { return &___regCtrl_regToken_16; }
	inline void set_regCtrl_regToken_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___regCtrl_regToken_16 = value;
		Il2CppCodeGenWriteBarrier((&___regCtrl_regToken_16), value);
	}

	inline static int32_t get_offset_of_regCtrl_authenticator_17() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___regCtrl_authenticator_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_regCtrl_authenticator_17() const { return ___regCtrl_authenticator_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_regCtrl_authenticator_17() { return &___regCtrl_authenticator_17; }
	inline void set_regCtrl_authenticator_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___regCtrl_authenticator_17 = value;
		Il2CppCodeGenWriteBarrier((&___regCtrl_authenticator_17), value);
	}

	inline static int32_t get_offset_of_regCtrl_pkiPublicationInfo_18() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___regCtrl_pkiPublicationInfo_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_regCtrl_pkiPublicationInfo_18() const { return ___regCtrl_pkiPublicationInfo_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_regCtrl_pkiPublicationInfo_18() { return &___regCtrl_pkiPublicationInfo_18; }
	inline void set_regCtrl_pkiPublicationInfo_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___regCtrl_pkiPublicationInfo_18 = value;
		Il2CppCodeGenWriteBarrier((&___regCtrl_pkiPublicationInfo_18), value);
	}

	inline static int32_t get_offset_of_regCtrl_pkiArchiveOptions_19() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___regCtrl_pkiArchiveOptions_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_regCtrl_pkiArchiveOptions_19() const { return ___regCtrl_pkiArchiveOptions_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_regCtrl_pkiArchiveOptions_19() { return &___regCtrl_pkiArchiveOptions_19; }
	inline void set_regCtrl_pkiArchiveOptions_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___regCtrl_pkiArchiveOptions_19 = value;
		Il2CppCodeGenWriteBarrier((&___regCtrl_pkiArchiveOptions_19), value);
	}

	inline static int32_t get_offset_of_regCtrl_oldCertID_20() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___regCtrl_oldCertID_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_regCtrl_oldCertID_20() const { return ___regCtrl_oldCertID_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_regCtrl_oldCertID_20() { return &___regCtrl_oldCertID_20; }
	inline void set_regCtrl_oldCertID_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___regCtrl_oldCertID_20 = value;
		Il2CppCodeGenWriteBarrier((&___regCtrl_oldCertID_20), value);
	}

	inline static int32_t get_offset_of_regCtrl_protocolEncrKey_21() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___regCtrl_protocolEncrKey_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_regCtrl_protocolEncrKey_21() const { return ___regCtrl_protocolEncrKey_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_regCtrl_protocolEncrKey_21() { return &___regCtrl_protocolEncrKey_21; }
	inline void set_regCtrl_protocolEncrKey_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___regCtrl_protocolEncrKey_21 = value;
		Il2CppCodeGenWriteBarrier((&___regCtrl_protocolEncrKey_21), value);
	}

	inline static int32_t get_offset_of_regCtrl_altCertTemplate_22() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___regCtrl_altCertTemplate_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_regCtrl_altCertTemplate_22() const { return ___regCtrl_altCertTemplate_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_regCtrl_altCertTemplate_22() { return &___regCtrl_altCertTemplate_22; }
	inline void set_regCtrl_altCertTemplate_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___regCtrl_altCertTemplate_22 = value;
		Il2CppCodeGenWriteBarrier((&___regCtrl_altCertTemplate_22), value);
	}

	inline static int32_t get_offset_of_regInfo_utf8Pairs_23() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___regInfo_utf8Pairs_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_regInfo_utf8Pairs_23() const { return ___regInfo_utf8Pairs_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_regInfo_utf8Pairs_23() { return &___regInfo_utf8Pairs_23; }
	inline void set_regInfo_utf8Pairs_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___regInfo_utf8Pairs_23 = value;
		Il2CppCodeGenWriteBarrier((&___regInfo_utf8Pairs_23), value);
	}

	inline static int32_t get_offset_of_regInfo_certReq_24() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___regInfo_certReq_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_regInfo_certReq_24() const { return ___regInfo_certReq_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_regInfo_certReq_24() { return &___regInfo_certReq_24; }
	inline void set_regInfo_certReq_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___regInfo_certReq_24 = value;
		Il2CppCodeGenWriteBarrier((&___regInfo_certReq_24), value);
	}

	inline static int32_t get_offset_of_ct_encKeyWithID_25() { return static_cast<int32_t>(offsetof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields, ___ct_encKeyWithID_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ct_encKeyWithID_25() const { return ___ct_encKeyWithID_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ct_encKeyWithID_25() { return &___ct_encKeyWithID_25; }
	inline void set_ct_encKeyWithID_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ct_encKeyWithID_25 = value;
		Il2CppCodeGenWriteBarrier((&___ct_encKeyWithID_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMPOBJECTIDENTIFIERS_T54E2668FFB58249273B5B953BDFC3ED62DE0B49E_H
#ifndef PKIHEADERBUILDER_T8416BB313C8738F174DDDA67BFA4C09F4B8798D8_H
#define PKIHEADERBUILDER_T8416BB313C8738F174DDDA67BFA4C09F4B8798D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder
struct  PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::pvno
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___pvno_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::sender
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___sender_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::recipient
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___recipient_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::messageTime
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___messageTime_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::protectionAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___protectionAlg_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::senderKID
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___senderKID_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::recipKID
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___recipKID_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::transactionID
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___transactionID_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::senderNonce
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___senderNonce_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::recipNonce
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___recipNonce_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiFreeText BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::freeText
	PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * ___freeText_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeaderBuilder::generalInfo
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___generalInfo_11;

public:
	inline static int32_t get_offset_of_pvno_0() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___pvno_0)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_pvno_0() const { return ___pvno_0; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_pvno_0() { return &___pvno_0; }
	inline void set_pvno_0(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___pvno_0 = value;
		Il2CppCodeGenWriteBarrier((&___pvno_0), value);
	}

	inline static int32_t get_offset_of_sender_1() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___sender_1)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_sender_1() const { return ___sender_1; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_sender_1() { return &___sender_1; }
	inline void set_sender_1(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___sender_1 = value;
		Il2CppCodeGenWriteBarrier((&___sender_1), value);
	}

	inline static int32_t get_offset_of_recipient_2() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___recipient_2)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_recipient_2() const { return ___recipient_2; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_recipient_2() { return &___recipient_2; }
	inline void set_recipient_2(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___recipient_2 = value;
		Il2CppCodeGenWriteBarrier((&___recipient_2), value);
	}

	inline static int32_t get_offset_of_messageTime_3() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___messageTime_3)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_messageTime_3() const { return ___messageTime_3; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_messageTime_3() { return &___messageTime_3; }
	inline void set_messageTime_3(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___messageTime_3 = value;
		Il2CppCodeGenWriteBarrier((&___messageTime_3), value);
	}

	inline static int32_t get_offset_of_protectionAlg_4() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___protectionAlg_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_protectionAlg_4() const { return ___protectionAlg_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_protectionAlg_4() { return &___protectionAlg_4; }
	inline void set_protectionAlg_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___protectionAlg_4 = value;
		Il2CppCodeGenWriteBarrier((&___protectionAlg_4), value);
	}

	inline static int32_t get_offset_of_senderKID_5() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___senderKID_5)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_senderKID_5() const { return ___senderKID_5; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_senderKID_5() { return &___senderKID_5; }
	inline void set_senderKID_5(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___senderKID_5 = value;
		Il2CppCodeGenWriteBarrier((&___senderKID_5), value);
	}

	inline static int32_t get_offset_of_recipKID_6() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___recipKID_6)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_recipKID_6() const { return ___recipKID_6; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_recipKID_6() { return &___recipKID_6; }
	inline void set_recipKID_6(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___recipKID_6 = value;
		Il2CppCodeGenWriteBarrier((&___recipKID_6), value);
	}

	inline static int32_t get_offset_of_transactionID_7() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___transactionID_7)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_transactionID_7() const { return ___transactionID_7; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_transactionID_7() { return &___transactionID_7; }
	inline void set_transactionID_7(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___transactionID_7 = value;
		Il2CppCodeGenWriteBarrier((&___transactionID_7), value);
	}

	inline static int32_t get_offset_of_senderNonce_8() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___senderNonce_8)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_senderNonce_8() const { return ___senderNonce_8; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_senderNonce_8() { return &___senderNonce_8; }
	inline void set_senderNonce_8(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___senderNonce_8 = value;
		Il2CppCodeGenWriteBarrier((&___senderNonce_8), value);
	}

	inline static int32_t get_offset_of_recipNonce_9() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___recipNonce_9)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_recipNonce_9() const { return ___recipNonce_9; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_recipNonce_9() { return &___recipNonce_9; }
	inline void set_recipNonce_9(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___recipNonce_9 = value;
		Il2CppCodeGenWriteBarrier((&___recipNonce_9), value);
	}

	inline static int32_t get_offset_of_freeText_10() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___freeText_10)); }
	inline PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * get_freeText_10() const { return ___freeText_10; }
	inline PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D ** get_address_of_freeText_10() { return &___freeText_10; }
	inline void set_freeText_10(PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * value)
	{
		___freeText_10 = value;
		Il2CppCodeGenWriteBarrier((&___freeText_10), value);
	}

	inline static int32_t get_offset_of_generalInfo_11() { return static_cast<int32_t>(offsetof(PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8, ___generalInfo_11)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_generalInfo_11() const { return ___generalInfo_11; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_generalInfo_11() { return &___generalInfo_11; }
	inline void set_generalInfo_11(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___generalInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___generalInfo_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIHEADERBUILDER_T8416BB313C8738F174DDDA67BFA4C09F4B8798D8_H
#ifndef REVREPCONTENTBUILDER_T97214484D6610706A1842C4B40187E605E7D7955_H
#define REVREPCONTENTBUILDER_T97214484D6610706A1842C4B40187E605E7D7955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevRepContentBuilder
struct  RevRepContentBuilder_t97214484D6610706A1842C4B40187E605E7D7955  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevRepContentBuilder::status
	Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * ___status_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevRepContentBuilder::revCerts
	Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * ___revCerts_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevRepContentBuilder::crls
	Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * ___crls_2;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(RevRepContentBuilder_t97214484D6610706A1842C4B40187E605E7D7955, ___status_0)); }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * get_status_0() const { return ___status_0; }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 ** get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * value)
	{
		___status_0 = value;
		Il2CppCodeGenWriteBarrier((&___status_0), value);
	}

	inline static int32_t get_offset_of_revCerts_1() { return static_cast<int32_t>(offsetof(RevRepContentBuilder_t97214484D6610706A1842C4B40187E605E7D7955, ___revCerts_1)); }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * get_revCerts_1() const { return ___revCerts_1; }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 ** get_address_of_revCerts_1() { return &___revCerts_1; }
	inline void set_revCerts_1(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * value)
	{
		___revCerts_1 = value;
		Il2CppCodeGenWriteBarrier((&___revCerts_1), value);
	}

	inline static int32_t get_offset_of_crls_2() { return static_cast<int32_t>(offsetof(RevRepContentBuilder_t97214484D6610706A1842C4B40187E605E7D7955, ___crls_2)); }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * get_crls_2() const { return ___crls_2; }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 ** get_address_of_crls_2() { return &___crls_2; }
	inline void set_crls_2(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * value)
	{
		___crls_2 = value;
		Il2CppCodeGenWriteBarrier((&___crls_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVREPCONTENTBUILDER_T97214484D6610706A1842C4B40187E605E7D7955_H
#ifndef SIGNEDDATAPARSER_T6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8_H
#define SIGNEDDATAPARSER_T6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedDataParser
struct  SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1SequenceParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedDataParser::_seq
	RuntimeObject* ____seq_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedDataParser::_version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ____version_1;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedDataParser::_nextObject
	RuntimeObject * ____nextObject_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedDataParser::_certsCalled
	bool ____certsCalled_3;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedDataParser::_crlsCalled
	bool ____crlsCalled_4;

public:
	inline static int32_t get_offset_of__seq_0() { return static_cast<int32_t>(offsetof(SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8, ____seq_0)); }
	inline RuntimeObject* get__seq_0() const { return ____seq_0; }
	inline RuntimeObject** get_address_of__seq_0() { return &____seq_0; }
	inline void set__seq_0(RuntimeObject* value)
	{
		____seq_0 = value;
		Il2CppCodeGenWriteBarrier((&____seq_0), value);
	}

	inline static int32_t get_offset_of__version_1() { return static_cast<int32_t>(offsetof(SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8, ____version_1)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get__version_1() const { return ____version_1; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of__version_1() { return &____version_1; }
	inline void set__version_1(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		____version_1 = value;
		Il2CppCodeGenWriteBarrier((&____version_1), value);
	}

	inline static int32_t get_offset_of__nextObject_2() { return static_cast<int32_t>(offsetof(SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8, ____nextObject_2)); }
	inline RuntimeObject * get__nextObject_2() const { return ____nextObject_2; }
	inline RuntimeObject ** get_address_of__nextObject_2() { return &____nextObject_2; }
	inline void set__nextObject_2(RuntimeObject * value)
	{
		____nextObject_2 = value;
		Il2CppCodeGenWriteBarrier((&____nextObject_2), value);
	}

	inline static int32_t get_offset_of__certsCalled_3() { return static_cast<int32_t>(offsetof(SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8, ____certsCalled_3)); }
	inline bool get__certsCalled_3() const { return ____certsCalled_3; }
	inline bool* get_address_of__certsCalled_3() { return &____certsCalled_3; }
	inline void set__certsCalled_3(bool value)
	{
		____certsCalled_3 = value;
	}

	inline static int32_t get_offset_of__crlsCalled_4() { return static_cast<int32_t>(offsetof(SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8, ____crlsCalled_4)); }
	inline bool get__crlsCalled_4() const { return ____crlsCalled_4; }
	inline bool* get_address_of__crlsCalled_4() { return &____crlsCalled_4; }
	inline void set__crlsCalled_4(bool value)
	{
		____crlsCalled_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNEDDATAPARSER_T6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8_H
#ifndef TIMESTAMPEDDATAPARSER_T855878AAD476054421C2BB066B0143910DDED62A_H
#define TIMESTAMPEDDATAPARSER_T855878AAD476054421C2BB066B0143910DDED62A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedDataParser
struct  TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedDataParser::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedDataParser::dataUri
	DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * ___dataUri_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.MetaData BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedDataParser::metaData
	MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A * ___metaData_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetStringParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedDataParser::content
	RuntimeObject* ___content_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Evidence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedDataParser::temporalEvidence
	Evidence_t6897C267406A516689941A0FF07964FF20B44F07 * ___temporalEvidence_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1SequenceParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedDataParser::parser
	RuntimeObject* ___parser_5;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A, ___version_0)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_0() const { return ___version_0; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier((&___version_0), value);
	}

	inline static int32_t get_offset_of_dataUri_1() { return static_cast<int32_t>(offsetof(TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A, ___dataUri_1)); }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * get_dataUri_1() const { return ___dataUri_1; }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE ** get_address_of_dataUri_1() { return &___dataUri_1; }
	inline void set_dataUri_1(DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * value)
	{
		___dataUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataUri_1), value);
	}

	inline static int32_t get_offset_of_metaData_2() { return static_cast<int32_t>(offsetof(TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A, ___metaData_2)); }
	inline MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A * get_metaData_2() const { return ___metaData_2; }
	inline MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A ** get_address_of_metaData_2() { return &___metaData_2; }
	inline void set_metaData_2(MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A * value)
	{
		___metaData_2 = value;
		Il2CppCodeGenWriteBarrier((&___metaData_2), value);
	}

	inline static int32_t get_offset_of_content_3() { return static_cast<int32_t>(offsetof(TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A, ___content_3)); }
	inline RuntimeObject* get_content_3() const { return ___content_3; }
	inline RuntimeObject** get_address_of_content_3() { return &___content_3; }
	inline void set_content_3(RuntimeObject* value)
	{
		___content_3 = value;
		Il2CppCodeGenWriteBarrier((&___content_3), value);
	}

	inline static int32_t get_offset_of_temporalEvidence_4() { return static_cast<int32_t>(offsetof(TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A, ___temporalEvidence_4)); }
	inline Evidence_t6897C267406A516689941A0FF07964FF20B44F07 * get_temporalEvidence_4() const { return ___temporalEvidence_4; }
	inline Evidence_t6897C267406A516689941A0FF07964FF20B44F07 ** get_address_of_temporalEvidence_4() { return &___temporalEvidence_4; }
	inline void set_temporalEvidence_4(Evidence_t6897C267406A516689941A0FF07964FF20B44F07 * value)
	{
		___temporalEvidence_4 = value;
		Il2CppCodeGenWriteBarrier((&___temporalEvidence_4), value);
	}

	inline static int32_t get_offset_of_parser_5() { return static_cast<int32_t>(offsetof(TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A, ___parser_5)); }
	inline RuntimeObject* get_parser_5() const { return ___parser_5; }
	inline RuntimeObject** get_address_of_parser_5() { return &___parser_5; }
	inline void set_parser_5(RuntimeObject* value)
	{
		___parser_5 = value;
		Il2CppCodeGenWriteBarrier((&___parser_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPEDDATAPARSER_T855878AAD476054421C2BB066B0143910DDED62A_H
#ifndef X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#define X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct  X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder::parameters
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * ___parameters_0;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB, ___parameters_0)); }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * get_parameters_0() const { return ___parameters_0; }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef HTTPMULTIPARTFORM_T16DCB285C2F3FB6094BE80C048DE85F47798F0F7_H
#define HTTPMULTIPARTFORM_T16DCB285C2F3FB6094BE80C048DE85F47798F0F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Forms.HTTPMultiPartForm
struct  HTTPMultiPartForm_t16DCB285C2F3FB6094BE80C048DE85F47798F0F7  : public HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21
{
public:
	// System.String BestHTTP.Forms.HTTPMultiPartForm::Boundary
	String_t* ___Boundary_5;
	// System.Byte[] BestHTTP.Forms.HTTPMultiPartForm::CachedData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___CachedData_6;

public:
	inline static int32_t get_offset_of_Boundary_5() { return static_cast<int32_t>(offsetof(HTTPMultiPartForm_t16DCB285C2F3FB6094BE80C048DE85F47798F0F7, ___Boundary_5)); }
	inline String_t* get_Boundary_5() const { return ___Boundary_5; }
	inline String_t** get_address_of_Boundary_5() { return &___Boundary_5; }
	inline void set_Boundary_5(String_t* value)
	{
		___Boundary_5 = value;
		Il2CppCodeGenWriteBarrier((&___Boundary_5), value);
	}

	inline static int32_t get_offset_of_CachedData_6() { return static_cast<int32_t>(offsetof(HTTPMultiPartForm_t16DCB285C2F3FB6094BE80C048DE85F47798F0F7, ___CachedData_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_CachedData_6() const { return ___CachedData_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_CachedData_6() { return &___CachedData_6; }
	inline void set_CachedData_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___CachedData_6 = value;
		Il2CppCodeGenWriteBarrier((&___CachedData_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMULTIPARTFORM_T16DCB285C2F3FB6094BE80C048DE85F47798F0F7_H
#ifndef HTTPURLENCODEDFORM_TD9FF747E110F1C48313F8FC20A6F843F2F587610_H
#define HTTPURLENCODEDFORM_TD9FF747E110F1C48313F8FC20A6F843F2F587610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Forms.HTTPUrlEncodedForm
struct  HTTPUrlEncodedForm_tD9FF747E110F1C48313F8FC20A6F843F2F587610  : public HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21
{
public:
	// System.Byte[] BestHTTP.Forms.HTTPUrlEncodedForm::CachedData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___CachedData_6;

public:
	inline static int32_t get_offset_of_CachedData_6() { return static_cast<int32_t>(offsetof(HTTPUrlEncodedForm_tD9FF747E110F1C48313F8FC20A6F843F2F587610, ___CachedData_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_CachedData_6() const { return ___CachedData_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_CachedData_6() { return &___CachedData_6; }
	inline void set_CachedData_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___CachedData_6 = value;
		Il2CppCodeGenWriteBarrier((&___CachedData_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPURLENCODEDFORM_TD9FF747E110F1C48313F8FC20A6F843F2F587610_H
#ifndef BUFFERSEGMENT_TC5D6E916DD1242E574031AB1CCFC629E3498907F_H
#define BUFFERSEGMENT_TC5D6E916DD1242E574031AB1CCFC629E3498907F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.Memory.BufferSegment
struct  BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F 
{
public:
	// System.Byte[] BestHTTP.PlatformSupport.Memory.BufferSegment::Data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Data_1;
	// System.Int32 BestHTTP.PlatformSupport.Memory.BufferSegment::Offset
	int32_t ___Offset_2;
	// System.Int32 BestHTTP.PlatformSupport.Memory.BufferSegment::Count
	int32_t ___Count_3;

public:
	inline static int32_t get_offset_of_Data_1() { return static_cast<int32_t>(offsetof(BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F, ___Data_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Data_1() const { return ___Data_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Data_1() { return &___Data_1; }
	inline void set_Data_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Data_1 = value;
		Il2CppCodeGenWriteBarrier((&___Data_1), value);
	}

	inline static int32_t get_offset_of_Offset_2() { return static_cast<int32_t>(offsetof(BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F, ___Offset_2)); }
	inline int32_t get_Offset_2() const { return ___Offset_2; }
	inline int32_t* get_address_of_Offset_2() { return &___Offset_2; }
	inline void set_Offset_2(int32_t value)
	{
		___Offset_2 = value;
	}

	inline static int32_t get_offset_of_Count_3() { return static_cast<int32_t>(offsetof(BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F, ___Count_3)); }
	inline int32_t get_Count_3() const { return ___Count_3; }
	inline int32_t* get_address_of_Count_3() { return &___Count_3; }
	inline void set_Count_3(int32_t value)
	{
		___Count_3 = value;
	}
};

struct BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F_StaticFields
{
public:
	// BestHTTP.PlatformSupport.Memory.BufferSegment BestHTTP.PlatformSupport.Memory.BufferSegment::Empty
	BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F  ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F_StaticFields, ___Empty_0)); }
	inline BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F  get_Empty_0() const { return ___Empty_0; }
	inline BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F  value)
	{
		___Empty_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERSEGMENT_TC5D6E916DD1242E574031AB1CCFC629E3498907F_H
#ifndef BUFFERSTORE_TB32CE48D79640ACF5CE5F804348EF77C6CDC1584_H
#define BUFFERSTORE_TB32CE48D79640ACF5CE5F804348EF77C6CDC1584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.Memory.BufferStore
struct  BufferStore_tB32CE48D79640ACF5CE5F804348EF77C6CDC1584 
{
public:
	// System.Int64 BestHTTP.PlatformSupport.Memory.BufferStore::Size
	int64_t ___Size_0;
	// System.Collections.Generic.List`1<BestHTTP.PlatformSupport.Memory.BufferDesc> BestHTTP.PlatformSupport.Memory.BufferStore::buffers
	List_1_t8EC08E7C8DC1966BECAA901BF0B9F2CA9CCA6153 * ___buffers_1;

public:
	inline static int32_t get_offset_of_Size_0() { return static_cast<int32_t>(offsetof(BufferStore_tB32CE48D79640ACF5CE5F804348EF77C6CDC1584, ___Size_0)); }
	inline int64_t get_Size_0() const { return ___Size_0; }
	inline int64_t* get_address_of_Size_0() { return &___Size_0; }
	inline void set_Size_0(int64_t value)
	{
		___Size_0 = value;
	}

	inline static int32_t get_offset_of_buffers_1() { return static_cast<int32_t>(offsetof(BufferStore_tB32CE48D79640ACF5CE5F804348EF77C6CDC1584, ___buffers_1)); }
	inline List_1_t8EC08E7C8DC1966BECAA901BF0B9F2CA9CCA6153 * get_buffers_1() const { return ___buffers_1; }
	inline List_1_t8EC08E7C8DC1966BECAA901BF0B9F2CA9CCA6153 ** get_address_of_buffers_1() { return &___buffers_1; }
	inline void set_buffers_1(List_1_t8EC08E7C8DC1966BECAA901BF0B9F2CA9CCA6153 * value)
	{
		___buffers_1 = value;
		Il2CppCodeGenWriteBarrier((&___buffers_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.PlatformSupport.Memory.BufferStore
struct BufferStore_tB32CE48D79640ACF5CE5F804348EF77C6CDC1584_marshaled_pinvoke
{
	int64_t ___Size_0;
	List_1_t8EC08E7C8DC1966BECAA901BF0B9F2CA9CCA6153 * ___buffers_1;
};
// Native definition for COM marshalling of BestHTTP.PlatformSupport.Memory.BufferStore
struct BufferStore_tB32CE48D79640ACF5CE5F804348EF77C6CDC1584_marshaled_com
{
	int64_t ___Size_0;
	List_1_t8EC08E7C8DC1966BECAA901BF0B9F2CA9CCA6153 * ___buffers_1;
};
#endif // BUFFERSTORE_TB32CE48D79640ACF5CE5F804348EF77C6CDC1584_H
#ifndef POOLEDBUFFER_T70463B01A10C07C0778C64900896B87AF9A0DB22_H
#define POOLEDBUFFER_T70463B01A10C07C0778C64900896B87AF9A0DB22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.Memory.PooledBuffer
struct  PooledBuffer_t70463B01A10C07C0778C64900896B87AF9A0DB22 
{
public:
	// System.Byte[] BestHTTP.PlatformSupport.Memory.PooledBuffer::Data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Data_0;
	// System.Int32 BestHTTP.PlatformSupport.Memory.PooledBuffer::Length
	int32_t ___Length_1;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(PooledBuffer_t70463B01A10C07C0778C64900896B87AF9A0DB22, ___Data_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Data_0() const { return ___Data_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier((&___Data_0), value);
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(PooledBuffer_t70463B01A10C07C0778C64900896B87AF9A0DB22, ___Length_1)); }
	inline int32_t get_Length_1() const { return ___Length_1; }
	inline int32_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int32_t value)
	{
		___Length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEDBUFFER_T70463B01A10C07C0778C64900896B87AF9A0DB22_H
#ifndef FRP256V1HOLDER_T483F89477F3215B64DBF54FF7F76E1993A856CEA_H
#define FRP256V1HOLDER_T483F89477F3215B64DBF54FF7F76E1993A856CEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves_Frp256v1Holder
struct  Frp256v1Holder_t483F89477F3215B64DBF54FF7F76E1993A856CEA  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Frp256v1Holder_t483F89477F3215B64DBF54FF7F76E1993A856CEA_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves_Frp256v1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Frp256v1Holder_t483F89477F3215B64DBF54FF7F76E1993A856CEA_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRP256V1HOLDER_T483F89477F3215B64DBF54FF7F76E1993A856CEA_H
#ifndef ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#define ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct  Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifndef LINKEDCERTIFICATE_TFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B_H
#define LINKEDCERTIFICATE_TFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.LinkedCertificate
struct  LinkedCertificate_tFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DigestInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.LinkedCertificate::mDigest
	DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70 * ___mDigest_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.LinkedCertificate::mCertLocation
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___mCertLocation_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.LinkedCertificate::mCertIssuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___mCertIssuer_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BC.LinkedCertificate::mCACerts
	GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * ___mCACerts_5;

public:
	inline static int32_t get_offset_of_mDigest_2() { return static_cast<int32_t>(offsetof(LinkedCertificate_tFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B, ___mDigest_2)); }
	inline DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70 * get_mDigest_2() const { return ___mDigest_2; }
	inline DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70 ** get_address_of_mDigest_2() { return &___mDigest_2; }
	inline void set_mDigest_2(DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70 * value)
	{
		___mDigest_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDigest_2), value);
	}

	inline static int32_t get_offset_of_mCertLocation_3() { return static_cast<int32_t>(offsetof(LinkedCertificate_tFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B, ___mCertLocation_3)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_mCertLocation_3() const { return ___mCertLocation_3; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_mCertLocation_3() { return &___mCertLocation_3; }
	inline void set_mCertLocation_3(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___mCertLocation_3 = value;
		Il2CppCodeGenWriteBarrier((&___mCertLocation_3), value);
	}

	inline static int32_t get_offset_of_mCertIssuer_4() { return static_cast<int32_t>(offsetof(LinkedCertificate_tFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B, ___mCertIssuer_4)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_mCertIssuer_4() const { return ___mCertIssuer_4; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_mCertIssuer_4() { return &___mCertIssuer_4; }
	inline void set_mCertIssuer_4(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___mCertIssuer_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCertIssuer_4), value);
	}

	inline static int32_t get_offset_of_mCACerts_5() { return static_cast<int32_t>(offsetof(LinkedCertificate_tFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B, ___mCACerts_5)); }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * get_mCACerts_5() const { return ___mCACerts_5; }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD ** get_address_of_mCACerts_5() { return &___mCACerts_5; }
	inline void set_mCACerts_5(GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * value)
	{
		___mCACerts_5 = value;
		Il2CppCodeGenWriteBarrier((&___mCACerts_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDCERTIFICATE_TFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B_H
#ifndef CAKEYUPDANNCONTENT_T84841B75E28B88F78419072E0DBC00026CA23D49_H
#define CAKEYUPDANNCONTENT_T84841B75E28B88F78419072E0DBC00026CA23D49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CAKeyUpdAnnContent
struct  CAKeyUpdAnnContent_t84841B75E28B88F78419072E0DBC00026CA23D49  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpCertificate BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CAKeyUpdAnnContent::oldWithNew
	CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * ___oldWithNew_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpCertificate BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CAKeyUpdAnnContent::newWithOld
	CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * ___newWithOld_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpCertificate BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CAKeyUpdAnnContent::newWithNew
	CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * ___newWithNew_4;

public:
	inline static int32_t get_offset_of_oldWithNew_2() { return static_cast<int32_t>(offsetof(CAKeyUpdAnnContent_t84841B75E28B88F78419072E0DBC00026CA23D49, ___oldWithNew_2)); }
	inline CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * get_oldWithNew_2() const { return ___oldWithNew_2; }
	inline CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 ** get_address_of_oldWithNew_2() { return &___oldWithNew_2; }
	inline void set_oldWithNew_2(CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * value)
	{
		___oldWithNew_2 = value;
		Il2CppCodeGenWriteBarrier((&___oldWithNew_2), value);
	}

	inline static int32_t get_offset_of_newWithOld_3() { return static_cast<int32_t>(offsetof(CAKeyUpdAnnContent_t84841B75E28B88F78419072E0DBC00026CA23D49, ___newWithOld_3)); }
	inline CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * get_newWithOld_3() const { return ___newWithOld_3; }
	inline CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 ** get_address_of_newWithOld_3() { return &___newWithOld_3; }
	inline void set_newWithOld_3(CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * value)
	{
		___newWithOld_3 = value;
		Il2CppCodeGenWriteBarrier((&___newWithOld_3), value);
	}

	inline static int32_t get_offset_of_newWithNew_4() { return static_cast<int32_t>(offsetof(CAKeyUpdAnnContent_t84841B75E28B88F78419072E0DBC00026CA23D49, ___newWithNew_4)); }
	inline CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * get_newWithNew_4() const { return ___newWithNew_4; }
	inline CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 ** get_address_of_newWithNew_4() { return &___newWithNew_4; }
	inline void set_newWithNew_4(CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * value)
	{
		___newWithNew_4 = value;
		Il2CppCodeGenWriteBarrier((&___newWithNew_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAKEYUPDANNCONTENT_T84841B75E28B88F78419072E0DBC00026CA23D49_H
#ifndef CERTCONFIRMCONTENT_T393863F2F49912FCB5E9CCE73327E2AB411C2B83_H
#define CERTCONFIRMCONTENT_T393863F2F49912FCB5E9CCE73327E2AB411C2B83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertConfirmContent
struct  CertConfirmContent_t393863F2F49912FCB5E9CCE73327E2AB411C2B83  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertConfirmContent::content
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___content_2;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(CertConfirmContent_t393863F2F49912FCB5E9CCE73327E2AB411C2B83, ___content_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_content_2() const { return ___content_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTCONFIRMCONTENT_T393863F2F49912FCB5E9CCE73327E2AB411C2B83_H
#ifndef CERTORENCCERT_T7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA_H
#define CERTORENCCERT_T7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertOrEncCert
struct  CertOrEncCert_t7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpCertificate BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertOrEncCert::certificate
	CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * ___certificate_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertOrEncCert::encryptedCert
	EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 * ___encryptedCert_3;

public:
	inline static int32_t get_offset_of_certificate_2() { return static_cast<int32_t>(offsetof(CertOrEncCert_t7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA, ___certificate_2)); }
	inline CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * get_certificate_2() const { return ___certificate_2; }
	inline CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 ** get_address_of_certificate_2() { return &___certificate_2; }
	inline void set_certificate_2(CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * value)
	{
		___certificate_2 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_2), value);
	}

	inline static int32_t get_offset_of_encryptedCert_3() { return static_cast<int32_t>(offsetof(CertOrEncCert_t7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA, ___encryptedCert_3)); }
	inline EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 * get_encryptedCert_3() const { return ___encryptedCert_3; }
	inline EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 ** get_address_of_encryptedCert_3() { return &___encryptedCert_3; }
	inline void set_encryptedCert_3(EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 * value)
	{
		___encryptedCert_3 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedCert_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTORENCCERT_T7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA_H
#ifndef CERTREPMESSAGE_T49FE21416545959EAF1AFA328D06E40071A10D1E_H
#define CERTREPMESSAGE_T49FE21416545959EAF1AFA328D06E40071A10D1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertRepMessage
struct  CertRepMessage_t49FE21416545959EAF1AFA328D06E40071A10D1E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertRepMessage::caPubs
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___caPubs_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertRepMessage::response
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___response_3;

public:
	inline static int32_t get_offset_of_caPubs_2() { return static_cast<int32_t>(offsetof(CertRepMessage_t49FE21416545959EAF1AFA328D06E40071A10D1E, ___caPubs_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_caPubs_2() const { return ___caPubs_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_caPubs_2() { return &___caPubs_2; }
	inline void set_caPubs_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___caPubs_2 = value;
		Il2CppCodeGenWriteBarrier((&___caPubs_2), value);
	}

	inline static int32_t get_offset_of_response_3() { return static_cast<int32_t>(offsetof(CertRepMessage_t49FE21416545959EAF1AFA328D06E40071A10D1E, ___response_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_response_3() const { return ___response_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_response_3() { return &___response_3; }
	inline void set_response_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___response_3 = value;
		Il2CppCodeGenWriteBarrier((&___response_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTREPMESSAGE_T49FE21416545959EAF1AFA328D06E40071A10D1E_H
#ifndef CERTRESPONSE_T31494006AAC46DF20936319DB715202F398DCCFE_H
#define CERTRESPONSE_T31494006AAC46DF20936319DB715202F398DCCFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertResponse
struct  CertResponse_t31494006AAC46DF20936319DB715202F398DCCFE  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertResponse::certReqId
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___certReqId_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertResponse::status
	PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * ___status_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertifiedKeyPair BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertResponse::certifiedKeyPair
	CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5 * ___certifiedKeyPair_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertResponse::rspInfo
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___rspInfo_5;

public:
	inline static int32_t get_offset_of_certReqId_2() { return static_cast<int32_t>(offsetof(CertResponse_t31494006AAC46DF20936319DB715202F398DCCFE, ___certReqId_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_certReqId_2() const { return ___certReqId_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_certReqId_2() { return &___certReqId_2; }
	inline void set_certReqId_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___certReqId_2 = value;
		Il2CppCodeGenWriteBarrier((&___certReqId_2), value);
	}

	inline static int32_t get_offset_of_status_3() { return static_cast<int32_t>(offsetof(CertResponse_t31494006AAC46DF20936319DB715202F398DCCFE, ___status_3)); }
	inline PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * get_status_3() const { return ___status_3; }
	inline PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E ** get_address_of_status_3() { return &___status_3; }
	inline void set_status_3(PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * value)
	{
		___status_3 = value;
		Il2CppCodeGenWriteBarrier((&___status_3), value);
	}

	inline static int32_t get_offset_of_certifiedKeyPair_4() { return static_cast<int32_t>(offsetof(CertResponse_t31494006AAC46DF20936319DB715202F398DCCFE, ___certifiedKeyPair_4)); }
	inline CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5 * get_certifiedKeyPair_4() const { return ___certifiedKeyPair_4; }
	inline CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5 ** get_address_of_certifiedKeyPair_4() { return &___certifiedKeyPair_4; }
	inline void set_certifiedKeyPair_4(CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5 * value)
	{
		___certifiedKeyPair_4 = value;
		Il2CppCodeGenWriteBarrier((&___certifiedKeyPair_4), value);
	}

	inline static int32_t get_offset_of_rspInfo_5() { return static_cast<int32_t>(offsetof(CertResponse_t31494006AAC46DF20936319DB715202F398DCCFE, ___rspInfo_5)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_rspInfo_5() const { return ___rspInfo_5; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_rspInfo_5() { return &___rspInfo_5; }
	inline void set_rspInfo_5(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___rspInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___rspInfo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTRESPONSE_T31494006AAC46DF20936319DB715202F398DCCFE_H
#ifndef CERTSTATUS_TE3FE27FF381A20391E4472652A50F600BA0DD5D4_H
#define CERTSTATUS_TE3FE27FF381A20391E4472652A50F600BA0DD5D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertStatus
struct  CertStatus_tE3FE27FF381A20391E4472652A50F600BA0DD5D4  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertStatus::certHash
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___certHash_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertStatus::certReqId
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___certReqId_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertStatus::statusInfo
	PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * ___statusInfo_4;

public:
	inline static int32_t get_offset_of_certHash_2() { return static_cast<int32_t>(offsetof(CertStatus_tE3FE27FF381A20391E4472652A50F600BA0DD5D4, ___certHash_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_certHash_2() const { return ___certHash_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_certHash_2() { return &___certHash_2; }
	inline void set_certHash_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___certHash_2 = value;
		Il2CppCodeGenWriteBarrier((&___certHash_2), value);
	}

	inline static int32_t get_offset_of_certReqId_3() { return static_cast<int32_t>(offsetof(CertStatus_tE3FE27FF381A20391E4472652A50F600BA0DD5D4, ___certReqId_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_certReqId_3() const { return ___certReqId_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_certReqId_3() { return &___certReqId_3; }
	inline void set_certReqId_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___certReqId_3 = value;
		Il2CppCodeGenWriteBarrier((&___certReqId_3), value);
	}

	inline static int32_t get_offset_of_statusInfo_4() { return static_cast<int32_t>(offsetof(CertStatus_tE3FE27FF381A20391E4472652A50F600BA0DD5D4, ___statusInfo_4)); }
	inline PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * get_statusInfo_4() const { return ___statusInfo_4; }
	inline PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E ** get_address_of_statusInfo_4() { return &___statusInfo_4; }
	inline void set_statusInfo_4(PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * value)
	{
		___statusInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___statusInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTSTATUS_TE3FE27FF381A20391E4472652A50F600BA0DD5D4_H
#ifndef CERTIFIEDKEYPAIR_T7C343C382CF9004893101A8C0A6C85A61B56FCB5_H
#define CERTIFIEDKEYPAIR_T7C343C382CF9004893101A8C0A6C85A61B56FCB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertifiedKeyPair
struct  CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertOrEncCert BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertifiedKeyPair::certOrEncCert
	CertOrEncCert_t7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA * ___certOrEncCert_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertifiedKeyPair::privateKey
	EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 * ___privateKey_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PkiPublicationInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CertifiedKeyPair::publicationInfo
	PkiPublicationInfo_t61EE698B09F3C95DFE0520755D3F4FA2CB059950 * ___publicationInfo_4;

public:
	inline static int32_t get_offset_of_certOrEncCert_2() { return static_cast<int32_t>(offsetof(CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5, ___certOrEncCert_2)); }
	inline CertOrEncCert_t7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA * get_certOrEncCert_2() const { return ___certOrEncCert_2; }
	inline CertOrEncCert_t7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA ** get_address_of_certOrEncCert_2() { return &___certOrEncCert_2; }
	inline void set_certOrEncCert_2(CertOrEncCert_t7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA * value)
	{
		___certOrEncCert_2 = value;
		Il2CppCodeGenWriteBarrier((&___certOrEncCert_2), value);
	}

	inline static int32_t get_offset_of_privateKey_3() { return static_cast<int32_t>(offsetof(CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5, ___privateKey_3)); }
	inline EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 * get_privateKey_3() const { return ___privateKey_3; }
	inline EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 ** get_address_of_privateKey_3() { return &___privateKey_3; }
	inline void set_privateKey_3(EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 * value)
	{
		___privateKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___privateKey_3), value);
	}

	inline static int32_t get_offset_of_publicationInfo_4() { return static_cast<int32_t>(offsetof(CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5, ___publicationInfo_4)); }
	inline PkiPublicationInfo_t61EE698B09F3C95DFE0520755D3F4FA2CB059950 * get_publicationInfo_4() const { return ___publicationInfo_4; }
	inline PkiPublicationInfo_t61EE698B09F3C95DFE0520755D3F4FA2CB059950 ** get_address_of_publicationInfo_4() { return &___publicationInfo_4; }
	inline void set_publicationInfo_4(PkiPublicationInfo_t61EE698B09F3C95DFE0520755D3F4FA2CB059950 * value)
	{
		___publicationInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___publicationInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFIEDKEYPAIR_T7C343C382CF9004893101A8C0A6C85A61B56FCB5_H
#ifndef CHALLENGE_TFF1EEA473061F8BB04DB89F4A6DE63A677C87593_H
#define CHALLENGE_TFF1EEA473061F8BB04DB89F4A6DE63A677C87593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.Challenge
struct  Challenge_tFF1EEA473061F8BB04DB89F4A6DE63A677C87593  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.Challenge::owf
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___owf_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.Challenge::witness
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___witness_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.Challenge::challenge
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___challenge_4;

public:
	inline static int32_t get_offset_of_owf_2() { return static_cast<int32_t>(offsetof(Challenge_tFF1EEA473061F8BB04DB89F4A6DE63A677C87593, ___owf_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_owf_2() const { return ___owf_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_owf_2() { return &___owf_2; }
	inline void set_owf_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___owf_2 = value;
		Il2CppCodeGenWriteBarrier((&___owf_2), value);
	}

	inline static int32_t get_offset_of_witness_3() { return static_cast<int32_t>(offsetof(Challenge_tFF1EEA473061F8BB04DB89F4A6DE63A677C87593, ___witness_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_witness_3() const { return ___witness_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_witness_3() { return &___witness_3; }
	inline void set_witness_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___witness_3 = value;
		Il2CppCodeGenWriteBarrier((&___witness_3), value);
	}

	inline static int32_t get_offset_of_challenge_4() { return static_cast<int32_t>(offsetof(Challenge_tFF1EEA473061F8BB04DB89F4A6DE63A677C87593, ___challenge_4)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_challenge_4() const { return ___challenge_4; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_challenge_4() { return &___challenge_4; }
	inline void set_challenge_4(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___challenge_4 = value;
		Il2CppCodeGenWriteBarrier((&___challenge_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGE_TFF1EEA473061F8BB04DB89F4A6DE63A677C87593_H
#ifndef CMPCERTIFICATE_T567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22_H
#define CMPCERTIFICATE_T567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpCertificate
struct  CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpCertificate::x509v3PKCert
	X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * ___x509v3PKCert_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificate BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpCertificate::x509v2AttrCert
	AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B * ___x509v2AttrCert_3;

public:
	inline static int32_t get_offset_of_x509v3PKCert_2() { return static_cast<int32_t>(offsetof(CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22, ___x509v3PKCert_2)); }
	inline X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * get_x509v3PKCert_2() const { return ___x509v3PKCert_2; }
	inline X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 ** get_address_of_x509v3PKCert_2() { return &___x509v3PKCert_2; }
	inline void set_x509v3PKCert_2(X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * value)
	{
		___x509v3PKCert_2 = value;
		Il2CppCodeGenWriteBarrier((&___x509v3PKCert_2), value);
	}

	inline static int32_t get_offset_of_x509v2AttrCert_3() { return static_cast<int32_t>(offsetof(CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22, ___x509v2AttrCert_3)); }
	inline AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B * get_x509v2AttrCert_3() const { return ___x509v2AttrCert_3; }
	inline AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B ** get_address_of_x509v2AttrCert_3() { return &___x509v2AttrCert_3; }
	inline void set_x509v2AttrCert_3(AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B * value)
	{
		___x509v2AttrCert_3 = value;
		Il2CppCodeGenWriteBarrier((&___x509v2AttrCert_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMPCERTIFICATE_T567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22_H
#ifndef CRLANNCONTENT_TD607DF865E9C64F1B8A6537D9E673E06C11DC14F_H
#define CRLANNCONTENT_TD607DF865E9C64F1B8A6537D9E673E06C11DC14F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CrlAnnContent
struct  CrlAnnContent_tD607DF865E9C64F1B8A6537D9E673E06C11DC14F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CrlAnnContent::content
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___content_2;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(CrlAnnContent_tD607DF865E9C64F1B8A6537D9E673E06C11DC14F, ___content_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_content_2() const { return ___content_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRLANNCONTENT_TD607DF865E9C64F1B8A6537D9E673E06C11DC14F_H
#ifndef ERRORMSGCONTENT_TC6EA36BA879EDB7CA0A2275F14D4F7297393A91C_H
#define ERRORMSGCONTENT_TC6EA36BA879EDB7CA0A2275F14D4F7297393A91C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.ErrorMsgContent
struct  ErrorMsgContent_tC6EA36BA879EDB7CA0A2275F14D4F7297393A91C  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.ErrorMsgContent::pkiStatusInfo
	PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * ___pkiStatusInfo_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.ErrorMsgContent::errorCode
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___errorCode_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiFreeText BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.ErrorMsgContent::errorDetails
	PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * ___errorDetails_4;

public:
	inline static int32_t get_offset_of_pkiStatusInfo_2() { return static_cast<int32_t>(offsetof(ErrorMsgContent_tC6EA36BA879EDB7CA0A2275F14D4F7297393A91C, ___pkiStatusInfo_2)); }
	inline PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * get_pkiStatusInfo_2() const { return ___pkiStatusInfo_2; }
	inline PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E ** get_address_of_pkiStatusInfo_2() { return &___pkiStatusInfo_2; }
	inline void set_pkiStatusInfo_2(PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * value)
	{
		___pkiStatusInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___pkiStatusInfo_2), value);
	}

	inline static int32_t get_offset_of_errorCode_3() { return static_cast<int32_t>(offsetof(ErrorMsgContent_tC6EA36BA879EDB7CA0A2275F14D4F7297393A91C, ___errorCode_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_errorCode_3() const { return ___errorCode_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_errorCode_3() { return &___errorCode_3; }
	inline void set_errorCode_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___errorCode_3 = value;
		Il2CppCodeGenWriteBarrier((&___errorCode_3), value);
	}

	inline static int32_t get_offset_of_errorDetails_4() { return static_cast<int32_t>(offsetof(ErrorMsgContent_tC6EA36BA879EDB7CA0A2275F14D4F7297393A91C, ___errorDetails_4)); }
	inline PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * get_errorDetails_4() const { return ___errorDetails_4; }
	inline PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D ** get_address_of_errorDetails_4() { return &___errorDetails_4; }
	inline void set_errorDetails_4(PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * value)
	{
		___errorDetails_4 = value;
		Il2CppCodeGenWriteBarrier((&___errorDetails_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORMSGCONTENT_TC6EA36BA879EDB7CA0A2275F14D4F7297393A91C_H
#ifndef GENMSGCONTENT_T0110D8F8FF6DAFA63073FA855C76FD33401AF10F_H
#define GENMSGCONTENT_T0110D8F8FF6DAFA63073FA855C76FD33401AF10F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.GenMsgContent
struct  GenMsgContent_t0110D8F8FF6DAFA63073FA855C76FD33401AF10F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.GenMsgContent::content
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___content_2;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(GenMsgContent_t0110D8F8FF6DAFA63073FA855C76FD33401AF10F, ___content_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_content_2() const { return ___content_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENMSGCONTENT_T0110D8F8FF6DAFA63073FA855C76FD33401AF10F_H
#ifndef GENREPCONTENT_T9FB18397725BD7F51EEE4777E6BAC30F10650D3A_H
#define GENREPCONTENT_T9FB18397725BD7F51EEE4777E6BAC30F10650D3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.GenRepContent
struct  GenRepContent_t9FB18397725BD7F51EEE4777E6BAC30F10650D3A  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.GenRepContent::content
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___content_2;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(GenRepContent_t9FB18397725BD7F51EEE4777E6BAC30F10650D3A, ___content_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_content_2() const { return ___content_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENREPCONTENT_T9FB18397725BD7F51EEE4777E6BAC30F10650D3A_H
#ifndef INFOTYPEANDVALUE_T04D46F4997E012F7FE48522DA9D6982295A95649_H
#define INFOTYPEANDVALUE_T04D46F4997E012F7FE48522DA9D6982295A95649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.InfoTypeAndValue
struct  InfoTypeAndValue_t04D46F4997E012F7FE48522DA9D6982295A95649  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.InfoTypeAndValue::infoType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___infoType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.InfoTypeAndValue::infoValue
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___infoValue_3;

public:
	inline static int32_t get_offset_of_infoType_2() { return static_cast<int32_t>(offsetof(InfoTypeAndValue_t04D46F4997E012F7FE48522DA9D6982295A95649, ___infoType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_infoType_2() const { return ___infoType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_infoType_2() { return &___infoType_2; }
	inline void set_infoType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___infoType_2 = value;
		Il2CppCodeGenWriteBarrier((&___infoType_2), value);
	}

	inline static int32_t get_offset_of_infoValue_3() { return static_cast<int32_t>(offsetof(InfoTypeAndValue_t04D46F4997E012F7FE48522DA9D6982295A95649, ___infoValue_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_infoValue_3() const { return ___infoValue_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_infoValue_3() { return &___infoValue_3; }
	inline void set_infoValue_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___infoValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___infoValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFOTYPEANDVALUE_T04D46F4997E012F7FE48522DA9D6982295A95649_H
#ifndef KEYRECREPCONTENT_TC6C4A2CDECDC0149A0DE96FECB95A518AC937625_H
#define KEYRECREPCONTENT_TC6C4A2CDECDC0149A0DE96FECB95A518AC937625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.KeyRecRepContent
struct  KeyRecRepContent_tC6C4A2CDECDC0149A0DE96FECB95A518AC937625  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.KeyRecRepContent::status
	PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * ___status_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.CmpCertificate BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.KeyRecRepContent::newSigCert
	CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * ___newSigCert_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.KeyRecRepContent::caCerts
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___caCerts_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.KeyRecRepContent::keyPairHist
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___keyPairHist_5;

public:
	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(KeyRecRepContent_tC6C4A2CDECDC0149A0DE96FECB95A518AC937625, ___status_2)); }
	inline PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * get_status_2() const { return ___status_2; }
	inline PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E ** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier((&___status_2), value);
	}

	inline static int32_t get_offset_of_newSigCert_3() { return static_cast<int32_t>(offsetof(KeyRecRepContent_tC6C4A2CDECDC0149A0DE96FECB95A518AC937625, ___newSigCert_3)); }
	inline CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * get_newSigCert_3() const { return ___newSigCert_3; }
	inline CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 ** get_address_of_newSigCert_3() { return &___newSigCert_3; }
	inline void set_newSigCert_3(CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22 * value)
	{
		___newSigCert_3 = value;
		Il2CppCodeGenWriteBarrier((&___newSigCert_3), value);
	}

	inline static int32_t get_offset_of_caCerts_4() { return static_cast<int32_t>(offsetof(KeyRecRepContent_tC6C4A2CDECDC0149A0DE96FECB95A518AC937625, ___caCerts_4)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_caCerts_4() const { return ___caCerts_4; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_caCerts_4() { return &___caCerts_4; }
	inline void set_caCerts_4(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___caCerts_4 = value;
		Il2CppCodeGenWriteBarrier((&___caCerts_4), value);
	}

	inline static int32_t get_offset_of_keyPairHist_5() { return static_cast<int32_t>(offsetof(KeyRecRepContent_tC6C4A2CDECDC0149A0DE96FECB95A518AC937625, ___keyPairHist_5)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_keyPairHist_5() const { return ___keyPairHist_5; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_keyPairHist_5() { return &___keyPairHist_5; }
	inline void set_keyPairHist_5(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___keyPairHist_5 = value;
		Il2CppCodeGenWriteBarrier((&___keyPairHist_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYRECREPCONTENT_TC6C4A2CDECDC0149A0DE96FECB95A518AC937625_H
#ifndef OOBCERTHASH_TA91F72629862ED56AB6071A519F8E1F8B4755F02_H
#define OOBCERTHASH_TA91F72629862ED56AB6071A519F8E1F8B4755F02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.OobCertHash
struct  OobCertHash_tA91F72629862ED56AB6071A519F8E1F8B4755F02  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.OobCertHash::hashAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___hashAlg_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertId BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.OobCertHash::certId
	CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D * ___certId_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.OobCertHash::hashVal
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___hashVal_4;

public:
	inline static int32_t get_offset_of_hashAlg_2() { return static_cast<int32_t>(offsetof(OobCertHash_tA91F72629862ED56AB6071A519F8E1F8B4755F02, ___hashAlg_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_hashAlg_2() const { return ___hashAlg_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_hashAlg_2() { return &___hashAlg_2; }
	inline void set_hashAlg_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___hashAlg_2 = value;
		Il2CppCodeGenWriteBarrier((&___hashAlg_2), value);
	}

	inline static int32_t get_offset_of_certId_3() { return static_cast<int32_t>(offsetof(OobCertHash_tA91F72629862ED56AB6071A519F8E1F8B4755F02, ___certId_3)); }
	inline CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D * get_certId_3() const { return ___certId_3; }
	inline CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D ** get_address_of_certId_3() { return &___certId_3; }
	inline void set_certId_3(CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D * value)
	{
		___certId_3 = value;
		Il2CppCodeGenWriteBarrier((&___certId_3), value);
	}

	inline static int32_t get_offset_of_hashVal_4() { return static_cast<int32_t>(offsetof(OobCertHash_tA91F72629862ED56AB6071A519F8E1F8B4755F02, ___hashVal_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_hashVal_4() const { return ___hashVal_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_hashVal_4() { return &___hashVal_4; }
	inline void set_hashVal_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___hashVal_4 = value;
		Il2CppCodeGenWriteBarrier((&___hashVal_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OOBCERTHASH_TA91F72629862ED56AB6071A519F8E1F8B4755F02_H
#ifndef PBMPARAMETER_T9BBB8795E15A93183BF2DC11B992309A7383C504_H
#define PBMPARAMETER_T9BBB8795E15A93183BF2DC11B992309A7383C504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PbmParameter
struct  PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PbmParameter::salt
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___salt_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PbmParameter::owf
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___owf_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PbmParameter::iterationCount
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___iterationCount_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PbmParameter::mac
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___mac_5;

public:
	inline static int32_t get_offset_of_salt_2() { return static_cast<int32_t>(offsetof(PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504, ___salt_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_salt_2() const { return ___salt_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_salt_2() { return &___salt_2; }
	inline void set_salt_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___salt_2 = value;
		Il2CppCodeGenWriteBarrier((&___salt_2), value);
	}

	inline static int32_t get_offset_of_owf_3() { return static_cast<int32_t>(offsetof(PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504, ___owf_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_owf_3() const { return ___owf_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_owf_3() { return &___owf_3; }
	inline void set_owf_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___owf_3 = value;
		Il2CppCodeGenWriteBarrier((&___owf_3), value);
	}

	inline static int32_t get_offset_of_iterationCount_4() { return static_cast<int32_t>(offsetof(PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504, ___iterationCount_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_iterationCount_4() const { return ___iterationCount_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_iterationCount_4() { return &___iterationCount_4; }
	inline void set_iterationCount_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___iterationCount_4 = value;
		Il2CppCodeGenWriteBarrier((&___iterationCount_4), value);
	}

	inline static int32_t get_offset_of_mac_5() { return static_cast<int32_t>(offsetof(PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504, ___mac_5)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_mac_5() const { return ___mac_5; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_mac_5() { return &___mac_5; }
	inline void set_mac_5(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___mac_5 = value;
		Il2CppCodeGenWriteBarrier((&___mac_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBMPARAMETER_T9BBB8795E15A93183BF2DC11B992309A7383C504_H
#ifndef PKIBODY_T8876624FD206FAE66D313ECB96C4D17DBE291A89_H
#define PKIBODY_T8876624FD206FAE66D313ECB96C4D17DBE291A89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiBody
struct  PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiBody::tagNo
	int32_t ___tagNo_29;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiBody::body
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___body_30;

public:
	inline static int32_t get_offset_of_tagNo_29() { return static_cast<int32_t>(offsetof(PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89, ___tagNo_29)); }
	inline int32_t get_tagNo_29() const { return ___tagNo_29; }
	inline int32_t* get_address_of_tagNo_29() { return &___tagNo_29; }
	inline void set_tagNo_29(int32_t value)
	{
		___tagNo_29 = value;
	}

	inline static int32_t get_offset_of_body_30() { return static_cast<int32_t>(offsetof(PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89, ___body_30)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_body_30() const { return ___body_30; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_body_30() { return &___body_30; }
	inline void set_body_30(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___body_30 = value;
		Il2CppCodeGenWriteBarrier((&___body_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIBODY_T8876624FD206FAE66D313ECB96C4D17DBE291A89_H
#ifndef PKICONFIRMCONTENT_T55926636670DC657E33D2E40C3B30A7C0196F46E_H
#define PKICONFIRMCONTENT_T55926636670DC657E33D2E40C3B30A7C0196F46E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiConfirmContent
struct  PkiConfirmContent_t55926636670DC657E33D2E40C3B30A7C0196F46E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKICONFIRMCONTENT_T55926636670DC657E33D2E40C3B30A7C0196F46E_H
#ifndef PKIFREETEXT_TBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D_H
#define PKIFREETEXT_TBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiFreeText
struct  PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiFreeText::strings
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___strings_2;

public:
	inline static int32_t get_offset_of_strings_2() { return static_cast<int32_t>(offsetof(PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D, ___strings_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_strings_2() const { return ___strings_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_strings_2() { return &___strings_2; }
	inline void set_strings_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___strings_2 = value;
		Il2CppCodeGenWriteBarrier((&___strings_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIFREETEXT_TBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D_H
#ifndef PKIHEADER_T3A26AFDB95FB16095589D59348E238CBF38821D0_H
#define PKIHEADER_T3A26AFDB95FB16095589D59348E238CBF38821D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader
struct  PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::pvno
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___pvno_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::sender
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___sender_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::recipient
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___recipient_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::messageTime
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___messageTime_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::protectionAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___protectionAlg_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::senderKID
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___senderKID_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::recipKID
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___recipKID_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::transactionID
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___transactionID_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::senderNonce
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___senderNonce_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::recipNonce
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___recipNonce_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiFreeText BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::freeText
	PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * ___freeText_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::generalInfo
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___generalInfo_16;

public:
	inline static int32_t get_offset_of_pvno_5() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___pvno_5)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_pvno_5() const { return ___pvno_5; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_pvno_5() { return &___pvno_5; }
	inline void set_pvno_5(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___pvno_5 = value;
		Il2CppCodeGenWriteBarrier((&___pvno_5), value);
	}

	inline static int32_t get_offset_of_sender_6() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___sender_6)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_sender_6() const { return ___sender_6; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_sender_6() { return &___sender_6; }
	inline void set_sender_6(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___sender_6 = value;
		Il2CppCodeGenWriteBarrier((&___sender_6), value);
	}

	inline static int32_t get_offset_of_recipient_7() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___recipient_7)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_recipient_7() const { return ___recipient_7; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_recipient_7() { return &___recipient_7; }
	inline void set_recipient_7(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___recipient_7 = value;
		Il2CppCodeGenWriteBarrier((&___recipient_7), value);
	}

	inline static int32_t get_offset_of_messageTime_8() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___messageTime_8)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_messageTime_8() const { return ___messageTime_8; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_messageTime_8() { return &___messageTime_8; }
	inline void set_messageTime_8(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___messageTime_8 = value;
		Il2CppCodeGenWriteBarrier((&___messageTime_8), value);
	}

	inline static int32_t get_offset_of_protectionAlg_9() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___protectionAlg_9)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_protectionAlg_9() const { return ___protectionAlg_9; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_protectionAlg_9() { return &___protectionAlg_9; }
	inline void set_protectionAlg_9(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___protectionAlg_9 = value;
		Il2CppCodeGenWriteBarrier((&___protectionAlg_9), value);
	}

	inline static int32_t get_offset_of_senderKID_10() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___senderKID_10)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_senderKID_10() const { return ___senderKID_10; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_senderKID_10() { return &___senderKID_10; }
	inline void set_senderKID_10(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___senderKID_10 = value;
		Il2CppCodeGenWriteBarrier((&___senderKID_10), value);
	}

	inline static int32_t get_offset_of_recipKID_11() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___recipKID_11)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_recipKID_11() const { return ___recipKID_11; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_recipKID_11() { return &___recipKID_11; }
	inline void set_recipKID_11(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___recipKID_11 = value;
		Il2CppCodeGenWriteBarrier((&___recipKID_11), value);
	}

	inline static int32_t get_offset_of_transactionID_12() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___transactionID_12)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_transactionID_12() const { return ___transactionID_12; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_transactionID_12() { return &___transactionID_12; }
	inline void set_transactionID_12(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___transactionID_12 = value;
		Il2CppCodeGenWriteBarrier((&___transactionID_12), value);
	}

	inline static int32_t get_offset_of_senderNonce_13() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___senderNonce_13)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_senderNonce_13() const { return ___senderNonce_13; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_senderNonce_13() { return &___senderNonce_13; }
	inline void set_senderNonce_13(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___senderNonce_13 = value;
		Il2CppCodeGenWriteBarrier((&___senderNonce_13), value);
	}

	inline static int32_t get_offset_of_recipNonce_14() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___recipNonce_14)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_recipNonce_14() const { return ___recipNonce_14; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_recipNonce_14() { return &___recipNonce_14; }
	inline void set_recipNonce_14(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___recipNonce_14 = value;
		Il2CppCodeGenWriteBarrier((&___recipNonce_14), value);
	}

	inline static int32_t get_offset_of_freeText_15() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___freeText_15)); }
	inline PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * get_freeText_15() const { return ___freeText_15; }
	inline PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D ** get_address_of_freeText_15() { return &___freeText_15; }
	inline void set_freeText_15(PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * value)
	{
		___freeText_15 = value;
		Il2CppCodeGenWriteBarrier((&___freeText_15), value);
	}

	inline static int32_t get_offset_of_generalInfo_16() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0, ___generalInfo_16)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_generalInfo_16() const { return ___generalInfo_16; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_generalInfo_16() { return &___generalInfo_16; }
	inline void set_generalInfo_16(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___generalInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___generalInfo_16), value);
	}
};

struct PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::NULL_NAME
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___NULL_NAME_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::CMP_1999
	int32_t ___CMP_1999_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader::CMP_2000
	int32_t ___CMP_2000_4;

public:
	inline static int32_t get_offset_of_NULL_NAME_2() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0_StaticFields, ___NULL_NAME_2)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_NULL_NAME_2() const { return ___NULL_NAME_2; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_NULL_NAME_2() { return &___NULL_NAME_2; }
	inline void set_NULL_NAME_2(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___NULL_NAME_2 = value;
		Il2CppCodeGenWriteBarrier((&___NULL_NAME_2), value);
	}

	inline static int32_t get_offset_of_CMP_1999_3() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0_StaticFields, ___CMP_1999_3)); }
	inline int32_t get_CMP_1999_3() const { return ___CMP_1999_3; }
	inline int32_t* get_address_of_CMP_1999_3() { return &___CMP_1999_3; }
	inline void set_CMP_1999_3(int32_t value)
	{
		___CMP_1999_3 = value;
	}

	inline static int32_t get_offset_of_CMP_2000_4() { return static_cast<int32_t>(offsetof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0_StaticFields, ___CMP_2000_4)); }
	inline int32_t get_CMP_2000_4() const { return ___CMP_2000_4; }
	inline int32_t* get_address_of_CMP_2000_4() { return &___CMP_2000_4; }
	inline void set_CMP_2000_4(int32_t value)
	{
		___CMP_2000_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIHEADER_T3A26AFDB95FB16095589D59348E238CBF38821D0_H
#ifndef PKIMESSAGE_T967F8D2C19DAC941842C357A75B16F0DE8E146F4_H
#define PKIMESSAGE_T967F8D2C19DAC941842C357A75B16F0DE8E146F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiMessage
struct  PkiMessage_t967F8D2C19DAC941842C357A75B16F0DE8E146F4  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiMessage::header
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0 * ___header_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiBody BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiMessage::body
	PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89 * ___body_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiMessage::protection
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___protection_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiMessage::extraCerts
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___extraCerts_5;

public:
	inline static int32_t get_offset_of_header_2() { return static_cast<int32_t>(offsetof(PkiMessage_t967F8D2C19DAC941842C357A75B16F0DE8E146F4, ___header_2)); }
	inline PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0 * get_header_2() const { return ___header_2; }
	inline PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0 ** get_address_of_header_2() { return &___header_2; }
	inline void set_header_2(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0 * value)
	{
		___header_2 = value;
		Il2CppCodeGenWriteBarrier((&___header_2), value);
	}

	inline static int32_t get_offset_of_body_3() { return static_cast<int32_t>(offsetof(PkiMessage_t967F8D2C19DAC941842C357A75B16F0DE8E146F4, ___body_3)); }
	inline PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89 * get_body_3() const { return ___body_3; }
	inline PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89 ** get_address_of_body_3() { return &___body_3; }
	inline void set_body_3(PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89 * value)
	{
		___body_3 = value;
		Il2CppCodeGenWriteBarrier((&___body_3), value);
	}

	inline static int32_t get_offset_of_protection_4() { return static_cast<int32_t>(offsetof(PkiMessage_t967F8D2C19DAC941842C357A75B16F0DE8E146F4, ___protection_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_protection_4() const { return ___protection_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_protection_4() { return &___protection_4; }
	inline void set_protection_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___protection_4 = value;
		Il2CppCodeGenWriteBarrier((&___protection_4), value);
	}

	inline static int32_t get_offset_of_extraCerts_5() { return static_cast<int32_t>(offsetof(PkiMessage_t967F8D2C19DAC941842C357A75B16F0DE8E146F4, ___extraCerts_5)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_extraCerts_5() const { return ___extraCerts_5; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_extraCerts_5() { return &___extraCerts_5; }
	inline void set_extraCerts_5(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___extraCerts_5 = value;
		Il2CppCodeGenWriteBarrier((&___extraCerts_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIMESSAGE_T967F8D2C19DAC941842C357A75B16F0DE8E146F4_H
#ifndef PKIMESSAGES_TABE46E886E9C1119F780D72A50406787A5EAF216_H
#define PKIMESSAGES_TABE46E886E9C1119F780D72A50406787A5EAF216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiMessages
struct  PkiMessages_tABE46E886E9C1119F780D72A50406787A5EAF216  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiMessages::content
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___content_2;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(PkiMessages_tABE46E886E9C1119F780D72A50406787A5EAF216, ___content_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_content_2() const { return ___content_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIMESSAGES_TABE46E886E9C1119F780D72A50406787A5EAF216_H
#ifndef PKISTATUSENCODABLE_T91EFDE2752AE3A66F2F5B002BE800E8478FF1100_H
#define PKISTATUSENCODABLE_T91EFDE2752AE3A66F2F5B002BE800E8478FF1100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable
struct  PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable::status
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___status_9;

public:
	inline static int32_t get_offset_of_status_9() { return static_cast<int32_t>(offsetof(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100, ___status_9)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_status_9() const { return ___status_9; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_status_9() { return &___status_9; }
	inline void set_status_9(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___status_9 = value;
		Il2CppCodeGenWriteBarrier((&___status_9), value);
	}
};

struct PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable::granted
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * ___granted_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable::grantedWithMods
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * ___grantedWithMods_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable::rejection
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * ___rejection_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable::waiting
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * ___waiting_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable::revocationWarning
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * ___revocationWarning_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable::revocationNotification
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * ___revocationNotification_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable::keyUpdateWaiting
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * ___keyUpdateWaiting_8;

public:
	inline static int32_t get_offset_of_granted_2() { return static_cast<int32_t>(offsetof(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields, ___granted_2)); }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * get_granted_2() const { return ___granted_2; }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 ** get_address_of_granted_2() { return &___granted_2; }
	inline void set_granted_2(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * value)
	{
		___granted_2 = value;
		Il2CppCodeGenWriteBarrier((&___granted_2), value);
	}

	inline static int32_t get_offset_of_grantedWithMods_3() { return static_cast<int32_t>(offsetof(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields, ___grantedWithMods_3)); }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * get_grantedWithMods_3() const { return ___grantedWithMods_3; }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 ** get_address_of_grantedWithMods_3() { return &___grantedWithMods_3; }
	inline void set_grantedWithMods_3(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * value)
	{
		___grantedWithMods_3 = value;
		Il2CppCodeGenWriteBarrier((&___grantedWithMods_3), value);
	}

	inline static int32_t get_offset_of_rejection_4() { return static_cast<int32_t>(offsetof(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields, ___rejection_4)); }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * get_rejection_4() const { return ___rejection_4; }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 ** get_address_of_rejection_4() { return &___rejection_4; }
	inline void set_rejection_4(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * value)
	{
		___rejection_4 = value;
		Il2CppCodeGenWriteBarrier((&___rejection_4), value);
	}

	inline static int32_t get_offset_of_waiting_5() { return static_cast<int32_t>(offsetof(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields, ___waiting_5)); }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * get_waiting_5() const { return ___waiting_5; }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 ** get_address_of_waiting_5() { return &___waiting_5; }
	inline void set_waiting_5(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * value)
	{
		___waiting_5 = value;
		Il2CppCodeGenWriteBarrier((&___waiting_5), value);
	}

	inline static int32_t get_offset_of_revocationWarning_6() { return static_cast<int32_t>(offsetof(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields, ___revocationWarning_6)); }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * get_revocationWarning_6() const { return ___revocationWarning_6; }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 ** get_address_of_revocationWarning_6() { return &___revocationWarning_6; }
	inline void set_revocationWarning_6(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * value)
	{
		___revocationWarning_6 = value;
		Il2CppCodeGenWriteBarrier((&___revocationWarning_6), value);
	}

	inline static int32_t get_offset_of_revocationNotification_7() { return static_cast<int32_t>(offsetof(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields, ___revocationNotification_7)); }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * get_revocationNotification_7() const { return ___revocationNotification_7; }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 ** get_address_of_revocationNotification_7() { return &___revocationNotification_7; }
	inline void set_revocationNotification_7(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * value)
	{
		___revocationNotification_7 = value;
		Il2CppCodeGenWriteBarrier((&___revocationNotification_7), value);
	}

	inline static int32_t get_offset_of_keyUpdateWaiting_8() { return static_cast<int32_t>(offsetof(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields, ___keyUpdateWaiting_8)); }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * get_keyUpdateWaiting_8() const { return ___keyUpdateWaiting_8; }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 ** get_address_of_keyUpdateWaiting_8() { return &___keyUpdateWaiting_8; }
	inline void set_keyUpdateWaiting_8(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * value)
	{
		___keyUpdateWaiting_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyUpdateWaiting_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKISTATUSENCODABLE_T91EFDE2752AE3A66F2F5B002BE800E8478FF1100_H
#ifndef PKISTATUSINFO_T15BA16AB659A7A930F085748710FF141C65EB13E_H
#define PKISTATUSINFO_T15BA16AB659A7A930F085748710FF141C65EB13E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusInfo
struct  PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusInfo::status
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___status_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiFreeText BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusInfo::statusString
	PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * ___statusString_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusInfo::failInfo
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___failInfo_4;

public:
	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E, ___status_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_status_2() const { return ___status_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier((&___status_2), value);
	}

	inline static int32_t get_offset_of_statusString_3() { return static_cast<int32_t>(offsetof(PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E, ___statusString_3)); }
	inline PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * get_statusString_3() const { return ___statusString_3; }
	inline PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D ** get_address_of_statusString_3() { return &___statusString_3; }
	inline void set_statusString_3(PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * value)
	{
		___statusString_3 = value;
		Il2CppCodeGenWriteBarrier((&___statusString_3), value);
	}

	inline static int32_t get_offset_of_failInfo_4() { return static_cast<int32_t>(offsetof(PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E, ___failInfo_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_failInfo_4() const { return ___failInfo_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_failInfo_4() { return &___failInfo_4; }
	inline void set_failInfo_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___failInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___failInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKISTATUSINFO_T15BA16AB659A7A930F085748710FF141C65EB13E_H
#ifndef POLLREPCONTENT_T3891E9F9658D1B12B6436DDBC3214FBE6D1993BA_H
#define POLLREPCONTENT_T3891E9F9658D1B12B6436DDBC3214FBE6D1993BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PollRepContent
struct  PollRepContent_t3891E9F9658D1B12B6436DDBC3214FBE6D1993BA  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PollRepContent::certReqId
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___certReqId_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PollRepContent::checkAfter
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___checkAfter_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiFreeText BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PollRepContent::reason
	PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * ___reason_4;

public:
	inline static int32_t get_offset_of_certReqId_2() { return static_cast<int32_t>(offsetof(PollRepContent_t3891E9F9658D1B12B6436DDBC3214FBE6D1993BA, ___certReqId_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_certReqId_2() const { return ___certReqId_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_certReqId_2() { return &___certReqId_2; }
	inline void set_certReqId_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___certReqId_2 = value;
		Il2CppCodeGenWriteBarrier((&___certReqId_2), value);
	}

	inline static int32_t get_offset_of_checkAfter_3() { return static_cast<int32_t>(offsetof(PollRepContent_t3891E9F9658D1B12B6436DDBC3214FBE6D1993BA, ___checkAfter_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_checkAfter_3() const { return ___checkAfter_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_checkAfter_3() { return &___checkAfter_3; }
	inline void set_checkAfter_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___checkAfter_3 = value;
		Il2CppCodeGenWriteBarrier((&___checkAfter_3), value);
	}

	inline static int32_t get_offset_of_reason_4() { return static_cast<int32_t>(offsetof(PollRepContent_t3891E9F9658D1B12B6436DDBC3214FBE6D1993BA, ___reason_4)); }
	inline PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * get_reason_4() const { return ___reason_4; }
	inline PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D ** get_address_of_reason_4() { return &___reason_4; }
	inline void set_reason_4(PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D * value)
	{
		___reason_4 = value;
		Il2CppCodeGenWriteBarrier((&___reason_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLLREPCONTENT_T3891E9F9658D1B12B6436DDBC3214FBE6D1993BA_H
#ifndef POLLREQCONTENT_T9B2D21F83F118B36596B19E367FF4992A9B7B3B5_H
#define POLLREQCONTENT_T9B2D21F83F118B36596B19E367FF4992A9B7B3B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PollReqContent
struct  PollReqContent_t9B2D21F83F118B36596B19E367FF4992A9B7B3B5  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PollReqContent::content
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___content_2;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(PollReqContent_t9B2D21F83F118B36596B19E367FF4992A9B7B3B5, ___content_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_content_2() const { return ___content_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLLREQCONTENT_T9B2D21F83F118B36596B19E367FF4992A9B7B3B5_H
#ifndef POPODECKEYCHALLCONTENT_T38663B40255D5090EE13108EFE526EAD57769D8C_H
#define POPODECKEYCHALLCONTENT_T38663B40255D5090EE13108EFE526EAD57769D8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PopoDecKeyChallContent
struct  PopoDecKeyChallContent_t38663B40255D5090EE13108EFE526EAD57769D8C  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PopoDecKeyChallContent::content
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___content_2;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(PopoDecKeyChallContent_t38663B40255D5090EE13108EFE526EAD57769D8C, ___content_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_content_2() const { return ___content_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPODECKEYCHALLCONTENT_T38663B40255D5090EE13108EFE526EAD57769D8C_H
#ifndef POPODECKEYRESPCONTENT_TBD490241E1A1518E5709E8D8A6921BDB5B52478A_H
#define POPODECKEYRESPCONTENT_TBD490241E1A1518E5709E8D8A6921BDB5B52478A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PopoDecKeyRespContent
struct  PopoDecKeyRespContent_tBD490241E1A1518E5709E8D8A6921BDB5B52478A  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PopoDecKeyRespContent::content
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___content_2;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(PopoDecKeyRespContent_tBD490241E1A1518E5709E8D8A6921BDB5B52478A, ___content_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_content_2() const { return ___content_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPODECKEYRESPCONTENT_TBD490241E1A1518E5709E8D8A6921BDB5B52478A_H
#ifndef PROTECTEDPART_T722EBC5C3DA1750E566F1B85F4E92A6DE9779EC6_H
#define PROTECTEDPART_T722EBC5C3DA1750E566F1B85F4E92A6DE9779EC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.ProtectedPart
struct  ProtectedPart_t722EBC5C3DA1750E566F1B85F4E92A6DE9779EC6  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiHeader BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.ProtectedPart::header
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0 * ___header_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiBody BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.ProtectedPart::body
	PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89 * ___body_3;

public:
	inline static int32_t get_offset_of_header_2() { return static_cast<int32_t>(offsetof(ProtectedPart_t722EBC5C3DA1750E566F1B85F4E92A6DE9779EC6, ___header_2)); }
	inline PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0 * get_header_2() const { return ___header_2; }
	inline PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0 ** get_address_of_header_2() { return &___header_2; }
	inline void set_header_2(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0 * value)
	{
		___header_2 = value;
		Il2CppCodeGenWriteBarrier((&___header_2), value);
	}

	inline static int32_t get_offset_of_body_3() { return static_cast<int32_t>(offsetof(ProtectedPart_t722EBC5C3DA1750E566F1B85F4E92A6DE9779EC6, ___body_3)); }
	inline PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89 * get_body_3() const { return ___body_3; }
	inline PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89 ** get_address_of_body_3() { return &___body_3; }
	inline void set_body_3(PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89 * value)
	{
		___body_3 = value;
		Il2CppCodeGenWriteBarrier((&___body_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDPART_T722EBC5C3DA1750E566F1B85F4E92A6DE9779EC6_H
#ifndef REVANNCONTENT_T039556C6478DC57D48ED74B5337771AC70212B9A_H
#define REVANNCONTENT_T039556C6478DC57D48ED74B5337771AC70212B9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevAnnContent
struct  RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusEncodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevAnnContent::status
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * ___status_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertId BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevAnnContent::certId
	CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D * ___certId_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevAnnContent::willBeRevokedAt
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___willBeRevokedAt_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevAnnContent::badSinceDate
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___badSinceDate_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevAnnContent::crlDetails
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___crlDetails_6;

public:
	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A, ___status_2)); }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * get_status_2() const { return ___status_2; }
	inline PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 ** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100 * value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier((&___status_2), value);
	}

	inline static int32_t get_offset_of_certId_3() { return static_cast<int32_t>(offsetof(RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A, ___certId_3)); }
	inline CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D * get_certId_3() const { return ___certId_3; }
	inline CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D ** get_address_of_certId_3() { return &___certId_3; }
	inline void set_certId_3(CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D * value)
	{
		___certId_3 = value;
		Il2CppCodeGenWriteBarrier((&___certId_3), value);
	}

	inline static int32_t get_offset_of_willBeRevokedAt_4() { return static_cast<int32_t>(offsetof(RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A, ___willBeRevokedAt_4)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_willBeRevokedAt_4() const { return ___willBeRevokedAt_4; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_willBeRevokedAt_4() { return &___willBeRevokedAt_4; }
	inline void set_willBeRevokedAt_4(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___willBeRevokedAt_4 = value;
		Il2CppCodeGenWriteBarrier((&___willBeRevokedAt_4), value);
	}

	inline static int32_t get_offset_of_badSinceDate_5() { return static_cast<int32_t>(offsetof(RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A, ___badSinceDate_5)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_badSinceDate_5() const { return ___badSinceDate_5; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_badSinceDate_5() { return &___badSinceDate_5; }
	inline void set_badSinceDate_5(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___badSinceDate_5 = value;
		Il2CppCodeGenWriteBarrier((&___badSinceDate_5), value);
	}

	inline static int32_t get_offset_of_crlDetails_6() { return static_cast<int32_t>(offsetof(RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A, ___crlDetails_6)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_crlDetails_6() const { return ___crlDetails_6; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_crlDetails_6() { return &___crlDetails_6; }
	inline void set_crlDetails_6(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___crlDetails_6 = value;
		Il2CppCodeGenWriteBarrier((&___crlDetails_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVANNCONTENT_T039556C6478DC57D48ED74B5337771AC70212B9A_H
#ifndef REVDETAILS_T7273B946D1D2C61B9EA8A1AE9A0205E17AFE63A3_H
#define REVDETAILS_T7273B946D1D2C61B9EA8A1AE9A0205E17AFE63A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevDetails
struct  RevDetails_t7273B946D1D2C61B9EA8A1AE9A0205E17AFE63A3  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevDetails::certDetails
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C * ___certDetails_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevDetails::crlEntryDetails
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___crlEntryDetails_3;

public:
	inline static int32_t get_offset_of_certDetails_2() { return static_cast<int32_t>(offsetof(RevDetails_t7273B946D1D2C61B9EA8A1AE9A0205E17AFE63A3, ___certDetails_2)); }
	inline CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C * get_certDetails_2() const { return ___certDetails_2; }
	inline CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C ** get_address_of_certDetails_2() { return &___certDetails_2; }
	inline void set_certDetails_2(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C * value)
	{
		___certDetails_2 = value;
		Il2CppCodeGenWriteBarrier((&___certDetails_2), value);
	}

	inline static int32_t get_offset_of_crlEntryDetails_3() { return static_cast<int32_t>(offsetof(RevDetails_t7273B946D1D2C61B9EA8A1AE9A0205E17AFE63A3, ___crlEntryDetails_3)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_crlEntryDetails_3() const { return ___crlEntryDetails_3; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_crlEntryDetails_3() { return &___crlEntryDetails_3; }
	inline void set_crlEntryDetails_3(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___crlEntryDetails_3 = value;
		Il2CppCodeGenWriteBarrier((&___crlEntryDetails_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVDETAILS_T7273B946D1D2C61B9EA8A1AE9A0205E17AFE63A3_H
#ifndef REVREPCONTENT_TA5363D4A7716CE0927BB84356FBB334A4DB0FE93_H
#define REVREPCONTENT_TA5363D4A7716CE0927BB84356FBB334A4DB0FE93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevRepContent
struct  RevRepContent_tA5363D4A7716CE0927BB84356FBB334A4DB0FE93  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevRepContent::status
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___status_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevRepContent::revCerts
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___revCerts_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevRepContent::crls
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___crls_4;

public:
	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(RevRepContent_tA5363D4A7716CE0927BB84356FBB334A4DB0FE93, ___status_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_status_2() const { return ___status_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier((&___status_2), value);
	}

	inline static int32_t get_offset_of_revCerts_3() { return static_cast<int32_t>(offsetof(RevRepContent_tA5363D4A7716CE0927BB84356FBB334A4DB0FE93, ___revCerts_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_revCerts_3() const { return ___revCerts_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_revCerts_3() { return &___revCerts_3; }
	inline void set_revCerts_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___revCerts_3 = value;
		Il2CppCodeGenWriteBarrier((&___revCerts_3), value);
	}

	inline static int32_t get_offset_of_crls_4() { return static_cast<int32_t>(offsetof(RevRepContent_tA5363D4A7716CE0927BB84356FBB334A4DB0FE93, ___crls_4)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_crls_4() const { return ___crls_4; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_crls_4() { return &___crls_4; }
	inline void set_crls_4(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___crls_4 = value;
		Il2CppCodeGenWriteBarrier((&___crls_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVREPCONTENT_TA5363D4A7716CE0927BB84356FBB334A4DB0FE93_H
#ifndef REVREQCONTENT_TE0C577C5435FD1AF2ED1E0463A75ECAC7F36C28D_H
#define REVREQCONTENT_TE0C577C5435FD1AF2ED1E0463A75ECAC7F36C28D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevReqContent
struct  RevReqContent_tE0C577C5435FD1AF2ED1E0463A75ECAC7F36C28D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.RevReqContent::content
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___content_2;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(RevReqContent_tE0C577C5435FD1AF2ED1E0463A75ECAC7F36C28D, ___content_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_content_2() const { return ___content_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVREQCONTENT_TE0C577C5435FD1AF2ED1E0463A75ECAC7F36C28D_H
#ifndef MQVUSERKEYINGMATERIAL_T7DC9FF450C45E93147682E732A2BB84DADB2E660_H
#define MQVUSERKEYINGMATERIAL_T7DC9FF450C45E93147682E732A2BB84DADB2E660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Ecc.MQVuserKeyingMaterial
struct  MQVuserKeyingMaterial_t7DC9FF450C45E93147682E732A2BB84DADB2E660  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorPublicKey BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Ecc.MQVuserKeyingMaterial::ephemeralPublicKey
	OriginatorPublicKey_t860ECE14956B5E7E4A98C44017B2529C39C8F209 * ___ephemeralPublicKey_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Ecc.MQVuserKeyingMaterial::addedukm
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___addedukm_3;

public:
	inline static int32_t get_offset_of_ephemeralPublicKey_2() { return static_cast<int32_t>(offsetof(MQVuserKeyingMaterial_t7DC9FF450C45E93147682E732A2BB84DADB2E660, ___ephemeralPublicKey_2)); }
	inline OriginatorPublicKey_t860ECE14956B5E7E4A98C44017B2529C39C8F209 * get_ephemeralPublicKey_2() const { return ___ephemeralPublicKey_2; }
	inline OriginatorPublicKey_t860ECE14956B5E7E4A98C44017B2529C39C8F209 ** get_address_of_ephemeralPublicKey_2() { return &___ephemeralPublicKey_2; }
	inline void set_ephemeralPublicKey_2(OriginatorPublicKey_t860ECE14956B5E7E4A98C44017B2529C39C8F209 * value)
	{
		___ephemeralPublicKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___ephemeralPublicKey_2), value);
	}

	inline static int32_t get_offset_of_addedukm_3() { return static_cast<int32_t>(offsetof(MQVuserKeyingMaterial_t7DC9FF450C45E93147682E732A2BB84DADB2E660, ___addedukm_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_addedukm_3() const { return ___addedukm_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_addedukm_3() { return &___addedukm_3; }
	inline void set_addedukm_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___addedukm_3 = value;
		Il2CppCodeGenWriteBarrier((&___addedukm_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MQVUSERKEYINGMATERIAL_T7DC9FF450C45E93147682E732A2BB84DADB2E660_H
#ifndef OTHERKEYATTRIBUTE_TACF39B83B18775C6C4011AB47D7F021E183E60AB_H
#define OTHERKEYATTRIBUTE_TACF39B83B18775C6C4011AB47D7F021E183E60AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherKeyAttribute
struct  OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherKeyAttribute::keyAttrId
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___keyAttrId_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherKeyAttribute::keyAttr
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___keyAttr_3;

public:
	inline static int32_t get_offset_of_keyAttrId_2() { return static_cast<int32_t>(offsetof(OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB, ___keyAttrId_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_keyAttrId_2() const { return ___keyAttrId_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_keyAttrId_2() { return &___keyAttrId_2; }
	inline void set_keyAttrId_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___keyAttrId_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyAttrId_2), value);
	}

	inline static int32_t get_offset_of_keyAttr_3() { return static_cast<int32_t>(offsetof(OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB, ___keyAttr_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_keyAttr_3() const { return ___keyAttr_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_keyAttr_3() { return &___keyAttr_3; }
	inline void set_keyAttr_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___keyAttr_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyAttr_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERKEYATTRIBUTE_TACF39B83B18775C6C4011AB47D7F021E183E60AB_H
#ifndef OTHERRECIPIENTINFO_TD315423F47920CE1A87332822006AAFB6AB66258_H
#define OTHERRECIPIENTINFO_TD315423F47920CE1A87332822006AAFB6AB66258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherRecipientInfo
struct  OtherRecipientInfo_tD315423F47920CE1A87332822006AAFB6AB66258  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherRecipientInfo::oriType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___oriType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherRecipientInfo::oriValue
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___oriValue_3;

public:
	inline static int32_t get_offset_of_oriType_2() { return static_cast<int32_t>(offsetof(OtherRecipientInfo_tD315423F47920CE1A87332822006AAFB6AB66258, ___oriType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_oriType_2() const { return ___oriType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_oriType_2() { return &___oriType_2; }
	inline void set_oriType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___oriType_2 = value;
		Il2CppCodeGenWriteBarrier((&___oriType_2), value);
	}

	inline static int32_t get_offset_of_oriValue_3() { return static_cast<int32_t>(offsetof(OtherRecipientInfo_tD315423F47920CE1A87332822006AAFB6AB66258, ___oriValue_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_oriValue_3() const { return ___oriValue_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_oriValue_3() { return &___oriValue_3; }
	inline void set_oriValue_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___oriValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___oriValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERRECIPIENTINFO_TD315423F47920CE1A87332822006AAFB6AB66258_H
#ifndef OTHERREVOCATIONINFOFORMAT_TB2AEFCBC863B0AD2100FEF5EA2E4E2E7AC330D03_H
#define OTHERREVOCATIONINFOFORMAT_TB2AEFCBC863B0AD2100FEF5EA2E4E2E7AC330D03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherRevocationInfoFormat
struct  OtherRevocationInfoFormat_tB2AEFCBC863B0AD2100FEF5EA2E4E2E7AC330D03  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherRevocationInfoFormat::otherRevInfoFormat
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___otherRevInfoFormat_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherRevocationInfoFormat::otherRevInfo
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___otherRevInfo_3;

public:
	inline static int32_t get_offset_of_otherRevInfoFormat_2() { return static_cast<int32_t>(offsetof(OtherRevocationInfoFormat_tB2AEFCBC863B0AD2100FEF5EA2E4E2E7AC330D03, ___otherRevInfoFormat_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_otherRevInfoFormat_2() const { return ___otherRevInfoFormat_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_otherRevInfoFormat_2() { return &___otherRevInfoFormat_2; }
	inline void set_otherRevInfoFormat_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___otherRevInfoFormat_2 = value;
		Il2CppCodeGenWriteBarrier((&___otherRevInfoFormat_2), value);
	}

	inline static int32_t get_offset_of_otherRevInfo_3() { return static_cast<int32_t>(offsetof(OtherRevocationInfoFormat_tB2AEFCBC863B0AD2100FEF5EA2E4E2E7AC330D03, ___otherRevInfo_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_otherRevInfo_3() const { return ___otherRevInfo_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_otherRevInfo_3() { return &___otherRevInfo_3; }
	inline void set_otherRevInfo_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___otherRevInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___otherRevInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERREVOCATIONINFOFORMAT_TB2AEFCBC863B0AD2100FEF5EA2E4E2E7AC330D03_H
#ifndef PASSWORDRECIPIENTINFO_TFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B_H
#define PASSWORDRECIPIENTINFO_TFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.PasswordRecipientInfo
struct  PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.PasswordRecipientInfo::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.PasswordRecipientInfo::keyDerivationAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___keyDerivationAlgorithm_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.PasswordRecipientInfo::keyEncryptionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___keyEncryptionAlgorithm_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.PasswordRecipientInfo::encryptedKey
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___encryptedKey_5;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_keyDerivationAlgorithm_3() { return static_cast<int32_t>(offsetof(PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B, ___keyDerivationAlgorithm_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_keyDerivationAlgorithm_3() const { return ___keyDerivationAlgorithm_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_keyDerivationAlgorithm_3() { return &___keyDerivationAlgorithm_3; }
	inline void set_keyDerivationAlgorithm_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___keyDerivationAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyDerivationAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_keyEncryptionAlgorithm_4() { return static_cast<int32_t>(offsetof(PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B, ___keyEncryptionAlgorithm_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_keyEncryptionAlgorithm_4() const { return ___keyEncryptionAlgorithm_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_keyEncryptionAlgorithm_4() { return &___keyEncryptionAlgorithm_4; }
	inline void set_keyEncryptionAlgorithm_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___keyEncryptionAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___keyEncryptionAlgorithm_4), value);
	}

	inline static int32_t get_offset_of_encryptedKey_5() { return static_cast<int32_t>(offsetof(PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B, ___encryptedKey_5)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_encryptedKey_5() const { return ___encryptedKey_5; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_encryptedKey_5() { return &___encryptedKey_5; }
	inline void set_encryptedKey_5(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___encryptedKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedKey_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSWORDRECIPIENTINFO_TFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B_H
#ifndef RECIPIENTENCRYPTEDKEY_T28F13449FA4B9B40F90FB7A27D607F3D1F45B922_H
#define RECIPIENTENCRYPTEDKEY_T28F13449FA4B9B40F90FB7A27D607F3D1F45B922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientEncryptedKey
struct  RecipientEncryptedKey_t28F13449FA4B9B40F90FB7A27D607F3D1F45B922  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientEncryptedKey::identifier
	KeyAgreeRecipientIdentifier_tE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9 * ___identifier_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientEncryptedKey::encryptedKey
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___encryptedKey_3;

public:
	inline static int32_t get_offset_of_identifier_2() { return static_cast<int32_t>(offsetof(RecipientEncryptedKey_t28F13449FA4B9B40F90FB7A27D607F3D1F45B922, ___identifier_2)); }
	inline KeyAgreeRecipientIdentifier_tE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9 * get_identifier_2() const { return ___identifier_2; }
	inline KeyAgreeRecipientIdentifier_tE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9 ** get_address_of_identifier_2() { return &___identifier_2; }
	inline void set_identifier_2(KeyAgreeRecipientIdentifier_tE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9 * value)
	{
		___identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_2), value);
	}

	inline static int32_t get_offset_of_encryptedKey_3() { return static_cast<int32_t>(offsetof(RecipientEncryptedKey_t28F13449FA4B9B40F90FB7A27D607F3D1F45B922, ___encryptedKey_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_encryptedKey_3() const { return ___encryptedKey_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_encryptedKey_3() { return &___encryptedKey_3; }
	inline void set_encryptedKey_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___encryptedKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedKey_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECIPIENTENCRYPTEDKEY_T28F13449FA4B9B40F90FB7A27D607F3D1F45B922_H
#ifndef RECIPIENTIDENTIFIER_T774592C8CB7234B60B03C10B73D4964324CCF570_H
#define RECIPIENTIDENTIFIER_T774592C8CB7234B60B03C10B73D4964324CCF570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientIdentifier
struct  RecipientIdentifier_t774592C8CB7234B60B03C10B73D4964324CCF570  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientIdentifier::id
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___id_2;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(RecipientIdentifier_t774592C8CB7234B60B03C10B73D4964324CCF570, ___id_2)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_id_2() const { return ___id_2; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECIPIENTIDENTIFIER_T774592C8CB7234B60B03C10B73D4964324CCF570_H
#ifndef RECIPIENTINFO_T3D1639C31248DD8F5D062C66B4A11FD44BBAB7E5_H
#define RECIPIENTINFO_T3D1639C31248DD8F5D062C66B4A11FD44BBAB7E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientInfo
struct  RecipientInfo_t3D1639C31248DD8F5D062C66B4A11FD44BBAB7E5  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientInfo::info
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___info_2;

public:
	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(RecipientInfo_t3D1639C31248DD8F5D062C66B4A11FD44BBAB7E5, ___info_2)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_info_2() const { return ___info_2; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier((&___info_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECIPIENTINFO_T3D1639C31248DD8F5D062C66B4A11FD44BBAB7E5_H
#ifndef RECIPIENTKEYIDENTIFIER_T42AD99C1DBF8D28884B823B3315F51777F16C164_H
#define RECIPIENTKEYIDENTIFIER_T42AD99C1DBF8D28884B823B3315F51777F16C164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientKeyIdentifier
struct  RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientKeyIdentifier::subjectKeyIdentifier
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___subjectKeyIdentifier_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientKeyIdentifier::date
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___date_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherKeyAttribute BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientKeyIdentifier::other
	OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB * ___other_4;

public:
	inline static int32_t get_offset_of_subjectKeyIdentifier_2() { return static_cast<int32_t>(offsetof(RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164, ___subjectKeyIdentifier_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_subjectKeyIdentifier_2() const { return ___subjectKeyIdentifier_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_subjectKeyIdentifier_2() { return &___subjectKeyIdentifier_2; }
	inline void set_subjectKeyIdentifier_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___subjectKeyIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___subjectKeyIdentifier_2), value);
	}

	inline static int32_t get_offset_of_date_3() { return static_cast<int32_t>(offsetof(RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164, ___date_3)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_date_3() const { return ___date_3; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_date_3() { return &___date_3; }
	inline void set_date_3(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___date_3 = value;
		Il2CppCodeGenWriteBarrier((&___date_3), value);
	}

	inline static int32_t get_offset_of_other_4() { return static_cast<int32_t>(offsetof(RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164, ___other_4)); }
	inline OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB * get_other_4() const { return ___other_4; }
	inline OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB ** get_address_of_other_4() { return &___other_4; }
	inline void set_other_4(OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB * value)
	{
		___other_4 = value;
		Il2CppCodeGenWriteBarrier((&___other_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECIPIENTKEYIDENTIFIER_T42AD99C1DBF8D28884B823B3315F51777F16C164_H
#ifndef SCVPREQRES_T6912916152CB10DCA26166AF213870737393FBA0_H
#define SCVPREQRES_T6912916152CB10DCA26166AF213870737393FBA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ScvpReqRes
struct  ScvpReqRes_t6912916152CB10DCA26166AF213870737393FBA0  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ScvpReqRes::request
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___request_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ScvpReqRes::response
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___response_3;

public:
	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(ScvpReqRes_t6912916152CB10DCA26166AF213870737393FBA0, ___request_2)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_request_2() const { return ___request_2; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier((&___request_2), value);
	}

	inline static int32_t get_offset_of_response_3() { return static_cast<int32_t>(offsetof(ScvpReqRes_t6912916152CB10DCA26166AF213870737393FBA0, ___response_3)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_response_3() const { return ___response_3; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_response_3() { return &___response_3; }
	inline void set_response_3(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___response_3 = value;
		Il2CppCodeGenWriteBarrier((&___response_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCVPREQRES_T6912916152CB10DCA26166AF213870737393FBA0_H
#ifndef SIGNEDDATA_TCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_H
#define SIGNEDDATA_TCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData
struct  SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::digestAlgorithms
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___digestAlgorithms_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::contentInfo
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___contentInfo_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::certificates
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___certificates_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::crls
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___crls_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::signerInfos
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___signerInfos_11;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::certsBer
	bool ___certsBer_12;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::crlsBer
	bool ___crlsBer_13;

public:
	inline static int32_t get_offset_of_version_6() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F, ___version_6)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_6() const { return ___version_6; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_6() { return &___version_6; }
	inline void set_version_6(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_6 = value;
		Il2CppCodeGenWriteBarrier((&___version_6), value);
	}

	inline static int32_t get_offset_of_digestAlgorithms_7() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F, ___digestAlgorithms_7)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_digestAlgorithms_7() const { return ___digestAlgorithms_7; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_digestAlgorithms_7() { return &___digestAlgorithms_7; }
	inline void set_digestAlgorithms_7(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___digestAlgorithms_7 = value;
		Il2CppCodeGenWriteBarrier((&___digestAlgorithms_7), value);
	}

	inline static int32_t get_offset_of_contentInfo_8() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F, ___contentInfo_8)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_contentInfo_8() const { return ___contentInfo_8; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_contentInfo_8() { return &___contentInfo_8; }
	inline void set_contentInfo_8(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___contentInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___contentInfo_8), value);
	}

	inline static int32_t get_offset_of_certificates_9() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F, ___certificates_9)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_certificates_9() const { return ___certificates_9; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_certificates_9() { return &___certificates_9; }
	inline void set_certificates_9(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___certificates_9 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_9), value);
	}

	inline static int32_t get_offset_of_crls_10() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F, ___crls_10)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_crls_10() const { return ___crls_10; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_crls_10() { return &___crls_10; }
	inline void set_crls_10(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___crls_10 = value;
		Il2CppCodeGenWriteBarrier((&___crls_10), value);
	}

	inline static int32_t get_offset_of_signerInfos_11() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F, ___signerInfos_11)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_signerInfos_11() const { return ___signerInfos_11; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_signerInfos_11() { return &___signerInfos_11; }
	inline void set_signerInfos_11(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___signerInfos_11 = value;
		Il2CppCodeGenWriteBarrier((&___signerInfos_11), value);
	}

	inline static int32_t get_offset_of_certsBer_12() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F, ___certsBer_12)); }
	inline bool get_certsBer_12() const { return ___certsBer_12; }
	inline bool* get_address_of_certsBer_12() { return &___certsBer_12; }
	inline void set_certsBer_12(bool value)
	{
		___certsBer_12 = value;
	}

	inline static int32_t get_offset_of_crlsBer_13() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F, ___crlsBer_13)); }
	inline bool get_crlsBer_13() const { return ___crlsBer_13; }
	inline bool* get_address_of_crlsBer_13() { return &___crlsBer_13; }
	inline void set_crlsBer_13(bool value)
	{
		___crlsBer_13 = value;
	}
};

struct SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::Version1
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___Version1_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::Version3
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___Version3_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::Version4
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___Version4_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData::Version5
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___Version5_5;

public:
	inline static int32_t get_offset_of_Version1_2() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_StaticFields, ___Version1_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_Version1_2() const { return ___Version1_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_Version1_2() { return &___Version1_2; }
	inline void set_Version1_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___Version1_2 = value;
		Il2CppCodeGenWriteBarrier((&___Version1_2), value);
	}

	inline static int32_t get_offset_of_Version3_3() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_StaticFields, ___Version3_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_Version3_3() const { return ___Version3_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_Version3_3() { return &___Version3_3; }
	inline void set_Version3_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___Version3_3 = value;
		Il2CppCodeGenWriteBarrier((&___Version3_3), value);
	}

	inline static int32_t get_offset_of_Version4_4() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_StaticFields, ___Version4_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_Version4_4() const { return ___Version4_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_Version4_4() { return &___Version4_4; }
	inline void set_Version4_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___Version4_4 = value;
		Il2CppCodeGenWriteBarrier((&___Version4_4), value);
	}

	inline static int32_t get_offset_of_Version5_5() { return static_cast<int32_t>(offsetof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_StaticFields, ___Version5_5)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_Version5_5() const { return ___Version5_5; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_Version5_5() { return &___Version5_5; }
	inline void set_Version5_5(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___Version5_5 = value;
		Il2CppCodeGenWriteBarrier((&___Version5_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNEDDATA_TCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_H
#ifndef SIGNERIDENTIFIER_TA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4_H
#define SIGNERIDENTIFIER_TA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerIdentifier
struct  SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerIdentifier::id
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___id_2;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4, ___id_2)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_id_2() const { return ___id_2; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERIDENTIFIER_TA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4_H
#ifndef SIGNERINFO_T9C562DFE2A91D18AD4100AC711D30C8BCC660B23_H
#define SIGNERINFO_T9C562DFE2A91D18AD4100AC711D30C8BCC660B23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerInfo
struct  SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerInfo::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerInfo::sid
	SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * ___sid_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerInfo::digAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___digAlgorithm_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerInfo::authenticatedAttributes
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___authenticatedAttributes_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerInfo::digEncryptionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___digEncryptionAlgorithm_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerInfo::encryptedDigest
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___encryptedDigest_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerInfo::unauthenticatedAttributes
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___unauthenticatedAttributes_8;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_sid_3() { return static_cast<int32_t>(offsetof(SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23, ___sid_3)); }
	inline SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * get_sid_3() const { return ___sid_3; }
	inline SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 ** get_address_of_sid_3() { return &___sid_3; }
	inline void set_sid_3(SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * value)
	{
		___sid_3 = value;
		Il2CppCodeGenWriteBarrier((&___sid_3), value);
	}

	inline static int32_t get_offset_of_digAlgorithm_4() { return static_cast<int32_t>(offsetof(SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23, ___digAlgorithm_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_digAlgorithm_4() const { return ___digAlgorithm_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_digAlgorithm_4() { return &___digAlgorithm_4; }
	inline void set_digAlgorithm_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___digAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___digAlgorithm_4), value);
	}

	inline static int32_t get_offset_of_authenticatedAttributes_5() { return static_cast<int32_t>(offsetof(SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23, ___authenticatedAttributes_5)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_authenticatedAttributes_5() const { return ___authenticatedAttributes_5; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_authenticatedAttributes_5() { return &___authenticatedAttributes_5; }
	inline void set_authenticatedAttributes_5(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___authenticatedAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___authenticatedAttributes_5), value);
	}

	inline static int32_t get_offset_of_digEncryptionAlgorithm_6() { return static_cast<int32_t>(offsetof(SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23, ___digEncryptionAlgorithm_6)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_digEncryptionAlgorithm_6() const { return ___digEncryptionAlgorithm_6; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_digEncryptionAlgorithm_6() { return &___digEncryptionAlgorithm_6; }
	inline void set_digEncryptionAlgorithm_6(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___digEncryptionAlgorithm_6 = value;
		Il2CppCodeGenWriteBarrier((&___digEncryptionAlgorithm_6), value);
	}

	inline static int32_t get_offset_of_encryptedDigest_7() { return static_cast<int32_t>(offsetof(SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23, ___encryptedDigest_7)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_encryptedDigest_7() const { return ___encryptedDigest_7; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_encryptedDigest_7() { return &___encryptedDigest_7; }
	inline void set_encryptedDigest_7(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___encryptedDigest_7 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedDigest_7), value);
	}

	inline static int32_t get_offset_of_unauthenticatedAttributes_8() { return static_cast<int32_t>(offsetof(SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23, ___unauthenticatedAttributes_8)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_unauthenticatedAttributes_8() const { return ___unauthenticatedAttributes_8; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_unauthenticatedAttributes_8() { return &___unauthenticatedAttributes_8; }
	inline void set_unauthenticatedAttributes_8(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___unauthenticatedAttributes_8 = value;
		Il2CppCodeGenWriteBarrier((&___unauthenticatedAttributes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERINFO_T9C562DFE2A91D18AD4100AC711D30C8BCC660B23_H
#ifndef TIME_T6318F5647228622221099EA986EA6ECB6C3C1ACD_H
#define TIME_T6318F5647228622221099EA986EA6ECB6C3C1ACD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Time
struct  Time_t6318F5647228622221099EA986EA6ECB6C3C1ACD  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Time::time
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___time_2;

public:
	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(Time_t6318F5647228622221099EA986EA6ECB6C3C1ACD, ___time_2)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_time_2() const { return ___time_2; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___time_2 = value;
		Il2CppCodeGenWriteBarrier((&___time_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIME_T6318F5647228622221099EA986EA6ECB6C3C1ACD_H
#ifndef TIMESTAMPANDCRL_TDC1C54343612EB603810766226B7FE9451AD18ED_H
#define TIMESTAMPANDCRL_TDC1C54343612EB603810766226B7FE9451AD18ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampAndCrl
struct  TimeStampAndCrl_tDC1C54343612EB603810766226B7FE9451AD18ED  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampAndCrl::timeStamp
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___timeStamp_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificateList BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampAndCrl::crl
	CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8 * ___crl_3;

public:
	inline static int32_t get_offset_of_timeStamp_2() { return static_cast<int32_t>(offsetof(TimeStampAndCrl_tDC1C54343612EB603810766226B7FE9451AD18ED, ___timeStamp_2)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_timeStamp_2() const { return ___timeStamp_2; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_timeStamp_2() { return &___timeStamp_2; }
	inline void set_timeStamp_2(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___timeStamp_2 = value;
		Il2CppCodeGenWriteBarrier((&___timeStamp_2), value);
	}

	inline static int32_t get_offset_of_crl_3() { return static_cast<int32_t>(offsetof(TimeStampAndCrl_tDC1C54343612EB603810766226B7FE9451AD18ED, ___crl_3)); }
	inline CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8 * get_crl_3() const { return ___crl_3; }
	inline CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8 ** get_address_of_crl_3() { return &___crl_3; }
	inline void set_crl_3(CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8 * value)
	{
		___crl_3 = value;
		Il2CppCodeGenWriteBarrier((&___crl_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPANDCRL_TDC1C54343612EB603810766226B7FE9451AD18ED_H
#ifndef TIMESTAMPTOKENEVIDENCE_T20CF5580B1920E8F80264B9321AC3D1AF7A02B98_H
#define TIMESTAMPTOKENEVIDENCE_T20CF5580B1920E8F80264B9321AC3D1AF7A02B98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampTokenEvidence
struct  TimeStampTokenEvidence_t20CF5580B1920E8F80264B9321AC3D1AF7A02B98  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampAndCrl[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampTokenEvidence::timeStampAndCrls
	TimeStampAndCrlU5BU5D_tB819CF986C00FD6731A39C2E86A78F6C473E5A1D* ___timeStampAndCrls_2;

public:
	inline static int32_t get_offset_of_timeStampAndCrls_2() { return static_cast<int32_t>(offsetof(TimeStampTokenEvidence_t20CF5580B1920E8F80264B9321AC3D1AF7A02B98, ___timeStampAndCrls_2)); }
	inline TimeStampAndCrlU5BU5D_tB819CF986C00FD6731A39C2E86A78F6C473E5A1D* get_timeStampAndCrls_2() const { return ___timeStampAndCrls_2; }
	inline TimeStampAndCrlU5BU5D_tB819CF986C00FD6731A39C2E86A78F6C473E5A1D** get_address_of_timeStampAndCrls_2() { return &___timeStampAndCrls_2; }
	inline void set_timeStampAndCrls_2(TimeStampAndCrlU5BU5D_tB819CF986C00FD6731A39C2E86A78F6C473E5A1D* value)
	{
		___timeStampAndCrls_2 = value;
		Il2CppCodeGenWriteBarrier((&___timeStampAndCrls_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPTOKENEVIDENCE_T20CF5580B1920E8F80264B9321AC3D1AF7A02B98_H
#ifndef TIMESTAMPEDDATA_TC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4_H
#define TIMESTAMPEDDATA_TC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedData
struct  TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedData::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedData::dataUri
	DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * ___dataUri_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.MetaData BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedData::metaData
	MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A * ___metaData_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedData::content
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___content_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Evidence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampedData::temporalEvidence
	Evidence_t6897C267406A516689941A0FF07964FF20B44F07 * ___temporalEvidence_6;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_dataUri_3() { return static_cast<int32_t>(offsetof(TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4, ___dataUri_3)); }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * get_dataUri_3() const { return ___dataUri_3; }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE ** get_address_of_dataUri_3() { return &___dataUri_3; }
	inline void set_dataUri_3(DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * value)
	{
		___dataUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___dataUri_3), value);
	}

	inline static int32_t get_offset_of_metaData_4() { return static_cast<int32_t>(offsetof(TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4, ___metaData_4)); }
	inline MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A * get_metaData_4() const { return ___metaData_4; }
	inline MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A ** get_address_of_metaData_4() { return &___metaData_4; }
	inline void set_metaData_4(MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A * value)
	{
		___metaData_4 = value;
		Il2CppCodeGenWriteBarrier((&___metaData_4), value);
	}

	inline static int32_t get_offset_of_content_5() { return static_cast<int32_t>(offsetof(TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4, ___content_5)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_content_5() const { return ___content_5; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_content_5() { return &___content_5; }
	inline void set_content_5(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___content_5 = value;
		Il2CppCodeGenWriteBarrier((&___content_5), value);
	}

	inline static int32_t get_offset_of_temporalEvidence_6() { return static_cast<int32_t>(offsetof(TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4, ___temporalEvidence_6)); }
	inline Evidence_t6897C267406A516689941A0FF07964FF20B44F07 * get_temporalEvidence_6() const { return ___temporalEvidence_6; }
	inline Evidence_t6897C267406A516689941A0FF07964FF20B44F07 ** get_address_of_temporalEvidence_6() { return &___temporalEvidence_6; }
	inline void set_temporalEvidence_6(Evidence_t6897C267406A516689941A0FF07964FF20B44F07 * value)
	{
		___temporalEvidence_6 = value;
		Il2CppCodeGenWriteBarrier((&___temporalEvidence_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPEDDATA_TC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef BUFFERPOOLMEMORYSTREAM_T65F3670F655A8995A8BEF13533F4237D2A6CD5AC_H
#define BUFFERPOOLMEMORYSTREAM_T65F3670F655A8995A8BEF13533F4237D2A6CD5AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.BufferPoolMemoryStream
struct  BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean BestHTTP.Extensions.BufferPoolMemoryStream::canWrite
	bool ___canWrite_5;
	// System.Boolean BestHTTP.Extensions.BufferPoolMemoryStream::allowGetBuffer
	bool ___allowGetBuffer_6;
	// System.Int32 BestHTTP.Extensions.BufferPoolMemoryStream::capacity
	int32_t ___capacity_7;
	// System.Int32 BestHTTP.Extensions.BufferPoolMemoryStream::length
	int32_t ___length_8;
	// System.Byte[] BestHTTP.Extensions.BufferPoolMemoryStream::internalBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___internalBuffer_9;
	// System.Int32 BestHTTP.Extensions.BufferPoolMemoryStream::initialIndex
	int32_t ___initialIndex_10;
	// System.Boolean BestHTTP.Extensions.BufferPoolMemoryStream::expandable
	bool ___expandable_11;
	// System.Boolean BestHTTP.Extensions.BufferPoolMemoryStream::streamClosed
	bool ___streamClosed_12;
	// System.Int32 BestHTTP.Extensions.BufferPoolMemoryStream::position
	int32_t ___position_13;
	// System.Int32 BestHTTP.Extensions.BufferPoolMemoryStream::dirty_bytes
	int32_t ___dirty_bytes_14;

public:
	inline static int32_t get_offset_of_canWrite_5() { return static_cast<int32_t>(offsetof(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC, ___canWrite_5)); }
	inline bool get_canWrite_5() const { return ___canWrite_5; }
	inline bool* get_address_of_canWrite_5() { return &___canWrite_5; }
	inline void set_canWrite_5(bool value)
	{
		___canWrite_5 = value;
	}

	inline static int32_t get_offset_of_allowGetBuffer_6() { return static_cast<int32_t>(offsetof(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC, ___allowGetBuffer_6)); }
	inline bool get_allowGetBuffer_6() const { return ___allowGetBuffer_6; }
	inline bool* get_address_of_allowGetBuffer_6() { return &___allowGetBuffer_6; }
	inline void set_allowGetBuffer_6(bool value)
	{
		___allowGetBuffer_6 = value;
	}

	inline static int32_t get_offset_of_capacity_7() { return static_cast<int32_t>(offsetof(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC, ___capacity_7)); }
	inline int32_t get_capacity_7() const { return ___capacity_7; }
	inline int32_t* get_address_of_capacity_7() { return &___capacity_7; }
	inline void set_capacity_7(int32_t value)
	{
		___capacity_7 = value;
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC, ___length_8)); }
	inline int32_t get_length_8() const { return ___length_8; }
	inline int32_t* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(int32_t value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_internalBuffer_9() { return static_cast<int32_t>(offsetof(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC, ___internalBuffer_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_internalBuffer_9() const { return ___internalBuffer_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_internalBuffer_9() { return &___internalBuffer_9; }
	inline void set_internalBuffer_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___internalBuffer_9 = value;
		Il2CppCodeGenWriteBarrier((&___internalBuffer_9), value);
	}

	inline static int32_t get_offset_of_initialIndex_10() { return static_cast<int32_t>(offsetof(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC, ___initialIndex_10)); }
	inline int32_t get_initialIndex_10() const { return ___initialIndex_10; }
	inline int32_t* get_address_of_initialIndex_10() { return &___initialIndex_10; }
	inline void set_initialIndex_10(int32_t value)
	{
		___initialIndex_10 = value;
	}

	inline static int32_t get_offset_of_expandable_11() { return static_cast<int32_t>(offsetof(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC, ___expandable_11)); }
	inline bool get_expandable_11() const { return ___expandable_11; }
	inline bool* get_address_of_expandable_11() { return &___expandable_11; }
	inline void set_expandable_11(bool value)
	{
		___expandable_11 = value;
	}

	inline static int32_t get_offset_of_streamClosed_12() { return static_cast<int32_t>(offsetof(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC, ___streamClosed_12)); }
	inline bool get_streamClosed_12() const { return ___streamClosed_12; }
	inline bool* get_address_of_streamClosed_12() { return &___streamClosed_12; }
	inline void set_streamClosed_12(bool value)
	{
		___streamClosed_12 = value;
	}

	inline static int32_t get_offset_of_position_13() { return static_cast<int32_t>(offsetof(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC, ___position_13)); }
	inline int32_t get_position_13() const { return ___position_13; }
	inline int32_t* get_address_of_position_13() { return &___position_13; }
	inline void set_position_13(int32_t value)
	{
		___position_13 = value;
	}

	inline static int32_t get_offset_of_dirty_bytes_14() { return static_cast<int32_t>(offsetof(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC, ___dirty_bytes_14)); }
	inline int32_t get_dirty_bytes_14() const { return ___dirty_bytes_14; }
	inline int32_t* get_address_of_dirty_bytes_14() { return &___dirty_bytes_14; }
	inline void set_dirty_bytes_14(int32_t value)
	{
		___dirty_bytes_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERPOOLMEMORYSTREAM_T65F3670F655A8995A8BEF13533F4237D2A6CD5AC_H
#ifndef BUFFERSEGMENTSTREAM_T6700EF58E2487E58AD20C3AB39746822031822DD_H
#define BUFFERSEGMENTSTREAM_T6700EF58E2487E58AD20C3AB39746822031822DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.BufferSegmentStream
struct  BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Int64 BestHTTP.Extensions.BufferSegmentStream::_length
	int64_t ____length_5;
	// System.Collections.Generic.List`1<BestHTTP.PlatformSupport.Memory.BufferSegment> BestHTTP.Extensions.BufferSegmentStream::bufferList
	List_1_t39F8A268286B4DC546055C5521B47EB5B8833BDD * ___bufferList_6;
	// System.Int32 BestHTTP.Extensions.BufferSegmentStream::subBufferPosition
	int32_t ___subBufferPosition_7;

public:
	inline static int32_t get_offset_of__length_5() { return static_cast<int32_t>(offsetof(BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD, ____length_5)); }
	inline int64_t get__length_5() const { return ____length_5; }
	inline int64_t* get_address_of__length_5() { return &____length_5; }
	inline void set__length_5(int64_t value)
	{
		____length_5 = value;
	}

	inline static int32_t get_offset_of_bufferList_6() { return static_cast<int32_t>(offsetof(BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD, ___bufferList_6)); }
	inline List_1_t39F8A268286B4DC546055C5521B47EB5B8833BDD * get_bufferList_6() const { return ___bufferList_6; }
	inline List_1_t39F8A268286B4DC546055C5521B47EB5B8833BDD ** get_address_of_bufferList_6() { return &___bufferList_6; }
	inline void set_bufferList_6(List_1_t39F8A268286B4DC546055C5521B47EB5B8833BDD * value)
	{
		___bufferList_6 = value;
		Il2CppCodeGenWriteBarrier((&___bufferList_6), value);
	}

	inline static int32_t get_offset_of_subBufferPosition_7() { return static_cast<int32_t>(offsetof(BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD, ___subBufferPosition_7)); }
	inline int32_t get_subBufferPosition_7() const { return ___subBufferPosition_7; }
	inline int32_t* get_address_of_subBufferPosition_7() { return &___subBufferPosition_7; }
	inline void set_subBufferPosition_7(int32_t value)
	{
		___subBufferPosition_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERSEGMENTSTREAM_T6700EF58E2487E58AD20C3AB39746822031822DD_H
#ifndef HTTPFORMUSAGE_TA7C75083C6FD2D3A73090E99E6C03C1645632CDB_H
#define HTTPFORMUSAGE_TA7C75083C6FD2D3A73090E99E6C03C1645632CDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Forms.HTTPFormUsage
struct  HTTPFormUsage_tA7C75083C6FD2D3A73090E99E6C03C1645632CDB 
{
public:
	// System.Int32 BestHTTP.Forms.HTTPFormUsage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTPFormUsage_tA7C75083C6FD2D3A73090E99E6C03C1645632CDB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPFORMUSAGE_TA7C75083C6FD2D3A73090E99E6C03C1645632CDB_H
#ifndef FUTURESTATE_TDC94D397E4F5657A1DC2385C02B3CE193E54E203_H
#define FUTURESTATE_TDC94D397E4F5657A1DC2385C02B3CE193E54E203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Futures.FutureState
struct  FutureState_tDC94D397E4F5657A1DC2385C02B3CE193E54E203 
{
public:
	// System.Int32 BestHTTP.Futures.FutureState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FutureState_tDC94D397E4F5657A1DC2385C02B3CE193E54E203, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUTURESTATE_TDC94D397E4F5657A1DC2385C02B3CE193E54E203_H
#ifndef LOGLEVELS_TF6A6E8E20D301C844D62B193BDC1C47162A5A033_H
#define LOGLEVELS_TF6A6E8E20D301C844D62B193BDC1C47162A5A033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Logger.Loglevels
struct  Loglevels_tF6A6E8E20D301C844D62B193BDC1C47162A5A033 
{
public:
	// System.Byte BestHTTP.Logger.Loglevels::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Loglevels_tF6A6E8E20D301C844D62B193BDC1C47162A5A033, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVELS_TF6A6E8E20D301C844D62B193BDC1C47162A5A033_H
#ifndef FILESTREAMMODES_T0C999A762FA85A5F073D8A091CAC093EC07A44BE_H
#define FILESTREAMMODES_T0C999A762FA85A5F073D8A091CAC093EC07A44BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.FileSystem.FileStreamModes
struct  FileStreamModes_t0C999A762FA85A5F073D8A091CAC093EC07A44BE 
{
public:
	// System.Int32 BestHTTP.PlatformSupport.FileSystem.FileStreamModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileStreamModes_t0C999A762FA85A5F073D8A091CAC093EC07A44BE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESTREAMMODES_T0C999A762FA85A5F073D8A091CAC093EC07A44BE_H
#ifndef BUFFERDESC_T3A4703231E897D91A50EFE099A5F824D7B05A388_H
#define BUFFERDESC_T3A4703231E897D91A50EFE099A5F824D7B05A388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.Memory.BufferDesc
struct  BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388 
{
public:
	// System.Byte[] BestHTTP.PlatformSupport.Memory.BufferDesc::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_1;
	// System.DateTime BestHTTP.PlatformSupport.Memory.BufferDesc::released
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___released_2;

public:
	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388, ___buffer_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_1() const { return ___buffer_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_1), value);
	}

	inline static int32_t get_offset_of_released_2() { return static_cast<int32_t>(offsetof(BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388, ___released_2)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_released_2() const { return ___released_2; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_released_2() { return &___released_2; }
	inline void set_released_2(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___released_2 = value;
	}
};

struct BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388_StaticFields
{
public:
	// BestHTTP.PlatformSupport.Memory.BufferDesc BestHTTP.PlatformSupport.Memory.BufferDesc::Empty
	BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388  ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388_StaticFields, ___Empty_0)); }
	inline BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388  get_Empty_0() const { return ___Empty_0; }
	inline BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388 * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388  value)
	{
		___Empty_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.PlatformSupport.Memory.BufferDesc
struct BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388_marshaled_pinvoke
{
	uint8_t* ___buffer_1;
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___released_2;
};
// Native definition for COM marshalling of BestHTTP.PlatformSupport.Memory.BufferDesc
struct BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388_marshaled_com
{
	uint8_t* ___buffer_1;
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___released_2;
};
#endif // BUFFERDESC_T3A4703231E897D91A50EFE099A5F824D7B05A388_H
#ifndef PROPERTIES_TBCE5319F4966D1423D1D27661B9D84D11A54A881_H
#define PROPERTIES_TBCE5319F4966D1423D1D27661B9D84D11A54A881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.TcpClient.General.TcpClient_Properties
struct  Properties_tBCE5319F4966D1423D1D27661B9D84D11A54A881 
{
public:
	// System.UInt32 BestHTTP.PlatformSupport.TcpClient.General.TcpClient_Properties::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Properties_tBCE5319F4966D1423D1D27661B9D84D11A54A881, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTIES_TBCE5319F4966D1423D1D27661B9D84D11A54A881_H
#ifndef PKISTATUS_T6CE847C49FFAF660A821103EBB226325A5332276_H
#define PKISTATUS_T6CE847C49FFAF660A821103EBB226325A5332276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatus
struct  PkiStatus_t6CE847C49FFAF660A821103EBB226325A5332276 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PkiStatus_t6CE847C49FFAF660A821103EBB226325A5332276, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKISTATUS_T6CE847C49FFAF660A821103EBB226325A5332276_H
#ifndef DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#define DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerStringBase
struct  DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef DEFAULTLOGGER_T3FC688F69B121FF57A513DA60CEC3EF57404B08D_H
#define DEFAULTLOGGER_T3FC688F69B121FF57A513DA60CEC3EF57404B08D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Logger.DefaultLogger
struct  DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D  : public RuntimeObject
{
public:
	// BestHTTP.Logger.Loglevels BestHTTP.Logger.DefaultLogger::<Level>k__BackingField
	uint8_t ___U3CLevelU3Ek__BackingField_0;
	// System.String BestHTTP.Logger.DefaultLogger::<FormatVerbose>k__BackingField
	String_t* ___U3CFormatVerboseU3Ek__BackingField_1;
	// System.String BestHTTP.Logger.DefaultLogger::<FormatInfo>k__BackingField
	String_t* ___U3CFormatInfoU3Ek__BackingField_2;
	// System.String BestHTTP.Logger.DefaultLogger::<FormatWarn>k__BackingField
	String_t* ___U3CFormatWarnU3Ek__BackingField_3;
	// System.String BestHTTP.Logger.DefaultLogger::<FormatErr>k__BackingField
	String_t* ___U3CFormatErrU3Ek__BackingField_4;
	// System.String BestHTTP.Logger.DefaultLogger::<FormatEx>k__BackingField
	String_t* ___U3CFormatExU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CLevelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D, ___U3CLevelU3Ek__BackingField_0)); }
	inline uint8_t get_U3CLevelU3Ek__BackingField_0() const { return ___U3CLevelU3Ek__BackingField_0; }
	inline uint8_t* get_address_of_U3CLevelU3Ek__BackingField_0() { return &___U3CLevelU3Ek__BackingField_0; }
	inline void set_U3CLevelU3Ek__BackingField_0(uint8_t value)
	{
		___U3CLevelU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CFormatVerboseU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D, ___U3CFormatVerboseU3Ek__BackingField_1)); }
	inline String_t* get_U3CFormatVerboseU3Ek__BackingField_1() const { return ___U3CFormatVerboseU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFormatVerboseU3Ek__BackingField_1() { return &___U3CFormatVerboseU3Ek__BackingField_1; }
	inline void set_U3CFormatVerboseU3Ek__BackingField_1(String_t* value)
	{
		___U3CFormatVerboseU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormatVerboseU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CFormatInfoU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D, ___U3CFormatInfoU3Ek__BackingField_2)); }
	inline String_t* get_U3CFormatInfoU3Ek__BackingField_2() const { return ___U3CFormatInfoU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CFormatInfoU3Ek__BackingField_2() { return &___U3CFormatInfoU3Ek__BackingField_2; }
	inline void set_U3CFormatInfoU3Ek__BackingField_2(String_t* value)
	{
		___U3CFormatInfoU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormatInfoU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CFormatWarnU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D, ___U3CFormatWarnU3Ek__BackingField_3)); }
	inline String_t* get_U3CFormatWarnU3Ek__BackingField_3() const { return ___U3CFormatWarnU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFormatWarnU3Ek__BackingField_3() { return &___U3CFormatWarnU3Ek__BackingField_3; }
	inline void set_U3CFormatWarnU3Ek__BackingField_3(String_t* value)
	{
		___U3CFormatWarnU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormatWarnU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CFormatErrU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D, ___U3CFormatErrU3Ek__BackingField_4)); }
	inline String_t* get_U3CFormatErrU3Ek__BackingField_4() const { return ___U3CFormatErrU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CFormatErrU3Ek__BackingField_4() { return &___U3CFormatErrU3Ek__BackingField_4; }
	inline void set_U3CFormatErrU3Ek__BackingField_4(String_t* value)
	{
		___U3CFormatErrU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormatErrU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CFormatExU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D, ___U3CFormatExU3Ek__BackingField_5)); }
	inline String_t* get_U3CFormatExU3Ek__BackingField_5() const { return ___U3CFormatExU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CFormatExU3Ek__BackingField_5() { return &___U3CFormatExU3Ek__BackingField_5; }
	inline void set_U3CFormatExU3Ek__BackingField_5(String_t* value)
	{
		___U3CFormatExU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormatExU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTLOGGER_T3FC688F69B121FF57A513DA60CEC3EF57404B08D_H
#ifndef BUFFERPOOL_T5C495E52F4BDF1C3CD111672ED565E63B5916E50_H
#define BUFFERPOOL_T5C495E52F4BDF1C3CD111672ED565E63B5916E50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.Memory.BufferPool
struct  BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50  : public RuntimeObject
{
public:

public:
};

struct BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields
{
public:
	// System.Byte[] BestHTTP.PlatformSupport.Memory.BufferPool::NoData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___NoData_0;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.PlatformSupport.Memory.BufferPool::_isEnabled
	bool ____isEnabled_1;
	// System.TimeSpan BestHTTP.PlatformSupport.Memory.BufferPool::RemoveOlderThan
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___RemoveOlderThan_2;
	// System.TimeSpan BestHTTP.PlatformSupport.Memory.BufferPool::RunMaintenanceEvery
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___RunMaintenanceEvery_3;
	// System.Int64 BestHTTP.PlatformSupport.Memory.BufferPool::MinBufferSize
	int64_t ___MinBufferSize_4;
	// System.Int64 BestHTTP.PlatformSupport.Memory.BufferPool::MaxBufferSize
	int64_t ___MaxBufferSize_5;
	// System.Int64 BestHTTP.PlatformSupport.Memory.BufferPool::MaxPoolSize
	int64_t ___MaxPoolSize_6;
	// System.Boolean BestHTTP.PlatformSupport.Memory.BufferPool::RemoveEmptyLists
	bool ___RemoveEmptyLists_7;
	// System.Boolean BestHTTP.PlatformSupport.Memory.BufferPool::IsDoubleReleaseCheckEnabled
	bool ___IsDoubleReleaseCheckEnabled_8;
	// System.Collections.Generic.List`1<BestHTTP.PlatformSupport.Memory.BufferStore> BestHTTP.PlatformSupport.Memory.BufferPool::FreeBuffers
	List_1_t9933B44A0BC696C4070E4E1148A24563517BC43E * ___FreeBuffers_9;
	// System.DateTime BestHTTP.PlatformSupport.Memory.BufferPool::lastMaintenance
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastMaintenance_10;
	// System.Int64 BestHTTP.PlatformSupport.Memory.BufferPool::PoolSize
	int64_t ___PoolSize_11;
	// System.Int64 BestHTTP.PlatformSupport.Memory.BufferPool::GetBuffers
	int64_t ___GetBuffers_12;
	// System.Int64 BestHTTP.PlatformSupport.Memory.BufferPool::ReleaseBuffers
	int64_t ___ReleaseBuffers_13;
	// System.Text.StringBuilder BestHTTP.PlatformSupport.Memory.BufferPool::statiscticsBuilder
	StringBuilder_t * ___statiscticsBuilder_14;
	// System.Threading.ReaderWriterLockSlim BestHTTP.PlatformSupport.Memory.BufferPool::rwLock
	ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * ___rwLock_15;

public:
	inline static int32_t get_offset_of_NoData_0() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___NoData_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_NoData_0() const { return ___NoData_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_NoData_0() { return &___NoData_0; }
	inline void set_NoData_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___NoData_0 = value;
		Il2CppCodeGenWriteBarrier((&___NoData_0), value);
	}

	inline static int32_t get_offset_of__isEnabled_1() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ____isEnabled_1)); }
	inline bool get__isEnabled_1() const { return ____isEnabled_1; }
	inline bool* get_address_of__isEnabled_1() { return &____isEnabled_1; }
	inline void set__isEnabled_1(bool value)
	{
		____isEnabled_1 = value;
	}

	inline static int32_t get_offset_of_RemoveOlderThan_2() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___RemoveOlderThan_2)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_RemoveOlderThan_2() const { return ___RemoveOlderThan_2; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_RemoveOlderThan_2() { return &___RemoveOlderThan_2; }
	inline void set_RemoveOlderThan_2(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___RemoveOlderThan_2 = value;
	}

	inline static int32_t get_offset_of_RunMaintenanceEvery_3() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___RunMaintenanceEvery_3)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_RunMaintenanceEvery_3() const { return ___RunMaintenanceEvery_3; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_RunMaintenanceEvery_3() { return &___RunMaintenanceEvery_3; }
	inline void set_RunMaintenanceEvery_3(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___RunMaintenanceEvery_3 = value;
	}

	inline static int32_t get_offset_of_MinBufferSize_4() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___MinBufferSize_4)); }
	inline int64_t get_MinBufferSize_4() const { return ___MinBufferSize_4; }
	inline int64_t* get_address_of_MinBufferSize_4() { return &___MinBufferSize_4; }
	inline void set_MinBufferSize_4(int64_t value)
	{
		___MinBufferSize_4 = value;
	}

	inline static int32_t get_offset_of_MaxBufferSize_5() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___MaxBufferSize_5)); }
	inline int64_t get_MaxBufferSize_5() const { return ___MaxBufferSize_5; }
	inline int64_t* get_address_of_MaxBufferSize_5() { return &___MaxBufferSize_5; }
	inline void set_MaxBufferSize_5(int64_t value)
	{
		___MaxBufferSize_5 = value;
	}

	inline static int32_t get_offset_of_MaxPoolSize_6() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___MaxPoolSize_6)); }
	inline int64_t get_MaxPoolSize_6() const { return ___MaxPoolSize_6; }
	inline int64_t* get_address_of_MaxPoolSize_6() { return &___MaxPoolSize_6; }
	inline void set_MaxPoolSize_6(int64_t value)
	{
		___MaxPoolSize_6 = value;
	}

	inline static int32_t get_offset_of_RemoveEmptyLists_7() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___RemoveEmptyLists_7)); }
	inline bool get_RemoveEmptyLists_7() const { return ___RemoveEmptyLists_7; }
	inline bool* get_address_of_RemoveEmptyLists_7() { return &___RemoveEmptyLists_7; }
	inline void set_RemoveEmptyLists_7(bool value)
	{
		___RemoveEmptyLists_7 = value;
	}

	inline static int32_t get_offset_of_IsDoubleReleaseCheckEnabled_8() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___IsDoubleReleaseCheckEnabled_8)); }
	inline bool get_IsDoubleReleaseCheckEnabled_8() const { return ___IsDoubleReleaseCheckEnabled_8; }
	inline bool* get_address_of_IsDoubleReleaseCheckEnabled_8() { return &___IsDoubleReleaseCheckEnabled_8; }
	inline void set_IsDoubleReleaseCheckEnabled_8(bool value)
	{
		___IsDoubleReleaseCheckEnabled_8 = value;
	}

	inline static int32_t get_offset_of_FreeBuffers_9() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___FreeBuffers_9)); }
	inline List_1_t9933B44A0BC696C4070E4E1148A24563517BC43E * get_FreeBuffers_9() const { return ___FreeBuffers_9; }
	inline List_1_t9933B44A0BC696C4070E4E1148A24563517BC43E ** get_address_of_FreeBuffers_9() { return &___FreeBuffers_9; }
	inline void set_FreeBuffers_9(List_1_t9933B44A0BC696C4070E4E1148A24563517BC43E * value)
	{
		___FreeBuffers_9 = value;
		Il2CppCodeGenWriteBarrier((&___FreeBuffers_9), value);
	}

	inline static int32_t get_offset_of_lastMaintenance_10() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___lastMaintenance_10)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastMaintenance_10() const { return ___lastMaintenance_10; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastMaintenance_10() { return &___lastMaintenance_10; }
	inline void set_lastMaintenance_10(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastMaintenance_10 = value;
	}

	inline static int32_t get_offset_of_PoolSize_11() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___PoolSize_11)); }
	inline int64_t get_PoolSize_11() const { return ___PoolSize_11; }
	inline int64_t* get_address_of_PoolSize_11() { return &___PoolSize_11; }
	inline void set_PoolSize_11(int64_t value)
	{
		___PoolSize_11 = value;
	}

	inline static int32_t get_offset_of_GetBuffers_12() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___GetBuffers_12)); }
	inline int64_t get_GetBuffers_12() const { return ___GetBuffers_12; }
	inline int64_t* get_address_of_GetBuffers_12() { return &___GetBuffers_12; }
	inline void set_GetBuffers_12(int64_t value)
	{
		___GetBuffers_12 = value;
	}

	inline static int32_t get_offset_of_ReleaseBuffers_13() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___ReleaseBuffers_13)); }
	inline int64_t get_ReleaseBuffers_13() const { return ___ReleaseBuffers_13; }
	inline int64_t* get_address_of_ReleaseBuffers_13() { return &___ReleaseBuffers_13; }
	inline void set_ReleaseBuffers_13(int64_t value)
	{
		___ReleaseBuffers_13 = value;
	}

	inline static int32_t get_offset_of_statiscticsBuilder_14() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___statiscticsBuilder_14)); }
	inline StringBuilder_t * get_statiscticsBuilder_14() const { return ___statiscticsBuilder_14; }
	inline StringBuilder_t ** get_address_of_statiscticsBuilder_14() { return &___statiscticsBuilder_14; }
	inline void set_statiscticsBuilder_14(StringBuilder_t * value)
	{
		___statiscticsBuilder_14 = value;
		Il2CppCodeGenWriteBarrier((&___statiscticsBuilder_14), value);
	}

	inline static int32_t get_offset_of_rwLock_15() { return static_cast<int32_t>(offsetof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields, ___rwLock_15)); }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * get_rwLock_15() const { return ___rwLock_15; }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 ** get_address_of_rwLock_15() { return &___rwLock_15; }
	inline void set_rwLock_15(ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * value)
	{
		___rwLock_15 = value;
		Il2CppCodeGenWriteBarrier((&___rwLock_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERPOOL_T5C495E52F4BDF1C3CD111672ED565E63B5916E50_H
#ifndef TCPCLIENT_T3503711DD74B893F80B5BFDEEB3593CFBF520617_H
#define TCPCLIENT_T3503711DD74B893F80B5BFDEEB3593CFBF520617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.PlatformSupport.TcpClient.General.TcpClient
struct  TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617  : public RuntimeObject
{
public:
	// System.Net.Sockets.NetworkStream BestHTTP.PlatformSupport.TcpClient.General.TcpClient::stream
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * ___stream_0;
	// System.Boolean BestHTTP.PlatformSupport.TcpClient.General.TcpClient::active
	bool ___active_1;
	// System.Net.Sockets.Socket BestHTTP.PlatformSupport.TcpClient.General.TcpClient::client
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___client_2;
	// System.Boolean BestHTTP.PlatformSupport.TcpClient.General.TcpClient::disposed
	bool ___disposed_3;
	// BestHTTP.PlatformSupport.TcpClient.General.TcpClient_Properties BestHTTP.PlatformSupport.TcpClient.General.TcpClient::values
	uint32_t ___values_4;
	// System.Int32 BestHTTP.PlatformSupport.TcpClient.General.TcpClient::recv_timeout
	int32_t ___recv_timeout_5;
	// System.Int32 BestHTTP.PlatformSupport.TcpClient.General.TcpClient::send_timeout
	int32_t ___send_timeout_6;
	// System.Int32 BestHTTP.PlatformSupport.TcpClient.General.TcpClient::recv_buffer_size
	int32_t ___recv_buffer_size_7;
	// System.Int32 BestHTTP.PlatformSupport.TcpClient.General.TcpClient::send_buffer_size
	int32_t ___send_buffer_size_8;
	// System.Net.Sockets.LingerOption BestHTTP.PlatformSupport.TcpClient.General.TcpClient::linger_state
	LingerOption_tC6A8E9C30F48D9C07C38B2730012ECA6067723C7 * ___linger_state_9;
	// System.Boolean BestHTTP.PlatformSupport.TcpClient.General.TcpClient::no_delay
	bool ___no_delay_10;
	// System.TimeSpan BestHTTP.PlatformSupport.TcpClient.General.TcpClient::<ConnectTimeout>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CConnectTimeoutU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___stream_0)); }
	inline NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * get_stream_0() const { return ___stream_0; }
	inline NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}

	inline static int32_t get_offset_of_active_1() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___active_1)); }
	inline bool get_active_1() const { return ___active_1; }
	inline bool* get_address_of_active_1() { return &___active_1; }
	inline void set_active_1(bool value)
	{
		___active_1 = value;
	}

	inline static int32_t get_offset_of_client_2() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___client_2)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_client_2() const { return ___client_2; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_client_2() { return &___client_2; }
	inline void set_client_2(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___client_2 = value;
		Il2CppCodeGenWriteBarrier((&___client_2), value);
	}

	inline static int32_t get_offset_of_disposed_3() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___disposed_3)); }
	inline bool get_disposed_3() const { return ___disposed_3; }
	inline bool* get_address_of_disposed_3() { return &___disposed_3; }
	inline void set_disposed_3(bool value)
	{
		___disposed_3 = value;
	}

	inline static int32_t get_offset_of_values_4() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___values_4)); }
	inline uint32_t get_values_4() const { return ___values_4; }
	inline uint32_t* get_address_of_values_4() { return &___values_4; }
	inline void set_values_4(uint32_t value)
	{
		___values_4 = value;
	}

	inline static int32_t get_offset_of_recv_timeout_5() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___recv_timeout_5)); }
	inline int32_t get_recv_timeout_5() const { return ___recv_timeout_5; }
	inline int32_t* get_address_of_recv_timeout_5() { return &___recv_timeout_5; }
	inline void set_recv_timeout_5(int32_t value)
	{
		___recv_timeout_5 = value;
	}

	inline static int32_t get_offset_of_send_timeout_6() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___send_timeout_6)); }
	inline int32_t get_send_timeout_6() const { return ___send_timeout_6; }
	inline int32_t* get_address_of_send_timeout_6() { return &___send_timeout_6; }
	inline void set_send_timeout_6(int32_t value)
	{
		___send_timeout_6 = value;
	}

	inline static int32_t get_offset_of_recv_buffer_size_7() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___recv_buffer_size_7)); }
	inline int32_t get_recv_buffer_size_7() const { return ___recv_buffer_size_7; }
	inline int32_t* get_address_of_recv_buffer_size_7() { return &___recv_buffer_size_7; }
	inline void set_recv_buffer_size_7(int32_t value)
	{
		___recv_buffer_size_7 = value;
	}

	inline static int32_t get_offset_of_send_buffer_size_8() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___send_buffer_size_8)); }
	inline int32_t get_send_buffer_size_8() const { return ___send_buffer_size_8; }
	inline int32_t* get_address_of_send_buffer_size_8() { return &___send_buffer_size_8; }
	inline void set_send_buffer_size_8(int32_t value)
	{
		___send_buffer_size_8 = value;
	}

	inline static int32_t get_offset_of_linger_state_9() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___linger_state_9)); }
	inline LingerOption_tC6A8E9C30F48D9C07C38B2730012ECA6067723C7 * get_linger_state_9() const { return ___linger_state_9; }
	inline LingerOption_tC6A8E9C30F48D9C07C38B2730012ECA6067723C7 ** get_address_of_linger_state_9() { return &___linger_state_9; }
	inline void set_linger_state_9(LingerOption_tC6A8E9C30F48D9C07C38B2730012ECA6067723C7 * value)
	{
		___linger_state_9 = value;
		Il2CppCodeGenWriteBarrier((&___linger_state_9), value);
	}

	inline static int32_t get_offset_of_no_delay_10() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___no_delay_10)); }
	inline bool get_no_delay_10() const { return ___no_delay_10; }
	inline bool* get_address_of_no_delay_10() { return &___no_delay_10; }
	inline void set_no_delay_10(bool value)
	{
		___no_delay_10 = value;
	}

	inline static int32_t get_offset_of_U3CConnectTimeoutU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617, ___U3CConnectTimeoutU3Ek__BackingField_11)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CConnectTimeoutU3Ek__BackingField_11() const { return ___U3CConnectTimeoutU3Ek__BackingField_11; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CConnectTimeoutU3Ek__BackingField_11() { return &___U3CConnectTimeoutU3Ek__BackingField_11; }
	inline void set_U3CConnectTimeoutU3Ek__BackingField_11(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CConnectTimeoutU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TCPCLIENT_T3503711DD74B893F80B5BFDEEB3593CFBF520617_H
#ifndef DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#define DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString
struct  DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::mData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mData_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::mPadBits
	int32_t ___mPadBits_4;

public:
	inline static int32_t get_offset_of_mData_3() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374, ___mData_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mData_3() const { return ___mData_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mData_3() { return &___mData_3; }
	inline void set_mData_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mData_3 = value;
		Il2CppCodeGenWriteBarrier((&___mData_3), value);
	}

	inline static int32_t get_offset_of_mPadBits_4() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374, ___mPadBits_4)); }
	inline int32_t get_mPadBits_4() const { return ___mPadBits_4; }
	inline int32_t* get_address_of_mPadBits_4() { return &___mPadBits_4; }
	inline void set_mPadBits_4(int32_t value)
	{
		___mPadBits_4 = value;
	}
};

struct DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields
{
public:
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::table
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___table_2;

public:
	inline static int32_t get_offset_of_table_2() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields, ___table_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_table_2() const { return ___table_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_table_2() { return &___table_2; }
	inline void set_table_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___table_2 = value;
		Il2CppCodeGenWriteBarrier((&___table_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef FUTUREERRORCALLBACK_T813247C496FFA8B5A43BFF8A0884F53C7AE11C83_H
#define FUTUREERRORCALLBACK_T813247C496FFA8B5A43BFF8A0884F53C7AE11C83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Futures.FutureErrorCallback
struct  FutureErrorCallback_t813247C496FFA8B5A43BFF8A0884F53C7AE11C83  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUTUREERRORCALLBACK_T813247C496FFA8B5A43BFF8A0884F53C7AE11C83_H
#ifndef PKIFAILUREINFO_TE86D124ECE54E0164FD25B7CFF9973331A4D7DAF_H
#define PKIFAILUREINFO_TE86D124ECE54E0164FD25B7CFF9973331A4D7DAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiFailureInfo
struct  PkiFailureInfo_tE86D124ECE54E0164FD25B7CFF9973331A4D7DAF  : public DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIFAILUREINFO_TE86D124ECE54E0164FD25B7CFF9973331A4D7DAF_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5600 = { sizeof (OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5600[2] = 
{
	OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB::get_offset_of_keyAttrId_2(),
	OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB::get_offset_of_keyAttr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5601 = { sizeof (OtherRecipientInfo_tD315423F47920CE1A87332822006AAFB6AB66258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5601[2] = 
{
	OtherRecipientInfo_tD315423F47920CE1A87332822006AAFB6AB66258::get_offset_of_oriType_2(),
	OtherRecipientInfo_tD315423F47920CE1A87332822006AAFB6AB66258::get_offset_of_oriValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5602 = { sizeof (OtherRevocationInfoFormat_tB2AEFCBC863B0AD2100FEF5EA2E4E2E7AC330D03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5602[2] = 
{
	OtherRevocationInfoFormat_tB2AEFCBC863B0AD2100FEF5EA2E4E2E7AC330D03::get_offset_of_otherRevInfoFormat_2(),
	OtherRevocationInfoFormat_tB2AEFCBC863B0AD2100FEF5EA2E4E2E7AC330D03::get_offset_of_otherRevInfo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5603 = { sizeof (PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5603[4] = 
{
	PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B::get_offset_of_version_2(),
	PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B::get_offset_of_keyDerivationAlgorithm_3(),
	PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B::get_offset_of_keyEncryptionAlgorithm_4(),
	PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B::get_offset_of_encryptedKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5604 = { sizeof (RecipientEncryptedKey_t28F13449FA4B9B40F90FB7A27D607F3D1F45B922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5604[2] = 
{
	RecipientEncryptedKey_t28F13449FA4B9B40F90FB7A27D607F3D1F45B922::get_offset_of_identifier_2(),
	RecipientEncryptedKey_t28F13449FA4B9B40F90FB7A27D607F3D1F45B922::get_offset_of_encryptedKey_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5605 = { sizeof (RecipientIdentifier_t774592C8CB7234B60B03C10B73D4964324CCF570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5605[1] = 
{
	RecipientIdentifier_t774592C8CB7234B60B03C10B73D4964324CCF570::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5606 = { sizeof (RecipientInfo_t3D1639C31248DD8F5D062C66B4A11FD44BBAB7E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5606[1] = 
{
	RecipientInfo_t3D1639C31248DD8F5D062C66B4A11FD44BBAB7E5::get_offset_of_info_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5607 = { sizeof (RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5607[3] = 
{
	RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164::get_offset_of_subjectKeyIdentifier_2(),
	RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164::get_offset_of_date_3(),
	RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164::get_offset_of_other_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5608 = { sizeof (ScvpReqRes_t6912916152CB10DCA26166AF213870737393FBA0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5608[2] = 
{
	ScvpReqRes_t6912916152CB10DCA26166AF213870737393FBA0::get_offset_of_request_2(),
	ScvpReqRes_t6912916152CB10DCA26166AF213870737393FBA0::get_offset_of_response_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5609 = { sizeof (SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F), -1, sizeof(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5609[12] = 
{
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_StaticFields::get_offset_of_Version1_2(),
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_StaticFields::get_offset_of_Version3_3(),
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_StaticFields::get_offset_of_Version4_4(),
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F_StaticFields::get_offset_of_Version5_5(),
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F::get_offset_of_version_6(),
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F::get_offset_of_digestAlgorithms_7(),
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F::get_offset_of_contentInfo_8(),
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F::get_offset_of_certificates_9(),
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F::get_offset_of_crls_10(),
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F::get_offset_of_signerInfos_11(),
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F::get_offset_of_certsBer_12(),
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F::get_offset_of_crlsBer_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5610 = { sizeof (SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5610[5] = 
{
	SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8::get_offset_of__seq_0(),
	SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8::get_offset_of__version_1(),
	SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8::get_offset_of__nextObject_2(),
	SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8::get_offset_of__certsCalled_3(),
	SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8::get_offset_of__crlsCalled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5611 = { sizeof (SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5611[1] = 
{
	SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5612 = { sizeof (SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5612[7] = 
{
	SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23::get_offset_of_version_2(),
	SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23::get_offset_of_sid_3(),
	SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23::get_offset_of_digAlgorithm_4(),
	SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23::get_offset_of_authenticatedAttributes_5(),
	SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23::get_offset_of_digEncryptionAlgorithm_6(),
	SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23::get_offset_of_encryptedDigest_7(),
	SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23::get_offset_of_unauthenticatedAttributes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5613 = { sizeof (Time_t6318F5647228622221099EA986EA6ECB6C3C1ACD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5613[1] = 
{
	Time_t6318F5647228622221099EA986EA6ECB6C3C1ACD::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5614 = { sizeof (TimeStampAndCrl_tDC1C54343612EB603810766226B7FE9451AD18ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5614[2] = 
{
	TimeStampAndCrl_tDC1C54343612EB603810766226B7FE9451AD18ED::get_offset_of_timeStamp_2(),
	TimeStampAndCrl_tDC1C54343612EB603810766226B7FE9451AD18ED::get_offset_of_crl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5615 = { sizeof (TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5615[5] = 
{
	TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4::get_offset_of_version_2(),
	TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4::get_offset_of_dataUri_3(),
	TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4::get_offset_of_metaData_4(),
	TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4::get_offset_of_content_5(),
	TimeStampedData_tC74BB22D9FB98897EEE11FC9CF33419D8CABF7E4::get_offset_of_temporalEvidence_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5616 = { sizeof (TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5616[6] = 
{
	TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A::get_offset_of_version_0(),
	TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A::get_offset_of_dataUri_1(),
	TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A::get_offset_of_metaData_2(),
	TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A::get_offset_of_content_3(),
	TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A::get_offset_of_temporalEvidence_4(),
	TimeStampedDataParser_t855878AAD476054421C2BB066B0143910DDED62A::get_offset_of_parser_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5617 = { sizeof (TimeStampTokenEvidence_t20CF5580B1920E8F80264B9321AC3D1AF7A02B98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5617[1] = 
{
	TimeStampTokenEvidence_t20CF5580B1920E8F80264B9321AC3D1AF7A02B98::get_offset_of_timeStampAndCrls_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5618 = { sizeof (MQVuserKeyingMaterial_t7DC9FF450C45E93147682E732A2BB84DADB2E660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5618[2] = 
{
	MQVuserKeyingMaterial_t7DC9FF450C45E93147682E732A2BB84DADB2E660::get_offset_of_ephemeralPublicKey_2(),
	MQVuserKeyingMaterial_t7DC9FF450C45E93147682E732A2BB84DADB2E660::get_offset_of_addedukm_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5619 = { sizeof (CAKeyUpdAnnContent_t84841B75E28B88F78419072E0DBC00026CA23D49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5619[3] = 
{
	CAKeyUpdAnnContent_t84841B75E28B88F78419072E0DBC00026CA23D49::get_offset_of_oldWithNew_2(),
	CAKeyUpdAnnContent_t84841B75E28B88F78419072E0DBC00026CA23D49::get_offset_of_newWithOld_3(),
	CAKeyUpdAnnContent_t84841B75E28B88F78419072E0DBC00026CA23D49::get_offset_of_newWithNew_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5620 = { sizeof (CertConfirmContent_t393863F2F49912FCB5E9CCE73327E2AB411C2B83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5620[1] = 
{
	CertConfirmContent_t393863F2F49912FCB5E9CCE73327E2AB411C2B83::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5621 = { sizeof (CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5621[3] = 
{
	CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5::get_offset_of_certOrEncCert_2(),
	CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5::get_offset_of_privateKey_3(),
	CertifiedKeyPair_t7C343C382CF9004893101A8C0A6C85A61B56FCB5::get_offset_of_publicationInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5622 = { sizeof (CertOrEncCert_t7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5622[2] = 
{
	CertOrEncCert_t7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA::get_offset_of_certificate_2(),
	CertOrEncCert_t7206F98DA80FC22A2C422BFEFDA9E3F2F2AD31DA::get_offset_of_encryptedCert_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5623 = { sizeof (CertRepMessage_t49FE21416545959EAF1AFA328D06E40071A10D1E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5623[2] = 
{
	CertRepMessage_t49FE21416545959EAF1AFA328D06E40071A10D1E::get_offset_of_caPubs_2(),
	CertRepMessage_t49FE21416545959EAF1AFA328D06E40071A10D1E::get_offset_of_response_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5624 = { sizeof (CertResponse_t31494006AAC46DF20936319DB715202F398DCCFE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5624[4] = 
{
	CertResponse_t31494006AAC46DF20936319DB715202F398DCCFE::get_offset_of_certReqId_2(),
	CertResponse_t31494006AAC46DF20936319DB715202F398DCCFE::get_offset_of_status_3(),
	CertResponse_t31494006AAC46DF20936319DB715202F398DCCFE::get_offset_of_certifiedKeyPair_4(),
	CertResponse_t31494006AAC46DF20936319DB715202F398DCCFE::get_offset_of_rspInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5625 = { sizeof (CertStatus_tE3FE27FF381A20391E4472652A50F600BA0DD5D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5625[3] = 
{
	CertStatus_tE3FE27FF381A20391E4472652A50F600BA0DD5D4::get_offset_of_certHash_2(),
	CertStatus_tE3FE27FF381A20391E4472652A50F600BA0DD5D4::get_offset_of_certReqId_3(),
	CertStatus_tE3FE27FF381A20391E4472652A50F600BA0DD5D4::get_offset_of_statusInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5626 = { sizeof (Challenge_tFF1EEA473061F8BB04DB89F4A6DE63A677C87593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5626[3] = 
{
	Challenge_tFF1EEA473061F8BB04DB89F4A6DE63A677C87593::get_offset_of_owf_2(),
	Challenge_tFF1EEA473061F8BB04DB89F4A6DE63A677C87593::get_offset_of_witness_3(),
	Challenge_tFF1EEA473061F8BB04DB89F4A6DE63A677C87593::get_offset_of_challenge_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5627 = { sizeof (CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5627[2] = 
{
	CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22::get_offset_of_x509v3PKCert_2(),
	CmpCertificate_t567BF8C185B1FC478CDD58C7B7A8FD09AD6F9C22::get_offset_of_x509v2AttrCert_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5628 = { sizeof (CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E), -1, sizeof(CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5628[26] = 
{
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_passwordBasedMac_0(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_dhBasedMac_1(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_caProtEncCert_2(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_signKeyPairTypes_3(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_encKeyPairTypes_4(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_preferredSymAlg_5(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_caKeyUpdateInfo_6(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_currentCRL_7(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_unsupportedOIDs_8(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_keyPairParamReq_9(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_keyPairParamRep_10(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_revPassphrase_11(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_implicitConfirm_12(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_confirmWaitTime_13(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_origPKIMessage_14(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_it_suppLangTags_15(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_regCtrl_regToken_16(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_regCtrl_authenticator_17(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_regCtrl_pkiPublicationInfo_18(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_regCtrl_pkiArchiveOptions_19(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_regCtrl_oldCertID_20(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_regCtrl_protocolEncrKey_21(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_regCtrl_altCertTemplate_22(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_regInfo_utf8Pairs_23(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_regInfo_certReq_24(),
	CmpObjectIdentifiers_t54E2668FFB58249273B5B953BDFC3ED62DE0B49E_StaticFields::get_offset_of_ct_encKeyWithID_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5629 = { sizeof (CrlAnnContent_tD607DF865E9C64F1B8A6537D9E673E06C11DC14F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5629[1] = 
{
	CrlAnnContent_tD607DF865E9C64F1B8A6537D9E673E06C11DC14F::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5630 = { sizeof (ErrorMsgContent_tC6EA36BA879EDB7CA0A2275F14D4F7297393A91C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5630[3] = 
{
	ErrorMsgContent_tC6EA36BA879EDB7CA0A2275F14D4F7297393A91C::get_offset_of_pkiStatusInfo_2(),
	ErrorMsgContent_tC6EA36BA879EDB7CA0A2275F14D4F7297393A91C::get_offset_of_errorCode_3(),
	ErrorMsgContent_tC6EA36BA879EDB7CA0A2275F14D4F7297393A91C::get_offset_of_errorDetails_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5631 = { sizeof (GenMsgContent_t0110D8F8FF6DAFA63073FA855C76FD33401AF10F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5631[1] = 
{
	GenMsgContent_t0110D8F8FF6DAFA63073FA855C76FD33401AF10F::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5632 = { sizeof (GenRepContent_t9FB18397725BD7F51EEE4777E6BAC30F10650D3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5632[1] = 
{
	GenRepContent_t9FB18397725BD7F51EEE4777E6BAC30F10650D3A::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5633 = { sizeof (InfoTypeAndValue_t04D46F4997E012F7FE48522DA9D6982295A95649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5633[2] = 
{
	InfoTypeAndValue_t04D46F4997E012F7FE48522DA9D6982295A95649::get_offset_of_infoType_2(),
	InfoTypeAndValue_t04D46F4997E012F7FE48522DA9D6982295A95649::get_offset_of_infoValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5634 = { sizeof (KeyRecRepContent_tC6C4A2CDECDC0149A0DE96FECB95A518AC937625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5634[4] = 
{
	KeyRecRepContent_tC6C4A2CDECDC0149A0DE96FECB95A518AC937625::get_offset_of_status_2(),
	KeyRecRepContent_tC6C4A2CDECDC0149A0DE96FECB95A518AC937625::get_offset_of_newSigCert_3(),
	KeyRecRepContent_tC6C4A2CDECDC0149A0DE96FECB95A518AC937625::get_offset_of_caCerts_4(),
	KeyRecRepContent_tC6C4A2CDECDC0149A0DE96FECB95A518AC937625::get_offset_of_keyPairHist_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5635 = { sizeof (OobCertHash_tA91F72629862ED56AB6071A519F8E1F8B4755F02), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5635[3] = 
{
	OobCertHash_tA91F72629862ED56AB6071A519F8E1F8B4755F02::get_offset_of_hashAlg_2(),
	OobCertHash_tA91F72629862ED56AB6071A519F8E1F8B4755F02::get_offset_of_certId_3(),
	OobCertHash_tA91F72629862ED56AB6071A519F8E1F8B4755F02::get_offset_of_hashVal_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5636 = { sizeof (PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5636[4] = 
{
	PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504::get_offset_of_salt_2(),
	PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504::get_offset_of_owf_3(),
	PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504::get_offset_of_iterationCount_4(),
	PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504::get_offset_of_mac_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5637 = { sizeof (PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5637[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89::get_offset_of_tagNo_29(),
	PkiBody_t8876624FD206FAE66D313ECB96C4D17DBE291A89::get_offset_of_body_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5638 = { sizeof (PkiConfirmContent_t55926636670DC657E33D2E40C3B30A7C0196F46E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5639 = { sizeof (PkiFailureInfo_tE86D124ECE54E0164FD25B7CFF9973331A4D7DAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5639[27] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5640 = { sizeof (PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5640[1] = 
{
	PkiFreeText_tBEC9B120E31A4AF486EE92D0A93C4D9FF41A2B2D::get_offset_of_strings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5641 = { sizeof (PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0), -1, sizeof(PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5641[15] = 
{
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0_StaticFields::get_offset_of_NULL_NAME_2(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0_StaticFields::get_offset_of_CMP_1999_3(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0_StaticFields::get_offset_of_CMP_2000_4(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_pvno_5(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_sender_6(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_recipient_7(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_messageTime_8(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_protectionAlg_9(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_senderKID_10(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_recipKID_11(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_transactionID_12(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_senderNonce_13(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_recipNonce_14(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_freeText_15(),
	PkiHeader_t3A26AFDB95FB16095589D59348E238CBF38821D0::get_offset_of_generalInfo_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5642 = { sizeof (PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5642[12] = 
{
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_pvno_0(),
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_sender_1(),
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_recipient_2(),
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_messageTime_3(),
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_protectionAlg_4(),
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_senderKID_5(),
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_recipKID_6(),
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_transactionID_7(),
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_senderNonce_8(),
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_recipNonce_9(),
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_freeText_10(),
	PkiHeaderBuilder_t8416BB313C8738F174DDDA67BFA4C09F4B8798D8::get_offset_of_generalInfo_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5643 = { sizeof (PkiMessage_t967F8D2C19DAC941842C357A75B16F0DE8E146F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5643[4] = 
{
	PkiMessage_t967F8D2C19DAC941842C357A75B16F0DE8E146F4::get_offset_of_header_2(),
	PkiMessage_t967F8D2C19DAC941842C357A75B16F0DE8E146F4::get_offset_of_body_3(),
	PkiMessage_t967F8D2C19DAC941842C357A75B16F0DE8E146F4::get_offset_of_protection_4(),
	PkiMessage_t967F8D2C19DAC941842C357A75B16F0DE8E146F4::get_offset_of_extraCerts_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5644 = { sizeof (PkiMessages_tABE46E886E9C1119F780D72A50406787A5EAF216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5644[1] = 
{
	PkiMessages_tABE46E886E9C1119F780D72A50406787A5EAF216::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5645 = { sizeof (PkiStatus_t6CE847C49FFAF660A821103EBB226325A5332276)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5645[8] = 
{
	PkiStatus_t6CE847C49FFAF660A821103EBB226325A5332276::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5646 = { sizeof (PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100), -1, sizeof(PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5646[8] = 
{
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields::get_offset_of_granted_2(),
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields::get_offset_of_grantedWithMods_3(),
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields::get_offset_of_rejection_4(),
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields::get_offset_of_waiting_5(),
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields::get_offset_of_revocationWarning_6(),
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields::get_offset_of_revocationNotification_7(),
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100_StaticFields::get_offset_of_keyUpdateWaiting_8(),
	PkiStatusEncodable_t91EFDE2752AE3A66F2F5B002BE800E8478FF1100::get_offset_of_status_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5647 = { sizeof (PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5647[3] = 
{
	PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E::get_offset_of_status_2(),
	PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E::get_offset_of_statusString_3(),
	PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E::get_offset_of_failInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5648 = { sizeof (PollRepContent_t3891E9F9658D1B12B6436DDBC3214FBE6D1993BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5648[3] = 
{
	PollRepContent_t3891E9F9658D1B12B6436DDBC3214FBE6D1993BA::get_offset_of_certReqId_2(),
	PollRepContent_t3891E9F9658D1B12B6436DDBC3214FBE6D1993BA::get_offset_of_checkAfter_3(),
	PollRepContent_t3891E9F9658D1B12B6436DDBC3214FBE6D1993BA::get_offset_of_reason_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5649 = { sizeof (PollReqContent_t9B2D21F83F118B36596B19E367FF4992A9B7B3B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5649[1] = 
{
	PollReqContent_t9B2D21F83F118B36596B19E367FF4992A9B7B3B5::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5650 = { sizeof (PopoDecKeyChallContent_t38663B40255D5090EE13108EFE526EAD57769D8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5650[1] = 
{
	PopoDecKeyChallContent_t38663B40255D5090EE13108EFE526EAD57769D8C::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5651 = { sizeof (PopoDecKeyRespContent_tBD490241E1A1518E5709E8D8A6921BDB5B52478A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5651[1] = 
{
	PopoDecKeyRespContent_tBD490241E1A1518E5709E8D8A6921BDB5B52478A::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5652 = { sizeof (ProtectedPart_t722EBC5C3DA1750E566F1B85F4E92A6DE9779EC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5652[2] = 
{
	ProtectedPart_t722EBC5C3DA1750E566F1B85F4E92A6DE9779EC6::get_offset_of_header_2(),
	ProtectedPart_t722EBC5C3DA1750E566F1B85F4E92A6DE9779EC6::get_offset_of_body_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5653 = { sizeof (RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5653[5] = 
{
	RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A::get_offset_of_status_2(),
	RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A::get_offset_of_certId_3(),
	RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A::get_offset_of_willBeRevokedAt_4(),
	RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A::get_offset_of_badSinceDate_5(),
	RevAnnContent_t039556C6478DC57D48ED74B5337771AC70212B9A::get_offset_of_crlDetails_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5654 = { sizeof (RevDetails_t7273B946D1D2C61B9EA8A1AE9A0205E17AFE63A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5654[2] = 
{
	RevDetails_t7273B946D1D2C61B9EA8A1AE9A0205E17AFE63A3::get_offset_of_certDetails_2(),
	RevDetails_t7273B946D1D2C61B9EA8A1AE9A0205E17AFE63A3::get_offset_of_crlEntryDetails_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5655 = { sizeof (RevRepContent_tA5363D4A7716CE0927BB84356FBB334A4DB0FE93), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5655[3] = 
{
	RevRepContent_tA5363D4A7716CE0927BB84356FBB334A4DB0FE93::get_offset_of_status_2(),
	RevRepContent_tA5363D4A7716CE0927BB84356FBB334A4DB0FE93::get_offset_of_revCerts_3(),
	RevRepContent_tA5363D4A7716CE0927BB84356FBB334A4DB0FE93::get_offset_of_crls_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5656 = { sizeof (RevRepContentBuilder_t97214484D6610706A1842C4B40187E605E7D7955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5656[3] = 
{
	RevRepContentBuilder_t97214484D6610706A1842C4B40187E605E7D7955::get_offset_of_status_0(),
	RevRepContentBuilder_t97214484D6610706A1842C4B40187E605E7D7955::get_offset_of_revCerts_1(),
	RevRepContentBuilder_t97214484D6610706A1842C4B40187E605E7D7955::get_offset_of_crls_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5657 = { sizeof (RevReqContent_tE0C577C5435FD1AF2ED1E0463A75ECAC7F36C28D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5657[1] = 
{
	RevReqContent_tE0C577C5435FD1AF2ED1E0463A75ECAC7F36C28D::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5658 = { sizeof (BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866), -1, sizeof(BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5658[23] = 
{
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_bsi_de_0(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_id_ecc_1(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecdsa_plain_signatures_2(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecdsa_plain_SHA1_3(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecdsa_plain_SHA224_4(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecdsa_plain_SHA256_5(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecdsa_plain_SHA384_6(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecdsa_plain_SHA512_7(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecdsa_plain_RIPEMD160_8(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_algorithm_9(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_10(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_X963kdf_11(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_X963kdf_SHA1_12(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_X963kdf_SHA224_13(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_X963kdf_SHA256_14(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_X963kdf_SHA384_15(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_X963kdf_SHA512_16(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_X963kdf_RIPEMD160_17(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_SessionKDF_18(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_SessionKDF_3DES_19(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_SessionKDF_AES128_20(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_SessionKDF_AES192_21(),
	BsiObjectIdentifiers_tAD93234865D9F12245F1DC377E8364C2451E4866_StaticFields::get_offset_of_ecka_eg_SessionKDF_AES256_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5659 = { sizeof (BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E), -1, sizeof(BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5659[36] = 
{
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_0(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_1(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha1_2(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha256_3(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha384_4(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha512_5(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha224_6(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha1_pkcs5_7(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha1_pkcs12_8(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha256_pkcs5_9(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha256_pkcs12_10(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha1_pkcs12_aes128_cbc_11(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha1_pkcs12_aes192_cbc_12(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha1_pkcs12_aes256_cbc_13(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha256_pkcs12_aes128_cbc_14(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha256_pkcs12_aes192_cbc_15(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_pbe_sha256_pkcs12_aes256_cbc_16(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_sig_17(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_sphincs256_18(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_sphincs256_with_BLAKE512_19(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_sphincs256_with_SHA512_20(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_sphincs256_with_SHA3_512_21(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_xmss_22(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_xmss_with_SHA256_23(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_xmss_with_SHA512_24(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_xmss_with_SHAKE128_25(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_xmss_with_SHAKE256_26(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_xmss_mt_27(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_xmss_mt_with_SHA256_28(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_xmss_mt_with_SHA512_29(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_xmss_mt_with_SHAKE128_30(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_xmss_mt_with_SHAKE256_31(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_exch_32(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_newHope_33(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_bc_ext_34(),
	BCObjectIdentifiers_tBD0133B809D97EB246678D2384E81C37B4E0829E_StaticFields::get_offset_of_linkedCertificate_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5660 = { sizeof (LinkedCertificate_tFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5660[4] = 
{
	LinkedCertificate_tFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B::get_offset_of_mDigest_2(),
	LinkedCertificate_tFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B::get_offset_of_mCertLocation_3(),
	LinkedCertificate_tFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B::get_offset_of_mCertIssuer_4(),
	LinkedCertificate_tFCE5C0F32B6CDEDA62E62B2ADA5E48AC0412131B::get_offset_of_mCACerts_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5661 = { sizeof (AnssiNamedCurves_t08E79A91F495D6C67ACACB026701848EE1DC068F), -1, sizeof(AnssiNamedCurves_t08E79A91F495D6C67ACACB026701848EE1DC068F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5661[3] = 
{
	AnssiNamedCurves_t08E79A91F495D6C67ACACB026701848EE1DC068F_StaticFields::get_offset_of_objIds_0(),
	AnssiNamedCurves_t08E79A91F495D6C67ACACB026701848EE1DC068F_StaticFields::get_offset_of_curves_1(),
	AnssiNamedCurves_t08E79A91F495D6C67ACACB026701848EE1DC068F_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5662 = { sizeof (Frp256v1Holder_t483F89477F3215B64DBF54FF7F76E1993A856CEA), -1, sizeof(Frp256v1Holder_t483F89477F3215B64DBF54FF7F76E1993A856CEA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5662[1] = 
{
	Frp256v1Holder_t483F89477F3215B64DBF54FF7F76E1993A856CEA_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5663 = { sizeof (AnssiObjectIdentifiers_tC0B69E7BE4ACE5C0CDE87079A28DB68D3EC55B96), -1, sizeof(AnssiObjectIdentifiers_tC0B69E7BE4ACE5C0CDE87079A28DB68D3EC55B96_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5663[1] = 
{
	AnssiObjectIdentifiers_tC0B69E7BE4ACE5C0CDE87079A28DB68D3EC55B96_StaticFields::get_offset_of_FRP256v1_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5664 = { sizeof (ThreadedRunner_tCB8A3AC54158CC5E982493914F21D804C39B4819), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5665 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5665[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5666 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5666[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5667 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5667[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5668 = { sizeof (U3CU3Ec__DisplayClass3_0_t84E818B624F332BBC7C4EA6C0F3671FF8F9B8617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5668[1] = 
{
	U3CU3Ec__DisplayClass3_0_t84E818B624F332BBC7C4EA6C0F3671FF8F9B8617::get_offset_of_job_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5669 = { sizeof (U3CU3Ec__DisplayClass4_0_tD31C8D5F1828F6B02D2B1EC9356CD85669FE4E0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5669[1] = 
{
	U3CU3Ec__DisplayClass4_0_tD31C8D5F1828F6B02D2B1EC9356CD85669FE4E0A::get_offset_of_job_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5670 = { sizeof (TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5670[12] = 
{
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_stream_0(),
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_active_1(),
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_client_2(),
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_disposed_3(),
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_values_4(),
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_recv_timeout_5(),
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_send_timeout_6(),
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_recv_buffer_size_7(),
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_send_buffer_size_8(),
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_linger_state_9(),
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_no_delay_10(),
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617::get_offset_of_U3CConnectTimeoutU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5671 = { sizeof (Properties_tBCE5319F4966D1423D1D27661B9D84D11A54A881)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5671[7] = 
{
	Properties_tBCE5319F4966D1423D1D27661B9D84D11A54A881::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5672 = { sizeof (U3CU3Ec__DisplayClass55_0_t73216E9A10FD4E95DA16CDD4DFF1924644BD2694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5672[1] = 
{
	U3CU3Ec__DisplayClass55_0_t73216E9A10FD4E95DA16CDD4DFF1924644BD2694::get_offset_of_mre_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5673 = { sizeof (U3CU3Ec__DisplayClass58_0_t0F1818FB4BDC3D89ECCC6185DFDCDBA5A462CB45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5673[1] = 
{
	U3CU3Ec__DisplayClass58_0_t0F1818FB4BDC3D89ECCC6185DFDCDBA5A462CB45::get_offset_of_mre_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5674 = { sizeof (BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F)+ sizeof (RuntimeObject), sizeof(BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F ), sizeof(BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5674[4] = 
{
	BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F_StaticFields::get_offset_of_Empty_0(),
	BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F::get_offset_of_Data_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F::get_offset_of_Offset_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BufferSegment_tC5D6E916DD1242E574031AB1CCFC629E3498907F::get_offset_of_Count_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5675 = { sizeof (PooledBuffer_t70463B01A10C07C0778C64900896B87AF9A0DB22)+ sizeof (RuntimeObject), sizeof(PooledBuffer_t70463B01A10C07C0778C64900896B87AF9A0DB22 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5675[2] = 
{
	PooledBuffer_t70463B01A10C07C0778C64900896B87AF9A0DB22::get_offset_of_Data_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PooledBuffer_t70463B01A10C07C0778C64900896B87AF9A0DB22::get_offset_of_Length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5676 = { sizeof (BufferStore_tB32CE48D79640ACF5CE5F804348EF77C6CDC1584)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5676[2] = 
{
	BufferStore_tB32CE48D79640ACF5CE5F804348EF77C6CDC1584::get_offset_of_Size_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BufferStore_tB32CE48D79640ACF5CE5F804348EF77C6CDC1584::get_offset_of_buffers_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5677 = { sizeof (BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388)+ sizeof (RuntimeObject), -1, sizeof(BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5677[3] = 
{
	BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388_StaticFields::get_offset_of_Empty_0(),
	BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388::get_offset_of_buffer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BufferDesc_t3A4703231E897D91A50EFE099A5F824D7B05A388::get_offset_of_released_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5678 = { sizeof (BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50), -1, sizeof(BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5678[16] = 
{
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_NoData_0(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of__isEnabled_1(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_RemoveOlderThan_2(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_RunMaintenanceEvery_3(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_MinBufferSize_4(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_MaxBufferSize_5(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_MaxPoolSize_6(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_RemoveEmptyLists_7(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_IsDoubleReleaseCheckEnabled_8(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_FreeBuffers_9(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_lastMaintenance_10(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_PoolSize_11(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_GetBuffers_12(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_ReleaseBuffers_13(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_statiscticsBuilder_14(),
	BufferPool_t5C495E52F4BDF1C3CD111672ED565E63B5916E50_StaticFields::get_offset_of_rwLock_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679 = { sizeof (DefaultIOService_t0BAFDF604B0344628BA3D68470F4DBA32DD8BF6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680 = { sizeof (FileStreamModes_t0C999A762FA85A5F073D8A091CAC093EC07A44BE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5680[4] = 
{
	FileStreamModes_t0C999A762FA85A5F073D8A091CAC093EC07A44BE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682 = { sizeof (DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5682[6] = 
{
	DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D::get_offset_of_U3CLevelU3Ek__BackingField_0(),
	DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D::get_offset_of_U3CFormatVerboseU3Ek__BackingField_1(),
	DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D::get_offset_of_U3CFormatInfoU3Ek__BackingField_2(),
	DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D::get_offset_of_U3CFormatWarnU3Ek__BackingField_3(),
	DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D::get_offset_of_U3CFormatErrU3Ek__BackingField_4(),
	DefaultLogger_t3FC688F69B121FF57A513DA60CEC3EF57404B08D::get_offset_of_U3CFormatExU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683 = { sizeof (Loglevels_tF6A6E8E20D301C844D62B193BDC1C47162A5A033)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5683[7] = 
{
	Loglevels_tF6A6E8E20D301C844D62B193BDC1C47162A5A033::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685 = { sizeof (Json_t9AFDDCC782A208C5DB6BB6B24029377E4098DD6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5685[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686 = { sizeof (HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5686[6] = 
{
	HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41::get_offset_of_U3CNameU3Ek__BackingField_0(),
	HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41::get_offset_of_U3CFileNameU3Ek__BackingField_1(),
	HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41::get_offset_of_U3CMimeTypeU3Ek__BackingField_2(),
	HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41::get_offset_of_U3CEncodingU3Ek__BackingField_3(),
	HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41::get_offset_of_U3CTextU3Ek__BackingField_4(),
	HTTPFieldData_t94B41FD45B6B2D78BB6D3A9C9813108249662F41::get_offset_of_U3CBinaryU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687 = { sizeof (HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5687[5] = 
{
	0,
	HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21::get_offset_of_U3CFieldsU3Ek__BackingField_1(),
	HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21::get_offset_of_U3CIsChangedU3Ek__BackingField_2(),
	HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21::get_offset_of_U3CHasBinaryU3Ek__BackingField_3(),
	HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21::get_offset_of_U3CHasLongValueU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688 = { sizeof (HTTPFormUsage_tA7C75083C6FD2D3A73090E99E6C03C1645632CDB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5688[4] = 
{
	HTTPFormUsage_tA7C75083C6FD2D3A73090E99E6C03C1645632CDB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689 = { sizeof (HTTPMultiPartForm_t16DCB285C2F3FB6094BE80C048DE85F47798F0F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5689[2] = 
{
	HTTPMultiPartForm_t16DCB285C2F3FB6094BE80C048DE85F47798F0F7::get_offset_of_Boundary_5(),
	HTTPMultiPartForm_t16DCB285C2F3FB6094BE80C048DE85F47798F0F7::get_offset_of_CachedData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690 = { sizeof (HTTPUrlEncodedForm_tD9FF747E110F1C48313F8FC20A6F843F2F587610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5690[2] = 
{
	0,
	HTTPUrlEncodedForm_tD9FF747E110F1C48313F8FC20A6F843F2F587610::get_offset_of_CachedData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691 = { sizeof (FutureState_tDC94D397E4F5657A1DC2385C02B3CE193E54E203)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5691[5] = 
{
	FutureState_tDC94D397E4F5657A1DC2385C02B3CE193E54E203::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695 = { sizeof (FutureErrorCallback_t813247C496FFA8B5A43BFF8A0884F53C7AE11C83), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5696[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697 = { sizeof (BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5697[10] = 
{
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC::get_offset_of_canWrite_5(),
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC::get_offset_of_allowGetBuffer_6(),
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC::get_offset_of_capacity_7(),
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC::get_offset_of_length_8(),
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC::get_offset_of_internalBuffer_9(),
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC::get_offset_of_initialIndex_10(),
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC::get_offset_of_expandable_11(),
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC::get_offset_of_streamClosed_12(),
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC::get_offset_of_position_13(),
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC::get_offset_of_dirty_bytes_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698 = { sizeof (BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5698[3] = 
{
	BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD::get_offset_of__length_5(),
	BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD::get_offset_of_bufferList_6(),
	BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD::get_offset_of_subBufferPosition_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5699[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
