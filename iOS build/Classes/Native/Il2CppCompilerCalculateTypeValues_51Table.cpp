﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable[]
struct Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString
struct Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence
struct Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set
struct Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser
struct Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetStringGenerator
struct BerOctetStringGenerator_tF1BB093CD2F03074CFDD7EC178499D2204B259F6;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator
struct BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable
struct AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfoParser
struct ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.IssuerAndSerialNumber
struct IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekIdentifier
struct KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekRecipientInfo
struct KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientInfo
struct KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyTransRecipientInfo
struct KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorInfo
struct OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.PasswordRecipientInfo
struct PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedDataParser
struct SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerIdentifier
struct SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerInfo
struct SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated[]
struct DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger
struct DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerOutputStream
struct DerOutputStream_tF14E89982B98990995F8610F800B024DD05ADFA1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo
struct SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure
struct TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name
struct X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerator
struct CmsAttributeTableGenerator_tA57B0778C2607AADC1C81E2F4F0B61F4C381650E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper
struct CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessable
struct CmsProcessable_tEBECBCD44AF58A497E35912A7F736AE7B740A3A5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSecureReadable
struct CmsSecureReadable_t30E58515DB98B90C62A8EBF27D1F604A29ADDEEC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator
struct CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator
struct CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper
struct CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsTypedStream
struct CmsTypedStream_t3C034672F2E5D2B8EDF5618BDE3E4DA90F540591;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.IDigestCalculator
struct IDigestCalculator_t2FDACF3BE75F8A844C9F00C05E68282DEECD0A9C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.ISignerInfoGenerator
struct ISignerInfoGenerator_t6108780E29B02A31D5A6D97B9C13AC68E726B6E1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientID
struct RecipientID_t25DA4BBF07A25C6047B6350910E7FCA460AE920B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerID
struct SignerID_tC9728986CC0E7C53B1C2C2DA90EB09DE973E0B15;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformationStore
struct SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair
struct AsymmetricCipherKeyPair_t4630D28256FA535784C97BBD4D410AB0DC19FC24;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISignatureFactory
struct ISignatureFactory_t1ABF092647E490BD98850BAF920E0041862DCB2A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISigner
struct ISigner_tB728365C47647B1E736C0C715DACEBA9341F6CDD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter
struct KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet
struct ISet_t585F03B00DBE773170901D2ABF9576187FAF92B5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject
struct DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Store
struct IX509Store_tAAF2E4C85341CF672C0779CD427B53EC4FFBED9C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate
struct X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerable
struct IEnumerable_tD74549CEA1AA48E768382B94FEACBB07E2E3FA2C;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#define ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct  Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifndef ASN1ENCODABLEVECTOR_T6C604FA3421FAF2C8EA0E464C76DFB4773682509_H
#define ASN1ENCODABLEVECTOR_T6C604FA3421FAF2C8EA0E464C76DFB4773682509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector
struct  Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector::elements
	Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* ___elements_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector::elementCount
	int32_t ___elementCount_3;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector::copyOnWrite
	bool ___copyOnWrite_4;

public:
	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509, ___elements_2)); }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* get_elements_2() const { return ___elements_2; }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}

	inline static int32_t get_offset_of_elementCount_3() { return static_cast<int32_t>(offsetof(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509, ___elementCount_3)); }
	inline int32_t get_elementCount_3() const { return ___elementCount_3; }
	inline int32_t* get_address_of_elementCount_3() { return &___elementCount_3; }
	inline void set_elementCount_3(int32_t value)
	{
		___elementCount_3 = value;
	}

	inline static int32_t get_offset_of_copyOnWrite_4() { return static_cast<int32_t>(offsetof(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509, ___copyOnWrite_4)); }
	inline bool get_copyOnWrite_4() const { return ___copyOnWrite_4; }
	inline bool* get_address_of_copyOnWrite_4() { return &___copyOnWrite_4; }
	inline void set_copyOnWrite_4(bool value)
	{
		___copyOnWrite_4 = value;
	}
};

struct Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector::EmptyElements
	Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* ___EmptyElements_0;

public:
	inline static int32_t get_offset_of_EmptyElements_0() { return static_cast<int32_t>(offsetof(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509_StaticFields, ___EmptyElements_0)); }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* get_EmptyElements_0() const { return ___EmptyElements_0; }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64** get_address_of_EmptyElements_0() { return &___EmptyElements_0; }
	inline void set_EmptyElements_0(Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* value)
	{
		___EmptyElements_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyElements_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLEVECTOR_T6C604FA3421FAF2C8EA0E464C76DFB4773682509_H
#ifndef ASN1GENERATOR_T56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A_H
#define ASN1GENERATOR_T56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Generator
struct  Asn1Generator_t56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A  : public RuntimeObject
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Generator::_out
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____out_0;

public:
	inline static int32_t get_offset_of__out_0() { return static_cast<int32_t>(offsetof(Asn1Generator_t56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A, ____out_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__out_0() const { return ____out_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__out_0() { return &____out_0; }
	inline void set__out_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____out_0 = value;
		Il2CppCodeGenWriteBarrier((&____out_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1GENERATOR_T56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A_H
#ifndef ASN1SEQUENCEPARSERIMPL_T1F76546A6B67A6D3F54BE558EE072A781F49489F_H
#define ASN1SEQUENCEPARSERIMPL_T1F76546A6B67A6D3F54BE558EE072A781F49489F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence_Asn1SequenceParserImpl
struct  Asn1SequenceParserImpl_t1F76546A6B67A6D3F54BE558EE072A781F49489F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence_Asn1SequenceParserImpl::outer
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___outer_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence_Asn1SequenceParserImpl::max
	int32_t ___max_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence_Asn1SequenceParserImpl::index
	int32_t ___index_2;

public:
	inline static int32_t get_offset_of_outer_0() { return static_cast<int32_t>(offsetof(Asn1SequenceParserImpl_t1F76546A6B67A6D3F54BE558EE072A781F49489F, ___outer_0)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_outer_0() const { return ___outer_0; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_outer_0() { return &___outer_0; }
	inline void set_outer_0(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___outer_0), value);
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Asn1SequenceParserImpl_t1F76546A6B67A6D3F54BE558EE072A781F49489F, ___max_1)); }
	inline int32_t get_max_1() const { return ___max_1; }
	inline int32_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(int32_t value)
	{
		___max_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Asn1SequenceParserImpl_t1F76546A6B67A6D3F54BE558EE072A781F49489F, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1SEQUENCEPARSERIMPL_T1F76546A6B67A6D3F54BE558EE072A781F49489F_H
#ifndef ASN1SETPARSERIMPL_T2D4FFC7B4A3F718D806C4CAC0278B4200C4A5D5F_H
#define ASN1SETPARSERIMPL_T2D4FFC7B4A3F718D806C4CAC0278B4200C4A5D5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set_Asn1SetParserImpl
struct  Asn1SetParserImpl_t2D4FFC7B4A3F718D806C4CAC0278B4200C4A5D5F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set_Asn1SetParserImpl::outer
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___outer_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set_Asn1SetParserImpl::max
	int32_t ___max_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set_Asn1SetParserImpl::index
	int32_t ___index_2;

public:
	inline static int32_t get_offset_of_outer_0() { return static_cast<int32_t>(offsetof(Asn1SetParserImpl_t2D4FFC7B4A3F718D806C4CAC0278B4200C4A5D5F, ___outer_0)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_outer_0() const { return ___outer_0; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_outer_0() { return &___outer_0; }
	inline void set_outer_0(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___outer_0), value);
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Asn1SetParserImpl_t2D4FFC7B4A3F718D806C4CAC0278B4200C4A5D5F, ___max_1)); }
	inline int32_t get_max_1() const { return ___max_1; }
	inline int32_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(int32_t value)
	{
		___max_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Asn1SetParserImpl_t2D4FFC7B4A3F718D806C4CAC0278B4200C4A5D5F, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1SETPARSERIMPL_T2D4FFC7B4A3F718D806C4CAC0278B4200C4A5D5F_H
#ifndef DERCOMPARER_T46FB79AFF30E476AF32F391A82FF1C64AA2CA696_H
#define DERCOMPARER_T46FB79AFF30E476AF32F391A82FF1C64AA2CA696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set_DerComparer
struct  DerComparer_t46FB79AFF30E476AF32F391A82FF1C64AA2CA696  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERCOMPARER_T46FB79AFF30E476AF32F391A82FF1C64AA2CA696_H
#ifndef ASN1STREAMPARSER_T89ECA5C95FABEF7D278274E073575A457B221317_H
#define ASN1STREAMPARSER_T89ECA5C95FABEF7D278274E073575A457B221317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser
struct  Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317  : public RuntimeObject
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser::_in
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____in_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser::_limit
	int32_t ____limit_1;
	// System.Byte[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser::tmpBuffers
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___tmpBuffers_2;

public:
	inline static int32_t get_offset_of__in_0() { return static_cast<int32_t>(offsetof(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317, ____in_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__in_0() const { return ____in_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__in_0() { return &____in_0; }
	inline void set__in_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____in_0 = value;
		Il2CppCodeGenWriteBarrier((&____in_0), value);
	}

	inline static int32_t get_offset_of__limit_1() { return static_cast<int32_t>(offsetof(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317, ____limit_1)); }
	inline int32_t get__limit_1() const { return ____limit_1; }
	inline int32_t* get_address_of__limit_1() { return &____limit_1; }
	inline void set__limit_1(int32_t value)
	{
		____limit_1 = value;
	}

	inline static int32_t get_offset_of_tmpBuffers_2() { return static_cast<int32_t>(offsetof(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317, ___tmpBuffers_2)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_tmpBuffers_2() const { return ___tmpBuffers_2; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_tmpBuffers_2() { return &___tmpBuffers_2; }
	inline void set_tmpBuffers_2(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___tmpBuffers_2 = value;
		Il2CppCodeGenWriteBarrier((&___tmpBuffers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1STREAMPARSER_T89ECA5C95FABEF7D278274E073575A457B221317_H
#ifndef ASN1TAGS_TE9792E54ECDAAC0A994FFCE6C9FEB4515DD94946_H
#define ASN1TAGS_TE9792E54ECDAAC0A994FFCE6C9FEB4515DD94946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Tags
struct  Asn1Tags_tE9792E54ECDAAC0A994FFCE6C9FEB4515DD94946  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1TAGS_TE9792E54ECDAAC0A994FFCE6C9FEB4515DD94946_H
#ifndef BERAPPLICATIONSPECIFICPARSER_TCEDDF7625FEF1CC43A444CAA7BC63B38DDEA8F41_H
#define BERAPPLICATIONSPECIFICPARSER_TCEDDF7625FEF1CC43A444CAA7BC63B38DDEA8F41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerApplicationSpecificParser
struct  BerApplicationSpecificParser_tCEDDF7625FEF1CC43A444CAA7BC63B38DDEA8F41  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerApplicationSpecificParser::tag
	int32_t ___tag_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerApplicationSpecificParser::parser
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * ___parser_1;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(BerApplicationSpecificParser_tCEDDF7625FEF1CC43A444CAA7BC63B38DDEA8F41, ___tag_0)); }
	inline int32_t get_tag_0() const { return ___tag_0; }
	inline int32_t* get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(int32_t value)
	{
		___tag_0 = value;
	}

	inline static int32_t get_offset_of_parser_1() { return static_cast<int32_t>(offsetof(BerApplicationSpecificParser_tCEDDF7625FEF1CC43A444CAA7BC63B38DDEA8F41, ___parser_1)); }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * get_parser_1() const { return ___parser_1; }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 ** get_address_of_parser_1() { return &___parser_1; }
	inline void set_parser_1(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * value)
	{
		___parser_1 = value;
		Il2CppCodeGenWriteBarrier((&___parser_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERAPPLICATIONSPECIFICPARSER_TCEDDF7625FEF1CC43A444CAA7BC63B38DDEA8F41_H
#ifndef BEROCTETSTRINGPARSER_TA651A13B96DA5D19BCD01A462A1FC6D8C9FD8CA3_H
#define BEROCTETSTRINGPARSER_TA651A13B96DA5D19BCD01A462A1FC6D8C9FD8CA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetStringParser
struct  BerOctetStringParser_tA651A13B96DA5D19BCD01A462A1FC6D8C9FD8CA3  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetStringParser::_parser
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * ____parser_0;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(BerOctetStringParser_tA651A13B96DA5D19BCD01A462A1FC6D8C9FD8CA3, ____parser_0)); }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * get__parser_0() const { return ____parser_0; }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEROCTETSTRINGPARSER_TA651A13B96DA5D19BCD01A462A1FC6D8C9FD8CA3_H
#ifndef BERSEQUENCEPARSER_TDAF35CA4508663B98086A0CA886B5E6CC8540AC8_H
#define BERSEQUENCEPARSER_TDAF35CA4508663B98086A0CA886B5E6CC8540AC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceParser
struct  BerSequenceParser_tDAF35CA4508663B98086A0CA886B5E6CC8540AC8  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceParser::_parser
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * ____parser_0;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(BerSequenceParser_tDAF35CA4508663B98086A0CA886B5E6CC8540AC8, ____parser_0)); }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * get__parser_0() const { return ____parser_0; }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERSEQUENCEPARSER_TDAF35CA4508663B98086A0CA886B5E6CC8540AC8_H
#ifndef BERSETPARSER_T77ADA7D5576259917D4266B90953E6D35A6C1CD1_H
#define BERSETPARSER_T77ADA7D5576259917D4266B90953E6D35A6C1CD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSetParser
struct  BerSetParser_t77ADA7D5576259917D4266B90953E6D35A6C1CD1  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSetParser::_parser
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * ____parser_0;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(BerSetParser_t77ADA7D5576259917D4266B90953E6D35A6C1CD1, ____parser_0)); }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * get__parser_0() const { return ____parser_0; }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERSETPARSER_T77ADA7D5576259917D4266B90953E6D35A6C1CD1_H
#ifndef BERTAGGEDOBJECTPARSER_T60543EEC9BC41C3C7B0AB81CBA0E654DF6D1043F_H
#define BERTAGGEDOBJECTPARSER_T60543EEC9BC41C3C7B0AB81CBA0E654DF6D1043F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerTaggedObjectParser
struct  BerTaggedObjectParser_t60543EEC9BC41C3C7B0AB81CBA0E654DF6D1043F  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerTaggedObjectParser::_constructed
	bool ____constructed_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerTaggedObjectParser::_tagNumber
	int32_t ____tagNumber_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerTaggedObjectParser::_parser
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * ____parser_2;

public:
	inline static int32_t get_offset_of__constructed_0() { return static_cast<int32_t>(offsetof(BerTaggedObjectParser_t60543EEC9BC41C3C7B0AB81CBA0E654DF6D1043F, ____constructed_0)); }
	inline bool get__constructed_0() const { return ____constructed_0; }
	inline bool* get_address_of__constructed_0() { return &____constructed_0; }
	inline void set__constructed_0(bool value)
	{
		____constructed_0 = value;
	}

	inline static int32_t get_offset_of__tagNumber_1() { return static_cast<int32_t>(offsetof(BerTaggedObjectParser_t60543EEC9BC41C3C7B0AB81CBA0E654DF6D1043F, ____tagNumber_1)); }
	inline int32_t get__tagNumber_1() const { return ____tagNumber_1; }
	inline int32_t* get_address_of__tagNumber_1() { return &____tagNumber_1; }
	inline void set__tagNumber_1(int32_t value)
	{
		____tagNumber_1 = value;
	}

	inline static int32_t get_offset_of__parser_2() { return static_cast<int32_t>(offsetof(BerTaggedObjectParser_t60543EEC9BC41C3C7B0AB81CBA0E654DF6D1043F, ____parser_2)); }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * get__parser_2() const { return ____parser_2; }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 ** get_address_of__parser_2() { return &____parser_2; }
	inline void set__parser_2(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * value)
	{
		____parser_2 = value;
		Il2CppCodeGenWriteBarrier((&____parser_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERTAGGEDOBJECTPARSER_T60543EEC9BC41C3C7B0AB81CBA0E654DF6D1043F_H
#ifndef CMSCONTENTINFOPARSER_T1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7_H
#define CMSCONTENTINFOPARSER_T1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsContentInfoParser
struct  CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfoParser BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsContentInfoParser::contentInfo
	ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC * ___contentInfo_0;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsContentInfoParser::data
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___data_1;

public:
	inline static int32_t get_offset_of_contentInfo_0() { return static_cast<int32_t>(offsetof(CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7, ___contentInfo_0)); }
	inline ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC * get_contentInfo_0() const { return ___contentInfo_0; }
	inline ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC ** get_address_of_contentInfo_0() { return &___contentInfo_0; }
	inline void set_contentInfo_0(ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC * value)
	{
		___contentInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentInfo_0), value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7, ___data_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_data_1() const { return ___data_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSCONTENTINFOPARSER_T1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7_H
#ifndef CMSPBEKEY_TADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1_H
#define CMSPBEKEY_TADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsPbeKey
struct  CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1  : public RuntimeObject
{
public:
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsPbeKey::password
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___password_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsPbeKey::salt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___salt_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsPbeKey::iterationCount
	int32_t ___iterationCount_2;

public:
	inline static int32_t get_offset_of_password_0() { return static_cast<int32_t>(offsetof(CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1, ___password_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_password_0() const { return ___password_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_password_0() { return &___password_0; }
	inline void set_password_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___password_0 = value;
		Il2CppCodeGenWriteBarrier((&___password_0), value);
	}

	inline static int32_t get_offset_of_salt_1() { return static_cast<int32_t>(offsetof(CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1, ___salt_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_salt_1() const { return ___salt_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_salt_1() { return &___salt_1; }
	inline void set_salt_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___salt_1 = value;
		Il2CppCodeGenWriteBarrier((&___salt_1), value);
	}

	inline static int32_t get_offset_of_iterationCount_2() { return static_cast<int32_t>(offsetof(CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1, ___iterationCount_2)); }
	inline int32_t get_iterationCount_2() const { return ___iterationCount_2; }
	inline int32_t* get_address_of_iterationCount_2() { return &___iterationCount_2; }
	inline void set_iterationCount_2(int32_t value)
	{
		___iterationCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSPBEKEY_TADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1_H
#ifndef SIGNERINF_TD8870DACB906B28D276ABA3B402CB3ED7BA37B3D_H
#define SIGNERINF_TD8870DACB906B28D276ABA3B402CB3ED7BA37B3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator_SignerInf
struct  SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator_SignerInf::outer
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44 * ___outer_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISignatureFactory BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator_SignerInf::sigCalc
	RuntimeObject* ___sigCalc_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator_SignerInf::signerIdentifier
	SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * ___signerIdentifier_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator_SignerInf::digestOID
	String_t* ___digestOID_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator_SignerInf::encOID
	String_t* ___encOID_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator_SignerInf::sAttr
	RuntimeObject* ___sAttr_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator_SignerInf::unsAttr
	RuntimeObject* ___unsAttr_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator_SignerInf::baseSignedTable
	AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * ___baseSignedTable_7;

public:
	inline static int32_t get_offset_of_outer_0() { return static_cast<int32_t>(offsetof(SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D, ___outer_0)); }
	inline CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44 * get_outer_0() const { return ___outer_0; }
	inline CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44 ** get_address_of_outer_0() { return &___outer_0; }
	inline void set_outer_0(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44 * value)
	{
		___outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___outer_0), value);
	}

	inline static int32_t get_offset_of_sigCalc_1() { return static_cast<int32_t>(offsetof(SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D, ___sigCalc_1)); }
	inline RuntimeObject* get_sigCalc_1() const { return ___sigCalc_1; }
	inline RuntimeObject** get_address_of_sigCalc_1() { return &___sigCalc_1; }
	inline void set_sigCalc_1(RuntimeObject* value)
	{
		___sigCalc_1 = value;
		Il2CppCodeGenWriteBarrier((&___sigCalc_1), value);
	}

	inline static int32_t get_offset_of_signerIdentifier_2() { return static_cast<int32_t>(offsetof(SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D, ___signerIdentifier_2)); }
	inline SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * get_signerIdentifier_2() const { return ___signerIdentifier_2; }
	inline SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 ** get_address_of_signerIdentifier_2() { return &___signerIdentifier_2; }
	inline void set_signerIdentifier_2(SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * value)
	{
		___signerIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___signerIdentifier_2), value);
	}

	inline static int32_t get_offset_of_digestOID_3() { return static_cast<int32_t>(offsetof(SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D, ___digestOID_3)); }
	inline String_t* get_digestOID_3() const { return ___digestOID_3; }
	inline String_t** get_address_of_digestOID_3() { return &___digestOID_3; }
	inline void set_digestOID_3(String_t* value)
	{
		___digestOID_3 = value;
		Il2CppCodeGenWriteBarrier((&___digestOID_3), value);
	}

	inline static int32_t get_offset_of_encOID_4() { return static_cast<int32_t>(offsetof(SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D, ___encOID_4)); }
	inline String_t* get_encOID_4() const { return ___encOID_4; }
	inline String_t** get_address_of_encOID_4() { return &___encOID_4; }
	inline void set_encOID_4(String_t* value)
	{
		___encOID_4 = value;
		Il2CppCodeGenWriteBarrier((&___encOID_4), value);
	}

	inline static int32_t get_offset_of_sAttr_5() { return static_cast<int32_t>(offsetof(SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D, ___sAttr_5)); }
	inline RuntimeObject* get_sAttr_5() const { return ___sAttr_5; }
	inline RuntimeObject** get_address_of_sAttr_5() { return &___sAttr_5; }
	inline void set_sAttr_5(RuntimeObject* value)
	{
		___sAttr_5 = value;
		Il2CppCodeGenWriteBarrier((&___sAttr_5), value);
	}

	inline static int32_t get_offset_of_unsAttr_6() { return static_cast<int32_t>(offsetof(SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D, ___unsAttr_6)); }
	inline RuntimeObject* get_unsAttr_6() const { return ___unsAttr_6; }
	inline RuntimeObject** get_address_of_unsAttr_6() { return &___unsAttr_6; }
	inline void set_unsAttr_6(RuntimeObject* value)
	{
		___unsAttr_6 = value;
		Il2CppCodeGenWriteBarrier((&___unsAttr_6), value);
	}

	inline static int32_t get_offset_of_baseSignedTable_7() { return static_cast<int32_t>(offsetof(SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D, ___baseSignedTable_7)); }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * get_baseSignedTable_7() const { return ___baseSignedTable_7; }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 ** get_address_of_baseSignedTable_7() { return &___baseSignedTable_7; }
	inline void set_baseSignedTable_7(AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * value)
	{
		___baseSignedTable_7 = value;
		Il2CppCodeGenWriteBarrier((&___baseSignedTable_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERINF_TD8870DACB906B28D276ABA3B402CB3ED7BA37B3D_H
#ifndef DIGESTANDSIGNERINFOGENERATORHOLDER_TB87B4E4EC99013C14C3BA2641C13D072F2A18A1B_H
#define DIGESTANDSIGNERINFOGENERATORHOLDER_TB87B4E4EC99013C14C3BA2641C13D072F2A18A1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_DigestAndSignerInfoGeneratorHolder
struct  DigestAndSignerInfoGeneratorHolder_tB87B4E4EC99013C14C3BA2641C13D072F2A18A1B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.ISignerInfoGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_DigestAndSignerInfoGeneratorHolder::signerInf
	RuntimeObject* ___signerInf_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_DigestAndSignerInfoGeneratorHolder::digestOID
	String_t* ___digestOID_1;

public:
	inline static int32_t get_offset_of_signerInf_0() { return static_cast<int32_t>(offsetof(DigestAndSignerInfoGeneratorHolder_tB87B4E4EC99013C14C3BA2641C13D072F2A18A1B, ___signerInf_0)); }
	inline RuntimeObject* get_signerInf_0() const { return ___signerInf_0; }
	inline RuntimeObject** get_address_of_signerInf_0() { return &___signerInf_0; }
	inline void set_signerInf_0(RuntimeObject* value)
	{
		___signerInf_0 = value;
		Il2CppCodeGenWriteBarrier((&___signerInf_0), value);
	}

	inline static int32_t get_offset_of_digestOID_1() { return static_cast<int32_t>(offsetof(DigestAndSignerInfoGeneratorHolder_tB87B4E4EC99013C14C3BA2641C13D072F2A18A1B, ___digestOID_1)); }
	inline String_t* get_digestOID_1() const { return ___digestOID_1; }
	inline String_t** get_address_of_digestOID_1() { return &___digestOID_1; }
	inline void set_digestOID_1(String_t* value)
	{
		___digestOID_1 = value;
		Il2CppCodeGenWriteBarrier((&___digestOID_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTANDSIGNERINFOGENERATORHOLDER_TB87B4E4EC99013C14C3BA2641C13D072F2A18A1B_H
#ifndef SIGNERINFOGENERATORIMPL_T01073E540A9A35E655B85F1758AFC09E4DC3883A_H
#define SIGNERINFOGENERATORIMPL_T01073E540A9A35E655B85F1758AFC09E4DC3883A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_SignerInfoGeneratorImpl
struct  SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_SignerInfoGeneratorImpl::outer
	CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D * ___outer_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_SignerInfoGeneratorImpl::_signerIdentifier
	SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * ____signerIdentifier_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_SignerInfoGeneratorImpl::_digestOID
	String_t* ____digestOID_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_SignerInfoGeneratorImpl::_encOID
	String_t* ____encOID_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_SignerInfoGeneratorImpl::_sAttr
	RuntimeObject* ____sAttr_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_SignerInfoGeneratorImpl::_unsAttr
	RuntimeObject* ____unsAttr_5;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_SignerInfoGeneratorImpl::_encName
	String_t* ____encName_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISigner BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_SignerInfoGeneratorImpl::_sig
	RuntimeObject* ____sig_7;

public:
	inline static int32_t get_offset_of_outer_0() { return static_cast<int32_t>(offsetof(SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A, ___outer_0)); }
	inline CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D * get_outer_0() const { return ___outer_0; }
	inline CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D ** get_address_of_outer_0() { return &___outer_0; }
	inline void set_outer_0(CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D * value)
	{
		___outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___outer_0), value);
	}

	inline static int32_t get_offset_of__signerIdentifier_1() { return static_cast<int32_t>(offsetof(SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A, ____signerIdentifier_1)); }
	inline SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * get__signerIdentifier_1() const { return ____signerIdentifier_1; }
	inline SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 ** get_address_of__signerIdentifier_1() { return &____signerIdentifier_1; }
	inline void set__signerIdentifier_1(SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * value)
	{
		____signerIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&____signerIdentifier_1), value);
	}

	inline static int32_t get_offset_of__digestOID_2() { return static_cast<int32_t>(offsetof(SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A, ____digestOID_2)); }
	inline String_t* get__digestOID_2() const { return ____digestOID_2; }
	inline String_t** get_address_of__digestOID_2() { return &____digestOID_2; }
	inline void set__digestOID_2(String_t* value)
	{
		____digestOID_2 = value;
		Il2CppCodeGenWriteBarrier((&____digestOID_2), value);
	}

	inline static int32_t get_offset_of__encOID_3() { return static_cast<int32_t>(offsetof(SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A, ____encOID_3)); }
	inline String_t* get__encOID_3() const { return ____encOID_3; }
	inline String_t** get_address_of__encOID_3() { return &____encOID_3; }
	inline void set__encOID_3(String_t* value)
	{
		____encOID_3 = value;
		Il2CppCodeGenWriteBarrier((&____encOID_3), value);
	}

	inline static int32_t get_offset_of__sAttr_4() { return static_cast<int32_t>(offsetof(SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A, ____sAttr_4)); }
	inline RuntimeObject* get__sAttr_4() const { return ____sAttr_4; }
	inline RuntimeObject** get_address_of__sAttr_4() { return &____sAttr_4; }
	inline void set__sAttr_4(RuntimeObject* value)
	{
		____sAttr_4 = value;
		Il2CppCodeGenWriteBarrier((&____sAttr_4), value);
	}

	inline static int32_t get_offset_of__unsAttr_5() { return static_cast<int32_t>(offsetof(SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A, ____unsAttr_5)); }
	inline RuntimeObject* get__unsAttr_5() const { return ____unsAttr_5; }
	inline RuntimeObject** get_address_of__unsAttr_5() { return &____unsAttr_5; }
	inline void set__unsAttr_5(RuntimeObject* value)
	{
		____unsAttr_5 = value;
		Il2CppCodeGenWriteBarrier((&____unsAttr_5), value);
	}

	inline static int32_t get_offset_of__encName_6() { return static_cast<int32_t>(offsetof(SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A, ____encName_6)); }
	inline String_t* get__encName_6() const { return ____encName_6; }
	inline String_t** get_address_of__encName_6() { return &____encName_6; }
	inline void set__encName_6(String_t* value)
	{
		____encName_6 = value;
		Il2CppCodeGenWriteBarrier((&____encName_6), value);
	}

	inline static int32_t get_offset_of__sig_7() { return static_cast<int32_t>(offsetof(SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A, ____sig_7)); }
	inline RuntimeObject* get__sig_7() const { return ____sig_7; }
	inline RuntimeObject** get_address_of__sig_7() { return &____sig_7; }
	inline void set__sig_7(RuntimeObject* value)
	{
		____sig_7 = value;
		Il2CppCodeGenWriteBarrier((&____sig_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERINFOGENERATORIMPL_T01073E540A9A35E655B85F1758AFC09E4DC3883A_H
#ifndef CMSSIGNEDGENERATOR_TC85671C8234B3E8E8176C2D0A5495A53265A2D44_H
#define CMSSIGNEDGENERATOR_TC85671C8234B3E8E8176C2D0A5495A53265A2D44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator
struct  CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_certs
	RuntimeObject* ____certs_17;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_crls
	RuntimeObject* ____crls_18;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_signers
	RuntimeObject* ____signers_19;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_digests
	RuntimeObject* ____digests_20;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_useDerForCerts
	bool ____useDerForCerts_21;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_useDerForCrls
	bool ____useDerForCrls_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::rand
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___rand_23;

public:
	inline static int32_t get_offset_of__certs_17() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____certs_17)); }
	inline RuntimeObject* get__certs_17() const { return ____certs_17; }
	inline RuntimeObject** get_address_of__certs_17() { return &____certs_17; }
	inline void set__certs_17(RuntimeObject* value)
	{
		____certs_17 = value;
		Il2CppCodeGenWriteBarrier((&____certs_17), value);
	}

	inline static int32_t get_offset_of__crls_18() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____crls_18)); }
	inline RuntimeObject* get__crls_18() const { return ____crls_18; }
	inline RuntimeObject** get_address_of__crls_18() { return &____crls_18; }
	inline void set__crls_18(RuntimeObject* value)
	{
		____crls_18 = value;
		Il2CppCodeGenWriteBarrier((&____crls_18), value);
	}

	inline static int32_t get_offset_of__signers_19() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____signers_19)); }
	inline RuntimeObject* get__signers_19() const { return ____signers_19; }
	inline RuntimeObject** get_address_of__signers_19() { return &____signers_19; }
	inline void set__signers_19(RuntimeObject* value)
	{
		____signers_19 = value;
		Il2CppCodeGenWriteBarrier((&____signers_19), value);
	}

	inline static int32_t get_offset_of__digests_20() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____digests_20)); }
	inline RuntimeObject* get__digests_20() const { return ____digests_20; }
	inline RuntimeObject** get_address_of__digests_20() { return &____digests_20; }
	inline void set__digests_20(RuntimeObject* value)
	{
		____digests_20 = value;
		Il2CppCodeGenWriteBarrier((&____digests_20), value);
	}

	inline static int32_t get_offset_of__useDerForCerts_21() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____useDerForCerts_21)); }
	inline bool get__useDerForCerts_21() const { return ____useDerForCerts_21; }
	inline bool* get_address_of__useDerForCerts_21() { return &____useDerForCerts_21; }
	inline void set__useDerForCerts_21(bool value)
	{
		____useDerForCerts_21 = value;
	}

	inline static int32_t get_offset_of__useDerForCrls_22() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____useDerForCrls_22)); }
	inline bool get__useDerForCrls_22() const { return ____useDerForCrls_22; }
	inline bool* get_address_of__useDerForCrls_22() { return &____useDerForCrls_22; }
	inline void set__useDerForCrls_22(bool value)
	{
		____useDerForCrls_22 = value;
	}

	inline static int32_t get_offset_of_rand_23() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ___rand_23)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_rand_23() const { return ___rand_23; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_rand_23() { return &___rand_23; }
	inline void set_rand_23(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___rand_23 = value;
		Il2CppCodeGenWriteBarrier((&___rand_23), value);
	}
};

struct CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::Data
	String_t* ___Data_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestSha1
	String_t* ___DigestSha1_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestSha224
	String_t* ___DigestSha224_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestSha256
	String_t* ___DigestSha256_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestSha384
	String_t* ___DigestSha384_4;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestSha512
	String_t* ___DigestSha512_5;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestMD5
	String_t* ___DigestMD5_6;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestGost3411
	String_t* ___DigestGost3411_7;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestRipeMD128
	String_t* ___DigestRipeMD128_8;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestRipeMD160
	String_t* ___DigestRipeMD160_9;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestRipeMD256
	String_t* ___DigestRipeMD256_10;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionRsa
	String_t* ___EncryptionRsa_11;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionDsa
	String_t* ___EncryptionDsa_12;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionECDsa
	String_t* ___EncryptionECDsa_13;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionRsaPss
	String_t* ___EncryptionRsaPss_14;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionGost3410
	String_t* ___EncryptionGost3410_15;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionECGost3410
	String_t* ___EncryptionECGost3410_16;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___Data_0)); }
	inline String_t* get_Data_0() const { return ___Data_0; }
	inline String_t** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(String_t* value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier((&___Data_0), value);
	}

	inline static int32_t get_offset_of_DigestSha1_1() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestSha1_1)); }
	inline String_t* get_DigestSha1_1() const { return ___DigestSha1_1; }
	inline String_t** get_address_of_DigestSha1_1() { return &___DigestSha1_1; }
	inline void set_DigestSha1_1(String_t* value)
	{
		___DigestSha1_1 = value;
		Il2CppCodeGenWriteBarrier((&___DigestSha1_1), value);
	}

	inline static int32_t get_offset_of_DigestSha224_2() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestSha224_2)); }
	inline String_t* get_DigestSha224_2() const { return ___DigestSha224_2; }
	inline String_t** get_address_of_DigestSha224_2() { return &___DigestSha224_2; }
	inline void set_DigestSha224_2(String_t* value)
	{
		___DigestSha224_2 = value;
		Il2CppCodeGenWriteBarrier((&___DigestSha224_2), value);
	}

	inline static int32_t get_offset_of_DigestSha256_3() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestSha256_3)); }
	inline String_t* get_DigestSha256_3() const { return ___DigestSha256_3; }
	inline String_t** get_address_of_DigestSha256_3() { return &___DigestSha256_3; }
	inline void set_DigestSha256_3(String_t* value)
	{
		___DigestSha256_3 = value;
		Il2CppCodeGenWriteBarrier((&___DigestSha256_3), value);
	}

	inline static int32_t get_offset_of_DigestSha384_4() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestSha384_4)); }
	inline String_t* get_DigestSha384_4() const { return ___DigestSha384_4; }
	inline String_t** get_address_of_DigestSha384_4() { return &___DigestSha384_4; }
	inline void set_DigestSha384_4(String_t* value)
	{
		___DigestSha384_4 = value;
		Il2CppCodeGenWriteBarrier((&___DigestSha384_4), value);
	}

	inline static int32_t get_offset_of_DigestSha512_5() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestSha512_5)); }
	inline String_t* get_DigestSha512_5() const { return ___DigestSha512_5; }
	inline String_t** get_address_of_DigestSha512_5() { return &___DigestSha512_5; }
	inline void set_DigestSha512_5(String_t* value)
	{
		___DigestSha512_5 = value;
		Il2CppCodeGenWriteBarrier((&___DigestSha512_5), value);
	}

	inline static int32_t get_offset_of_DigestMD5_6() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestMD5_6)); }
	inline String_t* get_DigestMD5_6() const { return ___DigestMD5_6; }
	inline String_t** get_address_of_DigestMD5_6() { return &___DigestMD5_6; }
	inline void set_DigestMD5_6(String_t* value)
	{
		___DigestMD5_6 = value;
		Il2CppCodeGenWriteBarrier((&___DigestMD5_6), value);
	}

	inline static int32_t get_offset_of_DigestGost3411_7() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestGost3411_7)); }
	inline String_t* get_DigestGost3411_7() const { return ___DigestGost3411_7; }
	inline String_t** get_address_of_DigestGost3411_7() { return &___DigestGost3411_7; }
	inline void set_DigestGost3411_7(String_t* value)
	{
		___DigestGost3411_7 = value;
		Il2CppCodeGenWriteBarrier((&___DigestGost3411_7), value);
	}

	inline static int32_t get_offset_of_DigestRipeMD128_8() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestRipeMD128_8)); }
	inline String_t* get_DigestRipeMD128_8() const { return ___DigestRipeMD128_8; }
	inline String_t** get_address_of_DigestRipeMD128_8() { return &___DigestRipeMD128_8; }
	inline void set_DigestRipeMD128_8(String_t* value)
	{
		___DigestRipeMD128_8 = value;
		Il2CppCodeGenWriteBarrier((&___DigestRipeMD128_8), value);
	}

	inline static int32_t get_offset_of_DigestRipeMD160_9() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestRipeMD160_9)); }
	inline String_t* get_DigestRipeMD160_9() const { return ___DigestRipeMD160_9; }
	inline String_t** get_address_of_DigestRipeMD160_9() { return &___DigestRipeMD160_9; }
	inline void set_DigestRipeMD160_9(String_t* value)
	{
		___DigestRipeMD160_9 = value;
		Il2CppCodeGenWriteBarrier((&___DigestRipeMD160_9), value);
	}

	inline static int32_t get_offset_of_DigestRipeMD256_10() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestRipeMD256_10)); }
	inline String_t* get_DigestRipeMD256_10() const { return ___DigestRipeMD256_10; }
	inline String_t** get_address_of_DigestRipeMD256_10() { return &___DigestRipeMD256_10; }
	inline void set_DigestRipeMD256_10(String_t* value)
	{
		___DigestRipeMD256_10 = value;
		Il2CppCodeGenWriteBarrier((&___DigestRipeMD256_10), value);
	}

	inline static int32_t get_offset_of_EncryptionRsa_11() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionRsa_11)); }
	inline String_t* get_EncryptionRsa_11() const { return ___EncryptionRsa_11; }
	inline String_t** get_address_of_EncryptionRsa_11() { return &___EncryptionRsa_11; }
	inline void set_EncryptionRsa_11(String_t* value)
	{
		___EncryptionRsa_11 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionRsa_11), value);
	}

	inline static int32_t get_offset_of_EncryptionDsa_12() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionDsa_12)); }
	inline String_t* get_EncryptionDsa_12() const { return ___EncryptionDsa_12; }
	inline String_t** get_address_of_EncryptionDsa_12() { return &___EncryptionDsa_12; }
	inline void set_EncryptionDsa_12(String_t* value)
	{
		___EncryptionDsa_12 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionDsa_12), value);
	}

	inline static int32_t get_offset_of_EncryptionECDsa_13() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionECDsa_13)); }
	inline String_t* get_EncryptionECDsa_13() const { return ___EncryptionECDsa_13; }
	inline String_t** get_address_of_EncryptionECDsa_13() { return &___EncryptionECDsa_13; }
	inline void set_EncryptionECDsa_13(String_t* value)
	{
		___EncryptionECDsa_13 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionECDsa_13), value);
	}

	inline static int32_t get_offset_of_EncryptionRsaPss_14() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionRsaPss_14)); }
	inline String_t* get_EncryptionRsaPss_14() const { return ___EncryptionRsaPss_14; }
	inline String_t** get_address_of_EncryptionRsaPss_14() { return &___EncryptionRsaPss_14; }
	inline void set_EncryptionRsaPss_14(String_t* value)
	{
		___EncryptionRsaPss_14 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionRsaPss_14), value);
	}

	inline static int32_t get_offset_of_EncryptionGost3410_15() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionGost3410_15)); }
	inline String_t* get_EncryptionGost3410_15() const { return ___EncryptionGost3410_15; }
	inline String_t** get_address_of_EncryptionGost3410_15() { return &___EncryptionGost3410_15; }
	inline void set_EncryptionGost3410_15(String_t* value)
	{
		___EncryptionGost3410_15 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionGost3410_15), value);
	}

	inline static int32_t get_offset_of_EncryptionECGost3410_16() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionECGost3410_16)); }
	inline String_t* get_EncryptionECGost3410_16() const { return ___EncryptionECGost3410_16; }
	inline String_t** get_address_of_EncryptionECGost3410_16() { return &___EncryptionECGost3410_16; }
	inline void set_EncryptionECGost3410_16(String_t* value)
	{
		___EncryptionECGost3410_16 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionECGost3410_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSSIGNEDGENERATOR_TC85671C8234B3E8E8176C2D0A5495A53265A2D44_H
#ifndef CMSSIGNEDHELPER_T20DE4DC580086367E603B7C85A06BAD57ACA9EDF_H
#define CMSSIGNEDHELPER_T20DE4DC580086367E603B7C85A06BAD57ACA9EDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper
struct  CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF  : public RuntimeObject
{
public:

public:
};

struct CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper::Instance
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * ___Instance_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper::EncryptionECDsaWithSha1
	String_t* ___EncryptionECDsaWithSha1_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper::EncryptionECDsaWithSha224
	String_t* ___EncryptionECDsaWithSha224_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper::EncryptionECDsaWithSha256
	String_t* ___EncryptionECDsaWithSha256_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper::EncryptionECDsaWithSha384
	String_t* ___EncryptionECDsaWithSha384_4;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper::EncryptionECDsaWithSha512
	String_t* ___EncryptionECDsaWithSha512_5;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper::encryptionAlgs
	RuntimeObject* ___encryptionAlgs_6;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper::digestAlgs
	RuntimeObject* ___digestAlgs_7;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper::digestAliases
	RuntimeObject* ___digestAliases_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper::noParams
	RuntimeObject* ___noParams_9;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper::ecAlgorithms
	RuntimeObject* ___ecAlgorithms_10;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields, ___Instance_0)); }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * get_Instance_0() const { return ___Instance_0; }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}

	inline static int32_t get_offset_of_EncryptionECDsaWithSha1_1() { return static_cast<int32_t>(offsetof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields, ___EncryptionECDsaWithSha1_1)); }
	inline String_t* get_EncryptionECDsaWithSha1_1() const { return ___EncryptionECDsaWithSha1_1; }
	inline String_t** get_address_of_EncryptionECDsaWithSha1_1() { return &___EncryptionECDsaWithSha1_1; }
	inline void set_EncryptionECDsaWithSha1_1(String_t* value)
	{
		___EncryptionECDsaWithSha1_1 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionECDsaWithSha1_1), value);
	}

	inline static int32_t get_offset_of_EncryptionECDsaWithSha224_2() { return static_cast<int32_t>(offsetof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields, ___EncryptionECDsaWithSha224_2)); }
	inline String_t* get_EncryptionECDsaWithSha224_2() const { return ___EncryptionECDsaWithSha224_2; }
	inline String_t** get_address_of_EncryptionECDsaWithSha224_2() { return &___EncryptionECDsaWithSha224_2; }
	inline void set_EncryptionECDsaWithSha224_2(String_t* value)
	{
		___EncryptionECDsaWithSha224_2 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionECDsaWithSha224_2), value);
	}

	inline static int32_t get_offset_of_EncryptionECDsaWithSha256_3() { return static_cast<int32_t>(offsetof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields, ___EncryptionECDsaWithSha256_3)); }
	inline String_t* get_EncryptionECDsaWithSha256_3() const { return ___EncryptionECDsaWithSha256_3; }
	inline String_t** get_address_of_EncryptionECDsaWithSha256_3() { return &___EncryptionECDsaWithSha256_3; }
	inline void set_EncryptionECDsaWithSha256_3(String_t* value)
	{
		___EncryptionECDsaWithSha256_3 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionECDsaWithSha256_3), value);
	}

	inline static int32_t get_offset_of_EncryptionECDsaWithSha384_4() { return static_cast<int32_t>(offsetof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields, ___EncryptionECDsaWithSha384_4)); }
	inline String_t* get_EncryptionECDsaWithSha384_4() const { return ___EncryptionECDsaWithSha384_4; }
	inline String_t** get_address_of_EncryptionECDsaWithSha384_4() { return &___EncryptionECDsaWithSha384_4; }
	inline void set_EncryptionECDsaWithSha384_4(String_t* value)
	{
		___EncryptionECDsaWithSha384_4 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionECDsaWithSha384_4), value);
	}

	inline static int32_t get_offset_of_EncryptionECDsaWithSha512_5() { return static_cast<int32_t>(offsetof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields, ___EncryptionECDsaWithSha512_5)); }
	inline String_t* get_EncryptionECDsaWithSha512_5() const { return ___EncryptionECDsaWithSha512_5; }
	inline String_t** get_address_of_EncryptionECDsaWithSha512_5() { return &___EncryptionECDsaWithSha512_5; }
	inline void set_EncryptionECDsaWithSha512_5(String_t* value)
	{
		___EncryptionECDsaWithSha512_5 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionECDsaWithSha512_5), value);
	}

	inline static int32_t get_offset_of_encryptionAlgs_6() { return static_cast<int32_t>(offsetof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields, ___encryptionAlgs_6)); }
	inline RuntimeObject* get_encryptionAlgs_6() const { return ___encryptionAlgs_6; }
	inline RuntimeObject** get_address_of_encryptionAlgs_6() { return &___encryptionAlgs_6; }
	inline void set_encryptionAlgs_6(RuntimeObject* value)
	{
		___encryptionAlgs_6 = value;
		Il2CppCodeGenWriteBarrier((&___encryptionAlgs_6), value);
	}

	inline static int32_t get_offset_of_digestAlgs_7() { return static_cast<int32_t>(offsetof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields, ___digestAlgs_7)); }
	inline RuntimeObject* get_digestAlgs_7() const { return ___digestAlgs_7; }
	inline RuntimeObject** get_address_of_digestAlgs_7() { return &___digestAlgs_7; }
	inline void set_digestAlgs_7(RuntimeObject* value)
	{
		___digestAlgs_7 = value;
		Il2CppCodeGenWriteBarrier((&___digestAlgs_7), value);
	}

	inline static int32_t get_offset_of_digestAliases_8() { return static_cast<int32_t>(offsetof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields, ___digestAliases_8)); }
	inline RuntimeObject* get_digestAliases_8() const { return ___digestAliases_8; }
	inline RuntimeObject** get_address_of_digestAliases_8() { return &___digestAliases_8; }
	inline void set_digestAliases_8(RuntimeObject* value)
	{
		___digestAliases_8 = value;
		Il2CppCodeGenWriteBarrier((&___digestAliases_8), value);
	}

	inline static int32_t get_offset_of_noParams_9() { return static_cast<int32_t>(offsetof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields, ___noParams_9)); }
	inline RuntimeObject* get_noParams_9() const { return ___noParams_9; }
	inline RuntimeObject** get_address_of_noParams_9() { return &___noParams_9; }
	inline void set_noParams_9(RuntimeObject* value)
	{
		___noParams_9 = value;
		Il2CppCodeGenWriteBarrier((&___noParams_9), value);
	}

	inline static int32_t get_offset_of_ecAlgorithms_10() { return static_cast<int32_t>(offsetof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields, ___ecAlgorithms_10)); }
	inline RuntimeObject* get_ecAlgorithms_10() const { return ___ecAlgorithms_10; }
	inline RuntimeObject** get_address_of_ecAlgorithms_10() { return &___ecAlgorithms_10; }
	inline void set_ecAlgorithms_10(RuntimeObject* value)
	{
		___ecAlgorithms_10 = value;
		Il2CppCodeGenWriteBarrier((&___ecAlgorithms_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSSIGNEDHELPER_T20DE4DC580086367E603B7C85A06BAD57ACA9EDF_H
#ifndef CMSTYPEDSTREAM_T3C034672F2E5D2B8EDF5618BDE3E4DA90F540591_H
#define CMSTYPEDSTREAM_T3C034672F2E5D2B8EDF5618BDE3E4DA90F540591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsTypedStream
struct  CmsTypedStream_t3C034672F2E5D2B8EDF5618BDE3E4DA90F540591  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsTypedStream::_oid
	String_t* ____oid_1;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsTypedStream::_in
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____in_2;

public:
	inline static int32_t get_offset_of__oid_1() { return static_cast<int32_t>(offsetof(CmsTypedStream_t3C034672F2E5D2B8EDF5618BDE3E4DA90F540591, ____oid_1)); }
	inline String_t* get__oid_1() const { return ____oid_1; }
	inline String_t** get_address_of__oid_1() { return &____oid_1; }
	inline void set__oid_1(String_t* value)
	{
		____oid_1 = value;
		Il2CppCodeGenWriteBarrier((&____oid_1), value);
	}

	inline static int32_t get_offset_of__in_2() { return static_cast<int32_t>(offsetof(CmsTypedStream_t3C034672F2E5D2B8EDF5618BDE3E4DA90F540591, ____in_2)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__in_2() const { return ____in_2; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__in_2() { return &____in_2; }
	inline void set__in_2(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____in_2 = value;
		Il2CppCodeGenWriteBarrier((&____in_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSTYPEDSTREAM_T3C034672F2E5D2B8EDF5618BDE3E4DA90F540591_H
#ifndef CMSUTILITIES_TEC89F9AE2F78B8E95619AFE5BFAC651765637340_H
#define CMSUTILITIES_TEC89F9AE2F78B8E95619AFE5BFAC651765637340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsUtilities
struct  CmsUtilities_tEC89F9AE2F78B8E95619AFE5BFAC651765637340  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSUTILITIES_TEC89F9AE2F78B8E95619AFE5BFAC651765637340_H
#ifndef COUNTERSIGNATUREDIGESTCALCULATOR_T34C8C9E3A64AA7D9CD73963DF9755DDDC5231E0E_H
#define COUNTERSIGNATUREDIGESTCALCULATOR_T34C8C9E3A64AA7D9CD73963DF9755DDDC5231E0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CounterSignatureDigestCalculator
struct  CounterSignatureDigestCalculator_t34C8C9E3A64AA7D9CD73963DF9755DDDC5231E0E  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CounterSignatureDigestCalculator::alg
	String_t* ___alg_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CounterSignatureDigestCalculator::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_1;

public:
	inline static int32_t get_offset_of_alg_0() { return static_cast<int32_t>(offsetof(CounterSignatureDigestCalculator_t34C8C9E3A64AA7D9CD73963DF9755DDDC5231E0E, ___alg_0)); }
	inline String_t* get_alg_0() const { return ___alg_0; }
	inline String_t** get_address_of_alg_0() { return &___alg_0; }
	inline void set_alg_0(String_t* value)
	{
		___alg_0 = value;
		Il2CppCodeGenWriteBarrier((&___alg_0), value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(CounterSignatureDigestCalculator_t34C8C9E3A64AA7D9CD73963DF9755DDDC5231E0E, ___data_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTERSIGNATUREDIGESTCALCULATOR_T34C8C9E3A64AA7D9CD73963DF9755DDDC5231E0E_H
#ifndef DEFAULTAUTHENTICATEDATTRIBUTETABLEGENERATOR_T10100711C917BC7A845765E91490C3E5FDB7EDDF_H
#define DEFAULTAUTHENTICATEDATTRIBUTETABLEGENERATOR_T10100711C917BC7A845765E91490C3E5FDB7EDDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultAuthenticatedAttributeTableGenerator
struct  DefaultAuthenticatedAttributeTableGenerator_t10100711C917BC7A845765E91490C3E5FDB7EDDF  : public RuntimeObject
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultAuthenticatedAttributeTableGenerator::table
	RuntimeObject* ___table_0;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(DefaultAuthenticatedAttributeTableGenerator_t10100711C917BC7A845765E91490C3E5FDB7EDDF, ___table_0)); }
	inline RuntimeObject* get_table_0() const { return ___table_0; }
	inline RuntimeObject** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(RuntimeObject* value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTAUTHENTICATEDATTRIBUTETABLEGENERATOR_T10100711C917BC7A845765E91490C3E5FDB7EDDF_H
#ifndef DEFAULTDIGESTALGORITHMIDENTIFIERFINDER_T5BE60CA84E15549D1FD606CCC88BA26483D72694_H
#define DEFAULTDIGESTALGORITHMIDENTIFIERFINDER_T5BE60CA84E15549D1FD606CCC88BA26483D72694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultDigestAlgorithmIdentifierFinder
struct  DefaultDigestAlgorithmIdentifierFinder_t5BE60CA84E15549D1FD606CCC88BA26483D72694  : public RuntimeObject
{
public:

public:
};

struct DefaultDigestAlgorithmIdentifierFinder_t5BE60CA84E15549D1FD606CCC88BA26483D72694_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultDigestAlgorithmIdentifierFinder::digestOids
	RuntimeObject* ___digestOids_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultDigestAlgorithmIdentifierFinder::digestNameToOids
	RuntimeObject* ___digestNameToOids_1;

public:
	inline static int32_t get_offset_of_digestOids_0() { return static_cast<int32_t>(offsetof(DefaultDigestAlgorithmIdentifierFinder_t5BE60CA84E15549D1FD606CCC88BA26483D72694_StaticFields, ___digestOids_0)); }
	inline RuntimeObject* get_digestOids_0() const { return ___digestOids_0; }
	inline RuntimeObject** get_address_of_digestOids_0() { return &___digestOids_0; }
	inline void set_digestOids_0(RuntimeObject* value)
	{
		___digestOids_0 = value;
		Il2CppCodeGenWriteBarrier((&___digestOids_0), value);
	}

	inline static int32_t get_offset_of_digestNameToOids_1() { return static_cast<int32_t>(offsetof(DefaultDigestAlgorithmIdentifierFinder_t5BE60CA84E15549D1FD606CCC88BA26483D72694_StaticFields, ___digestNameToOids_1)); }
	inline RuntimeObject* get_digestNameToOids_1() const { return ___digestNameToOids_1; }
	inline RuntimeObject** get_address_of_digestNameToOids_1() { return &___digestNameToOids_1; }
	inline void set_digestNameToOids_1(RuntimeObject* value)
	{
		___digestNameToOids_1 = value;
		Il2CppCodeGenWriteBarrier((&___digestNameToOids_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTDIGESTALGORITHMIDENTIFIERFINDER_T5BE60CA84E15549D1FD606CCC88BA26483D72694_H
#ifndef DEFAULTSIGNATUREALGORITHMIDENTIFIERFINDER_T832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_H
#define DEFAULTSIGNATUREALGORITHMIDENTIFIERFINDER_T832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder
struct  DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E  : public RuntimeObject
{
public:

public:
};

struct DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::algorithms
	RuntimeObject* ___algorithms_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::noParams
	RuntimeObject* ___noParams_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::_params
	RuntimeObject* ____params_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::pkcs15RsaEncryption
	RuntimeObject* ___pkcs15RsaEncryption_3;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::digestOids
	RuntimeObject* ___digestOids_4;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::digestBuilders
	RuntimeObject* ___digestBuilders_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::ENCRYPTION_RSA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ENCRYPTION_RSA_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::ENCRYPTION_DSA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ENCRYPTION_DSA_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::ENCRYPTION_ECDSA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ENCRYPTION_ECDSA_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::ENCRYPTION_RSA_PSS
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ENCRYPTION_RSA_PSS_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::ENCRYPTION_GOST3410
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ENCRYPTION_GOST3410_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::ENCRYPTION_ECGOST3410
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ENCRYPTION_ECGOST3410_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::ENCRYPTION_ECGOST3410_2012_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ENCRYPTION_ECGOST3410_2012_256_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignatureAlgorithmIdentifierFinder::ENCRYPTION_ECGOST3410_2012_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ENCRYPTION_ECGOST3410_2012_512_13;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}

	inline static int32_t get_offset_of_noParams_1() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___noParams_1)); }
	inline RuntimeObject* get_noParams_1() const { return ___noParams_1; }
	inline RuntimeObject** get_address_of_noParams_1() { return &___noParams_1; }
	inline void set_noParams_1(RuntimeObject* value)
	{
		___noParams_1 = value;
		Il2CppCodeGenWriteBarrier((&___noParams_1), value);
	}

	inline static int32_t get_offset_of__params_2() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ____params_2)); }
	inline RuntimeObject* get__params_2() const { return ____params_2; }
	inline RuntimeObject** get_address_of__params_2() { return &____params_2; }
	inline void set__params_2(RuntimeObject* value)
	{
		____params_2 = value;
		Il2CppCodeGenWriteBarrier((&____params_2), value);
	}

	inline static int32_t get_offset_of_pkcs15RsaEncryption_3() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___pkcs15RsaEncryption_3)); }
	inline RuntimeObject* get_pkcs15RsaEncryption_3() const { return ___pkcs15RsaEncryption_3; }
	inline RuntimeObject** get_address_of_pkcs15RsaEncryption_3() { return &___pkcs15RsaEncryption_3; }
	inline void set_pkcs15RsaEncryption_3(RuntimeObject* value)
	{
		___pkcs15RsaEncryption_3 = value;
		Il2CppCodeGenWriteBarrier((&___pkcs15RsaEncryption_3), value);
	}

	inline static int32_t get_offset_of_digestOids_4() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___digestOids_4)); }
	inline RuntimeObject* get_digestOids_4() const { return ___digestOids_4; }
	inline RuntimeObject** get_address_of_digestOids_4() { return &___digestOids_4; }
	inline void set_digestOids_4(RuntimeObject* value)
	{
		___digestOids_4 = value;
		Il2CppCodeGenWriteBarrier((&___digestOids_4), value);
	}

	inline static int32_t get_offset_of_digestBuilders_5() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___digestBuilders_5)); }
	inline RuntimeObject* get_digestBuilders_5() const { return ___digestBuilders_5; }
	inline RuntimeObject** get_address_of_digestBuilders_5() { return &___digestBuilders_5; }
	inline void set_digestBuilders_5(RuntimeObject* value)
	{
		___digestBuilders_5 = value;
		Il2CppCodeGenWriteBarrier((&___digestBuilders_5), value);
	}

	inline static int32_t get_offset_of_ENCRYPTION_RSA_6() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___ENCRYPTION_RSA_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ENCRYPTION_RSA_6() const { return ___ENCRYPTION_RSA_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ENCRYPTION_RSA_6() { return &___ENCRYPTION_RSA_6; }
	inline void set_ENCRYPTION_RSA_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ENCRYPTION_RSA_6 = value;
		Il2CppCodeGenWriteBarrier((&___ENCRYPTION_RSA_6), value);
	}

	inline static int32_t get_offset_of_ENCRYPTION_DSA_7() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___ENCRYPTION_DSA_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ENCRYPTION_DSA_7() const { return ___ENCRYPTION_DSA_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ENCRYPTION_DSA_7() { return &___ENCRYPTION_DSA_7; }
	inline void set_ENCRYPTION_DSA_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ENCRYPTION_DSA_7 = value;
		Il2CppCodeGenWriteBarrier((&___ENCRYPTION_DSA_7), value);
	}

	inline static int32_t get_offset_of_ENCRYPTION_ECDSA_8() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___ENCRYPTION_ECDSA_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ENCRYPTION_ECDSA_8() const { return ___ENCRYPTION_ECDSA_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ENCRYPTION_ECDSA_8() { return &___ENCRYPTION_ECDSA_8; }
	inline void set_ENCRYPTION_ECDSA_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ENCRYPTION_ECDSA_8 = value;
		Il2CppCodeGenWriteBarrier((&___ENCRYPTION_ECDSA_8), value);
	}

	inline static int32_t get_offset_of_ENCRYPTION_RSA_PSS_9() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___ENCRYPTION_RSA_PSS_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ENCRYPTION_RSA_PSS_9() const { return ___ENCRYPTION_RSA_PSS_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ENCRYPTION_RSA_PSS_9() { return &___ENCRYPTION_RSA_PSS_9; }
	inline void set_ENCRYPTION_RSA_PSS_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ENCRYPTION_RSA_PSS_9 = value;
		Il2CppCodeGenWriteBarrier((&___ENCRYPTION_RSA_PSS_9), value);
	}

	inline static int32_t get_offset_of_ENCRYPTION_GOST3410_10() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___ENCRYPTION_GOST3410_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ENCRYPTION_GOST3410_10() const { return ___ENCRYPTION_GOST3410_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ENCRYPTION_GOST3410_10() { return &___ENCRYPTION_GOST3410_10; }
	inline void set_ENCRYPTION_GOST3410_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ENCRYPTION_GOST3410_10 = value;
		Il2CppCodeGenWriteBarrier((&___ENCRYPTION_GOST3410_10), value);
	}

	inline static int32_t get_offset_of_ENCRYPTION_ECGOST3410_11() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___ENCRYPTION_ECGOST3410_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ENCRYPTION_ECGOST3410_11() const { return ___ENCRYPTION_ECGOST3410_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ENCRYPTION_ECGOST3410_11() { return &___ENCRYPTION_ECGOST3410_11; }
	inline void set_ENCRYPTION_ECGOST3410_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ENCRYPTION_ECGOST3410_11 = value;
		Il2CppCodeGenWriteBarrier((&___ENCRYPTION_ECGOST3410_11), value);
	}

	inline static int32_t get_offset_of_ENCRYPTION_ECGOST3410_2012_256_12() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___ENCRYPTION_ECGOST3410_2012_256_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ENCRYPTION_ECGOST3410_2012_256_12() const { return ___ENCRYPTION_ECGOST3410_2012_256_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ENCRYPTION_ECGOST3410_2012_256_12() { return &___ENCRYPTION_ECGOST3410_2012_256_12; }
	inline void set_ENCRYPTION_ECGOST3410_2012_256_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ENCRYPTION_ECGOST3410_2012_256_12 = value;
		Il2CppCodeGenWriteBarrier((&___ENCRYPTION_ECGOST3410_2012_256_12), value);
	}

	inline static int32_t get_offset_of_ENCRYPTION_ECGOST3410_2012_512_13() { return static_cast<int32_t>(offsetof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields, ___ENCRYPTION_ECGOST3410_2012_512_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ENCRYPTION_ECGOST3410_2012_512_13() const { return ___ENCRYPTION_ECGOST3410_2012_512_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ENCRYPTION_ECGOST3410_2012_512_13() { return &___ENCRYPTION_ECGOST3410_2012_512_13; }
	inline void set_ENCRYPTION_ECGOST3410_2012_512_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ENCRYPTION_ECGOST3410_2012_512_13 = value;
		Il2CppCodeGenWriteBarrier((&___ENCRYPTION_ECGOST3410_2012_512_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSIGNATUREALGORITHMIDENTIFIERFINDER_T832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_H
#ifndef DEFAULTSIGNEDATTRIBUTETABLEGENERATOR_TE94AB39D68713C599FCC9FFA5D7347D4C9121DD8_H
#define DEFAULTSIGNEDATTRIBUTETABLEGENERATOR_TE94AB39D68713C599FCC9FFA5D7347D4C9121DD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignedAttributeTableGenerator
struct  DefaultSignedAttributeTableGenerator_tE94AB39D68713C599FCC9FFA5D7347D4C9121DD8  : public RuntimeObject
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.DefaultSignedAttributeTableGenerator::table
	RuntimeObject* ___table_0;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(DefaultSignedAttributeTableGenerator_tE94AB39D68713C599FCC9FFA5D7347D4C9121DD8, ___table_0)); }
	inline RuntimeObject* get_table_0() const { return ___table_0; }
	inline RuntimeObject** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(RuntimeObject* value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSIGNEDATTRIBUTETABLEGENERATOR_TE94AB39D68713C599FCC9FFA5D7347D4C9121DD8_H
#ifndef ENVELOPEDDATAHELPER_T927BADD2921320FC22E97784092748945C3385B5_H
#define ENVELOPEDDATAHELPER_T927BADD2921320FC22E97784092748945C3385B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.EnvelopedDataHelper
struct  EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5  : public RuntimeObject
{
public:

public:
};

struct EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.EnvelopedDataHelper::BaseCipherNames
	RuntimeObject* ___BaseCipherNames_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.EnvelopedDataHelper::MacAlgNames
	RuntimeObject* ___MacAlgNames_1;

public:
	inline static int32_t get_offset_of_BaseCipherNames_0() { return static_cast<int32_t>(offsetof(EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5_StaticFields, ___BaseCipherNames_0)); }
	inline RuntimeObject* get_BaseCipherNames_0() const { return ___BaseCipherNames_0; }
	inline RuntimeObject** get_address_of_BaseCipherNames_0() { return &___BaseCipherNames_0; }
	inline void set_BaseCipherNames_0(RuntimeObject* value)
	{
		___BaseCipherNames_0 = value;
		Il2CppCodeGenWriteBarrier((&___BaseCipherNames_0), value);
	}

	inline static int32_t get_offset_of_MacAlgNames_1() { return static_cast<int32_t>(offsetof(EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5_StaticFields, ___MacAlgNames_1)); }
	inline RuntimeObject* get_MacAlgNames_1() const { return ___MacAlgNames_1; }
	inline RuntimeObject** get_address_of_MacAlgNames_1() { return &___MacAlgNames_1; }
	inline void set_MacAlgNames_1(RuntimeObject* value)
	{
		___MacAlgNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___MacAlgNames_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVELOPEDDATAHELPER_T927BADD2921320FC22E97784092748945C3385B5_H
#ifndef KEKRECIPIENTINFOGENERATOR_TF8307F35689C819FBF5DBB384E62ECE676B47076_H
#define KEKRECIPIENTINFOGENERATOR_TF8307F35689C819FBF5DBB384E62ECE676B47076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KekRecipientInfoGenerator
struct  KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KekRecipientInfoGenerator::keyEncryptionKey
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * ___keyEncryptionKey_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KekRecipientInfoGenerator::keyEncryptionKeyOID
	String_t* ___keyEncryptionKeyOID_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KekRecipientInfoGenerator::kekIdentifier
	KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658 * ___kekIdentifier_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KekRecipientInfoGenerator::keyEncryptionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___keyEncryptionAlgorithm_4;

public:
	inline static int32_t get_offset_of_keyEncryptionKey_1() { return static_cast<int32_t>(offsetof(KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076, ___keyEncryptionKey_1)); }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * get_keyEncryptionKey_1() const { return ___keyEncryptionKey_1; }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F ** get_address_of_keyEncryptionKey_1() { return &___keyEncryptionKey_1; }
	inline void set_keyEncryptionKey_1(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * value)
	{
		___keyEncryptionKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___keyEncryptionKey_1), value);
	}

	inline static int32_t get_offset_of_keyEncryptionKeyOID_2() { return static_cast<int32_t>(offsetof(KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076, ___keyEncryptionKeyOID_2)); }
	inline String_t* get_keyEncryptionKeyOID_2() const { return ___keyEncryptionKeyOID_2; }
	inline String_t** get_address_of_keyEncryptionKeyOID_2() { return &___keyEncryptionKeyOID_2; }
	inline void set_keyEncryptionKeyOID_2(String_t* value)
	{
		___keyEncryptionKeyOID_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyEncryptionKeyOID_2), value);
	}

	inline static int32_t get_offset_of_kekIdentifier_3() { return static_cast<int32_t>(offsetof(KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076, ___kekIdentifier_3)); }
	inline KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658 * get_kekIdentifier_3() const { return ___kekIdentifier_3; }
	inline KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658 ** get_address_of_kekIdentifier_3() { return &___kekIdentifier_3; }
	inline void set_kekIdentifier_3(KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658 * value)
	{
		___kekIdentifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___kekIdentifier_3), value);
	}

	inline static int32_t get_offset_of_keyEncryptionAlgorithm_4() { return static_cast<int32_t>(offsetof(KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076, ___keyEncryptionAlgorithm_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_keyEncryptionAlgorithm_4() const { return ___keyEncryptionAlgorithm_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_keyEncryptionAlgorithm_4() { return &___keyEncryptionAlgorithm_4; }
	inline void set_keyEncryptionAlgorithm_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___keyEncryptionAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___keyEncryptionAlgorithm_4), value);
	}
};

struct KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KekRecipientInfoGenerator::Helper
	CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * ___Helper_0;

public:
	inline static int32_t get_offset_of_Helper_0() { return static_cast<int32_t>(offsetof(KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076_StaticFields, ___Helper_0)); }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * get_Helper_0() const { return ___Helper_0; }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 ** get_address_of_Helper_0() { return &___Helper_0; }
	inline void set_Helper_0(CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * value)
	{
		___Helper_0 = value;
		Il2CppCodeGenWriteBarrier((&___Helper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEKRECIPIENTINFOGENERATOR_TF8307F35689C819FBF5DBB384E62ECE676B47076_H
#ifndef KEYAGREERECIPIENTINFOGENERATOR_T5218DF122585417200F76F86844A7EBC76C8F2BB_H
#define KEYAGREERECIPIENTINFOGENERATOR_T5218DF122585417200F76F86844A7EBC76C8F2BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyAgreeRecipientInfoGenerator
struct  KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyAgreeRecipientInfoGenerator::keyAgreementOID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___keyAgreementOID_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyAgreeRecipientInfoGenerator::keyEncryptionOID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___keyEncryptionOID_2;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyAgreeRecipientInfoGenerator::recipientCerts
	RuntimeObject* ___recipientCerts_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyAgreeRecipientInfoGenerator::senderKeyPair
	AsymmetricCipherKeyPair_t4630D28256FA535784C97BBD4D410AB0DC19FC24 * ___senderKeyPair_4;

public:
	inline static int32_t get_offset_of_keyAgreementOID_1() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB, ___keyAgreementOID_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_keyAgreementOID_1() const { return ___keyAgreementOID_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_keyAgreementOID_1() { return &___keyAgreementOID_1; }
	inline void set_keyAgreementOID_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___keyAgreementOID_1 = value;
		Il2CppCodeGenWriteBarrier((&___keyAgreementOID_1), value);
	}

	inline static int32_t get_offset_of_keyEncryptionOID_2() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB, ___keyEncryptionOID_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_keyEncryptionOID_2() const { return ___keyEncryptionOID_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_keyEncryptionOID_2() { return &___keyEncryptionOID_2; }
	inline void set_keyEncryptionOID_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___keyEncryptionOID_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyEncryptionOID_2), value);
	}

	inline static int32_t get_offset_of_recipientCerts_3() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB, ___recipientCerts_3)); }
	inline RuntimeObject* get_recipientCerts_3() const { return ___recipientCerts_3; }
	inline RuntimeObject** get_address_of_recipientCerts_3() { return &___recipientCerts_3; }
	inline void set_recipientCerts_3(RuntimeObject* value)
	{
		___recipientCerts_3 = value;
		Il2CppCodeGenWriteBarrier((&___recipientCerts_3), value);
	}

	inline static int32_t get_offset_of_senderKeyPair_4() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB, ___senderKeyPair_4)); }
	inline AsymmetricCipherKeyPair_t4630D28256FA535784C97BBD4D410AB0DC19FC24 * get_senderKeyPair_4() const { return ___senderKeyPair_4; }
	inline AsymmetricCipherKeyPair_t4630D28256FA535784C97BBD4D410AB0DC19FC24 ** get_address_of_senderKeyPair_4() { return &___senderKeyPair_4; }
	inline void set_senderKeyPair_4(AsymmetricCipherKeyPair_t4630D28256FA535784C97BBD4D410AB0DC19FC24 * value)
	{
		___senderKeyPair_4 = value;
		Il2CppCodeGenWriteBarrier((&___senderKeyPair_4), value);
	}
};

struct KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyAgreeRecipientInfoGenerator::Helper
	CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * ___Helper_0;

public:
	inline static int32_t get_offset_of_Helper_0() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB_StaticFields, ___Helper_0)); }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * get_Helper_0() const { return ___Helper_0; }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 ** get_address_of_Helper_0() { return &___Helper_0; }
	inline void set_Helper_0(CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * value)
	{
		___Helper_0 = value;
		Il2CppCodeGenWriteBarrier((&___Helper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYAGREERECIPIENTINFOGENERATOR_T5218DF122585417200F76F86844A7EBC76C8F2BB_H
#ifndef KEYTRANSRECIPIENTINFOGENERATOR_T2A9824564CB74998707741E748634E314C7CF453_H
#define KEYTRANSRECIPIENTINFOGENERATOR_T2A9824564CB74998707741E748634E314C7CF453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator
struct  KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::recipientTbsCert
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 * ___recipientTbsCert_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::recipientPublicKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___recipientPublicKey_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::subjectKeyIdentifier
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___subjectKeyIdentifier_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::info
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___info_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.IssuerAndSerialNumber BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::issuerAndSerialNumber
	IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC * ___issuerAndSerialNumber_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_6;

public:
	inline static int32_t get_offset_of_recipientTbsCert_1() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___recipientTbsCert_1)); }
	inline TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 * get_recipientTbsCert_1() const { return ___recipientTbsCert_1; }
	inline TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 ** get_address_of_recipientTbsCert_1() { return &___recipientTbsCert_1; }
	inline void set_recipientTbsCert_1(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 * value)
	{
		___recipientTbsCert_1 = value;
		Il2CppCodeGenWriteBarrier((&___recipientTbsCert_1), value);
	}

	inline static int32_t get_offset_of_recipientPublicKey_2() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___recipientPublicKey_2)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_recipientPublicKey_2() const { return ___recipientPublicKey_2; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_recipientPublicKey_2() { return &___recipientPublicKey_2; }
	inline void set_recipientPublicKey_2(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___recipientPublicKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___recipientPublicKey_2), value);
	}

	inline static int32_t get_offset_of_subjectKeyIdentifier_3() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___subjectKeyIdentifier_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_subjectKeyIdentifier_3() const { return ___subjectKeyIdentifier_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_subjectKeyIdentifier_3() { return &___subjectKeyIdentifier_3; }
	inline void set_subjectKeyIdentifier_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___subjectKeyIdentifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___subjectKeyIdentifier_3), value);
	}

	inline static int32_t get_offset_of_info_4() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___info_4)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_info_4() const { return ___info_4; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_info_4() { return &___info_4; }
	inline void set_info_4(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___info_4 = value;
		Il2CppCodeGenWriteBarrier((&___info_4), value);
	}

	inline static int32_t get_offset_of_issuerAndSerialNumber_5() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___issuerAndSerialNumber_5)); }
	inline IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC * get_issuerAndSerialNumber_5() const { return ___issuerAndSerialNumber_5; }
	inline IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC ** get_address_of_issuerAndSerialNumber_5() { return &___issuerAndSerialNumber_5; }
	inline void set_issuerAndSerialNumber_5(IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC * value)
	{
		___issuerAndSerialNumber_5 = value;
		Il2CppCodeGenWriteBarrier((&___issuerAndSerialNumber_5), value);
	}

	inline static int32_t get_offset_of_random_6() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___random_6)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_6() const { return ___random_6; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_6() { return &___random_6; }
	inline void set_random_6(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_6 = value;
		Il2CppCodeGenWriteBarrier((&___random_6), value);
	}
};

struct KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::Helper
	CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * ___Helper_0;

public:
	inline static int32_t get_offset_of_Helper_0() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453_StaticFields, ___Helper_0)); }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * get_Helper_0() const { return ___Helper_0; }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 ** get_address_of_Helper_0() { return &___Helper_0; }
	inline void set_Helper_0(CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * value)
	{
		___Helper_0 = value;
		Il2CppCodeGenWriteBarrier((&___Helper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYTRANSRECIPIENTINFOGENERATOR_T2A9824564CB74998707741E748634E314C7CF453_H
#ifndef ORIGINATORINFOGENERATOR_T1BDF7D5D5770B2B788B322D9FAF4A011D6B1AC35_H
#define ORIGINATORINFOGENERATOR_T1BDF7D5D5770B2B788B322D9FAF4A011D6B1AC35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.OriginatorInfoGenerator
struct  OriginatorInfoGenerator_t1BDF7D5D5770B2B788B322D9FAF4A011D6B1AC35  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.OriginatorInfoGenerator::origCerts
	RuntimeObject* ___origCerts_0;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.OriginatorInfoGenerator::origCrls
	RuntimeObject* ___origCrls_1;

public:
	inline static int32_t get_offset_of_origCerts_0() { return static_cast<int32_t>(offsetof(OriginatorInfoGenerator_t1BDF7D5D5770B2B788B322D9FAF4A011D6B1AC35, ___origCerts_0)); }
	inline RuntimeObject* get_origCerts_0() const { return ___origCerts_0; }
	inline RuntimeObject** get_address_of_origCerts_0() { return &___origCerts_0; }
	inline void set_origCerts_0(RuntimeObject* value)
	{
		___origCerts_0 = value;
		Il2CppCodeGenWriteBarrier((&___origCerts_0), value);
	}

	inline static int32_t get_offset_of_origCrls_1() { return static_cast<int32_t>(offsetof(OriginatorInfoGenerator_t1BDF7D5D5770B2B788B322D9FAF4A011D6B1AC35, ___origCrls_1)); }
	inline RuntimeObject* get_origCrls_1() const { return ___origCrls_1; }
	inline RuntimeObject** get_address_of_origCrls_1() { return &___origCrls_1; }
	inline void set_origCrls_1(RuntimeObject* value)
	{
		___origCrls_1 = value;
		Il2CppCodeGenWriteBarrier((&___origCrls_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIGINATORINFOGENERATOR_T1BDF7D5D5770B2B788B322D9FAF4A011D6B1AC35_H
#ifndef ORIGINATORINFORMATION_T7A1FD3BA30B65CAB1D4B2C4F037E5697AB3A9604_H
#define ORIGINATORINFORMATION_T7A1FD3BA30B65CAB1D4B2C4F037E5697AB3A9604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.OriginatorInformation
struct  OriginatorInformation_t7A1FD3BA30B65CAB1D4B2C4F037E5697AB3A9604  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.OriginatorInformation::originatorInfo
	OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * ___originatorInfo_0;

public:
	inline static int32_t get_offset_of_originatorInfo_0() { return static_cast<int32_t>(offsetof(OriginatorInformation_t7A1FD3BA30B65CAB1D4B2C4F037E5697AB3A9604, ___originatorInfo_0)); }
	inline OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * get_originatorInfo_0() const { return ___originatorInfo_0; }
	inline OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F ** get_address_of_originatorInfo_0() { return &___originatorInfo_0; }
	inline void set_originatorInfo_0(OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * value)
	{
		___originatorInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___originatorInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIGINATORINFORMATION_T7A1FD3BA30B65CAB1D4B2C4F037E5697AB3A9604_H
#ifndef PASSWORDRECIPIENTINFOGENERATOR_T02923FCB678BC61C246E7A4E18BE8814332ECB23_H
#define PASSWORDRECIPIENTINFOGENERATOR_T02923FCB678BC61C246E7A4E18BE8814332ECB23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.PasswordRecipientInfoGenerator
struct  PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.PasswordRecipientInfoGenerator::keyDerivationAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___keyDerivationAlgorithm_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.PasswordRecipientInfoGenerator::keyEncryptionKey
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * ___keyEncryptionKey_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.PasswordRecipientInfoGenerator::keyEncryptionKeyOID
	String_t* ___keyEncryptionKeyOID_3;

public:
	inline static int32_t get_offset_of_keyDerivationAlgorithm_1() { return static_cast<int32_t>(offsetof(PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23, ___keyDerivationAlgorithm_1)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_keyDerivationAlgorithm_1() const { return ___keyDerivationAlgorithm_1; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_keyDerivationAlgorithm_1() { return &___keyDerivationAlgorithm_1; }
	inline void set_keyDerivationAlgorithm_1(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___keyDerivationAlgorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&___keyDerivationAlgorithm_1), value);
	}

	inline static int32_t get_offset_of_keyEncryptionKey_2() { return static_cast<int32_t>(offsetof(PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23, ___keyEncryptionKey_2)); }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * get_keyEncryptionKey_2() const { return ___keyEncryptionKey_2; }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F ** get_address_of_keyEncryptionKey_2() { return &___keyEncryptionKey_2; }
	inline void set_keyEncryptionKey_2(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * value)
	{
		___keyEncryptionKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyEncryptionKey_2), value);
	}

	inline static int32_t get_offset_of_keyEncryptionKeyOID_3() { return static_cast<int32_t>(offsetof(PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23, ___keyEncryptionKeyOID_3)); }
	inline String_t* get_keyEncryptionKeyOID_3() const { return ___keyEncryptionKeyOID_3; }
	inline String_t** get_address_of_keyEncryptionKeyOID_3() { return &___keyEncryptionKeyOID_3; }
	inline void set_keyEncryptionKeyOID_3(String_t* value)
	{
		___keyEncryptionKeyOID_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyEncryptionKeyOID_3), value);
	}
};

struct PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.PasswordRecipientInfoGenerator::Helper
	CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * ___Helper_0;

public:
	inline static int32_t get_offset_of_Helper_0() { return static_cast<int32_t>(offsetof(PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23_StaticFields, ___Helper_0)); }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * get_Helper_0() const { return ___Helper_0; }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 ** get_address_of_Helper_0() { return &___Helper_0; }
	inline void set_Helper_0(CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * value)
	{
		___Helper_0 = value;
		Il2CppCodeGenWriteBarrier((&___Helper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSWORDRECIPIENTINFOGENERATOR_T02923FCB678BC61C246E7A4E18BE8814332ECB23_H
#ifndef RECIPIENTINFORMATION_T3F7F606CAD85887D18351A4C022570EADC97E483_H
#define RECIPIENTINFORMATION_T3F7F606CAD85887D18351A4C022570EADC97E483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformation
struct  RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientID BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformation::rid
	RecipientID_t25DA4BBF07A25C6047B6350910E7FCA460AE920B * ___rid_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformation::keyEncAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___keyEncAlg_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSecureReadable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformation::secureReadable
	RuntimeObject* ___secureReadable_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformation::resultMac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___resultMac_3;

public:
	inline static int32_t get_offset_of_rid_0() { return static_cast<int32_t>(offsetof(RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483, ___rid_0)); }
	inline RecipientID_t25DA4BBF07A25C6047B6350910E7FCA460AE920B * get_rid_0() const { return ___rid_0; }
	inline RecipientID_t25DA4BBF07A25C6047B6350910E7FCA460AE920B ** get_address_of_rid_0() { return &___rid_0; }
	inline void set_rid_0(RecipientID_t25DA4BBF07A25C6047B6350910E7FCA460AE920B * value)
	{
		___rid_0 = value;
		Il2CppCodeGenWriteBarrier((&___rid_0), value);
	}

	inline static int32_t get_offset_of_keyEncAlg_1() { return static_cast<int32_t>(offsetof(RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483, ___keyEncAlg_1)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_keyEncAlg_1() const { return ___keyEncAlg_1; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_keyEncAlg_1() { return &___keyEncAlg_1; }
	inline void set_keyEncAlg_1(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___keyEncAlg_1 = value;
		Il2CppCodeGenWriteBarrier((&___keyEncAlg_1), value);
	}

	inline static int32_t get_offset_of_secureReadable_2() { return static_cast<int32_t>(offsetof(RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483, ___secureReadable_2)); }
	inline RuntimeObject* get_secureReadable_2() const { return ___secureReadable_2; }
	inline RuntimeObject** get_address_of_secureReadable_2() { return &___secureReadable_2; }
	inline void set_secureReadable_2(RuntimeObject* value)
	{
		___secureReadable_2 = value;
		Il2CppCodeGenWriteBarrier((&___secureReadable_2), value);
	}

	inline static int32_t get_offset_of_resultMac_3() { return static_cast<int32_t>(offsetof(RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483, ___resultMac_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_resultMac_3() const { return ___resultMac_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_resultMac_3() { return &___resultMac_3; }
	inline void set_resultMac_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___resultMac_3 = value;
		Il2CppCodeGenWriteBarrier((&___resultMac_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECIPIENTINFORMATION_T3F7F606CAD85887D18351A4C022570EADC97E483_H
#ifndef RECIPIENTINFORMATIONSTORE_T10C56AA2333E7D948FC6BE5326CCF5B463B614DB_H
#define RECIPIENTINFORMATIONSTORE_T10C56AA2333E7D948FC6BE5326CCF5B463B614DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformationStore
struct  RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformationStore::all
	RuntimeObject* ___all_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformationStore::table
	RuntimeObject* ___table_1;

public:
	inline static int32_t get_offset_of_all_0() { return static_cast<int32_t>(offsetof(RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB, ___all_0)); }
	inline RuntimeObject* get_all_0() const { return ___all_0; }
	inline RuntimeObject** get_address_of_all_0() { return &___all_0; }
	inline void set_all_0(RuntimeObject* value)
	{
		___all_0 = value;
		Il2CppCodeGenWriteBarrier((&___all_0), value);
	}

	inline static int32_t get_offset_of_table_1() { return static_cast<int32_t>(offsetof(RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB, ___table_1)); }
	inline RuntimeObject* get_table_1() const { return ___table_1; }
	inline RuntimeObject** get_address_of_table_1() { return &___table_1; }
	inline void set_table_1(RuntimeObject* value)
	{
		___table_1 = value;
		Il2CppCodeGenWriteBarrier((&___table_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECIPIENTINFORMATIONSTORE_T10C56AA2333E7D948FC6BE5326CCF5B463B614DB_H
#ifndef SIGNERINFOGENERATOR_T6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B_H
#define SIGNERINFOGENERATOR_T6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInfoGenerator
struct  SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInfoGenerator::certificate
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * ___certificate_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISignatureFactory BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInfoGenerator::contentSigner
	RuntimeObject* ___contentSigner_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInfoGenerator::sigId
	SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * ___sigId_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInfoGenerator::signedGen
	RuntimeObject* ___signedGen_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInfoGenerator::unsignedGen
	RuntimeObject* ___unsignedGen_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInfoGenerator::isDirectSignature
	bool ___isDirectSignature_5;

public:
	inline static int32_t get_offset_of_certificate_0() { return static_cast<int32_t>(offsetof(SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B, ___certificate_0)); }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * get_certificate_0() const { return ___certificate_0; }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A ** get_address_of_certificate_0() { return &___certificate_0; }
	inline void set_certificate_0(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * value)
	{
		___certificate_0 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_0), value);
	}

	inline static int32_t get_offset_of_contentSigner_1() { return static_cast<int32_t>(offsetof(SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B, ___contentSigner_1)); }
	inline RuntimeObject* get_contentSigner_1() const { return ___contentSigner_1; }
	inline RuntimeObject** get_address_of_contentSigner_1() { return &___contentSigner_1; }
	inline void set_contentSigner_1(RuntimeObject* value)
	{
		___contentSigner_1 = value;
		Il2CppCodeGenWriteBarrier((&___contentSigner_1), value);
	}

	inline static int32_t get_offset_of_sigId_2() { return static_cast<int32_t>(offsetof(SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B, ___sigId_2)); }
	inline SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * get_sigId_2() const { return ___sigId_2; }
	inline SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 ** get_address_of_sigId_2() { return &___sigId_2; }
	inline void set_sigId_2(SignerIdentifier_tA0E23B3850CF58D37B6AEDC17BF0C7B3D2A473B4 * value)
	{
		___sigId_2 = value;
		Il2CppCodeGenWriteBarrier((&___sigId_2), value);
	}

	inline static int32_t get_offset_of_signedGen_3() { return static_cast<int32_t>(offsetof(SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B, ___signedGen_3)); }
	inline RuntimeObject* get_signedGen_3() const { return ___signedGen_3; }
	inline RuntimeObject** get_address_of_signedGen_3() { return &___signedGen_3; }
	inline void set_signedGen_3(RuntimeObject* value)
	{
		___signedGen_3 = value;
		Il2CppCodeGenWriteBarrier((&___signedGen_3), value);
	}

	inline static int32_t get_offset_of_unsignedGen_4() { return static_cast<int32_t>(offsetof(SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B, ___unsignedGen_4)); }
	inline RuntimeObject* get_unsignedGen_4() const { return ___unsignedGen_4; }
	inline RuntimeObject** get_address_of_unsignedGen_4() { return &___unsignedGen_4; }
	inline void set_unsignedGen_4(RuntimeObject* value)
	{
		___unsignedGen_4 = value;
		Il2CppCodeGenWriteBarrier((&___unsignedGen_4), value);
	}

	inline static int32_t get_offset_of_isDirectSignature_5() { return static_cast<int32_t>(offsetof(SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B, ___isDirectSignature_5)); }
	inline bool get_isDirectSignature_5() const { return ___isDirectSignature_5; }
	inline bool* get_address_of_isDirectSignature_5() { return &___isDirectSignature_5; }
	inline void set_isDirectSignature_5(bool value)
	{
		___isDirectSignature_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERINFOGENERATOR_T6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B_H
#ifndef SIGNERINFOGENERATORBUILDER_TF36884E6798566C94006141926E62168477D61D8_H
#define SIGNERINFOGENERATORBUILDER_TF36884E6798566C94006141926E62168477D61D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInfoGeneratorBuilder
struct  SignerInfoGeneratorBuilder_tF36884E6798566C94006141926E62168477D61D8  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInfoGeneratorBuilder::directSignature
	bool ___directSignature_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInfoGeneratorBuilder::signedGen
	RuntimeObject* ___signedGen_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInfoGeneratorBuilder::unsignedGen
	RuntimeObject* ___unsignedGen_2;

public:
	inline static int32_t get_offset_of_directSignature_0() { return static_cast<int32_t>(offsetof(SignerInfoGeneratorBuilder_tF36884E6798566C94006141926E62168477D61D8, ___directSignature_0)); }
	inline bool get_directSignature_0() const { return ___directSignature_0; }
	inline bool* get_address_of_directSignature_0() { return &___directSignature_0; }
	inline void set_directSignature_0(bool value)
	{
		___directSignature_0 = value;
	}

	inline static int32_t get_offset_of_signedGen_1() { return static_cast<int32_t>(offsetof(SignerInfoGeneratorBuilder_tF36884E6798566C94006141926E62168477D61D8, ___signedGen_1)); }
	inline RuntimeObject* get_signedGen_1() const { return ___signedGen_1; }
	inline RuntimeObject** get_address_of_signedGen_1() { return &___signedGen_1; }
	inline void set_signedGen_1(RuntimeObject* value)
	{
		___signedGen_1 = value;
		Il2CppCodeGenWriteBarrier((&___signedGen_1), value);
	}

	inline static int32_t get_offset_of_unsignedGen_2() { return static_cast<int32_t>(offsetof(SignerInfoGeneratorBuilder_tF36884E6798566C94006141926E62168477D61D8, ___unsignedGen_2)); }
	inline RuntimeObject* get_unsignedGen_2() const { return ___unsignedGen_2; }
	inline RuntimeObject** get_address_of_unsignedGen_2() { return &___unsignedGen_2; }
	inline void set_unsignedGen_2(RuntimeObject* value)
	{
		___unsignedGen_2 = value;
		Il2CppCodeGenWriteBarrier((&___unsignedGen_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERINFOGENERATORBUILDER_TF36884E6798566C94006141926E62168477D61D8_H
#ifndef SIGNERINFORMATION_T29C492A05BA5B4FAC711F9BB477C0A5C254B09A2_H
#define SIGNERINFORMATION_T29C492A05BA5B4FAC711F9BB477C0A5C254B09A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation
struct  SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerID BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::sid
	SignerID_tC9728986CC0E7C53B1C2C2DA90EB09DE973E0B15 * ___sid_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignerInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::info
	SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23 * ___info_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::digestAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___digestAlgorithm_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::encryptionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___encryptionAlgorithm_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::signedAttributeSet
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___signedAttributeSet_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::unsignedAttributeSet
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___unsignedAttributeSet_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::content
	RuntimeObject* ___content_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::signature
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___signature_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::contentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___contentType_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.IDigestCalculator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::digestCalculator
	RuntimeObject* ___digestCalculator_10;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::resultDigest
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___resultDigest_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::signedAttributeTable
	AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * ___signedAttributeTable_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::unsignedAttributeTable
	AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * ___unsignedAttributeTable_13;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::isCounterSignature
	bool ___isCounterSignature_14;

public:
	inline static int32_t get_offset_of_sid_1() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___sid_1)); }
	inline SignerID_tC9728986CC0E7C53B1C2C2DA90EB09DE973E0B15 * get_sid_1() const { return ___sid_1; }
	inline SignerID_tC9728986CC0E7C53B1C2C2DA90EB09DE973E0B15 ** get_address_of_sid_1() { return &___sid_1; }
	inline void set_sid_1(SignerID_tC9728986CC0E7C53B1C2C2DA90EB09DE973E0B15 * value)
	{
		___sid_1 = value;
		Il2CppCodeGenWriteBarrier((&___sid_1), value);
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___info_2)); }
	inline SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23 * get_info_2() const { return ___info_2; }
	inline SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23 ** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(SignerInfo_t9C562DFE2A91D18AD4100AC711D30C8BCC660B23 * value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier((&___info_2), value);
	}

	inline static int32_t get_offset_of_digestAlgorithm_3() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___digestAlgorithm_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_digestAlgorithm_3() const { return ___digestAlgorithm_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_digestAlgorithm_3() { return &___digestAlgorithm_3; }
	inline void set_digestAlgorithm_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___digestAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___digestAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_encryptionAlgorithm_4() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___encryptionAlgorithm_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_encryptionAlgorithm_4() const { return ___encryptionAlgorithm_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_encryptionAlgorithm_4() { return &___encryptionAlgorithm_4; }
	inline void set_encryptionAlgorithm_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___encryptionAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___encryptionAlgorithm_4), value);
	}

	inline static int32_t get_offset_of_signedAttributeSet_5() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___signedAttributeSet_5)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_signedAttributeSet_5() const { return ___signedAttributeSet_5; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_signedAttributeSet_5() { return &___signedAttributeSet_5; }
	inline void set_signedAttributeSet_5(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___signedAttributeSet_5 = value;
		Il2CppCodeGenWriteBarrier((&___signedAttributeSet_5), value);
	}

	inline static int32_t get_offset_of_unsignedAttributeSet_6() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___unsignedAttributeSet_6)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_unsignedAttributeSet_6() const { return ___unsignedAttributeSet_6; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_unsignedAttributeSet_6() { return &___unsignedAttributeSet_6; }
	inline void set_unsignedAttributeSet_6(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___unsignedAttributeSet_6 = value;
		Il2CppCodeGenWriteBarrier((&___unsignedAttributeSet_6), value);
	}

	inline static int32_t get_offset_of_content_7() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___content_7)); }
	inline RuntimeObject* get_content_7() const { return ___content_7; }
	inline RuntimeObject** get_address_of_content_7() { return &___content_7; }
	inline void set_content_7(RuntimeObject* value)
	{
		___content_7 = value;
		Il2CppCodeGenWriteBarrier((&___content_7), value);
	}

	inline static int32_t get_offset_of_signature_8() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___signature_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_signature_8() const { return ___signature_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_signature_8() { return &___signature_8; }
	inline void set_signature_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___signature_8 = value;
		Il2CppCodeGenWriteBarrier((&___signature_8), value);
	}

	inline static int32_t get_offset_of_contentType_9() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___contentType_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_contentType_9() const { return ___contentType_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_contentType_9() { return &___contentType_9; }
	inline void set_contentType_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___contentType_9 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_9), value);
	}

	inline static int32_t get_offset_of_digestCalculator_10() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___digestCalculator_10)); }
	inline RuntimeObject* get_digestCalculator_10() const { return ___digestCalculator_10; }
	inline RuntimeObject** get_address_of_digestCalculator_10() { return &___digestCalculator_10; }
	inline void set_digestCalculator_10(RuntimeObject* value)
	{
		___digestCalculator_10 = value;
		Il2CppCodeGenWriteBarrier((&___digestCalculator_10), value);
	}

	inline static int32_t get_offset_of_resultDigest_11() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___resultDigest_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_resultDigest_11() const { return ___resultDigest_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_resultDigest_11() { return &___resultDigest_11; }
	inline void set_resultDigest_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___resultDigest_11 = value;
		Il2CppCodeGenWriteBarrier((&___resultDigest_11), value);
	}

	inline static int32_t get_offset_of_signedAttributeTable_12() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___signedAttributeTable_12)); }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * get_signedAttributeTable_12() const { return ___signedAttributeTable_12; }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 ** get_address_of_signedAttributeTable_12() { return &___signedAttributeTable_12; }
	inline void set_signedAttributeTable_12(AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * value)
	{
		___signedAttributeTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___signedAttributeTable_12), value);
	}

	inline static int32_t get_offset_of_unsignedAttributeTable_13() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___unsignedAttributeTable_13)); }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * get_unsignedAttributeTable_13() const { return ___unsignedAttributeTable_13; }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 ** get_address_of_unsignedAttributeTable_13() { return &___unsignedAttributeTable_13; }
	inline void set_unsignedAttributeTable_13(AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * value)
	{
		___unsignedAttributeTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___unsignedAttributeTable_13), value);
	}

	inline static int32_t get_offset_of_isCounterSignature_14() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2, ___isCounterSignature_14)); }
	inline bool get_isCounterSignature_14() const { return ___isCounterSignature_14; }
	inline bool* get_address_of_isCounterSignature_14() { return &___isCounterSignature_14; }
	inline void set_isCounterSignature_14(bool value)
	{
		___isCounterSignature_14 = value;
	}
};

struct SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation::Helper
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * ___Helper_0;

public:
	inline static int32_t get_offset_of_Helper_0() { return static_cast<int32_t>(offsetof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2_StaticFields, ___Helper_0)); }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * get_Helper_0() const { return ___Helper_0; }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF ** get_address_of_Helper_0() { return &___Helper_0; }
	inline void set_Helper_0(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * value)
	{
		___Helper_0 = value;
		Il2CppCodeGenWriteBarrier((&___Helper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERINFORMATION_T29C492A05BA5B4FAC711F9BB477C0A5C254B09A2_H
#ifndef SIGNERINFORMATIONSTORE_T048803C47E6447E8380F2410432F7371601A6575_H
#define SIGNERINFORMATIONSTORE_T048803C47E6447E8380F2410432F7371601A6575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformationStore
struct  SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformationStore::all
	RuntimeObject* ___all_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformationStore::table
	RuntimeObject* ___table_1;

public:
	inline static int32_t get_offset_of_all_0() { return static_cast<int32_t>(offsetof(SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575, ___all_0)); }
	inline RuntimeObject* get_all_0() const { return ___all_0; }
	inline RuntimeObject** get_address_of_all_0() { return &___all_0; }
	inline void set_all_0(RuntimeObject* value)
	{
		___all_0 = value;
		Il2CppCodeGenWriteBarrier((&___all_0), value);
	}

	inline static int32_t get_offset_of_table_1() { return static_cast<int32_t>(offsetof(SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575, ___table_1)); }
	inline RuntimeObject* get_table_1() const { return ___table_1; }
	inline RuntimeObject** get_address_of_table_1() { return &___table_1; }
	inline void set_table_1(RuntimeObject* value)
	{
		___table_1 = value;
		Il2CppCodeGenWriteBarrier((&___table_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERINFORMATIONSTORE_T048803C47E6447E8380F2410432F7371601A6575_H
#ifndef SIMPLEATTRIBUTETABLEGENERATOR_TF8A55E8F0256D8BE547DAE85DE888E588305045B_H
#define SIMPLEATTRIBUTETABLEGENERATOR_TF8A55E8F0256D8BE547DAE85DE888E588305045B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SimpleAttributeTableGenerator
struct  SimpleAttributeTableGenerator_tF8A55E8F0256D8BE547DAE85DE888E588305045B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SimpleAttributeTableGenerator::attributes
	AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * ___attributes_0;

public:
	inline static int32_t get_offset_of_attributes_0() { return static_cast<int32_t>(offsetof(SimpleAttributeTableGenerator_tF8A55E8F0256D8BE547DAE85DE888E588305045B, ___attributes_0)); }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * get_attributes_0() const { return ___attributes_0; }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 ** get_address_of_attributes_0() { return &___attributes_0; }
	inline void set_attributes_0(AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * value)
	{
		___attributes_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEATTRIBUTETABLEGENERATOR_TF8A55E8F0256D8BE547DAE85DE888E588305045B_H
#ifndef X509CERTSTORESELECTOR_TB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9_H
#define X509CERTSTORESELECTOR_TB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector
struct  X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::authorityKeyIdentifier
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___authorityKeyIdentifier_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::basicConstraints
	int32_t ___basicConstraints_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::certificate
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * ___certificate_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::certificateValid
	DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * ___certificateValid_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::extendedKeyUsage
	RuntimeObject* ___extendedKeyUsage_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::ignoreX509NameOrdering
	bool ___ignoreX509NameOrdering_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::issuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___issuer_6;
	// System.Boolean[] BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::keyUsage
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___keyUsage_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::policy
	RuntimeObject* ___policy_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::privateKeyValid
	DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * ___privateKeyValid_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::serialNumber
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___serialNumber_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::subject
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___subject_11;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::subjectKeyIdentifier
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___subjectKeyIdentifier_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::subjectPublicKey
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___subjectPublicKey_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::subjectPublicKeyAlgID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___subjectPublicKeyAlgID_14;

public:
	inline static int32_t get_offset_of_authorityKeyIdentifier_0() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___authorityKeyIdentifier_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_authorityKeyIdentifier_0() const { return ___authorityKeyIdentifier_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_authorityKeyIdentifier_0() { return &___authorityKeyIdentifier_0; }
	inline void set_authorityKeyIdentifier_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___authorityKeyIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___authorityKeyIdentifier_0), value);
	}

	inline static int32_t get_offset_of_basicConstraints_1() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___basicConstraints_1)); }
	inline int32_t get_basicConstraints_1() const { return ___basicConstraints_1; }
	inline int32_t* get_address_of_basicConstraints_1() { return &___basicConstraints_1; }
	inline void set_basicConstraints_1(int32_t value)
	{
		___basicConstraints_1 = value;
	}

	inline static int32_t get_offset_of_certificate_2() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___certificate_2)); }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * get_certificate_2() const { return ___certificate_2; }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A ** get_address_of_certificate_2() { return &___certificate_2; }
	inline void set_certificate_2(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * value)
	{
		___certificate_2 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_2), value);
	}

	inline static int32_t get_offset_of_certificateValid_3() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___certificateValid_3)); }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * get_certificateValid_3() const { return ___certificateValid_3; }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F ** get_address_of_certificateValid_3() { return &___certificateValid_3; }
	inline void set_certificateValid_3(DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * value)
	{
		___certificateValid_3 = value;
		Il2CppCodeGenWriteBarrier((&___certificateValid_3), value);
	}

	inline static int32_t get_offset_of_extendedKeyUsage_4() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___extendedKeyUsage_4)); }
	inline RuntimeObject* get_extendedKeyUsage_4() const { return ___extendedKeyUsage_4; }
	inline RuntimeObject** get_address_of_extendedKeyUsage_4() { return &___extendedKeyUsage_4; }
	inline void set_extendedKeyUsage_4(RuntimeObject* value)
	{
		___extendedKeyUsage_4 = value;
		Il2CppCodeGenWriteBarrier((&___extendedKeyUsage_4), value);
	}

	inline static int32_t get_offset_of_ignoreX509NameOrdering_5() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___ignoreX509NameOrdering_5)); }
	inline bool get_ignoreX509NameOrdering_5() const { return ___ignoreX509NameOrdering_5; }
	inline bool* get_address_of_ignoreX509NameOrdering_5() { return &___ignoreX509NameOrdering_5; }
	inline void set_ignoreX509NameOrdering_5(bool value)
	{
		___ignoreX509NameOrdering_5 = value;
	}

	inline static int32_t get_offset_of_issuer_6() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___issuer_6)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_issuer_6() const { return ___issuer_6; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_issuer_6() { return &___issuer_6; }
	inline void set_issuer_6(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___issuer_6 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_6), value);
	}

	inline static int32_t get_offset_of_keyUsage_7() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___keyUsage_7)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_keyUsage_7() const { return ___keyUsage_7; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_keyUsage_7() { return &___keyUsage_7; }
	inline void set_keyUsage_7(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___keyUsage_7 = value;
		Il2CppCodeGenWriteBarrier((&___keyUsage_7), value);
	}

	inline static int32_t get_offset_of_policy_8() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___policy_8)); }
	inline RuntimeObject* get_policy_8() const { return ___policy_8; }
	inline RuntimeObject** get_address_of_policy_8() { return &___policy_8; }
	inline void set_policy_8(RuntimeObject* value)
	{
		___policy_8 = value;
		Il2CppCodeGenWriteBarrier((&___policy_8), value);
	}

	inline static int32_t get_offset_of_privateKeyValid_9() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___privateKeyValid_9)); }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * get_privateKeyValid_9() const { return ___privateKeyValid_9; }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F ** get_address_of_privateKeyValid_9() { return &___privateKeyValid_9; }
	inline void set_privateKeyValid_9(DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * value)
	{
		___privateKeyValid_9 = value;
		Il2CppCodeGenWriteBarrier((&___privateKeyValid_9), value);
	}

	inline static int32_t get_offset_of_serialNumber_10() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___serialNumber_10)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_serialNumber_10() const { return ___serialNumber_10; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_serialNumber_10() { return &___serialNumber_10; }
	inline void set_serialNumber_10(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___serialNumber_10 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_10), value);
	}

	inline static int32_t get_offset_of_subject_11() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___subject_11)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_subject_11() const { return ___subject_11; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_subject_11() { return &___subject_11; }
	inline void set_subject_11(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___subject_11 = value;
		Il2CppCodeGenWriteBarrier((&___subject_11), value);
	}

	inline static int32_t get_offset_of_subjectKeyIdentifier_12() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___subjectKeyIdentifier_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_subjectKeyIdentifier_12() const { return ___subjectKeyIdentifier_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_subjectKeyIdentifier_12() { return &___subjectKeyIdentifier_12; }
	inline void set_subjectKeyIdentifier_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___subjectKeyIdentifier_12 = value;
		Il2CppCodeGenWriteBarrier((&___subjectKeyIdentifier_12), value);
	}

	inline static int32_t get_offset_of_subjectPublicKey_13() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___subjectPublicKey_13)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_subjectPublicKey_13() const { return ___subjectPublicKey_13; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_subjectPublicKey_13() { return &___subjectPublicKey_13; }
	inline void set_subjectPublicKey_13(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___subjectPublicKey_13 = value;
		Il2CppCodeGenWriteBarrier((&___subjectPublicKey_13), value);
	}

	inline static int32_t get_offset_of_subjectPublicKeyAlgID_14() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___subjectPublicKeyAlgID_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_subjectPublicKeyAlgID_14() const { return ___subjectPublicKeyAlgID_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_subjectPublicKeyAlgID_14() { return &___subjectPublicKeyAlgID_14; }
	inline void set_subjectPublicKeyAlgID_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___subjectPublicKeyAlgID_14 = value;
		Il2CppCodeGenWriteBarrier((&___subjectPublicKeyAlgID_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTSTORESELECTOR_TB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#define ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct  Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifndef BERGENERATOR_T1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0_H
#define BERGENERATOR_T1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerGenerator
struct  BerGenerator_t1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0  : public Asn1Generator_t56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerGenerator::_tagged
	bool ____tagged_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerGenerator::_isExplicit
	bool ____isExplicit_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerGenerator::_tagNo
	int32_t ____tagNo_3;

public:
	inline static int32_t get_offset_of__tagged_1() { return static_cast<int32_t>(offsetof(BerGenerator_t1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0, ____tagged_1)); }
	inline bool get__tagged_1() const { return ____tagged_1; }
	inline bool* get_address_of__tagged_1() { return &____tagged_1; }
	inline void set__tagged_1(bool value)
	{
		____tagged_1 = value;
	}

	inline static int32_t get_offset_of__isExplicit_2() { return static_cast<int32_t>(offsetof(BerGenerator_t1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0, ____isExplicit_2)); }
	inline bool get__isExplicit_2() const { return ____isExplicit_2; }
	inline bool* get_address_of__isExplicit_2() { return &____isExplicit_2; }
	inline void set__isExplicit_2(bool value)
	{
		____isExplicit_2 = value;
	}

	inline static int32_t get_offset_of__tagNo_3() { return static_cast<int32_t>(offsetof(BerGenerator_t1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0, ____tagNo_3)); }
	inline int32_t get__tagNo_3() const { return ____tagNo_3; }
	inline int32_t* get_address_of__tagNo_3() { return &____tagNo_3; }
	inline void set__tagNo_3(int32_t value)
	{
		____tagNo_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERGENERATOR_T1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0_H
#ifndef DEREXTERNALPARSER_T064BC06E39CFF6D49F7DAC50485ACB0DB94E5905_H
#define DEREXTERNALPARSER_T064BC06E39CFF6D49F7DAC50485ACB0DB94E5905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerExternalParser
struct  DerExternalParser_t064BC06E39CFF6D49F7DAC50485ACB0DB94E5905  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerExternalParser::_parser
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * ____parser_2;

public:
	inline static int32_t get_offset_of__parser_2() { return static_cast<int32_t>(offsetof(DerExternalParser_t064BC06E39CFF6D49F7DAC50485ACB0DB94E5905, ____parser_2)); }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * get__parser_2() const { return ____parser_2; }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 ** get_address_of__parser_2() { return &____parser_2; }
	inline void set__parser_2(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * value)
	{
		____parser_2 = value;
		Il2CppCodeGenWriteBarrier((&____parser_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEREXTERNALPARSER_T064BC06E39CFF6D49F7DAC50485ACB0DB94E5905_H
#ifndef DERGENERATOR_T61583277391BC057473563CDC35A6DE1070E078E_H
#define DERGENERATOR_T61583277391BC057473563CDC35A6DE1070E078E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGenerator
struct  DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E  : public Asn1Generator_t56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGenerator::_tagged
	bool ____tagged_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGenerator::_isExplicit
	bool ____isExplicit_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGenerator::_tagNo
	int32_t ____tagNo_3;

public:
	inline static int32_t get_offset_of__tagged_1() { return static_cast<int32_t>(offsetof(DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E, ____tagged_1)); }
	inline bool get__tagged_1() const { return ____tagged_1; }
	inline bool* get_address_of__tagged_1() { return &____tagged_1; }
	inline void set__tagged_1(bool value)
	{
		____tagged_1 = value;
	}

	inline static int32_t get_offset_of__isExplicit_2() { return static_cast<int32_t>(offsetof(DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E, ____isExplicit_2)); }
	inline bool get__isExplicit_2() const { return ____isExplicit_2; }
	inline bool* get_address_of__isExplicit_2() { return &____isExplicit_2; }
	inline void set__isExplicit_2(bool value)
	{
		____isExplicit_2 = value;
	}

	inline static int32_t get_offset_of__tagNo_3() { return static_cast<int32_t>(offsetof(DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E, ____tagNo_3)); }
	inline int32_t get__tagNo_3() const { return ____tagNo_3; }
	inline int32_t* get_address_of__tagNo_3() { return &____tagNo_3; }
	inline void set__tagNo_3(int32_t value)
	{
		____tagNo_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERGENERATOR_T61583277391BC057473563CDC35A6DE1070E078E_H
#ifndef CMSSIGNEDDATAPARSER_T4A4AED9D994AB971FAD94F3454052FFBF0520C35_H
#define CMSSIGNEDDATAPARSER_T4A4AED9D994AB971FAD94F3454052FFBF0520C35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser
struct  CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35  : public CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedDataParser BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_signedData
	SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8 * ____signedData_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_signedContentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ____signedContentType_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsTypedStream BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_signedContent
	CmsTypedStream_t3C034672F2E5D2B8EDF5618BDE3E4DA90F540591 * ____signedContent_5;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_digests
	RuntimeObject* ____digests_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_digestOids
	RuntimeObject* ____digestOids_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformationStore BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_signerInfoStore
	SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575 * ____signerInfoStore_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_certSet
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ____certSet_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_crlSet
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ____crlSet_10;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_isCertCrlParsed
	bool ____isCertCrlParsed_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Store BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_attributeStore
	RuntimeObject* ____attributeStore_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Store BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_certificateStore
	RuntimeObject* ____certificateStore_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Store BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::_crlStore
	RuntimeObject* ____crlStore_14;

public:
	inline static int32_t get_offset_of__signedData_3() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____signedData_3)); }
	inline SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8 * get__signedData_3() const { return ____signedData_3; }
	inline SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8 ** get_address_of__signedData_3() { return &____signedData_3; }
	inline void set__signedData_3(SignedDataParser_t6192B8359DD930C83BCD5846BB0C6B3C6F67ACA8 * value)
	{
		____signedData_3 = value;
		Il2CppCodeGenWriteBarrier((&____signedData_3), value);
	}

	inline static int32_t get_offset_of__signedContentType_4() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____signedContentType_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get__signedContentType_4() const { return ____signedContentType_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of__signedContentType_4() { return &____signedContentType_4; }
	inline void set__signedContentType_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		____signedContentType_4 = value;
		Il2CppCodeGenWriteBarrier((&____signedContentType_4), value);
	}

	inline static int32_t get_offset_of__signedContent_5() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____signedContent_5)); }
	inline CmsTypedStream_t3C034672F2E5D2B8EDF5618BDE3E4DA90F540591 * get__signedContent_5() const { return ____signedContent_5; }
	inline CmsTypedStream_t3C034672F2E5D2B8EDF5618BDE3E4DA90F540591 ** get_address_of__signedContent_5() { return &____signedContent_5; }
	inline void set__signedContent_5(CmsTypedStream_t3C034672F2E5D2B8EDF5618BDE3E4DA90F540591 * value)
	{
		____signedContent_5 = value;
		Il2CppCodeGenWriteBarrier((&____signedContent_5), value);
	}

	inline static int32_t get_offset_of__digests_6() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____digests_6)); }
	inline RuntimeObject* get__digests_6() const { return ____digests_6; }
	inline RuntimeObject** get_address_of__digests_6() { return &____digests_6; }
	inline void set__digests_6(RuntimeObject* value)
	{
		____digests_6 = value;
		Il2CppCodeGenWriteBarrier((&____digests_6), value);
	}

	inline static int32_t get_offset_of__digestOids_7() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____digestOids_7)); }
	inline RuntimeObject* get__digestOids_7() const { return ____digestOids_7; }
	inline RuntimeObject** get_address_of__digestOids_7() { return &____digestOids_7; }
	inline void set__digestOids_7(RuntimeObject* value)
	{
		____digestOids_7 = value;
		Il2CppCodeGenWriteBarrier((&____digestOids_7), value);
	}

	inline static int32_t get_offset_of__signerInfoStore_8() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____signerInfoStore_8)); }
	inline SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575 * get__signerInfoStore_8() const { return ____signerInfoStore_8; }
	inline SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575 ** get_address_of__signerInfoStore_8() { return &____signerInfoStore_8; }
	inline void set__signerInfoStore_8(SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575 * value)
	{
		____signerInfoStore_8 = value;
		Il2CppCodeGenWriteBarrier((&____signerInfoStore_8), value);
	}

	inline static int32_t get_offset_of__certSet_9() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____certSet_9)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get__certSet_9() const { return ____certSet_9; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of__certSet_9() { return &____certSet_9; }
	inline void set__certSet_9(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		____certSet_9 = value;
		Il2CppCodeGenWriteBarrier((&____certSet_9), value);
	}

	inline static int32_t get_offset_of__crlSet_10() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____crlSet_10)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get__crlSet_10() const { return ____crlSet_10; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of__crlSet_10() { return &____crlSet_10; }
	inline void set__crlSet_10(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		____crlSet_10 = value;
		Il2CppCodeGenWriteBarrier((&____crlSet_10), value);
	}

	inline static int32_t get_offset_of__isCertCrlParsed_11() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____isCertCrlParsed_11)); }
	inline bool get__isCertCrlParsed_11() const { return ____isCertCrlParsed_11; }
	inline bool* get_address_of__isCertCrlParsed_11() { return &____isCertCrlParsed_11; }
	inline void set__isCertCrlParsed_11(bool value)
	{
		____isCertCrlParsed_11 = value;
	}

	inline static int32_t get_offset_of__attributeStore_12() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____attributeStore_12)); }
	inline RuntimeObject* get__attributeStore_12() const { return ____attributeStore_12; }
	inline RuntimeObject** get_address_of__attributeStore_12() { return &____attributeStore_12; }
	inline void set__attributeStore_12(RuntimeObject* value)
	{
		____attributeStore_12 = value;
		Il2CppCodeGenWriteBarrier((&____attributeStore_12), value);
	}

	inline static int32_t get_offset_of__certificateStore_13() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____certificateStore_13)); }
	inline RuntimeObject* get__certificateStore_13() const { return ____certificateStore_13; }
	inline RuntimeObject** get_address_of__certificateStore_13() { return &____certificateStore_13; }
	inline void set__certificateStore_13(RuntimeObject* value)
	{
		____certificateStore_13 = value;
		Il2CppCodeGenWriteBarrier((&____certificateStore_13), value);
	}

	inline static int32_t get_offset_of__crlStore_14() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35, ____crlStore_14)); }
	inline RuntimeObject* get__crlStore_14() const { return ____crlStore_14; }
	inline RuntimeObject** get_address_of__crlStore_14() { return &____crlStore_14; }
	inline void set__crlStore_14(RuntimeObject* value)
	{
		____crlStore_14 = value;
		Il2CppCodeGenWriteBarrier((&____crlStore_14), value);
	}
};

struct CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataParser::Helper
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * ___Helper_2;

public:
	inline static int32_t get_offset_of_Helper_2() { return static_cast<int32_t>(offsetof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35_StaticFields, ___Helper_2)); }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * get_Helper_2() const { return ___Helper_2; }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF ** get_address_of_Helper_2() { return &___Helper_2; }
	inline void set_Helper_2(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * value)
	{
		___Helper_2 = value;
		Il2CppCodeGenWriteBarrier((&___Helper_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSSIGNEDDATAPARSER_T4A4AED9D994AB971FAD94F3454052FFBF0520C35_H
#ifndef CMSSIGNEDDATASTREAMGENERATOR_T34D1424AD2560BE95C123E597A22ECE811249D2D_H
#define CMSSIGNEDDATASTREAMGENERATOR_T34D1424AD2560BE95C123E597A22ECE811249D2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator
struct  CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D  : public CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator::_signerInfs
	RuntimeObject* ____signerInfs_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator::_messageDigestOids
	RuntimeObject* ____messageDigestOids_26;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator::_messageDigests
	RuntimeObject* ____messageDigests_27;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator::_messageHashes
	RuntimeObject* ____messageHashes_28;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator::_messageDigestsLocked
	bool ____messageDigestsLocked_29;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator::_bufferSize
	int32_t ____bufferSize_30;

public:
	inline static int32_t get_offset_of__signerInfs_25() { return static_cast<int32_t>(offsetof(CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D, ____signerInfs_25)); }
	inline RuntimeObject* get__signerInfs_25() const { return ____signerInfs_25; }
	inline RuntimeObject** get_address_of__signerInfs_25() { return &____signerInfs_25; }
	inline void set__signerInfs_25(RuntimeObject* value)
	{
		____signerInfs_25 = value;
		Il2CppCodeGenWriteBarrier((&____signerInfs_25), value);
	}

	inline static int32_t get_offset_of__messageDigestOids_26() { return static_cast<int32_t>(offsetof(CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D, ____messageDigestOids_26)); }
	inline RuntimeObject* get__messageDigestOids_26() const { return ____messageDigestOids_26; }
	inline RuntimeObject** get_address_of__messageDigestOids_26() { return &____messageDigestOids_26; }
	inline void set__messageDigestOids_26(RuntimeObject* value)
	{
		____messageDigestOids_26 = value;
		Il2CppCodeGenWriteBarrier((&____messageDigestOids_26), value);
	}

	inline static int32_t get_offset_of__messageDigests_27() { return static_cast<int32_t>(offsetof(CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D, ____messageDigests_27)); }
	inline RuntimeObject* get__messageDigests_27() const { return ____messageDigests_27; }
	inline RuntimeObject** get_address_of__messageDigests_27() { return &____messageDigests_27; }
	inline void set__messageDigests_27(RuntimeObject* value)
	{
		____messageDigests_27 = value;
		Il2CppCodeGenWriteBarrier((&____messageDigests_27), value);
	}

	inline static int32_t get_offset_of__messageHashes_28() { return static_cast<int32_t>(offsetof(CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D, ____messageHashes_28)); }
	inline RuntimeObject* get__messageHashes_28() const { return ____messageHashes_28; }
	inline RuntimeObject** get_address_of__messageHashes_28() { return &____messageHashes_28; }
	inline void set__messageHashes_28(RuntimeObject* value)
	{
		____messageHashes_28 = value;
		Il2CppCodeGenWriteBarrier((&____messageHashes_28), value);
	}

	inline static int32_t get_offset_of__messageDigestsLocked_29() { return static_cast<int32_t>(offsetof(CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D, ____messageDigestsLocked_29)); }
	inline bool get__messageDigestsLocked_29() const { return ____messageDigestsLocked_29; }
	inline bool* get_address_of__messageDigestsLocked_29() { return &____messageDigestsLocked_29; }
	inline void set__messageDigestsLocked_29(bool value)
	{
		____messageDigestsLocked_29 = value;
	}

	inline static int32_t get_offset_of__bufferSize_30() { return static_cast<int32_t>(offsetof(CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D, ____bufferSize_30)); }
	inline int32_t get__bufferSize_30() const { return ____bufferSize_30; }
	inline int32_t* get_address_of__bufferSize_30() { return &____bufferSize_30; }
	inline void set__bufferSize_30(int32_t value)
	{
		____bufferSize_30 = value;
	}
};

struct CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator::Helper
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * ___Helper_24;

public:
	inline static int32_t get_offset_of_Helper_24() { return static_cast<int32_t>(offsetof(CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D_StaticFields, ___Helper_24)); }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * get_Helper_24() const { return ___Helper_24; }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF ** get_address_of_Helper_24() { return &___Helper_24; }
	inline void set_Helper_24(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * value)
	{
		___Helper_24 = value;
		Il2CppCodeGenWriteBarrier((&___Helper_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSSIGNEDDATASTREAMGENERATOR_T34D1424AD2560BE95C123E597A22ECE811249D2D_H
#ifndef KEKRECIPIENTINFORMATION_TA9FEDD2E1E55EF74573143E980B44D5C77E6DD18_H
#define KEKRECIPIENTINFORMATION_TA9FEDD2E1E55EF74573143E980B44D5C77E6DD18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KekRecipientInformation
struct  KekRecipientInformation_tA9FEDD2E1E55EF74573143E980B44D5C77E6DD18  : public RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekRecipientInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KekRecipientInformation::info
	KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6 * ___info_4;

public:
	inline static int32_t get_offset_of_info_4() { return static_cast<int32_t>(offsetof(KekRecipientInformation_tA9FEDD2E1E55EF74573143E980B44D5C77E6DD18, ___info_4)); }
	inline KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6 * get_info_4() const { return ___info_4; }
	inline KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6 ** get_address_of_info_4() { return &___info_4; }
	inline void set_info_4(KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6 * value)
	{
		___info_4 = value;
		Il2CppCodeGenWriteBarrier((&___info_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEKRECIPIENTINFORMATION_TA9FEDD2E1E55EF74573143E980B44D5C77E6DD18_H
#ifndef KEYAGREERECIPIENTINFORMATION_TFCEBDF940682EDC6E6C5C972749DA991964EDD34_H
#define KEYAGREERECIPIENTINFORMATION_TFCEBDF940682EDC6E6C5C972749DA991964EDD34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyAgreeRecipientInformation
struct  KeyAgreeRecipientInformation_tFCEBDF940682EDC6E6C5C972749DA991964EDD34  : public RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyAgreeRecipientInformation::info
	KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E * ___info_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyAgreeRecipientInformation::encryptedKey
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___encryptedKey_5;

public:
	inline static int32_t get_offset_of_info_4() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInformation_tFCEBDF940682EDC6E6C5C972749DA991964EDD34, ___info_4)); }
	inline KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E * get_info_4() const { return ___info_4; }
	inline KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E ** get_address_of_info_4() { return &___info_4; }
	inline void set_info_4(KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E * value)
	{
		___info_4 = value;
		Il2CppCodeGenWriteBarrier((&___info_4), value);
	}

	inline static int32_t get_offset_of_encryptedKey_5() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInformation_tFCEBDF940682EDC6E6C5C972749DA991964EDD34, ___encryptedKey_5)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_encryptedKey_5() const { return ___encryptedKey_5; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_encryptedKey_5() { return &___encryptedKey_5; }
	inline void set_encryptedKey_5(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___encryptedKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedKey_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYAGREERECIPIENTINFORMATION_TFCEBDF940682EDC6E6C5C972749DA991964EDD34_H
#ifndef KEYTRANSRECIPIENTINFORMATION_T917347F49BFA4B838E11F2F11DC7191E00E7DC47_H
#define KEYTRANSRECIPIENTINFORMATION_T917347F49BFA4B838E11F2F11DC7191E00E7DC47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInformation
struct  KeyTransRecipientInformation_t917347F49BFA4B838E11F2F11DC7191E00E7DC47  : public RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyTransRecipientInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInformation::info
	KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9 * ___info_4;

public:
	inline static int32_t get_offset_of_info_4() { return static_cast<int32_t>(offsetof(KeyTransRecipientInformation_t917347F49BFA4B838E11F2F11DC7191E00E7DC47, ___info_4)); }
	inline KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9 * get_info_4() const { return ___info_4; }
	inline KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9 ** get_address_of_info_4() { return &___info_4; }
	inline void set_info_4(KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9 * value)
	{
		___info_4 = value;
		Il2CppCodeGenWriteBarrier((&___info_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYTRANSRECIPIENTINFORMATION_T917347F49BFA4B838E11F2F11DC7191E00E7DC47_H
#ifndef ORIGINATORID_T882B95B8A4875CAC72134FCA20A997CF517EB400_H
#define ORIGINATORID_T882B95B8A4875CAC72134FCA20A997CF517EB400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.OriginatorID
struct  OriginatorID_t882B95B8A4875CAC72134FCA20A997CF517EB400  : public X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIGINATORID_T882B95B8A4875CAC72134FCA20A997CF517EB400_H
#ifndef PASSWORDRECIPIENTINFORMATION_T2245D57E1455981673C3B8F9120A922D91B10029_H
#define PASSWORDRECIPIENTINFORMATION_T2245D57E1455981673C3B8F9120A922D91B10029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.PasswordRecipientInformation
struct  PasswordRecipientInformation_t2245D57E1455981673C3B8F9120A922D91B10029  : public RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.PasswordRecipientInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.PasswordRecipientInformation::info
	PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B * ___info_4;

public:
	inline static int32_t get_offset_of_info_4() { return static_cast<int32_t>(offsetof(PasswordRecipientInformation_t2245D57E1455981673C3B8F9120A922D91B10029, ___info_4)); }
	inline PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B * get_info_4() const { return ___info_4; }
	inline PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B ** get_address_of_info_4() { return &___info_4; }
	inline void set_info_4(PasswordRecipientInfo_tFA5770BCE78C3FC6CC7BB475AAC25F9CA8F44F4B * value)
	{
		___info_4 = value;
		Il2CppCodeGenWriteBarrier((&___info_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSWORDRECIPIENTINFORMATION_T2245D57E1455981673C3B8F9120A922D91B10029_H
#ifndef PKCS5SCHEME2PBEKEY_T8D9B8F31F6796AAD2E73E5BC17909E8E1779AB61_H
#define PKCS5SCHEME2PBEKEY_T8D9B8F31F6796AAD2E73E5BC17909E8E1779AB61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.Pkcs5Scheme2PbeKey
struct  Pkcs5Scheme2PbeKey_t8D9B8F31F6796AAD2E73E5BC17909E8E1779AB61  : public CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS5SCHEME2PBEKEY_T8D9B8F31F6796AAD2E73E5BC17909E8E1779AB61_H
#ifndef PKCS5SCHEME2UTF8PBEKEY_TFBA7065A9BC5EF68CFED19DFB066767AD6B93C47_H
#define PKCS5SCHEME2UTF8PBEKEY_TFBA7065A9BC5EF68CFED19DFB066767AD6B93C47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.Pkcs5Scheme2Utf8PbeKey
struct  Pkcs5Scheme2Utf8PbeKey_tFBA7065A9BC5EF68CFED19DFB066767AD6B93C47  : public CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS5SCHEME2UTF8PBEKEY_TFBA7065A9BC5EF68CFED19DFB066767AD6B93C47_H
#ifndef RECIPIENTID_T25DA4BBF07A25C6047B6350910E7FCA460AE920B_H
#define RECIPIENTID_T25DA4BBF07A25C6047B6350910E7FCA460AE920B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientID
struct  RecipientID_t25DA4BBF07A25C6047B6350910E7FCA460AE920B  : public X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientID::keyIdentifier
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___keyIdentifier_15;

public:
	inline static int32_t get_offset_of_keyIdentifier_15() { return static_cast<int32_t>(offsetof(RecipientID_t25DA4BBF07A25C6047B6350910E7FCA460AE920B, ___keyIdentifier_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_keyIdentifier_15() const { return ___keyIdentifier_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_keyIdentifier_15() { return &___keyIdentifier_15; }
	inline void set_keyIdentifier_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___keyIdentifier_15 = value;
		Il2CppCodeGenWriteBarrier((&___keyIdentifier_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECIPIENTID_T25DA4BBF07A25C6047B6350910E7FCA460AE920B_H
#ifndef SIGNERID_TC9728986CC0E7C53B1C2C2DA90EB09DE973E0B15_H
#define SIGNERID_TC9728986CC0E7C53B1C2C2DA90EB09DE973E0B15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerID
struct  SignerID_tC9728986CC0E7C53B1C2C2DA90EB09DE973E0B15  : public X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERID_TC9728986CC0E7C53B1C2C2DA90EB09DE973E0B15_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef ASN1NULL_T3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3_H
#define ASN1NULL_T3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Null
struct  Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1NULL_T3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3_H
#ifndef ASN1OCTETSTRING_T013D79AEF2791863689C38F8305C12D7F540024B_H
#define ASN1OCTETSTRING_T013D79AEF2791863689C38F8305C12D7F540024B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString
struct  Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString::str
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B, ___str_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_str_2() const { return ___str_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OCTETSTRING_T013D79AEF2791863689C38F8305C12D7F540024B_H
#ifndef ASN1SEQUENCE_T8D18DC0674425C28D79ACDD26FD19D10BD62C205_H
#define ASN1SEQUENCE_T8D18DC0674425C28D79ACDD26FD19D10BD62C205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence
struct  Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence::elements
	Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* ___elements_2;

public:
	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205, ___elements_2)); }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* get_elements_2() const { return ___elements_2; }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1SEQUENCE_T8D18DC0674425C28D79ACDD26FD19D10BD62C205_H
#ifndef ASN1SET_TB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29_H
#define ASN1SET_TB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set
struct  Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set::elements
	Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* ___elements_2;

public:
	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29, ___elements_2)); }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* get_elements_2() const { return ___elements_2; }
	inline Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Asn1EncodableU5BU5D_tD68B1F1A7ADD4FF6B3A213C08AA346DF629B0F64* value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1SET_TB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29_H
#ifndef ASN1TAGGEDOBJECT_T02C4D2D88510F17ED8F969894CA89B565BA3F0FD_H
#define ASN1TAGGEDOBJECT_T02C4D2D88510F17ED8F969894CA89B565BA3F0FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObject
struct  Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObject::tagNo
	int32_t ___tagNo_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObject::explicitly
	bool ___explicitly_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObject::obj
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___obj_4;

public:
	inline static int32_t get_offset_of_tagNo_2() { return static_cast<int32_t>(offsetof(Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD, ___tagNo_2)); }
	inline int32_t get_tagNo_2() const { return ___tagNo_2; }
	inline int32_t* get_address_of_tagNo_2() { return &___tagNo_2; }
	inline void set_tagNo_2(int32_t value)
	{
		___tagNo_2 = value;
	}

	inline static int32_t get_offset_of_explicitly_3() { return static_cast<int32_t>(offsetof(Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD, ___explicitly_3)); }
	inline bool get_explicitly_3() const { return ___explicitly_3; }
	inline bool* get_address_of_explicitly_3() { return &___explicitly_3; }
	inline void set_explicitly_3(bool value)
	{
		___explicitly_3 = value;
	}

	inline static int32_t get_offset_of_obj_4() { return static_cast<int32_t>(offsetof(Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD, ___obj_4)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_obj_4() const { return ___obj_4; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_obj_4() { return &___obj_4; }
	inline void set_obj_4(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___obj_4 = value;
		Il2CppCodeGenWriteBarrier((&___obj_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1TAGGEDOBJECT_T02C4D2D88510F17ED8F969894CA89B565BA3F0FD_H
#ifndef BEROCTETSTRINGGENERATOR_TF1BB093CD2F03074CFDD7EC178499D2204B259F6_H
#define BEROCTETSTRINGGENERATOR_TF1BB093CD2F03074CFDD7EC178499D2204B259F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetStringGenerator
struct  BerOctetStringGenerator_tF1BB093CD2F03074CFDD7EC178499D2204B259F6  : public BerGenerator_t1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEROCTETSTRINGGENERATOR_TF1BB093CD2F03074CFDD7EC178499D2204B259F6_H
#ifndef BERSEQUENCEGENERATOR_TB08F3E7FAB8B098C647670310FD7DD5021D8B1CE_H
#define BERSEQUENCEGENERATOR_TB08F3E7FAB8B098C647670310FD7DD5021D8B1CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator
struct  BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE  : public BerGenerator_t1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERSEQUENCEGENERATOR_TB08F3E7FAB8B098C647670310FD7DD5021D8B1CE_H
#ifndef BERSETGENERATOR_TE45B4F869E2D61BF2B0674AD39F7495B437009B6_H
#define BERSETGENERATOR_TE45B4F869E2D61BF2B0674AD39F7495B437009B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSetGenerator
struct  BerSetGenerator_tE45B4F869E2D61BF2B0674AD39F7495B437009B6  : public BerGenerator_t1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERSETGENERATOR_TE45B4F869E2D61BF2B0674AD39F7495B437009B6_H
#ifndef DERAPPLICATIONSPECIFIC_T5D72A362581D6315E480D7EDC518751C1D33099D_H
#define DERAPPLICATIONSPECIFIC_T5D72A362581D6315E480D7EDC518751C1D33099D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerApplicationSpecific
struct  DerApplicationSpecific_t5D72A362581D6315E480D7EDC518751C1D33099D  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerApplicationSpecific::isConstructed
	bool ___isConstructed_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerApplicationSpecific::tag
	int32_t ___tag_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerApplicationSpecific::octets
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___octets_4;

public:
	inline static int32_t get_offset_of_isConstructed_2() { return static_cast<int32_t>(offsetof(DerApplicationSpecific_t5D72A362581D6315E480D7EDC518751C1D33099D, ___isConstructed_2)); }
	inline bool get_isConstructed_2() const { return ___isConstructed_2; }
	inline bool* get_address_of_isConstructed_2() { return &___isConstructed_2; }
	inline void set_isConstructed_2(bool value)
	{
		___isConstructed_2 = value;
	}

	inline static int32_t get_offset_of_tag_3() { return static_cast<int32_t>(offsetof(DerApplicationSpecific_t5D72A362581D6315E480D7EDC518751C1D33099D, ___tag_3)); }
	inline int32_t get_tag_3() const { return ___tag_3; }
	inline int32_t* get_address_of_tag_3() { return &___tag_3; }
	inline void set_tag_3(int32_t value)
	{
		___tag_3 = value;
	}

	inline static int32_t get_offset_of_octets_4() { return static_cast<int32_t>(offsetof(DerApplicationSpecific_t5D72A362581D6315E480D7EDC518751C1D33099D, ___octets_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_octets_4() const { return ___octets_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_octets_4() { return &___octets_4; }
	inline void set_octets_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___octets_4 = value;
		Il2CppCodeGenWriteBarrier((&___octets_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERAPPLICATIONSPECIFIC_T5D72A362581D6315E480D7EDC518751C1D33099D_H
#ifndef DERBOOLEAN_T6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C_H
#define DERBOOLEAN_T6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean
struct  DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean::value
	uint8_t ___value_2;

public:
	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C, ___value_2)); }
	inline uint8_t get_value_2() const { return ___value_2; }
	inline uint8_t* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(uint8_t value)
	{
		___value_2 = value;
	}
};

struct DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean::False
	DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * ___False_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean::True
	DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * ___True_4;

public:
	inline static int32_t get_offset_of_False_3() { return static_cast<int32_t>(offsetof(DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C_StaticFields, ___False_3)); }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * get_False_3() const { return ___False_3; }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C ** get_address_of_False_3() { return &___False_3; }
	inline void set_False_3(DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * value)
	{
		___False_3 = value;
		Il2CppCodeGenWriteBarrier((&___False_3), value);
	}

	inline static int32_t get_offset_of_True_4() { return static_cast<int32_t>(offsetof(DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C_StaticFields, ___True_4)); }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * get_True_4() const { return ___True_4; }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C ** get_address_of_True_4() { return &___True_4; }
	inline void set_True_4(DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * value)
	{
		___True_4 = value;
		Il2CppCodeGenWriteBarrier((&___True_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERBOOLEAN_T6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C_H
#ifndef DERENUMERATED_TD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_H
#define DERENUMERATED_TD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated
struct  DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated::start
	int32_t ___start_3;

public:
	inline static int32_t get_offset_of_bytes_2() { return static_cast<int32_t>(offsetof(DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F, ___bytes_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_2() const { return ___bytes_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_2() { return &___bytes_2; }
	inline void set_bytes_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_2), value);
	}

	inline static int32_t get_offset_of_start_3() { return static_cast<int32_t>(offsetof(DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F, ___start_3)); }
	inline int32_t get_start_3() const { return ___start_3; }
	inline int32_t* get_address_of_start_3() { return &___start_3; }
	inline void set_start_3(int32_t value)
	{
		___start_3 = value;
	}
};

struct DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated::cache
	DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD* ___cache_4;

public:
	inline static int32_t get_offset_of_cache_4() { return static_cast<int32_t>(offsetof(DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_StaticFields, ___cache_4)); }
	inline DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD* get_cache_4() const { return ___cache_4; }
	inline DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD** get_address_of_cache_4() { return &___cache_4; }
	inline void set_cache_4(DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD* value)
	{
		___cache_4 = value;
		Il2CppCodeGenWriteBarrier((&___cache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERENUMERATED_TD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_H
#ifndef DEREXTERNAL_T31395586E8440842B73081A589776D6788A16E53_H
#define DEREXTERNAL_T31395586E8440842B73081A589776D6788A16E53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerExternal
struct  DerExternal_t31395586E8440842B73081A589776D6788A16E53  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerExternal::directReference
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___directReference_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerExternal::indirectReference
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___indirectReference_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerExternal::dataValueDescriptor
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___dataValueDescriptor_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerExternal::encoding
	int32_t ___encoding_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerExternal::externalContent
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___externalContent_6;

public:
	inline static int32_t get_offset_of_directReference_2() { return static_cast<int32_t>(offsetof(DerExternal_t31395586E8440842B73081A589776D6788A16E53, ___directReference_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_directReference_2() const { return ___directReference_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_directReference_2() { return &___directReference_2; }
	inline void set_directReference_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___directReference_2 = value;
		Il2CppCodeGenWriteBarrier((&___directReference_2), value);
	}

	inline static int32_t get_offset_of_indirectReference_3() { return static_cast<int32_t>(offsetof(DerExternal_t31395586E8440842B73081A589776D6788A16E53, ___indirectReference_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_indirectReference_3() const { return ___indirectReference_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_indirectReference_3() { return &___indirectReference_3; }
	inline void set_indirectReference_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___indirectReference_3 = value;
		Il2CppCodeGenWriteBarrier((&___indirectReference_3), value);
	}

	inline static int32_t get_offset_of_dataValueDescriptor_4() { return static_cast<int32_t>(offsetof(DerExternal_t31395586E8440842B73081A589776D6788A16E53, ___dataValueDescriptor_4)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_dataValueDescriptor_4() const { return ___dataValueDescriptor_4; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_dataValueDescriptor_4() { return &___dataValueDescriptor_4; }
	inline void set_dataValueDescriptor_4(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___dataValueDescriptor_4 = value;
		Il2CppCodeGenWriteBarrier((&___dataValueDescriptor_4), value);
	}

	inline static int32_t get_offset_of_encoding_5() { return static_cast<int32_t>(offsetof(DerExternal_t31395586E8440842B73081A589776D6788A16E53, ___encoding_5)); }
	inline int32_t get_encoding_5() const { return ___encoding_5; }
	inline int32_t* get_address_of_encoding_5() { return &___encoding_5; }
	inline void set_encoding_5(int32_t value)
	{
		___encoding_5 = value;
	}

	inline static int32_t get_offset_of_externalContent_6() { return static_cast<int32_t>(offsetof(DerExternal_t31395586E8440842B73081A589776D6788A16E53, ___externalContent_6)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_externalContent_6() const { return ___externalContent_6; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_externalContent_6() { return &___externalContent_6; }
	inline void set_externalContent_6(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___externalContent_6 = value;
		Il2CppCodeGenWriteBarrier((&___externalContent_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEREXTERNAL_T31395586E8440842B73081A589776D6788A16E53_H
#ifndef DERGENERALIZEDTIME_TC685B4F90AFB44A874F0D7C16787EB079FB81A6A_H
#define DERGENERALIZEDTIME_TC685B4F90AFB44A874F0D7C16787EB079FB81A6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime
struct  DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime::time
	String_t* ___time_2;

public:
	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A, ___time_2)); }
	inline String_t* get_time_2() const { return ___time_2; }
	inline String_t** get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(String_t* value)
	{
		___time_2 = value;
		Il2CppCodeGenWriteBarrier((&___time_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERGENERALIZEDTIME_TC685B4F90AFB44A874F0D7C16787EB079FB81A6A_H
#ifndef DERINTEGER_TD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96_H
#define DERINTEGER_TD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger
struct  DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger::start
	int32_t ___start_6;

public:
	inline static int32_t get_offset_of_bytes_5() { return static_cast<int32_t>(offsetof(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96, ___bytes_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_5() const { return ___bytes_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_5() { return &___bytes_5; }
	inline void set_bytes_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_5 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_5), value);
	}

	inline static int32_t get_offset_of_start_6() { return static_cast<int32_t>(offsetof(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96, ___start_6)); }
	inline int32_t get_start_6() const { return ___start_6; }
	inline int32_t* get_address_of_start_6() { return &___start_6; }
	inline void set_start_6(int32_t value)
	{
		___start_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERINTEGER_TD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96_H
#ifndef DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#define DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerStringBase
struct  DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifndef BASEINPUTSTREAM_TFB934C38CB5B81842A543CE8228DDE5235F1E2E4_H
#define BASEINPUTSTREAM_TFB934C38CB5B81842A543CE8228DDE5235F1E2E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseInputStream
struct  BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseInputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTSTREAM_TFB934C38CB5B81842A543CE8228DDE5235F1E2E4_H
#ifndef BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#define BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream
struct  BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#ifndef FILTERSTREAM_T45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF_H
#define FILTERSTREAM_T45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.FilterStream
struct  FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.FilterStream::s
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___s_5;

public:
	inline static int32_t get_offset_of_s_5() { return static_cast<int32_t>(offsetof(FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF, ___s_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_s_5() const { return ___s_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_s_5() { return &___s_5; }
	inline void set_s_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___s_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERSTREAM_T45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF_H
#ifndef IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#define IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IOException
struct  IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.IO.IOException::_maybeFullPath
	String_t* ____maybeFullPath_17;

public:
	inline static int32_t get_offset_of__maybeFullPath_17() { return static_cast<int32_t>(offsetof(IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA, ____maybeFullPath_17)); }
	inline String_t* get__maybeFullPath_17() const { return ____maybeFullPath_17; }
	inline String_t** get_address_of__maybeFullPath_17() { return &____maybeFullPath_17; }
	inline void set__maybeFullPath_17(String_t* value)
	{
		____maybeFullPath_17 = value;
		Il2CppCodeGenWriteBarrier((&____maybeFullPath_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifndef INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#define INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifndef ASN1EXCEPTION_T144D1DCE60247FE659A34C18115EC3BC68AB9552_H
#define ASN1EXCEPTION_T144D1DCE60247FE659A34C18115EC3BC68AB9552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Exception
struct  Asn1Exception_t144D1DCE60247FE659A34C18115EC3BC68AB9552  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1EXCEPTION_T144D1DCE60247FE659A34C18115EC3BC68AB9552_H
#ifndef ASN1INPUTSTREAM_T41FB5C019456D8C95B07D0B145B80E4952061D33_H
#define ASN1INPUTSTREAM_T41FB5C019456D8C95B07D0B145B80E4952061D33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1InputStream
struct  Asn1InputStream_t41FB5C019456D8C95B07D0B145B80E4952061D33  : public FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1InputStream::limit
	int32_t ___limit_6;
	// System.Byte[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1InputStream::tmpBuffers
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___tmpBuffers_7;

public:
	inline static int32_t get_offset_of_limit_6() { return static_cast<int32_t>(offsetof(Asn1InputStream_t41FB5C019456D8C95B07D0B145B80E4952061D33, ___limit_6)); }
	inline int32_t get_limit_6() const { return ___limit_6; }
	inline int32_t* get_address_of_limit_6() { return &___limit_6; }
	inline void set_limit_6(int32_t value)
	{
		___limit_6 = value;
	}

	inline static int32_t get_offset_of_tmpBuffers_7() { return static_cast<int32_t>(offsetof(Asn1InputStream_t41FB5C019456D8C95B07D0B145B80E4952061D33, ___tmpBuffers_7)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_tmpBuffers_7() const { return ___tmpBuffers_7; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_tmpBuffers_7() { return &___tmpBuffers_7; }
	inline void set_tmpBuffers_7(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___tmpBuffers_7 = value;
		Il2CppCodeGenWriteBarrier((&___tmpBuffers_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1INPUTSTREAM_T41FB5C019456D8C95B07D0B145B80E4952061D33_H
#ifndef ASN1PARSINGEXCEPTION_T70F867DA13E608942B6B9E84E88BF21D9B4B8325_H
#define ASN1PARSINGEXCEPTION_T70F867DA13E608942B6B9E84E88BF21D9B4B8325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1ParsingException
struct  Asn1ParsingException_t70F867DA13E608942B6B9E84E88BF21D9B4B8325  : public InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1PARSINGEXCEPTION_T70F867DA13E608942B6B9E84E88BF21D9B4B8325_H
#ifndef BERAPPLICATIONSPECIFIC_T2C9A3C98F2FFCB3BF1576D27FD93108DB2523AA1_H
#define BERAPPLICATIONSPECIFIC_T2C9A3C98F2FFCB3BF1576D27FD93108DB2523AA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerApplicationSpecific
struct  BerApplicationSpecific_t2C9A3C98F2FFCB3BF1576D27FD93108DB2523AA1  : public DerApplicationSpecific_t5D72A362581D6315E480D7EDC518751C1D33099D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERAPPLICATIONSPECIFIC_T2C9A3C98F2FFCB3BF1576D27FD93108DB2523AA1_H
#ifndef BUFFEREDBEROCTETSTREAM_TD45251DB21F20A789A51520F5BC512BCA9ACE2CE_H
#define BUFFEREDBEROCTETSTREAM_TD45251DB21F20A789A51520F5BC512BCA9ACE2CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetStringGenerator_BufferedBerOctetStream
struct  BufferedBerOctetStream_tD45251DB21F20A789A51520F5BC512BCA9ACE2CE  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetStringGenerator_BufferedBerOctetStream::_buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buf_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetStringGenerator_BufferedBerOctetStream::_off
	int32_t ____off_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetStringGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetStringGenerator_BufferedBerOctetStream::_gen
	BerOctetStringGenerator_tF1BB093CD2F03074CFDD7EC178499D2204B259F6 * ____gen_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerOutputStream BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetStringGenerator_BufferedBerOctetStream::_derOut
	DerOutputStream_tF14E89982B98990995F8610F800B024DD05ADFA1 * ____derOut_9;

public:
	inline static int32_t get_offset_of__buf_6() { return static_cast<int32_t>(offsetof(BufferedBerOctetStream_tD45251DB21F20A789A51520F5BC512BCA9ACE2CE, ____buf_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buf_6() const { return ____buf_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buf_6() { return &____buf_6; }
	inline void set__buf_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buf_6 = value;
		Il2CppCodeGenWriteBarrier((&____buf_6), value);
	}

	inline static int32_t get_offset_of__off_7() { return static_cast<int32_t>(offsetof(BufferedBerOctetStream_tD45251DB21F20A789A51520F5BC512BCA9ACE2CE, ____off_7)); }
	inline int32_t get__off_7() const { return ____off_7; }
	inline int32_t* get_address_of__off_7() { return &____off_7; }
	inline void set__off_7(int32_t value)
	{
		____off_7 = value;
	}

	inline static int32_t get_offset_of__gen_8() { return static_cast<int32_t>(offsetof(BufferedBerOctetStream_tD45251DB21F20A789A51520F5BC512BCA9ACE2CE, ____gen_8)); }
	inline BerOctetStringGenerator_tF1BB093CD2F03074CFDD7EC178499D2204B259F6 * get__gen_8() const { return ____gen_8; }
	inline BerOctetStringGenerator_tF1BB093CD2F03074CFDD7EC178499D2204B259F6 ** get_address_of__gen_8() { return &____gen_8; }
	inline void set__gen_8(BerOctetStringGenerator_tF1BB093CD2F03074CFDD7EC178499D2204B259F6 * value)
	{
		____gen_8 = value;
		Il2CppCodeGenWriteBarrier((&____gen_8), value);
	}

	inline static int32_t get_offset_of__derOut_9() { return static_cast<int32_t>(offsetof(BufferedBerOctetStream_tD45251DB21F20A789A51520F5BC512BCA9ACE2CE, ____derOut_9)); }
	inline DerOutputStream_tF14E89982B98990995F8610F800B024DD05ADFA1 * get__derOut_9() const { return ____derOut_9; }
	inline DerOutputStream_tF14E89982B98990995F8610F800B024DD05ADFA1 ** get_address_of__derOut_9() { return &____derOut_9; }
	inline void set__derOut_9(DerOutputStream_tF14E89982B98990995F8610F800B024DD05ADFA1 * value)
	{
		____derOut_9 = value;
		Il2CppCodeGenWriteBarrier((&____derOut_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDBEROCTETSTREAM_TD45251DB21F20A789A51520F5BC512BCA9ACE2CE_H
#ifndef CONSTRUCTEDOCTETSTREAM_TCFA941EA97FC1233325702E1A013FC3FDABCED84_H
#define CONSTRUCTEDOCTETSTREAM_TCFA941EA97FC1233325702E1A013FC3FDABCED84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.ConstructedOctetStream
struct  ConstructedOctetStream_tCFA941EA97FC1233325702E1A013FC3FDABCED84  : public BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1StreamParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.ConstructedOctetStream::_parser
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * ____parser_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.ConstructedOctetStream::_first
	bool ____first_7;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.ConstructedOctetStream::_currentStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____currentStream_8;

public:
	inline static int32_t get_offset_of__parser_6() { return static_cast<int32_t>(offsetof(ConstructedOctetStream_tCFA941EA97FC1233325702E1A013FC3FDABCED84, ____parser_6)); }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * get__parser_6() const { return ____parser_6; }
	inline Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 ** get_address_of__parser_6() { return &____parser_6; }
	inline void set__parser_6(Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317 * value)
	{
		____parser_6 = value;
		Il2CppCodeGenWriteBarrier((&____parser_6), value);
	}

	inline static int32_t get_offset_of__first_7() { return static_cast<int32_t>(offsetof(ConstructedOctetStream_tCFA941EA97FC1233325702E1A013FC3FDABCED84, ____first_7)); }
	inline bool get__first_7() const { return ____first_7; }
	inline bool* get_address_of__first_7() { return &____first_7; }
	inline void set__first_7(bool value)
	{
		____first_7 = value;
	}

	inline static int32_t get_offset_of__currentStream_8() { return static_cast<int32_t>(offsetof(ConstructedOctetStream_tCFA941EA97FC1233325702E1A013FC3FDABCED84, ____currentStream_8)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__currentStream_8() const { return ____currentStream_8; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__currentStream_8() { return &____currentStream_8; }
	inline void set__currentStream_8(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____currentStream_8 = value;
		Il2CppCodeGenWriteBarrier((&____currentStream_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTEDOCTETSTREAM_TCFA941EA97FC1233325702E1A013FC3FDABCED84_H
#ifndef DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#define DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString
struct  DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::mData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mData_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::mPadBits
	int32_t ___mPadBits_4;

public:
	inline static int32_t get_offset_of_mData_3() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374, ___mData_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mData_3() const { return ___mData_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mData_3() { return &___mData_3; }
	inline void set_mData_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mData_3 = value;
		Il2CppCodeGenWriteBarrier((&___mData_3), value);
	}

	inline static int32_t get_offset_of_mPadBits_4() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374, ___mPadBits_4)); }
	inline int32_t get_mPadBits_4() const { return ___mPadBits_4; }
	inline int32_t* get_address_of_mPadBits_4() { return &___mPadBits_4; }
	inline void set_mPadBits_4(int32_t value)
	{
		___mPadBits_4 = value;
	}
};

struct DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields
{
public:
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::table
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___table_2;

public:
	inline static int32_t get_offset_of_table_2() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields, ___table_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_table_2() const { return ___table_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_table_2() { return &___table_2; }
	inline void set_table_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___table_2 = value;
		Il2CppCodeGenWriteBarrier((&___table_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#ifndef DERBMPSTRING_T3857ACCA2B0C039687D93A2A90561B890E2155E7_H
#define DERBMPSTRING_T3857ACCA2B0C039687D93A2A90561B890E2155E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBmpString
struct  DerBmpString_t3857ACCA2B0C039687D93A2A90561B890E2155E7  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBmpString::str
	String_t* ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(DerBmpString_t3857ACCA2B0C039687D93A2A90561B890E2155E7, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERBMPSTRING_T3857ACCA2B0C039687D93A2A90561B890E2155E7_H
#ifndef DERGENERALSTRING_T4B7BB0EE812FECFF6F3C5A1E791C722CB9A83F8F_H
#define DERGENERALSTRING_T4B7BB0EE812FECFF6F3C5A1E791C722CB9A83F8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralString
struct  DerGeneralString_t4B7BB0EE812FECFF6F3C5A1E791C722CB9A83F8F  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralString::str
	String_t* ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(DerGeneralString_t4B7BB0EE812FECFF6F3C5A1E791C722CB9A83F8F, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERGENERALSTRING_T4B7BB0EE812FECFF6F3C5A1E791C722CB9A83F8F_H
#ifndef DERGRAPHICSTRING_T1BFC8E710CE9879C4961C96DE178ED718B18A4B1_H
#define DERGRAPHICSTRING_T1BFC8E710CE9879C4961C96DE178ED718B18A4B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGraphicString
struct  DerGraphicString_t1BFC8E710CE9879C4961C96DE178ED718B18A4B1  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGraphicString::mString
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mString_2;

public:
	inline static int32_t get_offset_of_mString_2() { return static_cast<int32_t>(offsetof(DerGraphicString_t1BFC8E710CE9879C4961C96DE178ED718B18A4B1, ___mString_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mString_2() const { return ___mString_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mString_2() { return &___mString_2; }
	inline void set_mString_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mString_2 = value;
		Il2CppCodeGenWriteBarrier((&___mString_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERGRAPHICSTRING_T1BFC8E710CE9879C4961C96DE178ED718B18A4B1_H
#ifndef DERIA5STRING_TB7145189E4E76E79FD67198F52692D7B119415DE_H
#define DERIA5STRING_TB7145189E4E76E79FD67198F52692D7B119415DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String
struct  DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String::str
	String_t* ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERIA5STRING_TB7145189E4E76E79FD67198F52692D7B119415DE_H
#ifndef DERNULL_T9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810_H
#define DERNULL_T9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerNull
struct  DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810  : public Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerNull::zeroBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___zeroBytes_3;

public:
	inline static int32_t get_offset_of_zeroBytes_3() { return static_cast<int32_t>(offsetof(DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810, ___zeroBytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_zeroBytes_3() const { return ___zeroBytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_zeroBytes_3() { return &___zeroBytes_3; }
	inline void set_zeroBytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___zeroBytes_3 = value;
		Il2CppCodeGenWriteBarrier((&___zeroBytes_3), value);
	}
};

struct DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerNull BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerNull::Instance
	DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810_StaticFields, ___Instance_2)); }
	inline DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810 * get_Instance_2() const { return ___Instance_2; }
	inline DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERNULL_T9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810_H
#ifndef DERNUMERICSTRING_TCAA5D338AB1F4F3F4D8E7BE4B77644070228A991_H
#define DERNUMERICSTRING_TCAA5D338AB1F4F3F4D8E7BE4B77644070228A991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerNumericString
struct  DerNumericString_tCAA5D338AB1F4F3F4D8E7BE4B77644070228A991  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerNumericString::str
	String_t* ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(DerNumericString_tCAA5D338AB1F4F3F4D8E7BE4B77644070228A991, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERNUMERICSTRING_TCAA5D338AB1F4F3F4D8E7BE4B77644070228A991_H
#ifndef DEROCTETSTRING_TB3FE7FC789AF57240C7AAC64F625F962F499050A_H
#define DEROCTETSTRING_TB3FE7FC789AF57240C7AAC64F625F962F499050A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerOctetString
struct  DerOctetString_tB3FE7FC789AF57240C7AAC64F625F962F499050A  : public Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEROCTETSTRING_TB3FE7FC789AF57240C7AAC64F625F962F499050A_H
#ifndef DEROUTPUTSTREAM_TF14E89982B98990995F8610F800B024DD05ADFA1_H
#define DEROUTPUTSTREAM_TF14E89982B98990995F8610F800B024DD05ADFA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerOutputStream
struct  DerOutputStream_tF14E89982B98990995F8610F800B024DD05ADFA1  : public FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEROUTPUTSTREAM_TF14E89982B98990995F8610F800B024DD05ADFA1_H
#ifndef DERSEQUENCE_T28172634167ADE42A32B1361A26C6469E70B66BD_H
#define DERSEQUENCE_T28172634167ADE42A32B1361A26C6469E70B66BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSequence
struct  DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD  : public Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205
{
public:

public:
};

struct DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSequence::Empty
	DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD * ___Empty_3;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD_StaticFields, ___Empty_3)); }
	inline DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD * get_Empty_3() const { return ___Empty_3; }
	inline DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSEQUENCE_T28172634167ADE42A32B1361A26C6469E70B66BD_H
#ifndef DERSET_T0EF6821F7F0694159B02B3C79CEB9915176B219C_H
#define DERSET_T0EF6821F7F0694159B02B3C79CEB9915176B219C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSet
struct  DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C  : public Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29
{
public:

public:
};

struct DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSet BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerSet::Empty
	DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C * ___Empty_3;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C_StaticFields, ___Empty_3)); }
	inline DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C * get_Empty_3() const { return ___Empty_3; }
	inline DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSET_T0EF6821F7F0694159B02B3C79CEB9915176B219C_H
#ifndef DERTAGGEDOBJECT_T96F301FC6AA54E94C6FF065F9FE47ABC4918FF89_H
#define DERTAGGEDOBJECT_T96F301FC6AA54E94C6FF065F9FE47ABC4918FF89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerTaggedObject
struct  DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89  : public Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERTAGGEDOBJECT_T96F301FC6AA54E94C6FF065F9FE47ABC4918FF89_H
#ifndef LIMITEDINPUTSTREAM_TC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76_H
#define LIMITEDINPUTSTREAM_TC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.LimitedInputStream
struct  LimitedInputStream_tC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76  : public BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.LimitedInputStream::_in
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____in_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.LimitedInputStream::_limit
	int32_t ____limit_7;

public:
	inline static int32_t get_offset_of__in_6() { return static_cast<int32_t>(offsetof(LimitedInputStream_tC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76, ____in_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__in_6() const { return ____in_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__in_6() { return &____in_6; }
	inline void set__in_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____in_6 = value;
		Il2CppCodeGenWriteBarrier((&____in_6), value);
	}

	inline static int32_t get_offset_of__limit_7() { return static_cast<int32_t>(offsetof(LimitedInputStream_tC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76, ____limit_7)); }
	inline int32_t get__limit_7() const { return ____limit_7; }
	inline int32_t* get_address_of__limit_7() { return &____limit_7; }
	inline void set__limit_7(int32_t value)
	{
		____limit_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIMITEDINPUTSTREAM_TC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76_H
#ifndef CMSSIGNEDDATAOUTPUTSTREAM_TCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3_H
#define CMSSIGNEDDATAOUTPUTSTREAM_TCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_CmsSignedDataOutputStream
struct  CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_CmsSignedDataOutputStream::outer
	CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D * ___outer_6;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_CmsSignedDataOutputStream::_out
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____out_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_CmsSignedDataOutputStream::_contentOID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ____contentOID_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_CmsSignedDataOutputStream::_sGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ____sGen_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_CmsSignedDataOutputStream::_sigGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ____sigGen_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataStreamGenerator_CmsSignedDataOutputStream::_eiGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ____eiGen_11;

public:
	inline static int32_t get_offset_of_outer_6() { return static_cast<int32_t>(offsetof(CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3, ___outer_6)); }
	inline CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D * get_outer_6() const { return ___outer_6; }
	inline CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D ** get_address_of_outer_6() { return &___outer_6; }
	inline void set_outer_6(CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D * value)
	{
		___outer_6 = value;
		Il2CppCodeGenWriteBarrier((&___outer_6), value);
	}

	inline static int32_t get_offset_of__out_7() { return static_cast<int32_t>(offsetof(CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3, ____out_7)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__out_7() const { return ____out_7; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__out_7() { return &____out_7; }
	inline void set__out_7(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____out_7 = value;
		Il2CppCodeGenWriteBarrier((&____out_7), value);
	}

	inline static int32_t get_offset_of__contentOID_8() { return static_cast<int32_t>(offsetof(CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3, ____contentOID_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get__contentOID_8() const { return ____contentOID_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of__contentOID_8() { return &____contentOID_8; }
	inline void set__contentOID_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		____contentOID_8 = value;
		Il2CppCodeGenWriteBarrier((&____contentOID_8), value);
	}

	inline static int32_t get_offset_of__sGen_9() { return static_cast<int32_t>(offsetof(CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3, ____sGen_9)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get__sGen_9() const { return ____sGen_9; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of__sGen_9() { return &____sGen_9; }
	inline void set__sGen_9(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		____sGen_9 = value;
		Il2CppCodeGenWriteBarrier((&____sGen_9), value);
	}

	inline static int32_t get_offset_of__sigGen_10() { return static_cast<int32_t>(offsetof(CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3, ____sigGen_10)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get__sigGen_10() const { return ____sigGen_10; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of__sigGen_10() { return &____sigGen_10; }
	inline void set__sigGen_10(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		____sigGen_10 = value;
		Il2CppCodeGenWriteBarrier((&____sigGen_10), value);
	}

	inline static int32_t get_offset_of__eiGen_11() { return static_cast<int32_t>(offsetof(CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3, ____eiGen_11)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get__eiGen_11() const { return ____eiGen_11; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of__eiGen_11() { return &____eiGen_11; }
	inline void set__eiGen_11(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		____eiGen_11 = value;
		Il2CppCodeGenWriteBarrier((&____eiGen_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSSIGNEDDATAOUTPUTSTREAM_TCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3_H
#ifndef CMSSTREAMEXCEPTION_TEA020BF67738881A935C5EF2F8EF146AFC43A6B4_H
#define CMSSTREAMEXCEPTION_TEA020BF67738881A935C5EF2F8EF146AFC43A6B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsStreamException
struct  CmsStreamException_tEA020BF67738881A935C5EF2F8EF146AFC43A6B4  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSSTREAMEXCEPTION_TEA020BF67738881A935C5EF2F8EF146AFC43A6B4_H
#ifndef FULLREADERSTREAM_T0619CA98726230876874E513352058F0FDF21A25_H
#define FULLREADERSTREAM_T0619CA98726230876874E513352058F0FDF21A25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsTypedStream_FullReaderStream
struct  FullReaderStream_t0619CA98726230876874E513352058F0FDF21A25  : public FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FULLREADERSTREAM_T0619CA98726230876874E513352058F0FDF21A25_H
#ifndef ASN1OUTPUTSTREAM_T9DAB6C8668FF81ABE1D7ECEAC89715D31E3780CE_H
#define ASN1OUTPUTSTREAM_T9DAB6C8668FF81ABE1D7ECEAC89715D31E3780CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OutputStream
struct  Asn1OutputStream_t9DAB6C8668FF81ABE1D7ECEAC89715D31E3780CE  : public DerOutputStream_tF14E89982B98990995F8610F800B024DD05ADFA1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OUTPUTSTREAM_T9DAB6C8668FF81ABE1D7ECEAC89715D31E3780CE_H
#ifndef BERBITSTRING_TA87F07F7ECE2A5CDC2FC3CD95D54DC4558778735_H
#define BERBITSTRING_TA87F07F7ECE2A5CDC2FC3CD95D54DC4558778735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerBitString
struct  BerBitString_tA87F07F7ECE2A5CDC2FC3CD95D54DC4558778735  : public DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERBITSTRING_TA87F07F7ECE2A5CDC2FC3CD95D54DC4558778735_H
#ifndef BERNULL_TBF060485A34957BE13E941769F450E19BF9E6646_H
#define BERNULL_TBF060485A34957BE13E941769F450E19BF9E6646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerNull
struct  BerNull_tBF060485A34957BE13E941769F450E19BF9E6646  : public DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810
{
public:

public:
};

struct BerNull_tBF060485A34957BE13E941769F450E19BF9E6646_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerNull BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerNull::Instance
	BerNull_tBF060485A34957BE13E941769F450E19BF9E6646 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(BerNull_tBF060485A34957BE13E941769F450E19BF9E6646_StaticFields, ___Instance_4)); }
	inline BerNull_tBF060485A34957BE13E941769F450E19BF9E6646 * get_Instance_4() const { return ___Instance_4; }
	inline BerNull_tBF060485A34957BE13E941769F450E19BF9E6646 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(BerNull_tBF060485A34957BE13E941769F450E19BF9E6646 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERNULL_TBF060485A34957BE13E941769F450E19BF9E6646_H
#ifndef BEROCTETSTRING_T527B8093AED349423C78F991EE2E7B556E6B32EF_H
#define BEROCTETSTRING_T527B8093AED349423C78F991EE2E7B556E6B32EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetString
struct  BerOctetString_t527B8093AED349423C78F991EE2E7B556E6B32EF  : public DerOctetString_tB3FE7FC789AF57240C7AAC64F625F962F499050A
{
public:
	// System.Collections.IEnumerable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOctetString::octs
	RuntimeObject* ___octs_4;

public:
	inline static int32_t get_offset_of_octs_4() { return static_cast<int32_t>(offsetof(BerOctetString_t527B8093AED349423C78F991EE2E7B556E6B32EF, ___octs_4)); }
	inline RuntimeObject* get_octs_4() const { return ___octs_4; }
	inline RuntimeObject** get_address_of_octs_4() { return &___octs_4; }
	inline void set_octs_4(RuntimeObject* value)
	{
		___octs_4 = value;
		Il2CppCodeGenWriteBarrier((&___octs_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEROCTETSTRING_T527B8093AED349423C78F991EE2E7B556E6B32EF_H
#ifndef BEROUTPUTSTREAM_TFE4E028FDC5CBF2EC1039456BA370F93BDF1235B_H
#define BEROUTPUTSTREAM_TFE4E028FDC5CBF2EC1039456BA370F93BDF1235B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerOutputStream
struct  BerOutputStream_tFE4E028FDC5CBF2EC1039456BA370F93BDF1235B  : public DerOutputStream_tF14E89982B98990995F8610F800B024DD05ADFA1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEROUTPUTSTREAM_TFE4E028FDC5CBF2EC1039456BA370F93BDF1235B_H
#ifndef BERSEQUENCE_TE2E7D6CBE12BC46965F5896896774760DD5D8324_H
#define BERSEQUENCE_TE2E7D6CBE12BC46965F5896896774760DD5D8324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequence
struct  BerSequence_tE2E7D6CBE12BC46965F5896896774760DD5D8324  : public DerSequence_t28172634167ADE42A32B1361A26C6469E70B66BD
{
public:

public:
};

struct BerSequence_tE2E7D6CBE12BC46965F5896896774760DD5D8324_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequence::Empty
	BerSequence_tE2E7D6CBE12BC46965F5896896774760DD5D8324 * ___Empty_4;

public:
	inline static int32_t get_offset_of_Empty_4() { return static_cast<int32_t>(offsetof(BerSequence_tE2E7D6CBE12BC46965F5896896774760DD5D8324_StaticFields, ___Empty_4)); }
	inline BerSequence_tE2E7D6CBE12BC46965F5896896774760DD5D8324 * get_Empty_4() const { return ___Empty_4; }
	inline BerSequence_tE2E7D6CBE12BC46965F5896896774760DD5D8324 ** get_address_of_Empty_4() { return &___Empty_4; }
	inline void set_Empty_4(BerSequence_tE2E7D6CBE12BC46965F5896896774760DD5D8324 * value)
	{
		___Empty_4 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERSEQUENCE_TE2E7D6CBE12BC46965F5896896774760DD5D8324_H
#ifndef BERSET_T89D6095AD9BFD266699B53A304C30B358343B6B6_H
#define BERSET_T89D6095AD9BFD266699B53A304C30B358343B6B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSet
struct  BerSet_t89D6095AD9BFD266699B53A304C30B358343B6B6  : public DerSet_t0EF6821F7F0694159B02B3C79CEB9915176B219C
{
public:

public:
};

struct BerSet_t89D6095AD9BFD266699B53A304C30B358343B6B6_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSet BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSet::Empty
	BerSet_t89D6095AD9BFD266699B53A304C30B358343B6B6 * ___Empty_4;

public:
	inline static int32_t get_offset_of_Empty_4() { return static_cast<int32_t>(offsetof(BerSet_t89D6095AD9BFD266699B53A304C30B358343B6B6_StaticFields, ___Empty_4)); }
	inline BerSet_t89D6095AD9BFD266699B53A304C30B358343B6B6 * get_Empty_4() const { return ___Empty_4; }
	inline BerSet_t89D6095AD9BFD266699B53A304C30B358343B6B6 ** get_address_of_Empty_4() { return &___Empty_4; }
	inline void set_Empty_4(BerSet_t89D6095AD9BFD266699B53A304C30B358343B6B6 * value)
	{
		___Empty_4 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERSET_T89D6095AD9BFD266699B53A304C30B358343B6B6_H
#ifndef BERTAGGEDOBJECT_TB22932A6FABE90BC621D0D8D6978B0D6276ABAC2_H
#define BERTAGGEDOBJECT_TB22932A6FABE90BC621D0D8D6978B0D6276ABAC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerTaggedObject
struct  BerTaggedObject_tB22932A6FABE90BC621D0D8D6978B0D6276ABAC2  : public DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERTAGGEDOBJECT_TB22932A6FABE90BC621D0D8D6978B0D6276ABAC2_H
#ifndef DEFINITELENGTHINPUTSTREAM_TFC940822F7F42DD1834499C3DA1887766B288141_H
#define DEFINITELENGTHINPUTSTREAM_TFC940822F7F42DD1834499C3DA1887766B288141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DefiniteLengthInputStream
struct  DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141  : public LimitedInputStream_tC2227C19A21B798EEA52FB696B8E1D5D7D7CFD76
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DefiniteLengthInputStream::_originalLength
	int32_t ____originalLength_9;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DefiniteLengthInputStream::_remaining
	int32_t ____remaining_10;

public:
	inline static int32_t get_offset_of__originalLength_9() { return static_cast<int32_t>(offsetof(DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141, ____originalLength_9)); }
	inline int32_t get__originalLength_9() const { return ____originalLength_9; }
	inline int32_t* get_address_of__originalLength_9() { return &____originalLength_9; }
	inline void set__originalLength_9(int32_t value)
	{
		____originalLength_9 = value;
	}

	inline static int32_t get_offset_of__remaining_10() { return static_cast<int32_t>(offsetof(DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141, ____remaining_10)); }
	inline int32_t get__remaining_10() const { return ____remaining_10; }
	inline int32_t* get_address_of__remaining_10() { return &____remaining_10; }
	inline void set__remaining_10(int32_t value)
	{
		____remaining_10 = value;
	}
};

struct DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DefiniteLengthInputStream::EmptyBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EmptyBytes_8;

public:
	inline static int32_t get_offset_of_EmptyBytes_8() { return static_cast<int32_t>(offsetof(DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141_StaticFields, ___EmptyBytes_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EmptyBytes_8() const { return ___EmptyBytes_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EmptyBytes_8() { return &___EmptyBytes_8; }
	inline void set_EmptyBytes_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EmptyBytes_8 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyBytes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITELENGTHINPUTSTREAM_TFC940822F7F42DD1834499C3DA1887766B288141_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5100 = { sizeof (SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5100[8] = 
{
	SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D::get_offset_of_outer_0(),
	SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D::get_offset_of_sigCalc_1(),
	SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D::get_offset_of_signerIdentifier_2(),
	SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D::get_offset_of_digestOID_3(),
	SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D::get_offset_of_encOID_4(),
	SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D::get_offset_of_sAttr_5(),
	SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D::get_offset_of_unsAttr_6(),
	SignerInf_tD8870DACB906B28D276ABA3B402CB3ED7BA37B3D::get_offset_of_baseSignedTable_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5101 = { sizeof (CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35), -1, sizeof(CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5101[13] = 
{
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35_StaticFields::get_offset_of_Helper_2(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__signedData_3(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__signedContentType_4(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__signedContent_5(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__digests_6(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__digestOids_7(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__signerInfoStore_8(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__certSet_9(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__crlSet_10(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__isCertCrlParsed_11(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__attributeStore_12(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__certificateStore_13(),
	CmsSignedDataParser_t4A4AED9D994AB971FAD94F3454052FFBF0520C35::get_offset_of__crlStore_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5102 = { sizeof (CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D), -1, sizeof(CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5102[7] = 
{
	CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D_StaticFields::get_offset_of_Helper_24(),
	CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D::get_offset_of__signerInfs_25(),
	CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D::get_offset_of__messageDigestOids_26(),
	CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D::get_offset_of__messageDigests_27(),
	CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D::get_offset_of__messageHashes_28(),
	CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D::get_offset_of__messageDigestsLocked_29(),
	CmsSignedDataStreamGenerator_t34D1424AD2560BE95C123E597A22ECE811249D2D::get_offset_of__bufferSize_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5103 = { sizeof (DigestAndSignerInfoGeneratorHolder_tB87B4E4EC99013C14C3BA2641C13D072F2A18A1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5103[2] = 
{
	DigestAndSignerInfoGeneratorHolder_tB87B4E4EC99013C14C3BA2641C13D072F2A18A1B::get_offset_of_signerInf_0(),
	DigestAndSignerInfoGeneratorHolder_tB87B4E4EC99013C14C3BA2641C13D072F2A18A1B::get_offset_of_digestOID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5104 = { sizeof (SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5104[8] = 
{
	SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A::get_offset_of_outer_0(),
	SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A::get_offset_of__signerIdentifier_1(),
	SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A::get_offset_of__digestOID_2(),
	SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A::get_offset_of__encOID_3(),
	SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A::get_offset_of__sAttr_4(),
	SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A::get_offset_of__unsAttr_5(),
	SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A::get_offset_of__encName_6(),
	SignerInfoGeneratorImpl_t01073E540A9A35E655B85F1758AFC09E4DC3883A::get_offset_of__sig_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5105 = { sizeof (CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5105[6] = 
{
	CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3::get_offset_of_outer_6(),
	CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3::get_offset_of__out_7(),
	CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3::get_offset_of__contentOID_8(),
	CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3::get_offset_of__sGen_9(),
	CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3::get_offset_of__sigGen_10(),
	CmsSignedDataOutputStream_tCF8A376E9B2DEC2FD338E520B2AD07E4DEE09AD3::get_offset_of__eiGen_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5106 = { sizeof (DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E), -1, sizeof(DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5106[14] = 
{
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_algorithms_0(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_noParams_1(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of__params_2(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_pkcs15RsaEncryption_3(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_digestOids_4(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_digestBuilders_5(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_ENCRYPTION_RSA_6(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_ENCRYPTION_DSA_7(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_ENCRYPTION_ECDSA_8(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_ENCRYPTION_RSA_PSS_9(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_ENCRYPTION_GOST3410_10(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_ENCRYPTION_ECGOST3410_11(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_ENCRYPTION_ECGOST3410_2012_256_12(),
	DefaultSignatureAlgorithmIdentifierFinder_t832C4C1AEDA8727ED39B33FD32DCF8CF5625189E_StaticFields::get_offset_of_ENCRYPTION_ECGOST3410_2012_512_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5107 = { sizeof (DefaultDigestAlgorithmIdentifierFinder_t5BE60CA84E15549D1FD606CCC88BA26483D72694), -1, sizeof(DefaultDigestAlgorithmIdentifierFinder_t5BE60CA84E15549D1FD606CCC88BA26483D72694_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5107[2] = 
{
	DefaultDigestAlgorithmIdentifierFinder_t5BE60CA84E15549D1FD606CCC88BA26483D72694_StaticFields::get_offset_of_digestOids_0(),
	DefaultDigestAlgorithmIdentifierFinder_t5BE60CA84E15549D1FD606CCC88BA26483D72694_StaticFields::get_offset_of_digestNameToOids_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5108 = { sizeof (CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44), -1, sizeof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5108[24] = 
{
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_Data_0(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_DigestSha1_1(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_DigestSha224_2(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_DigestSha256_3(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_DigestSha384_4(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_DigestSha512_5(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_DigestMD5_6(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_DigestGost3411_7(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_DigestRipeMD128_8(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_DigestRipeMD160_9(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_DigestRipeMD256_10(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_EncryptionRsa_11(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_EncryptionDsa_12(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_EncryptionECDsa_13(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_EncryptionRsaPss_14(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_EncryptionGost3410_15(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields::get_offset_of_EncryptionECGost3410_16(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44::get_offset_of__certs_17(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44::get_offset_of__crls_18(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44::get_offset_of__signers_19(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44::get_offset_of__digests_20(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44::get_offset_of__useDerForCerts_21(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44::get_offset_of__useDerForCrls_22(),
	CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44::get_offset_of_rand_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5109 = { sizeof (CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF), -1, sizeof(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5109[11] = 
{
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields::get_offset_of_Instance_0(),
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields::get_offset_of_EncryptionECDsaWithSha1_1(),
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields::get_offset_of_EncryptionECDsaWithSha224_2(),
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields::get_offset_of_EncryptionECDsaWithSha256_3(),
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields::get_offset_of_EncryptionECDsaWithSha384_4(),
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields::get_offset_of_EncryptionECDsaWithSha512_5(),
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields::get_offset_of_encryptionAlgs_6(),
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields::get_offset_of_digestAlgs_7(),
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields::get_offset_of_digestAliases_8(),
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields::get_offset_of_noParams_9(),
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF_StaticFields::get_offset_of_ecAlgorithms_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5110 = { sizeof (CmsStreamException_tEA020BF67738881A935C5EF2F8EF146AFC43A6B4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5111 = { sizeof (CmsTypedStream_t3C034672F2E5D2B8EDF5618BDE3E4DA90F540591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5111[3] = 
{
	0,
	CmsTypedStream_t3C034672F2E5D2B8EDF5618BDE3E4DA90F540591::get_offset_of__oid_1(),
	CmsTypedStream_t3C034672F2E5D2B8EDF5618BDE3E4DA90F540591::get_offset_of__in_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5112 = { sizeof (FullReaderStream_t0619CA98726230876874E513352058F0FDF21A25), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5113 = { sizeof (CmsUtilities_tEC89F9AE2F78B8E95619AFE5BFAC651765637340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5114 = { sizeof (CounterSignatureDigestCalculator_t34C8C9E3A64AA7D9CD73963DF9755DDDC5231E0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5114[2] = 
{
	CounterSignatureDigestCalculator_t34C8C9E3A64AA7D9CD73963DF9755DDDC5231E0E::get_offset_of_alg_0(),
	CounterSignatureDigestCalculator_t34C8C9E3A64AA7D9CD73963DF9755DDDC5231E0E::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5115 = { sizeof (DefaultAuthenticatedAttributeTableGenerator_t10100711C917BC7A845765E91490C3E5FDB7EDDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5115[1] = 
{
	DefaultAuthenticatedAttributeTableGenerator_t10100711C917BC7A845765E91490C3E5FDB7EDDF::get_offset_of_table_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5116 = { sizeof (DefaultSignedAttributeTableGenerator_tE94AB39D68713C599FCC9FFA5D7347D4C9121DD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5116[1] = 
{
	DefaultSignedAttributeTableGenerator_tE94AB39D68713C599FCC9FFA5D7347D4C9121DD8::get_offset_of_table_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5117 = { sizeof (EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5), -1, sizeof(EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5117[2] = 
{
	EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5_StaticFields::get_offset_of_BaseCipherNames_0(),
	EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5_StaticFields::get_offset_of_MacAlgNames_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5118 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5119 = { sizeof (KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076), -1, sizeof(KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5119[5] = 
{
	KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076_StaticFields::get_offset_of_Helper_0(),
	KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076::get_offset_of_keyEncryptionKey_1(),
	KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076::get_offset_of_keyEncryptionKeyOID_2(),
	KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076::get_offset_of_kekIdentifier_3(),
	KekRecipientInfoGenerator_tF8307F35689C819FBF5DBB384E62ECE676B47076::get_offset_of_keyEncryptionAlgorithm_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5120 = { sizeof (KekRecipientInformation_tA9FEDD2E1E55EF74573143E980B44D5C77E6DD18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5120[1] = 
{
	KekRecipientInformation_tA9FEDD2E1E55EF74573143E980B44D5C77E6DD18::get_offset_of_info_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5121 = { sizeof (KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB), -1, sizeof(KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5121[5] = 
{
	KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB_StaticFields::get_offset_of_Helper_0(),
	KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB::get_offset_of_keyAgreementOID_1(),
	KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB::get_offset_of_keyEncryptionOID_2(),
	KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB::get_offset_of_recipientCerts_3(),
	KeyAgreeRecipientInfoGenerator_t5218DF122585417200F76F86844A7EBC76C8F2BB::get_offset_of_senderKeyPair_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5122 = { sizeof (KeyAgreeRecipientInformation_tFCEBDF940682EDC6E6C5C972749DA991964EDD34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5122[2] = 
{
	KeyAgreeRecipientInformation_tFCEBDF940682EDC6E6C5C972749DA991964EDD34::get_offset_of_info_4(),
	KeyAgreeRecipientInformation_tFCEBDF940682EDC6E6C5C972749DA991964EDD34::get_offset_of_encryptedKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5123 = { sizeof (KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453), -1, sizeof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5123[7] = 
{
	KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453_StaticFields::get_offset_of_Helper_0(),
	KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453::get_offset_of_recipientTbsCert_1(),
	KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453::get_offset_of_recipientPublicKey_2(),
	KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453::get_offset_of_subjectKeyIdentifier_3(),
	KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453::get_offset_of_info_4(),
	KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453::get_offset_of_issuerAndSerialNumber_5(),
	KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453::get_offset_of_random_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5124 = { sizeof (KeyTransRecipientInformation_t917347F49BFA4B838E11F2F11DC7191E00E7DC47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5124[1] = 
{
	KeyTransRecipientInformation_t917347F49BFA4B838E11F2F11DC7191E00E7DC47::get_offset_of_info_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5125 = { sizeof (OriginatorID_t882B95B8A4875CAC72134FCA20A997CF517EB400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5126 = { sizeof (OriginatorInfoGenerator_t1BDF7D5D5770B2B788B322D9FAF4A011D6B1AC35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5126[2] = 
{
	OriginatorInfoGenerator_t1BDF7D5D5770B2B788B322D9FAF4A011D6B1AC35::get_offset_of_origCerts_0(),
	OriginatorInfoGenerator_t1BDF7D5D5770B2B788B322D9FAF4A011D6B1AC35::get_offset_of_origCrls_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5127 = { sizeof (OriginatorInformation_t7A1FD3BA30B65CAB1D4B2C4F037E5697AB3A9604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5127[1] = 
{
	OriginatorInformation_t7A1FD3BA30B65CAB1D4B2C4F037E5697AB3A9604::get_offset_of_originatorInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5128 = { sizeof (PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23), -1, sizeof(PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5128[4] = 
{
	PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23_StaticFields::get_offset_of_Helper_0(),
	PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23::get_offset_of_keyDerivationAlgorithm_1(),
	PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23::get_offset_of_keyEncryptionKey_2(),
	PasswordRecipientInfoGenerator_t02923FCB678BC61C246E7A4E18BE8814332ECB23::get_offset_of_keyEncryptionKeyOID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5129 = { sizeof (PasswordRecipientInformation_t2245D57E1455981673C3B8F9120A922D91B10029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5129[1] = 
{
	PasswordRecipientInformation_t2245D57E1455981673C3B8F9120A922D91B10029::get_offset_of_info_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5130 = { sizeof (Pkcs5Scheme2PbeKey_t8D9B8F31F6796AAD2E73E5BC17909E8E1779AB61), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5131 = { sizeof (Pkcs5Scheme2Utf8PbeKey_tFBA7065A9BC5EF68CFED19DFB066767AD6B93C47), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5132 = { sizeof (RecipientID_t25DA4BBF07A25C6047B6350910E7FCA460AE920B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5132[1] = 
{
	RecipientID_t25DA4BBF07A25C6047B6350910E7FCA460AE920B::get_offset_of_keyIdentifier_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5133 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5134 = { sizeof (RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5134[4] = 
{
	RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483::get_offset_of_rid_0(),
	RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483::get_offset_of_keyEncAlg_1(),
	RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483::get_offset_of_secureReadable_2(),
	RecipientInformation_t3F7F606CAD85887D18351A4C022570EADC97E483::get_offset_of_resultMac_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5135 = { sizeof (RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5135[2] = 
{
	RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB::get_offset_of_all_0(),
	RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB::get_offset_of_table_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5136 = { sizeof (SignerID_tC9728986CC0E7C53B1C2C2DA90EB09DE973E0B15), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5137 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5138 = { sizeof (SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5138[6] = 
{
	SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B::get_offset_of_certificate_0(),
	SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B::get_offset_of_contentSigner_1(),
	SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B::get_offset_of_sigId_2(),
	SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B::get_offset_of_signedGen_3(),
	SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B::get_offset_of_unsignedGen_4(),
	SignerInfoGenerator_t6D07A63F8D83F22DF855E27FCCDEF0187AF6D25B::get_offset_of_isDirectSignature_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5139 = { sizeof (SignerInfoGeneratorBuilder_tF36884E6798566C94006141926E62168477D61D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5139[3] = 
{
	SignerInfoGeneratorBuilder_tF36884E6798566C94006141926E62168477D61D8::get_offset_of_directSignature_0(),
	SignerInfoGeneratorBuilder_tF36884E6798566C94006141926E62168477D61D8::get_offset_of_signedGen_1(),
	SignerInfoGeneratorBuilder_tF36884E6798566C94006141926E62168477D61D8::get_offset_of_unsignedGen_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5140 = { sizeof (SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2), -1, sizeof(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5140[15] = 
{
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2_StaticFields::get_offset_of_Helper_0(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_sid_1(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_info_2(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_digestAlgorithm_3(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_encryptionAlgorithm_4(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_signedAttributeSet_5(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_unsignedAttributeSet_6(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_content_7(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_signature_8(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_contentType_9(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_digestCalculator_10(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_resultDigest_11(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_signedAttributeTable_12(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_unsignedAttributeTable_13(),
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2::get_offset_of_isCounterSignature_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5141 = { sizeof (SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5141[2] = 
{
	SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575::get_offset_of_all_0(),
	SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575::get_offset_of_table_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5142 = { sizeof (SimpleAttributeTableGenerator_tF8A55E8F0256D8BE547DAE85DE888E588305045B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5142[1] = 
{
	SimpleAttributeTableGenerator_tF8A55E8F0256D8BE547DAE85DE888E588305045B::get_offset_of_attributes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5143 = { sizeof (Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5143[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5144 = { sizeof (Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509), -1, sizeof(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5144[5] = 
{
	Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509_StaticFields::get_offset_of_EmptyElements_0(),
	0,
	Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509::get_offset_of_elements_2(),
	Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509::get_offset_of_elementCount_3(),
	Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509::get_offset_of_copyOnWrite_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5145 = { sizeof (Asn1Exception_t144D1DCE60247FE659A34C18115EC3BC68AB9552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5146 = { sizeof (Asn1Generator_t56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5146[1] = 
{
	Asn1Generator_t56AE36F6C5A461219F38DA7F401C20D6AE9F5B6A::get_offset_of__out_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5147 = { sizeof (Asn1InputStream_t41FB5C019456D8C95B07D0B145B80E4952061D33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5147[2] = 
{
	Asn1InputStream_t41FB5C019456D8C95B07D0B145B80E4952061D33::get_offset_of_limit_6(),
	Asn1InputStream_t41FB5C019456D8C95B07D0B145B80E4952061D33::get_offset_of_tmpBuffers_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5148 = { sizeof (Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5149 = { sizeof (Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5150 = { sizeof (Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5150[1] = 
{
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5151 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5152 = { sizeof (Asn1OutputStream_t9DAB6C8668FF81ABE1D7ECEAC89715D31E3780CE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5153 = { sizeof (Asn1ParsingException_t70F867DA13E608942B6B9E84E88BF21D9B4B8325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5154 = { sizeof (Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5154[1] = 
{
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205::get_offset_of_elements_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5155 = { sizeof (Asn1SequenceParserImpl_t1F76546A6B67A6D3F54BE558EE072A781F49489F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5155[3] = 
{
	Asn1SequenceParserImpl_t1F76546A6B67A6D3F54BE558EE072A781F49489F::get_offset_of_outer_0(),
	Asn1SequenceParserImpl_t1F76546A6B67A6D3F54BE558EE072A781F49489F::get_offset_of_max_1(),
	Asn1SequenceParserImpl_t1F76546A6B67A6D3F54BE558EE072A781F49489F::get_offset_of_index_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5156 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5157 = { sizeof (Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5157[1] = 
{
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29::get_offset_of_elements_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5158 = { sizeof (Asn1SetParserImpl_t2D4FFC7B4A3F718D806C4CAC0278B4200C4A5D5F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5158[3] = 
{
	Asn1SetParserImpl_t2D4FFC7B4A3F718D806C4CAC0278B4200C4A5D5F::get_offset_of_outer_0(),
	Asn1SetParserImpl_t2D4FFC7B4A3F718D806C4CAC0278B4200C4A5D5F::get_offset_of_max_1(),
	Asn1SetParserImpl_t2D4FFC7B4A3F718D806C4CAC0278B4200C4A5D5F::get_offset_of_index_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5159 = { sizeof (DerComparer_t46FB79AFF30E476AF32F391A82FF1C64AA2CA696), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5160 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5161 = { sizeof (Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5161[3] = 
{
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317::get_offset_of__in_0(),
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317::get_offset_of__limit_1(),
	Asn1StreamParser_t89ECA5C95FABEF7D278274E073575A457B221317::get_offset_of_tmpBuffers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5162 = { sizeof (Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5162[3] = 
{
	Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD::get_offset_of_tagNo_2(),
	Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD::get_offset_of_explicitly_3(),
	Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD::get_offset_of_obj_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5163 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5164 = { sizeof (Asn1Tags_tE9792E54ECDAAC0A994FFCE6C9FEB4515DD94946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5164[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5165 = { sizeof (BerApplicationSpecific_t2C9A3C98F2FFCB3BF1576D27FD93108DB2523AA1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5166 = { sizeof (BerApplicationSpecificParser_tCEDDF7625FEF1CC43A444CAA7BC63B38DDEA8F41), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5166[2] = 
{
	BerApplicationSpecificParser_tCEDDF7625FEF1CC43A444CAA7BC63B38DDEA8F41::get_offset_of_tag_0(),
	BerApplicationSpecificParser_tCEDDF7625FEF1CC43A444CAA7BC63B38DDEA8F41::get_offset_of_parser_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5167 = { sizeof (BerBitString_tA87F07F7ECE2A5CDC2FC3CD95D54DC4558778735), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5168 = { sizeof (BerGenerator_t1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5168[3] = 
{
	BerGenerator_t1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0::get_offset_of__tagged_1(),
	BerGenerator_t1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0::get_offset_of__isExplicit_2(),
	BerGenerator_t1973C71F92B61A39B2BEBCE58119D1E8AC8DDFF0::get_offset_of__tagNo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5169 = { sizeof (BerNull_tBF060485A34957BE13E941769F450E19BF9E6646), -1, sizeof(BerNull_tBF060485A34957BE13E941769F450E19BF9E6646_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5169[1] = 
{
	BerNull_tBF060485A34957BE13E941769F450E19BF9E6646_StaticFields::get_offset_of_Instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5170 = { sizeof (BerOctetString_t527B8093AED349423C78F991EE2E7B556E6B32EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5170[2] = 
{
	0,
	BerOctetString_t527B8093AED349423C78F991EE2E7B556E6B32EF::get_offset_of_octs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5171 = { sizeof (BerOctetStringGenerator_tF1BB093CD2F03074CFDD7EC178499D2204B259F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5172 = { sizeof (BufferedBerOctetStream_tD45251DB21F20A789A51520F5BC512BCA9ACE2CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5172[4] = 
{
	BufferedBerOctetStream_tD45251DB21F20A789A51520F5BC512BCA9ACE2CE::get_offset_of__buf_6(),
	BufferedBerOctetStream_tD45251DB21F20A789A51520F5BC512BCA9ACE2CE::get_offset_of__off_7(),
	BufferedBerOctetStream_tD45251DB21F20A789A51520F5BC512BCA9ACE2CE::get_offset_of__gen_8(),
	BufferedBerOctetStream_tD45251DB21F20A789A51520F5BC512BCA9ACE2CE::get_offset_of__derOut_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5173 = { sizeof (BerOctetStringParser_tA651A13B96DA5D19BCD01A462A1FC6D8C9FD8CA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5173[1] = 
{
	BerOctetStringParser_tA651A13B96DA5D19BCD01A462A1FC6D8C9FD8CA3::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5174 = { sizeof (BerOutputStream_tFE4E028FDC5CBF2EC1039456BA370F93BDF1235B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5175 = { sizeof (BerSequence_tE2E7D6CBE12BC46965F5896896774760DD5D8324), -1, sizeof(BerSequence_tE2E7D6CBE12BC46965F5896896774760DD5D8324_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5175[1] = 
{
	BerSequence_tE2E7D6CBE12BC46965F5896896774760DD5D8324_StaticFields::get_offset_of_Empty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5176 = { sizeof (BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5177 = { sizeof (BerSequenceParser_tDAF35CA4508663B98086A0CA886B5E6CC8540AC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5177[1] = 
{
	BerSequenceParser_tDAF35CA4508663B98086A0CA886B5E6CC8540AC8::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5178 = { sizeof (BerSet_t89D6095AD9BFD266699B53A304C30B358343B6B6), -1, sizeof(BerSet_t89D6095AD9BFD266699B53A304C30B358343B6B6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5178[1] = 
{
	BerSet_t89D6095AD9BFD266699B53A304C30B358343B6B6_StaticFields::get_offset_of_Empty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5179 = { sizeof (BerSetGenerator_tE45B4F869E2D61BF2B0674AD39F7495B437009B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5180 = { sizeof (BerSetParser_t77ADA7D5576259917D4266B90953E6D35A6C1CD1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5180[1] = 
{
	BerSetParser_t77ADA7D5576259917D4266B90953E6D35A6C1CD1::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5181 = { sizeof (BerTaggedObject_tB22932A6FABE90BC621D0D8D6978B0D6276ABAC2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5182 = { sizeof (BerTaggedObjectParser_t60543EEC9BC41C3C7B0AB81CBA0E654DF6D1043F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5182[3] = 
{
	BerTaggedObjectParser_t60543EEC9BC41C3C7B0AB81CBA0E654DF6D1043F::get_offset_of__constructed_0(),
	BerTaggedObjectParser_t60543EEC9BC41C3C7B0AB81CBA0E654DF6D1043F::get_offset_of__tagNumber_1(),
	BerTaggedObjectParser_t60543EEC9BC41C3C7B0AB81CBA0E654DF6D1043F::get_offset_of__parser_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5183 = { sizeof (ConstructedOctetStream_tCFA941EA97FC1233325702E1A013FC3FDABCED84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5183[3] = 
{
	ConstructedOctetStream_tCFA941EA97FC1233325702E1A013FC3FDABCED84::get_offset_of__parser_6(),
	ConstructedOctetStream_tCFA941EA97FC1233325702E1A013FC3FDABCED84::get_offset_of__first_7(),
	ConstructedOctetStream_tCFA941EA97FC1233325702E1A013FC3FDABCED84::get_offset_of__currentStream_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5184 = { sizeof (DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141), -1, sizeof(DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5184[3] = 
{
	DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141_StaticFields::get_offset_of_EmptyBytes_8(),
	DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141::get_offset_of__originalLength_9(),
	DefiniteLengthInputStream_tFC940822F7F42DD1834499C3DA1887766B288141::get_offset_of__remaining_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5185 = { sizeof (DerApplicationSpecific_t5D72A362581D6315E480D7EDC518751C1D33099D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5185[3] = 
{
	DerApplicationSpecific_t5D72A362581D6315E480D7EDC518751C1D33099D::get_offset_of_isConstructed_2(),
	DerApplicationSpecific_t5D72A362581D6315E480D7EDC518751C1D33099D::get_offset_of_tag_3(),
	DerApplicationSpecific_t5D72A362581D6315E480D7EDC518751C1D33099D::get_offset_of_octets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5186 = { sizeof (DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374), -1, sizeof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5186[3] = 
{
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields::get_offset_of_table_2(),
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374::get_offset_of_mData_3(),
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374::get_offset_of_mPadBits_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5187 = { sizeof (DerBmpString_t3857ACCA2B0C039687D93A2A90561B890E2155E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5187[1] = 
{
	DerBmpString_t3857ACCA2B0C039687D93A2A90561B890E2155E7::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5188 = { sizeof (DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C), -1, sizeof(DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5188[3] = 
{
	DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C::get_offset_of_value_2(),
	DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C_StaticFields::get_offset_of_False_3(),
	DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C_StaticFields::get_offset_of_True_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5189 = { sizeof (DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F), -1, sizeof(DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5189[3] = 
{
	DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F::get_offset_of_bytes_2(),
	DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F::get_offset_of_start_3(),
	DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_StaticFields::get_offset_of_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5190 = { sizeof (DerExternal_t31395586E8440842B73081A589776D6788A16E53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5190[5] = 
{
	DerExternal_t31395586E8440842B73081A589776D6788A16E53::get_offset_of_directReference_2(),
	DerExternal_t31395586E8440842B73081A589776D6788A16E53::get_offset_of_indirectReference_3(),
	DerExternal_t31395586E8440842B73081A589776D6788A16E53::get_offset_of_dataValueDescriptor_4(),
	DerExternal_t31395586E8440842B73081A589776D6788A16E53::get_offset_of_encoding_5(),
	DerExternal_t31395586E8440842B73081A589776D6788A16E53::get_offset_of_externalContent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5191 = { sizeof (DerExternalParser_t064BC06E39CFF6D49F7DAC50485ACB0DB94E5905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5191[1] = 
{
	DerExternalParser_t064BC06E39CFF6D49F7DAC50485ACB0DB94E5905::get_offset_of__parser_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5192 = { sizeof (DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5192[1] = 
{
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5193 = { sizeof (DerGeneralString_t4B7BB0EE812FECFF6F3C5A1E791C722CB9A83F8F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5193[1] = 
{
	DerGeneralString_t4B7BB0EE812FECFF6F3C5A1E791C722CB9A83F8F::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5194 = { sizeof (DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5194[3] = 
{
	DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E::get_offset_of__tagged_1(),
	DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E::get_offset_of__isExplicit_2(),
	DerGenerator_t61583277391BC057473563CDC35A6DE1070E078E::get_offset_of__tagNo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5195 = { sizeof (DerGraphicString_t1BFC8E710CE9879C4961C96DE178ED718B18A4B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5195[1] = 
{
	DerGraphicString_t1BFC8E710CE9879C4961C96DE178ED718B18A4B1::get_offset_of_mString_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5196 = { sizeof (DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5196[1] = 
{
	DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5197 = { sizeof (DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5197[5] = 
{
	0,
	0,
	0,
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96::get_offset_of_bytes_5(),
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96::get_offset_of_start_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5198 = { sizeof (DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810), -1, sizeof(DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5198[2] = 
{
	DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810_StaticFields::get_offset_of_Instance_2(),
	DerNull_t9CDC8F1D69D899ED1C74FC6BA7F3706D5A73C810::get_offset_of_zeroBytes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5199 = { sizeof (DerNumericString_tCAA5D338AB1F4F3F4D8E7BE4B77644070228A991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5199[1] = 
{
	DerNumericString_tCAA5D338AB1F4F3F4D8E7BE4B77644070228A991::get_offset_of_str_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
