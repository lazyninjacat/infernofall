﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector
struct Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString
struct Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence
struct Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set
struct Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusInfo
struct PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo
struct ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString
struct DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean
struct DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated
struct DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime
struct DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String
struct DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger
struct DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier[]
struct DerObjectIdentifierU5BU5D_tC595A7256A12DE98032B8267A8795C3EC8F3D6B9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerStringBase
struct DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerTaggedObject
struct DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.Accuracy
struct Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.MessageImprint
struct MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString
struct DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertIssuer
struct AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DisplayText
struct DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName
struct GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName[]
struct GeneralNameU5BU5D_t8C8E87C7B1F2854AD266B704A0BA432004D2DC73;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames
struct GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Holder
struct Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial
struct IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.NoticeReference
struct NoticeReference_t881A2D3508F9AA3C2EEC071137E809179E643234;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ObjectDigestInfo
struct ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.Iso4217CurrencyCode
struct Iso4217CurrencyCode_tA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.TypeOfBiometricData
struct TypeOfBiometricData_t7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.NameOrPseudonym
struct NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo
struct SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure
struct TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time
struct Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions
struct X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name
struct X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509NameEntryConverter
struct X509NameEntryConverter_t007AA731425354FFC770B335EB86EC1126045D28;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters
struct X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerable
struct IEnumerable_tD74549CEA1AA48E768382B94FEACBB07E2E3FA2C;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#define ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct  Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifndef SECNAMEDCURVES_T21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A_H
#define SECNAMEDCURVES_T21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves
struct  SecNamedCurves_t21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A  : public RuntimeObject
{
public:

public:
};

struct SecNamedCurves_t21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves::curves
	RuntimeObject* ___curves_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves::names
	RuntimeObject* ___names_2;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(SecNamedCurves_t21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_curves_1() { return static_cast<int32_t>(offsetof(SecNamedCurves_t21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A_StaticFields, ___curves_1)); }
	inline RuntimeObject* get_curves_1() const { return ___curves_1; }
	inline RuntimeObject** get_address_of_curves_1() { return &___curves_1; }
	inline void set_curves_1(RuntimeObject* value)
	{
		___curves_1 = value;
		Il2CppCodeGenWriteBarrier((&___curves_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(SecNamedCurves_t21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A_StaticFields, ___names_2)); }
	inline RuntimeObject* get_names_2() const { return ___names_2; }
	inline RuntimeObject** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(RuntimeObject* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECNAMEDCURVES_T21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A_H
#ifndef SMIMEATTRIBUTES_T6A797E12F55684FDF7459D0E6093CE62B34CAE7E_H
#define SMIMEATTRIBUTES_T6A797E12F55684FDF7459D0E6093CE62B34CAE7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeAttributes
struct  SmimeAttributes_t6A797E12F55684FDF7459D0E6093CE62B34CAE7E  : public RuntimeObject
{
public:

public:
};

struct SmimeAttributes_t6A797E12F55684FDF7459D0E6093CE62B34CAE7E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeAttributes::SmimeCapabilities
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SmimeCapabilities_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeAttributes::EncrypKeyPref
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EncrypKeyPref_1;

public:
	inline static int32_t get_offset_of_SmimeCapabilities_0() { return static_cast<int32_t>(offsetof(SmimeAttributes_t6A797E12F55684FDF7459D0E6093CE62B34CAE7E_StaticFields, ___SmimeCapabilities_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SmimeCapabilities_0() const { return ___SmimeCapabilities_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SmimeCapabilities_0() { return &___SmimeCapabilities_0; }
	inline void set_SmimeCapabilities_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SmimeCapabilities_0 = value;
		Il2CppCodeGenWriteBarrier((&___SmimeCapabilities_0), value);
	}

	inline static int32_t get_offset_of_EncrypKeyPref_1() { return static_cast<int32_t>(offsetof(SmimeAttributes_t6A797E12F55684FDF7459D0E6093CE62B34CAE7E_StaticFields, ___EncrypKeyPref_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EncrypKeyPref_1() const { return ___EncrypKeyPref_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EncrypKeyPref_1() { return &___EncrypKeyPref_1; }
	inline void set_EncrypKeyPref_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EncrypKeyPref_1 = value;
		Il2CppCodeGenWriteBarrier((&___EncrypKeyPref_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMIMEATTRIBUTES_T6A797E12F55684FDF7459D0E6093CE62B34CAE7E_H
#ifndef SMIMECAPABILITYVECTOR_T1D336FCCD77F16F27122120C87F4258A7AC96F67_H
#define SMIMECAPABILITYVECTOR_T1D336FCCD77F16F27122120C87F4258A7AC96F67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilityVector
struct  SmimeCapabilityVector_t1D336FCCD77F16F27122120C87F4258A7AC96F67  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilityVector::capabilities
	Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * ___capabilities_0;

public:
	inline static int32_t get_offset_of_capabilities_0() { return static_cast<int32_t>(offsetof(SmimeCapabilityVector_t1D336FCCD77F16F27122120C87F4258A7AC96F67, ___capabilities_0)); }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * get_capabilities_0() const { return ___capabilities_0; }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 ** get_address_of_capabilities_0() { return &___capabilities_0; }
	inline void set_capabilities_0(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * value)
	{
		___capabilities_0 = value;
		Il2CppCodeGenWriteBarrier((&___capabilities_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMIMECAPABILITYVECTOR_T1D336FCCD77F16F27122120C87F4258A7AC96F67_H
#ifndef TELETRUSTNAMEDCURVES_TA499005BAC3A263142091AB9C1809ACEEF485277_H
#define TELETRUSTNAMEDCURVES_TA499005BAC3A263142091AB9C1809ACEEF485277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves
struct  TeleTrusTNamedCurves_tA499005BAC3A263142091AB9C1809ACEEF485277  : public RuntimeObject
{
public:

public:
};

struct TeleTrusTNamedCurves_tA499005BAC3A263142091AB9C1809ACEEF485277_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves::curves
	RuntimeObject* ___curves_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves::names
	RuntimeObject* ___names_2;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(TeleTrusTNamedCurves_tA499005BAC3A263142091AB9C1809ACEEF485277_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_curves_1() { return static_cast<int32_t>(offsetof(TeleTrusTNamedCurves_tA499005BAC3A263142091AB9C1809ACEEF485277_StaticFields, ___curves_1)); }
	inline RuntimeObject* get_curves_1() const { return ___curves_1; }
	inline RuntimeObject** get_address_of_curves_1() { return &___curves_1; }
	inline void set_curves_1(RuntimeObject* value)
	{
		___curves_1 = value;
		Il2CppCodeGenWriteBarrier((&___curves_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(TeleTrusTNamedCurves_tA499005BAC3A263142091AB9C1809ACEEF485277_StaticFields, ___names_2)); }
	inline RuntimeObject* get_names_2() const { return ___names_2; }
	inline RuntimeObject** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(RuntimeObject* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELETRUSTNAMEDCURVES_TA499005BAC3A263142091AB9C1809ACEEF485277_H
#ifndef TELETRUSTOBJECTIDENTIFIERS_TA82CC0DFAECBEB246464026892FDBC42DA792599_H
#define TELETRUSTOBJECTIDENTIFIERS_TA82CC0DFAECBEB246464026892FDBC42DA792599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers
struct  TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599  : public RuntimeObject
{
public:

public:
};

struct TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::TeleTrusTAlgorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___TeleTrusTAlgorithm_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RipeMD160
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RipeMD160_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RipeMD128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RipeMD128_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RipeMD256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RipeMD256_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::TeleTrusTRsaSignatureAlgorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___TeleTrusTRsaSignatureAlgorithm_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RsaSignatureWithRipeMD160
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RsaSignatureWithRipeMD160_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RsaSignatureWithRipeMD128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RsaSignatureWithRipeMD128_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RsaSignatureWithRipeMD256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RsaSignatureWithRipeMD256_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::ECSign
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ECSign_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::ECSignWithSha1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ECSignWithSha1_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::ECSignWithRipeMD160
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ECSignWithRipeMD160_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::EccBrainpool
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EccBrainpool_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::EllipticCurve
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EllipticCurve_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::VersionOne
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___VersionOne_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP160R1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP160R1_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP160T1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP160T1_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP192R1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP192R1_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP192T1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP192T1_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP224R1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP224R1_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP224T1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP224T1_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP256R1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP256R1_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP256T1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP256T1_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP320R1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP320R1_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP320T1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP320T1_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP384R1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP384R1_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP384T1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP384T1_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP512R1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP512R1_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP512T1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BrainpoolP512T1_27;

public:
	inline static int32_t get_offset_of_TeleTrusTAlgorithm_0() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___TeleTrusTAlgorithm_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_TeleTrusTAlgorithm_0() const { return ___TeleTrusTAlgorithm_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_TeleTrusTAlgorithm_0() { return &___TeleTrusTAlgorithm_0; }
	inline void set_TeleTrusTAlgorithm_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___TeleTrusTAlgorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___TeleTrusTAlgorithm_0), value);
	}

	inline static int32_t get_offset_of_RipeMD160_1() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___RipeMD160_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RipeMD160_1() const { return ___RipeMD160_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RipeMD160_1() { return &___RipeMD160_1; }
	inline void set_RipeMD160_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RipeMD160_1 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD160_1), value);
	}

	inline static int32_t get_offset_of_RipeMD128_2() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___RipeMD128_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RipeMD128_2() const { return ___RipeMD128_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RipeMD128_2() { return &___RipeMD128_2; }
	inline void set_RipeMD128_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RipeMD128_2 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD128_2), value);
	}

	inline static int32_t get_offset_of_RipeMD256_3() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___RipeMD256_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RipeMD256_3() const { return ___RipeMD256_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RipeMD256_3() { return &___RipeMD256_3; }
	inline void set_RipeMD256_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RipeMD256_3 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD256_3), value);
	}

	inline static int32_t get_offset_of_TeleTrusTRsaSignatureAlgorithm_4() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___TeleTrusTRsaSignatureAlgorithm_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_TeleTrusTRsaSignatureAlgorithm_4() const { return ___TeleTrusTRsaSignatureAlgorithm_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_TeleTrusTRsaSignatureAlgorithm_4() { return &___TeleTrusTRsaSignatureAlgorithm_4; }
	inline void set_TeleTrusTRsaSignatureAlgorithm_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___TeleTrusTRsaSignatureAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___TeleTrusTRsaSignatureAlgorithm_4), value);
	}

	inline static int32_t get_offset_of_RsaSignatureWithRipeMD160_5() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___RsaSignatureWithRipeMD160_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RsaSignatureWithRipeMD160_5() const { return ___RsaSignatureWithRipeMD160_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RsaSignatureWithRipeMD160_5() { return &___RsaSignatureWithRipeMD160_5; }
	inline void set_RsaSignatureWithRipeMD160_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RsaSignatureWithRipeMD160_5 = value;
		Il2CppCodeGenWriteBarrier((&___RsaSignatureWithRipeMD160_5), value);
	}

	inline static int32_t get_offset_of_RsaSignatureWithRipeMD128_6() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___RsaSignatureWithRipeMD128_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RsaSignatureWithRipeMD128_6() const { return ___RsaSignatureWithRipeMD128_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RsaSignatureWithRipeMD128_6() { return &___RsaSignatureWithRipeMD128_6; }
	inline void set_RsaSignatureWithRipeMD128_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RsaSignatureWithRipeMD128_6 = value;
		Il2CppCodeGenWriteBarrier((&___RsaSignatureWithRipeMD128_6), value);
	}

	inline static int32_t get_offset_of_RsaSignatureWithRipeMD256_7() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___RsaSignatureWithRipeMD256_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RsaSignatureWithRipeMD256_7() const { return ___RsaSignatureWithRipeMD256_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RsaSignatureWithRipeMD256_7() { return &___RsaSignatureWithRipeMD256_7; }
	inline void set_RsaSignatureWithRipeMD256_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RsaSignatureWithRipeMD256_7 = value;
		Il2CppCodeGenWriteBarrier((&___RsaSignatureWithRipeMD256_7), value);
	}

	inline static int32_t get_offset_of_ECSign_8() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___ECSign_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ECSign_8() const { return ___ECSign_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ECSign_8() { return &___ECSign_8; }
	inline void set_ECSign_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ECSign_8 = value;
		Il2CppCodeGenWriteBarrier((&___ECSign_8), value);
	}

	inline static int32_t get_offset_of_ECSignWithSha1_9() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___ECSignWithSha1_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ECSignWithSha1_9() const { return ___ECSignWithSha1_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ECSignWithSha1_9() { return &___ECSignWithSha1_9; }
	inline void set_ECSignWithSha1_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ECSignWithSha1_9 = value;
		Il2CppCodeGenWriteBarrier((&___ECSignWithSha1_9), value);
	}

	inline static int32_t get_offset_of_ECSignWithRipeMD160_10() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___ECSignWithRipeMD160_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ECSignWithRipeMD160_10() const { return ___ECSignWithRipeMD160_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ECSignWithRipeMD160_10() { return &___ECSignWithRipeMD160_10; }
	inline void set_ECSignWithRipeMD160_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ECSignWithRipeMD160_10 = value;
		Il2CppCodeGenWriteBarrier((&___ECSignWithRipeMD160_10), value);
	}

	inline static int32_t get_offset_of_EccBrainpool_11() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___EccBrainpool_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EccBrainpool_11() const { return ___EccBrainpool_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EccBrainpool_11() { return &___EccBrainpool_11; }
	inline void set_EccBrainpool_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EccBrainpool_11 = value;
		Il2CppCodeGenWriteBarrier((&___EccBrainpool_11), value);
	}

	inline static int32_t get_offset_of_EllipticCurve_12() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___EllipticCurve_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EllipticCurve_12() const { return ___EllipticCurve_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EllipticCurve_12() { return &___EllipticCurve_12; }
	inline void set_EllipticCurve_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EllipticCurve_12 = value;
		Il2CppCodeGenWriteBarrier((&___EllipticCurve_12), value);
	}

	inline static int32_t get_offset_of_VersionOne_13() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___VersionOne_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_VersionOne_13() const { return ___VersionOne_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_VersionOne_13() { return &___VersionOne_13; }
	inline void set_VersionOne_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___VersionOne_13 = value;
		Il2CppCodeGenWriteBarrier((&___VersionOne_13), value);
	}

	inline static int32_t get_offset_of_BrainpoolP160R1_14() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP160R1_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP160R1_14() const { return ___BrainpoolP160R1_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP160R1_14() { return &___BrainpoolP160R1_14; }
	inline void set_BrainpoolP160R1_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP160R1_14 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP160R1_14), value);
	}

	inline static int32_t get_offset_of_BrainpoolP160T1_15() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP160T1_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP160T1_15() const { return ___BrainpoolP160T1_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP160T1_15() { return &___BrainpoolP160T1_15; }
	inline void set_BrainpoolP160T1_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP160T1_15 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP160T1_15), value);
	}

	inline static int32_t get_offset_of_BrainpoolP192R1_16() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP192R1_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP192R1_16() const { return ___BrainpoolP192R1_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP192R1_16() { return &___BrainpoolP192R1_16; }
	inline void set_BrainpoolP192R1_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP192R1_16 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP192R1_16), value);
	}

	inline static int32_t get_offset_of_BrainpoolP192T1_17() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP192T1_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP192T1_17() const { return ___BrainpoolP192T1_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP192T1_17() { return &___BrainpoolP192T1_17; }
	inline void set_BrainpoolP192T1_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP192T1_17 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP192T1_17), value);
	}

	inline static int32_t get_offset_of_BrainpoolP224R1_18() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP224R1_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP224R1_18() const { return ___BrainpoolP224R1_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP224R1_18() { return &___BrainpoolP224R1_18; }
	inline void set_BrainpoolP224R1_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP224R1_18 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP224R1_18), value);
	}

	inline static int32_t get_offset_of_BrainpoolP224T1_19() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP224T1_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP224T1_19() const { return ___BrainpoolP224T1_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP224T1_19() { return &___BrainpoolP224T1_19; }
	inline void set_BrainpoolP224T1_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP224T1_19 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP224T1_19), value);
	}

	inline static int32_t get_offset_of_BrainpoolP256R1_20() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP256R1_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP256R1_20() const { return ___BrainpoolP256R1_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP256R1_20() { return &___BrainpoolP256R1_20; }
	inline void set_BrainpoolP256R1_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP256R1_20 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP256R1_20), value);
	}

	inline static int32_t get_offset_of_BrainpoolP256T1_21() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP256T1_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP256T1_21() const { return ___BrainpoolP256T1_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP256T1_21() { return &___BrainpoolP256T1_21; }
	inline void set_BrainpoolP256T1_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP256T1_21 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP256T1_21), value);
	}

	inline static int32_t get_offset_of_BrainpoolP320R1_22() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP320R1_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP320R1_22() const { return ___BrainpoolP320R1_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP320R1_22() { return &___BrainpoolP320R1_22; }
	inline void set_BrainpoolP320R1_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP320R1_22 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP320R1_22), value);
	}

	inline static int32_t get_offset_of_BrainpoolP320T1_23() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP320T1_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP320T1_23() const { return ___BrainpoolP320T1_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP320T1_23() { return &___BrainpoolP320T1_23; }
	inline void set_BrainpoolP320T1_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP320T1_23 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP320T1_23), value);
	}

	inline static int32_t get_offset_of_BrainpoolP384R1_24() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP384R1_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP384R1_24() const { return ___BrainpoolP384R1_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP384R1_24() { return &___BrainpoolP384R1_24; }
	inline void set_BrainpoolP384R1_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP384R1_24 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP384R1_24), value);
	}

	inline static int32_t get_offset_of_BrainpoolP384T1_25() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP384T1_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP384T1_25() const { return ___BrainpoolP384T1_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP384T1_25() { return &___BrainpoolP384T1_25; }
	inline void set_BrainpoolP384T1_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP384T1_25 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP384T1_25), value);
	}

	inline static int32_t get_offset_of_BrainpoolP512R1_26() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP512R1_26)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP512R1_26() const { return ___BrainpoolP512R1_26; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP512R1_26() { return &___BrainpoolP512R1_26; }
	inline void set_BrainpoolP512R1_26(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP512R1_26 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP512R1_26), value);
	}

	inline static int32_t get_offset_of_BrainpoolP512T1_27() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields, ___BrainpoolP512T1_27)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BrainpoolP512T1_27() const { return ___BrainpoolP512T1_27; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BrainpoolP512T1_27() { return &___BrainpoolP512T1_27; }
	inline void set_BrainpoolP512T1_27(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BrainpoolP512T1_27 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP512T1_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELETRUSTOBJECTIDENTIFIERS_TA82CC0DFAECBEB246464026892FDBC42DA792599_H
#ifndef UAOBJECTIDENTIFIERS_TC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_H
#define UAOBJECTIDENTIFIERS_TC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers
struct  UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74  : public RuntimeObject
{
public:

public:
};

struct UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::UaOid
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___UaOid_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu4145le
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu4145le_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu4145be
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu4145be_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7564digest_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7564digest_256_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7564digest_384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7564digest_384_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7564digest_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7564digest_512_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7564mac_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7564mac_256_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7564mac_384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7564mac_384_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7564mac_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7564mac_512_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ecb_128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ecb_128_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ecb_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ecb_256_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ecb_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ecb_512_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ctr_128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ctr_128_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ctr_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ctr_256_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ctr_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ctr_512_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624cfb_128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624cfb_128_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624cfb_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624cfb_256_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624cfb_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624cfb_512_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624cmac_128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624cmac_128_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624cmac_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624cmac_256_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624cmac_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624cmac_512_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624cbc_128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624cbc_128_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624cbc_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624cbc_256_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624cbc_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624cbc_512_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ofb_128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ofb_128_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ofb_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ofb_256_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ofb_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ofb_512_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624gmac_128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624gmac_128_27;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624gmac_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624gmac_256_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624gmac_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624gmac_512_29;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ccm_128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ccm_128_30;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ccm_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ccm_256_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624ccm_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624ccm_512_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624xts_128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624xts_128_33;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624xts_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624xts_256_34;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624xts_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624xts_512_35;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624kw_128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624kw_128_36;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624kw_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624kw_256_37;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.UA.UAObjectIdentifiers::dstu7624kw_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___dstu7624kw_512_38;

public:
	inline static int32_t get_offset_of_UaOid_0() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___UaOid_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_UaOid_0() const { return ___UaOid_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_UaOid_0() { return &___UaOid_0; }
	inline void set_UaOid_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___UaOid_0 = value;
		Il2CppCodeGenWriteBarrier((&___UaOid_0), value);
	}

	inline static int32_t get_offset_of_dstu4145le_1() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu4145le_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu4145le_1() const { return ___dstu4145le_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu4145le_1() { return &___dstu4145le_1; }
	inline void set_dstu4145le_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu4145le_1 = value;
		Il2CppCodeGenWriteBarrier((&___dstu4145le_1), value);
	}

	inline static int32_t get_offset_of_dstu4145be_2() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu4145be_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu4145be_2() const { return ___dstu4145be_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu4145be_2() { return &___dstu4145be_2; }
	inline void set_dstu4145be_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu4145be_2 = value;
		Il2CppCodeGenWriteBarrier((&___dstu4145be_2), value);
	}

	inline static int32_t get_offset_of_dstu7564digest_256_3() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7564digest_256_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7564digest_256_3() const { return ___dstu7564digest_256_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7564digest_256_3() { return &___dstu7564digest_256_3; }
	inline void set_dstu7564digest_256_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7564digest_256_3 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7564digest_256_3), value);
	}

	inline static int32_t get_offset_of_dstu7564digest_384_4() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7564digest_384_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7564digest_384_4() const { return ___dstu7564digest_384_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7564digest_384_4() { return &___dstu7564digest_384_4; }
	inline void set_dstu7564digest_384_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7564digest_384_4 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7564digest_384_4), value);
	}

	inline static int32_t get_offset_of_dstu7564digest_512_5() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7564digest_512_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7564digest_512_5() const { return ___dstu7564digest_512_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7564digest_512_5() { return &___dstu7564digest_512_5; }
	inline void set_dstu7564digest_512_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7564digest_512_5 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7564digest_512_5), value);
	}

	inline static int32_t get_offset_of_dstu7564mac_256_6() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7564mac_256_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7564mac_256_6() const { return ___dstu7564mac_256_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7564mac_256_6() { return &___dstu7564mac_256_6; }
	inline void set_dstu7564mac_256_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7564mac_256_6 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7564mac_256_6), value);
	}

	inline static int32_t get_offset_of_dstu7564mac_384_7() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7564mac_384_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7564mac_384_7() const { return ___dstu7564mac_384_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7564mac_384_7() { return &___dstu7564mac_384_7; }
	inline void set_dstu7564mac_384_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7564mac_384_7 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7564mac_384_7), value);
	}

	inline static int32_t get_offset_of_dstu7564mac_512_8() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7564mac_512_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7564mac_512_8() const { return ___dstu7564mac_512_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7564mac_512_8() { return &___dstu7564mac_512_8; }
	inline void set_dstu7564mac_512_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7564mac_512_8 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7564mac_512_8), value);
	}

	inline static int32_t get_offset_of_dstu7624ecb_128_9() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ecb_128_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ecb_128_9() const { return ___dstu7624ecb_128_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ecb_128_9() { return &___dstu7624ecb_128_9; }
	inline void set_dstu7624ecb_128_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ecb_128_9 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ecb_128_9), value);
	}

	inline static int32_t get_offset_of_dstu7624ecb_256_10() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ecb_256_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ecb_256_10() const { return ___dstu7624ecb_256_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ecb_256_10() { return &___dstu7624ecb_256_10; }
	inline void set_dstu7624ecb_256_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ecb_256_10 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ecb_256_10), value);
	}

	inline static int32_t get_offset_of_dstu7624ecb_512_11() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ecb_512_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ecb_512_11() const { return ___dstu7624ecb_512_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ecb_512_11() { return &___dstu7624ecb_512_11; }
	inline void set_dstu7624ecb_512_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ecb_512_11 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ecb_512_11), value);
	}

	inline static int32_t get_offset_of_dstu7624ctr_128_12() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ctr_128_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ctr_128_12() const { return ___dstu7624ctr_128_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ctr_128_12() { return &___dstu7624ctr_128_12; }
	inline void set_dstu7624ctr_128_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ctr_128_12 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ctr_128_12), value);
	}

	inline static int32_t get_offset_of_dstu7624ctr_256_13() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ctr_256_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ctr_256_13() const { return ___dstu7624ctr_256_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ctr_256_13() { return &___dstu7624ctr_256_13; }
	inline void set_dstu7624ctr_256_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ctr_256_13 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ctr_256_13), value);
	}

	inline static int32_t get_offset_of_dstu7624ctr_512_14() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ctr_512_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ctr_512_14() const { return ___dstu7624ctr_512_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ctr_512_14() { return &___dstu7624ctr_512_14; }
	inline void set_dstu7624ctr_512_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ctr_512_14 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ctr_512_14), value);
	}

	inline static int32_t get_offset_of_dstu7624cfb_128_15() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624cfb_128_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624cfb_128_15() const { return ___dstu7624cfb_128_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624cfb_128_15() { return &___dstu7624cfb_128_15; }
	inline void set_dstu7624cfb_128_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624cfb_128_15 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624cfb_128_15), value);
	}

	inline static int32_t get_offset_of_dstu7624cfb_256_16() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624cfb_256_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624cfb_256_16() const { return ___dstu7624cfb_256_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624cfb_256_16() { return &___dstu7624cfb_256_16; }
	inline void set_dstu7624cfb_256_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624cfb_256_16 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624cfb_256_16), value);
	}

	inline static int32_t get_offset_of_dstu7624cfb_512_17() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624cfb_512_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624cfb_512_17() const { return ___dstu7624cfb_512_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624cfb_512_17() { return &___dstu7624cfb_512_17; }
	inline void set_dstu7624cfb_512_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624cfb_512_17 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624cfb_512_17), value);
	}

	inline static int32_t get_offset_of_dstu7624cmac_128_18() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624cmac_128_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624cmac_128_18() const { return ___dstu7624cmac_128_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624cmac_128_18() { return &___dstu7624cmac_128_18; }
	inline void set_dstu7624cmac_128_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624cmac_128_18 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624cmac_128_18), value);
	}

	inline static int32_t get_offset_of_dstu7624cmac_256_19() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624cmac_256_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624cmac_256_19() const { return ___dstu7624cmac_256_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624cmac_256_19() { return &___dstu7624cmac_256_19; }
	inline void set_dstu7624cmac_256_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624cmac_256_19 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624cmac_256_19), value);
	}

	inline static int32_t get_offset_of_dstu7624cmac_512_20() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624cmac_512_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624cmac_512_20() const { return ___dstu7624cmac_512_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624cmac_512_20() { return &___dstu7624cmac_512_20; }
	inline void set_dstu7624cmac_512_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624cmac_512_20 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624cmac_512_20), value);
	}

	inline static int32_t get_offset_of_dstu7624cbc_128_21() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624cbc_128_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624cbc_128_21() const { return ___dstu7624cbc_128_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624cbc_128_21() { return &___dstu7624cbc_128_21; }
	inline void set_dstu7624cbc_128_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624cbc_128_21 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624cbc_128_21), value);
	}

	inline static int32_t get_offset_of_dstu7624cbc_256_22() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624cbc_256_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624cbc_256_22() const { return ___dstu7624cbc_256_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624cbc_256_22() { return &___dstu7624cbc_256_22; }
	inline void set_dstu7624cbc_256_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624cbc_256_22 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624cbc_256_22), value);
	}

	inline static int32_t get_offset_of_dstu7624cbc_512_23() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624cbc_512_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624cbc_512_23() const { return ___dstu7624cbc_512_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624cbc_512_23() { return &___dstu7624cbc_512_23; }
	inline void set_dstu7624cbc_512_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624cbc_512_23 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624cbc_512_23), value);
	}

	inline static int32_t get_offset_of_dstu7624ofb_128_24() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ofb_128_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ofb_128_24() const { return ___dstu7624ofb_128_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ofb_128_24() { return &___dstu7624ofb_128_24; }
	inline void set_dstu7624ofb_128_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ofb_128_24 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ofb_128_24), value);
	}

	inline static int32_t get_offset_of_dstu7624ofb_256_25() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ofb_256_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ofb_256_25() const { return ___dstu7624ofb_256_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ofb_256_25() { return &___dstu7624ofb_256_25; }
	inline void set_dstu7624ofb_256_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ofb_256_25 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ofb_256_25), value);
	}

	inline static int32_t get_offset_of_dstu7624ofb_512_26() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ofb_512_26)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ofb_512_26() const { return ___dstu7624ofb_512_26; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ofb_512_26() { return &___dstu7624ofb_512_26; }
	inline void set_dstu7624ofb_512_26(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ofb_512_26 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ofb_512_26), value);
	}

	inline static int32_t get_offset_of_dstu7624gmac_128_27() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624gmac_128_27)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624gmac_128_27() const { return ___dstu7624gmac_128_27; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624gmac_128_27() { return &___dstu7624gmac_128_27; }
	inline void set_dstu7624gmac_128_27(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624gmac_128_27 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624gmac_128_27), value);
	}

	inline static int32_t get_offset_of_dstu7624gmac_256_28() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624gmac_256_28)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624gmac_256_28() const { return ___dstu7624gmac_256_28; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624gmac_256_28() { return &___dstu7624gmac_256_28; }
	inline void set_dstu7624gmac_256_28(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624gmac_256_28 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624gmac_256_28), value);
	}

	inline static int32_t get_offset_of_dstu7624gmac_512_29() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624gmac_512_29)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624gmac_512_29() const { return ___dstu7624gmac_512_29; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624gmac_512_29() { return &___dstu7624gmac_512_29; }
	inline void set_dstu7624gmac_512_29(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624gmac_512_29 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624gmac_512_29), value);
	}

	inline static int32_t get_offset_of_dstu7624ccm_128_30() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ccm_128_30)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ccm_128_30() const { return ___dstu7624ccm_128_30; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ccm_128_30() { return &___dstu7624ccm_128_30; }
	inline void set_dstu7624ccm_128_30(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ccm_128_30 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ccm_128_30), value);
	}

	inline static int32_t get_offset_of_dstu7624ccm_256_31() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ccm_256_31)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ccm_256_31() const { return ___dstu7624ccm_256_31; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ccm_256_31() { return &___dstu7624ccm_256_31; }
	inline void set_dstu7624ccm_256_31(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ccm_256_31 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ccm_256_31), value);
	}

	inline static int32_t get_offset_of_dstu7624ccm_512_32() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624ccm_512_32)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624ccm_512_32() const { return ___dstu7624ccm_512_32; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624ccm_512_32() { return &___dstu7624ccm_512_32; }
	inline void set_dstu7624ccm_512_32(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624ccm_512_32 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624ccm_512_32), value);
	}

	inline static int32_t get_offset_of_dstu7624xts_128_33() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624xts_128_33)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624xts_128_33() const { return ___dstu7624xts_128_33; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624xts_128_33() { return &___dstu7624xts_128_33; }
	inline void set_dstu7624xts_128_33(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624xts_128_33 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624xts_128_33), value);
	}

	inline static int32_t get_offset_of_dstu7624xts_256_34() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624xts_256_34)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624xts_256_34() const { return ___dstu7624xts_256_34; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624xts_256_34() { return &___dstu7624xts_256_34; }
	inline void set_dstu7624xts_256_34(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624xts_256_34 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624xts_256_34), value);
	}

	inline static int32_t get_offset_of_dstu7624xts_512_35() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624xts_512_35)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624xts_512_35() const { return ___dstu7624xts_512_35; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624xts_512_35() { return &___dstu7624xts_512_35; }
	inline void set_dstu7624xts_512_35(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624xts_512_35 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624xts_512_35), value);
	}

	inline static int32_t get_offset_of_dstu7624kw_128_36() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624kw_128_36)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624kw_128_36() const { return ___dstu7624kw_128_36; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624kw_128_36() { return &___dstu7624kw_128_36; }
	inline void set_dstu7624kw_128_36(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624kw_128_36 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624kw_128_36), value);
	}

	inline static int32_t get_offset_of_dstu7624kw_256_37() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624kw_256_37)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624kw_256_37() const { return ___dstu7624kw_256_37; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624kw_256_37() { return &___dstu7624kw_256_37; }
	inline void set_dstu7624kw_256_37(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624kw_256_37 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624kw_256_37), value);
	}

	inline static int32_t get_offset_of_dstu7624kw_512_38() { return static_cast<int32_t>(offsetof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields, ___dstu7624kw_512_38)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_dstu7624kw_512_38() const { return ___dstu7624kw_512_38; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_dstu7624kw_512_38() { return &___dstu7624kw_512_38; }
	inline void set_dstu7624kw_512_38(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___dstu7624kw_512_38 = value;
		Il2CppCodeGenWriteBarrier((&___dstu7624kw_512_38), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UAOBJECTIDENTIFIERS_TC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_H
#ifndef ASN1DUMP_TFCA8D611C62D064BC367E124B616021CC3AD6CEA_H
#define ASN1DUMP_TFCA8D611C62D064BC367E124B616021CC3AD6CEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Utilities.Asn1Dump
struct  Asn1Dump_tFCA8D611C62D064BC367E124B616021CC3AD6CEA  : public RuntimeObject
{
public:

public:
};

struct Asn1Dump_tFCA8D611C62D064BC367E124B616021CC3AD6CEA_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Utilities.Asn1Dump::NewLine
	String_t* ___NewLine_0;

public:
	inline static int32_t get_offset_of_NewLine_0() { return static_cast<int32_t>(offsetof(Asn1Dump_tFCA8D611C62D064BC367E124B616021CC3AD6CEA_StaticFields, ___NewLine_0)); }
	inline String_t* get_NewLine_0() const { return ___NewLine_0; }
	inline String_t** get_address_of_NewLine_0() { return &___NewLine_0; }
	inline void set_NewLine_0(String_t* value)
	{
		___NewLine_0 = value;
		Il2CppCodeGenWriteBarrier((&___NewLine_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1DUMP_TFCA8D611C62D064BC367E124B616021CC3AD6CEA_H
#ifndef IETFUTILITIES_T04748B3229C3A70F5662483B0121EEE3DF709499_H
#define IETFUTILITIES_T04748B3229C3A70F5662483B0121EEE3DF709499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.Style.IetfUtilities
struct  IetfUtilities_t04748B3229C3A70F5662483B0121EEE3DF709499  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IETFUTILITIES_T04748B3229C3A70F5662483B0121EEE3DF709499_H
#ifndef ETSIQCOBJECTIDENTIFIERS_T2272D860F101FE2235B85C342C710FA512AC6019_H
#define ETSIQCOBJECTIDENTIFIERS_T2272D860F101FE2235B85C342C710FA512AC6019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.EtsiQCObjectIdentifiers
struct  EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019  : public RuntimeObject
{
public:

public:
};

struct EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.EtsiQCObjectIdentifiers::IdEtsiQcs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdEtsiQcs_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.EtsiQCObjectIdentifiers::IdEtsiQcsQcCompliance
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdEtsiQcsQcCompliance_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.EtsiQCObjectIdentifiers::IdEtsiQcsLimitValue
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdEtsiQcsLimitValue_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.EtsiQCObjectIdentifiers::IdEtsiQcsRetentionPeriod
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdEtsiQcsRetentionPeriod_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.EtsiQCObjectIdentifiers::IdEtsiQcsQcSscd
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdEtsiQcsQcSscd_4;

public:
	inline static int32_t get_offset_of_IdEtsiQcs_0() { return static_cast<int32_t>(offsetof(EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields, ___IdEtsiQcs_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdEtsiQcs_0() const { return ___IdEtsiQcs_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdEtsiQcs_0() { return &___IdEtsiQcs_0; }
	inline void set_IdEtsiQcs_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdEtsiQcs_0 = value;
		Il2CppCodeGenWriteBarrier((&___IdEtsiQcs_0), value);
	}

	inline static int32_t get_offset_of_IdEtsiQcsQcCompliance_1() { return static_cast<int32_t>(offsetof(EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields, ___IdEtsiQcsQcCompliance_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdEtsiQcsQcCompliance_1() const { return ___IdEtsiQcsQcCompliance_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdEtsiQcsQcCompliance_1() { return &___IdEtsiQcsQcCompliance_1; }
	inline void set_IdEtsiQcsQcCompliance_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdEtsiQcsQcCompliance_1 = value;
		Il2CppCodeGenWriteBarrier((&___IdEtsiQcsQcCompliance_1), value);
	}

	inline static int32_t get_offset_of_IdEtsiQcsLimitValue_2() { return static_cast<int32_t>(offsetof(EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields, ___IdEtsiQcsLimitValue_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdEtsiQcsLimitValue_2() const { return ___IdEtsiQcsLimitValue_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdEtsiQcsLimitValue_2() { return &___IdEtsiQcsLimitValue_2; }
	inline void set_IdEtsiQcsLimitValue_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdEtsiQcsLimitValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___IdEtsiQcsLimitValue_2), value);
	}

	inline static int32_t get_offset_of_IdEtsiQcsRetentionPeriod_3() { return static_cast<int32_t>(offsetof(EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields, ___IdEtsiQcsRetentionPeriod_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdEtsiQcsRetentionPeriod_3() const { return ___IdEtsiQcsRetentionPeriod_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdEtsiQcsRetentionPeriod_3() { return &___IdEtsiQcsRetentionPeriod_3; }
	inline void set_IdEtsiQcsRetentionPeriod_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdEtsiQcsRetentionPeriod_3 = value;
		Il2CppCodeGenWriteBarrier((&___IdEtsiQcsRetentionPeriod_3), value);
	}

	inline static int32_t get_offset_of_IdEtsiQcsQcSscd_4() { return static_cast<int32_t>(offsetof(EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields, ___IdEtsiQcsQcSscd_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdEtsiQcsQcSscd_4() const { return ___IdEtsiQcsQcSscd_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdEtsiQcsQcSscd_4() { return &___IdEtsiQcsQcSscd_4; }
	inline void set_IdEtsiQcsQcSscd_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdEtsiQcsQcSscd_4 = value;
		Il2CppCodeGenWriteBarrier((&___IdEtsiQcsQcSscd_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETSIQCOBJECTIDENTIFIERS_T2272D860F101FE2235B85C342C710FA512AC6019_H
#ifndef RFC3739QCOBJECTIDENTIFIERS_T6F603C5186BA153D221E0FCDD504D37B6DDB0682_H
#define RFC3739QCOBJECTIDENTIFIERS_T6F603C5186BA153D221E0FCDD504D37B6DDB0682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.Rfc3739QCObjectIdentifiers
struct  Rfc3739QCObjectIdentifiers_t6F603C5186BA153D221E0FCDD504D37B6DDB0682  : public RuntimeObject
{
public:

public:
};

struct Rfc3739QCObjectIdentifiers_t6F603C5186BA153D221E0FCDD504D37B6DDB0682_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.Rfc3739QCObjectIdentifiers::IdQcs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdQcs_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.Rfc3739QCObjectIdentifiers::IdQcsPkixQCSyntaxV1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdQcsPkixQCSyntaxV1_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.Rfc3739QCObjectIdentifiers::IdQcsPkixQCSyntaxV2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdQcsPkixQCSyntaxV2_2;

public:
	inline static int32_t get_offset_of_IdQcs_0() { return static_cast<int32_t>(offsetof(Rfc3739QCObjectIdentifiers_t6F603C5186BA153D221E0FCDD504D37B6DDB0682_StaticFields, ___IdQcs_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdQcs_0() const { return ___IdQcs_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdQcs_0() { return &___IdQcs_0; }
	inline void set_IdQcs_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdQcs_0 = value;
		Il2CppCodeGenWriteBarrier((&___IdQcs_0), value);
	}

	inline static int32_t get_offset_of_IdQcsPkixQCSyntaxV1_1() { return static_cast<int32_t>(offsetof(Rfc3739QCObjectIdentifiers_t6F603C5186BA153D221E0FCDD504D37B6DDB0682_StaticFields, ___IdQcsPkixQCSyntaxV1_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdQcsPkixQCSyntaxV1_1() const { return ___IdQcsPkixQCSyntaxV1_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdQcsPkixQCSyntaxV1_1() { return &___IdQcsPkixQCSyntaxV1_1; }
	inline void set_IdQcsPkixQCSyntaxV1_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdQcsPkixQCSyntaxV1_1 = value;
		Il2CppCodeGenWriteBarrier((&___IdQcsPkixQCSyntaxV1_1), value);
	}

	inline static int32_t get_offset_of_IdQcsPkixQCSyntaxV2_2() { return static_cast<int32_t>(offsetof(Rfc3739QCObjectIdentifiers_t6F603C5186BA153D221E0FCDD504D37B6DDB0682_StaticFields, ___IdQcsPkixQCSyntaxV2_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdQcsPkixQCSyntaxV2_2() const { return ___IdQcsPkixQCSyntaxV2_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdQcsPkixQCSyntaxV2_2() { return &___IdQcsPkixQCSyntaxV2_2; }
	inline void set_IdQcsPkixQCSyntaxV2_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdQcsPkixQCSyntaxV2_2 = value;
		Il2CppCodeGenWriteBarrier((&___IdQcsPkixQCSyntaxV2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RFC3739QCOBJECTIDENTIFIERS_T6F603C5186BA153D221E0FCDD504D37B6DDB0682_H
#ifndef SIGIOBJECTIDENTIFIERS_TD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_H
#define SIGIOBJECTIDENTIFIERS_TD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.SigIObjectIdentifiers
struct  SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259  : public RuntimeObject
{
public:

public:
};

struct SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.SigIObjectIdentifiers::IdSigI
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSigI_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.SigIObjectIdentifiers::IdSigIKP
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSigIKP_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.SigIObjectIdentifiers::IdSigICP
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSigICP_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.SigIObjectIdentifiers::IdSigION
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSigION_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.SigIObjectIdentifiers::IdSigIKPDirectoryService
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSigIKPDirectoryService_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.SigIObjectIdentifiers::IdSigIONPersonalData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSigIONPersonalData_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.SigIObjectIdentifiers::IdSigICPSigConform
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSigICPSigConform_6;

public:
	inline static int32_t get_offset_of_IdSigI_0() { return static_cast<int32_t>(offsetof(SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields, ___IdSigI_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSigI_0() const { return ___IdSigI_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSigI_0() { return &___IdSigI_0; }
	inline void set_IdSigI_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSigI_0 = value;
		Il2CppCodeGenWriteBarrier((&___IdSigI_0), value);
	}

	inline static int32_t get_offset_of_IdSigIKP_1() { return static_cast<int32_t>(offsetof(SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields, ___IdSigIKP_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSigIKP_1() const { return ___IdSigIKP_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSigIKP_1() { return &___IdSigIKP_1; }
	inline void set_IdSigIKP_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSigIKP_1 = value;
		Il2CppCodeGenWriteBarrier((&___IdSigIKP_1), value);
	}

	inline static int32_t get_offset_of_IdSigICP_2() { return static_cast<int32_t>(offsetof(SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields, ___IdSigICP_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSigICP_2() const { return ___IdSigICP_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSigICP_2() { return &___IdSigICP_2; }
	inline void set_IdSigICP_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSigICP_2 = value;
		Il2CppCodeGenWriteBarrier((&___IdSigICP_2), value);
	}

	inline static int32_t get_offset_of_IdSigION_3() { return static_cast<int32_t>(offsetof(SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields, ___IdSigION_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSigION_3() const { return ___IdSigION_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSigION_3() { return &___IdSigION_3; }
	inline void set_IdSigION_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSigION_3 = value;
		Il2CppCodeGenWriteBarrier((&___IdSigION_3), value);
	}

	inline static int32_t get_offset_of_IdSigIKPDirectoryService_4() { return static_cast<int32_t>(offsetof(SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields, ___IdSigIKPDirectoryService_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSigIKPDirectoryService_4() const { return ___IdSigIKPDirectoryService_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSigIKPDirectoryService_4() { return &___IdSigIKPDirectoryService_4; }
	inline void set_IdSigIKPDirectoryService_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSigIKPDirectoryService_4 = value;
		Il2CppCodeGenWriteBarrier((&___IdSigIKPDirectoryService_4), value);
	}

	inline static int32_t get_offset_of_IdSigIONPersonalData_5() { return static_cast<int32_t>(offsetof(SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields, ___IdSigIONPersonalData_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSigIONPersonalData_5() const { return ___IdSigIONPersonalData_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSigIONPersonalData_5() { return &___IdSigIONPersonalData_5; }
	inline void set_IdSigIONPersonalData_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSigIONPersonalData_5 = value;
		Il2CppCodeGenWriteBarrier((&___IdSigIONPersonalData_5), value);
	}

	inline static int32_t get_offset_of_IdSigICPSigConform_6() { return static_cast<int32_t>(offsetof(SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields, ___IdSigICPSigConform_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSigICPSigConform_6() const { return ___IdSigICPSigConform_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSigICPSigConform_6() { return &___IdSigICPSigConform_6; }
	inline void set_IdSigICPSigConform_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSigICPSigConform_6 = value;
		Il2CppCodeGenWriteBarrier((&___IdSigICPSigConform_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGIOBJECTIDENTIFIERS_TD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_H
#ifndef REVOKEDCERTIFICATESENUMERATION_TA5745D9F80919FD73DE390F7A4986054A21C92CC_H
#define REVOKEDCERTIFICATESENUMERATION_TA5745D9F80919FD73DE390F7A4986054A21C92CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList_RevokedCertificatesEnumeration
struct  RevokedCertificatesEnumeration_tA5745D9F80919FD73DE390F7A4986054A21C92CC  : public RuntimeObject
{
public:
	// System.Collections.IEnumerable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList_RevokedCertificatesEnumeration::en
	RuntimeObject* ___en_0;

public:
	inline static int32_t get_offset_of_en_0() { return static_cast<int32_t>(offsetof(RevokedCertificatesEnumeration_tA5745D9F80919FD73DE390F7A4986054A21C92CC, ___en_0)); }
	inline RuntimeObject* get_en_0() const { return ___en_0; }
	inline RuntimeObject** get_address_of_en_0() { return &___en_0; }
	inline void set_en_0(RuntimeObject* value)
	{
		___en_0 = value;
		Il2CppCodeGenWriteBarrier((&___en_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVOKEDCERTIFICATESENUMERATION_TA5745D9F80919FD73DE390F7A4986054A21C92CC_H
#ifndef REVOKEDCERTIFICATESENUMERATOR_T5E2BD09913613D13773B1EFC061FE5EBCA89041A_H
#define REVOKEDCERTIFICATESENUMERATOR_T5E2BD09913613D13773B1EFC061FE5EBCA89041A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList_RevokedCertificatesEnumeration_RevokedCertificatesEnumerator
struct  RevokedCertificatesEnumerator_t5E2BD09913613D13773B1EFC061FE5EBCA89041A  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList_RevokedCertificatesEnumeration_RevokedCertificatesEnumerator::e
	RuntimeObject* ___e_0;

public:
	inline static int32_t get_offset_of_e_0() { return static_cast<int32_t>(offsetof(RevokedCertificatesEnumerator_t5E2BD09913613D13773B1EFC061FE5EBCA89041A, ___e_0)); }
	inline RuntimeObject* get_e_0() const { return ___e_0; }
	inline RuntimeObject** get_address_of_e_0() { return &___e_0; }
	inline void set_e_0(RuntimeObject* value)
	{
		___e_0 = value;
		Il2CppCodeGenWriteBarrier((&___e_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVOKEDCERTIFICATESENUMERATOR_T5E2BD09913613D13773B1EFC061FE5EBCA89041A_H
#ifndef V1TBSCERTIFICATEGENERATOR_T33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53_H
#define V1TBSCERTIFICATEGENERATOR_T33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V1TbsCertificateGenerator
struct  V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerTaggedObject BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V1TbsCertificateGenerator::version
	DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89 * ___version_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V1TbsCertificateGenerator::serialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serialNumber_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V1TbsCertificateGenerator::signature
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signature_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V1TbsCertificateGenerator::issuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___issuer_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V1TbsCertificateGenerator::startDate
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___startDate_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V1TbsCertificateGenerator::endDate
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___endDate_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V1TbsCertificateGenerator::subject
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___subject_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V1TbsCertificateGenerator::subjectPublicKeyInfo
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___subjectPublicKeyInfo_7;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53, ___version_0)); }
	inline DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89 * get_version_0() const { return ___version_0; }
	inline DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89 ** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89 * value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier((&___version_0), value);
	}

	inline static int32_t get_offset_of_serialNumber_1() { return static_cast<int32_t>(offsetof(V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53, ___serialNumber_1)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serialNumber_1() const { return ___serialNumber_1; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serialNumber_1() { return &___serialNumber_1; }
	inline void set_serialNumber_1(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serialNumber_1 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_1), value);
	}

	inline static int32_t get_offset_of_signature_2() { return static_cast<int32_t>(offsetof(V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53, ___signature_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signature_2() const { return ___signature_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signature_2() { return &___signature_2; }
	inline void set_signature_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signature_2 = value;
		Il2CppCodeGenWriteBarrier((&___signature_2), value);
	}

	inline static int32_t get_offset_of_issuer_3() { return static_cast<int32_t>(offsetof(V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53, ___issuer_3)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_issuer_3() const { return ___issuer_3; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_issuer_3() { return &___issuer_3; }
	inline void set_issuer_3(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___issuer_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_3), value);
	}

	inline static int32_t get_offset_of_startDate_4() { return static_cast<int32_t>(offsetof(V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53, ___startDate_4)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_startDate_4() const { return ___startDate_4; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_startDate_4() { return &___startDate_4; }
	inline void set_startDate_4(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___startDate_4 = value;
		Il2CppCodeGenWriteBarrier((&___startDate_4), value);
	}

	inline static int32_t get_offset_of_endDate_5() { return static_cast<int32_t>(offsetof(V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53, ___endDate_5)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_endDate_5() const { return ___endDate_5; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_endDate_5() { return &___endDate_5; }
	inline void set_endDate_5(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___endDate_5 = value;
		Il2CppCodeGenWriteBarrier((&___endDate_5), value);
	}

	inline static int32_t get_offset_of_subject_6() { return static_cast<int32_t>(offsetof(V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53, ___subject_6)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_subject_6() const { return ___subject_6; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_subject_6() { return &___subject_6; }
	inline void set_subject_6(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___subject_6 = value;
		Il2CppCodeGenWriteBarrier((&___subject_6), value);
	}

	inline static int32_t get_offset_of_subjectPublicKeyInfo_7() { return static_cast<int32_t>(offsetof(V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53, ___subjectPublicKeyInfo_7)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_subjectPublicKeyInfo_7() const { return ___subjectPublicKeyInfo_7; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_subjectPublicKeyInfo_7() { return &___subjectPublicKeyInfo_7; }
	inline void set_subjectPublicKeyInfo_7(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___subjectPublicKeyInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___subjectPublicKeyInfo_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // V1TBSCERTIFICATEGENERATOR_T33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53_H
#ifndef V2ATTRIBUTECERTIFICATEINFOGENERATOR_T3F66539768809639E7E5464B5FAAB97106042FF7_H
#define V2ATTRIBUTECERTIFICATEINFOGENERATOR_T3F66539768809639E7E5464B5FAAB97106042FF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator
struct  V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Holder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator::holder
	Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D * ___holder_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttCertIssuer BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator::issuer
	AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF * ___issuer_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator::signature
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signature_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator::serialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serialNumber_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator::attributes
	Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * ___attributes_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator::issuerUniqueID
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___issuerUniqueID_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator::startDate
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___startDate_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator::endDate
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___endDate_9;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7, ___version_0)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_0() const { return ___version_0; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier((&___version_0), value);
	}

	inline static int32_t get_offset_of_holder_1() { return static_cast<int32_t>(offsetof(V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7, ___holder_1)); }
	inline Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D * get_holder_1() const { return ___holder_1; }
	inline Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D ** get_address_of_holder_1() { return &___holder_1; }
	inline void set_holder_1(Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D * value)
	{
		___holder_1 = value;
		Il2CppCodeGenWriteBarrier((&___holder_1), value);
	}

	inline static int32_t get_offset_of_issuer_2() { return static_cast<int32_t>(offsetof(V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7, ___issuer_2)); }
	inline AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF * get_issuer_2() const { return ___issuer_2; }
	inline AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF ** get_address_of_issuer_2() { return &___issuer_2; }
	inline void set_issuer_2(AttCertIssuer_t09FEB8F36DE6DD2229E11B9F5A4B723C56947CAF * value)
	{
		___issuer_2 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_2), value);
	}

	inline static int32_t get_offset_of_signature_3() { return static_cast<int32_t>(offsetof(V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7, ___signature_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signature_3() const { return ___signature_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signature_3() { return &___signature_3; }
	inline void set_signature_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signature_3 = value;
		Il2CppCodeGenWriteBarrier((&___signature_3), value);
	}

	inline static int32_t get_offset_of_serialNumber_4() { return static_cast<int32_t>(offsetof(V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7, ___serialNumber_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serialNumber_4() const { return ___serialNumber_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serialNumber_4() { return &___serialNumber_4; }
	inline void set_serialNumber_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serialNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_4), value);
	}

	inline static int32_t get_offset_of_attributes_5() { return static_cast<int32_t>(offsetof(V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7, ___attributes_5)); }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * get_attributes_5() const { return ___attributes_5; }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 ** get_address_of_attributes_5() { return &___attributes_5; }
	inline void set_attributes_5(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * value)
	{
		___attributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_5), value);
	}

	inline static int32_t get_offset_of_issuerUniqueID_6() { return static_cast<int32_t>(offsetof(V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7, ___issuerUniqueID_6)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_issuerUniqueID_6() const { return ___issuerUniqueID_6; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_issuerUniqueID_6() { return &___issuerUniqueID_6; }
	inline void set_issuerUniqueID_6(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___issuerUniqueID_6 = value;
		Il2CppCodeGenWriteBarrier((&___issuerUniqueID_6), value);
	}

	inline static int32_t get_offset_of_extensions_7() { return static_cast<int32_t>(offsetof(V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7, ___extensions_7)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_7() const { return ___extensions_7; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_7() { return &___extensions_7; }
	inline void set_extensions_7(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_7), value);
	}

	inline static int32_t get_offset_of_startDate_8() { return static_cast<int32_t>(offsetof(V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7, ___startDate_8)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_startDate_8() const { return ___startDate_8; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_startDate_8() { return &___startDate_8; }
	inline void set_startDate_8(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___startDate_8 = value;
		Il2CppCodeGenWriteBarrier((&___startDate_8), value);
	}

	inline static int32_t get_offset_of_endDate_9() { return static_cast<int32_t>(offsetof(V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7, ___endDate_9)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_endDate_9() const { return ___endDate_9; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_endDate_9() { return &___endDate_9; }
	inline void set_endDate_9(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___endDate_9 = value;
		Il2CppCodeGenWriteBarrier((&___endDate_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // V2ATTRIBUTECERTIFICATEINFOGENERATOR_T3F66539768809639E7E5464B5FAAB97106042FF7_H
#ifndef V2TBSCERTLISTGENERATOR_T01974F84CB55935CA00B1719E3385C8F277C356D_H
#define V2TBSCERTLISTGENERATOR_T01974F84CB55935CA00B1719E3385C8F277C356D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2TbsCertListGenerator
struct  V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2TbsCertListGenerator::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2TbsCertListGenerator::signature
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signature_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2TbsCertListGenerator::issuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___issuer_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2TbsCertListGenerator::thisUpdate
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___thisUpdate_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2TbsCertListGenerator::nextUpdate
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___nextUpdate_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2TbsCertListGenerator::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_5;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2TbsCertListGenerator::crlEntries
	RuntimeObject* ___crlEntries_6;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D, ___version_0)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_0() const { return ___version_0; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier((&___version_0), value);
	}

	inline static int32_t get_offset_of_signature_1() { return static_cast<int32_t>(offsetof(V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D, ___signature_1)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signature_1() const { return ___signature_1; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signature_1() { return &___signature_1; }
	inline void set_signature_1(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signature_1 = value;
		Il2CppCodeGenWriteBarrier((&___signature_1), value);
	}

	inline static int32_t get_offset_of_issuer_2() { return static_cast<int32_t>(offsetof(V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D, ___issuer_2)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_issuer_2() const { return ___issuer_2; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_issuer_2() { return &___issuer_2; }
	inline void set_issuer_2(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___issuer_2 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_2), value);
	}

	inline static int32_t get_offset_of_thisUpdate_3() { return static_cast<int32_t>(offsetof(V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D, ___thisUpdate_3)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_thisUpdate_3() const { return ___thisUpdate_3; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_thisUpdate_3() { return &___thisUpdate_3; }
	inline void set_thisUpdate_3(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___thisUpdate_3 = value;
		Il2CppCodeGenWriteBarrier((&___thisUpdate_3), value);
	}

	inline static int32_t get_offset_of_nextUpdate_4() { return static_cast<int32_t>(offsetof(V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D, ___nextUpdate_4)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_nextUpdate_4() const { return ___nextUpdate_4; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_nextUpdate_4() { return &___nextUpdate_4; }
	inline void set_nextUpdate_4(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___nextUpdate_4 = value;
		Il2CppCodeGenWriteBarrier((&___nextUpdate_4), value);
	}

	inline static int32_t get_offset_of_extensions_5() { return static_cast<int32_t>(offsetof(V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D, ___extensions_5)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_5() const { return ___extensions_5; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_5() { return &___extensions_5; }
	inline void set_extensions_5(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_5 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_5), value);
	}

	inline static int32_t get_offset_of_crlEntries_6() { return static_cast<int32_t>(offsetof(V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D, ___crlEntries_6)); }
	inline RuntimeObject* get_crlEntries_6() const { return ___crlEntries_6; }
	inline RuntimeObject** get_address_of_crlEntries_6() { return &___crlEntries_6; }
	inline void set_crlEntries_6(RuntimeObject* value)
	{
		___crlEntries_6 = value;
		Il2CppCodeGenWriteBarrier((&___crlEntries_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // V2TBSCERTLISTGENERATOR_T01974F84CB55935CA00B1719E3385C8F277C356D_H
#ifndef V3TBSCERTIFICATEGENERATOR_TB3A4A9391AE49D47FE5A24FE16A6F14D49043B16_H
#define V3TBSCERTIFICATEGENERATOR_TB3A4A9391AE49D47FE5A24FE16A6F14D49043B16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator
struct  V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerTaggedObject BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::version
	DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89 * ___version_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::serialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serialNumber_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::signature
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signature_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::issuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___issuer_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::startDate
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___startDate_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::endDate
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___endDate_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::subject
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___subject_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::subjectPublicKeyInfo
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___subjectPublicKeyInfo_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::altNamePresentAndCritical
	bool ___altNamePresentAndCritical_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::issuerUniqueID
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___issuerUniqueID_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator::subjectUniqueID
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___subjectUniqueID_11;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___version_0)); }
	inline DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89 * get_version_0() const { return ___version_0; }
	inline DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89 ** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(DerTaggedObject_t96F301FC6AA54E94C6FF065F9FE47ABC4918FF89 * value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier((&___version_0), value);
	}

	inline static int32_t get_offset_of_serialNumber_1() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___serialNumber_1)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serialNumber_1() const { return ___serialNumber_1; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serialNumber_1() { return &___serialNumber_1; }
	inline void set_serialNumber_1(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serialNumber_1 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_1), value);
	}

	inline static int32_t get_offset_of_signature_2() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___signature_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signature_2() const { return ___signature_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signature_2() { return &___signature_2; }
	inline void set_signature_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signature_2 = value;
		Il2CppCodeGenWriteBarrier((&___signature_2), value);
	}

	inline static int32_t get_offset_of_issuer_3() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___issuer_3)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_issuer_3() const { return ___issuer_3; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_issuer_3() { return &___issuer_3; }
	inline void set_issuer_3(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___issuer_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_3), value);
	}

	inline static int32_t get_offset_of_startDate_4() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___startDate_4)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_startDate_4() const { return ___startDate_4; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_startDate_4() { return &___startDate_4; }
	inline void set_startDate_4(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___startDate_4 = value;
		Il2CppCodeGenWriteBarrier((&___startDate_4), value);
	}

	inline static int32_t get_offset_of_endDate_5() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___endDate_5)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_endDate_5() const { return ___endDate_5; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_endDate_5() { return &___endDate_5; }
	inline void set_endDate_5(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___endDate_5 = value;
		Il2CppCodeGenWriteBarrier((&___endDate_5), value);
	}

	inline static int32_t get_offset_of_subject_6() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___subject_6)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_subject_6() const { return ___subject_6; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_subject_6() { return &___subject_6; }
	inline void set_subject_6(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___subject_6 = value;
		Il2CppCodeGenWriteBarrier((&___subject_6), value);
	}

	inline static int32_t get_offset_of_subjectPublicKeyInfo_7() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___subjectPublicKeyInfo_7)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_subjectPublicKeyInfo_7() const { return ___subjectPublicKeyInfo_7; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_subjectPublicKeyInfo_7() { return &___subjectPublicKeyInfo_7; }
	inline void set_subjectPublicKeyInfo_7(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___subjectPublicKeyInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___subjectPublicKeyInfo_7), value);
	}

	inline static int32_t get_offset_of_extensions_8() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___extensions_8)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_8() const { return ___extensions_8; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_8() { return &___extensions_8; }
	inline void set_extensions_8(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_8), value);
	}

	inline static int32_t get_offset_of_altNamePresentAndCritical_9() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___altNamePresentAndCritical_9)); }
	inline bool get_altNamePresentAndCritical_9() const { return ___altNamePresentAndCritical_9; }
	inline bool* get_address_of_altNamePresentAndCritical_9() { return &___altNamePresentAndCritical_9; }
	inline void set_altNamePresentAndCritical_9(bool value)
	{
		___altNamePresentAndCritical_9 = value;
	}

	inline static int32_t get_offset_of_issuerUniqueID_10() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___issuerUniqueID_10)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_issuerUniqueID_10() const { return ___issuerUniqueID_10; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_issuerUniqueID_10() { return &___issuerUniqueID_10; }
	inline void set_issuerUniqueID_10(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___issuerUniqueID_10 = value;
		Il2CppCodeGenWriteBarrier((&___issuerUniqueID_10), value);
	}

	inline static int32_t get_offset_of_subjectUniqueID_11() { return static_cast<int32_t>(offsetof(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16, ___subjectUniqueID_11)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_subjectUniqueID_11() const { return ___subjectUniqueID_11; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_subjectUniqueID_11() { return &___subjectUniqueID_11; }
	inline void set_subjectUniqueID_11(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___subjectUniqueID_11 = value;
		Il2CppCodeGenWriteBarrier((&___subjectUniqueID_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // V3TBSCERTIFICATEGENERATOR_TB3A4A9391AE49D47FE5A24FE16A6F14D49043B16_H
#ifndef X509ATTRIBUTES_TFBFBC66BBD2E9F3AD4770593880D9384A55F567C_H
#define X509ATTRIBUTES_TFBFBC66BBD2E9F3AD4770593880D9384A55F567C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Attributes
struct  X509Attributes_tFBFBC66BBD2E9F3AD4770593880D9384A55F567C  : public RuntimeObject
{
public:

public:
};

struct X509Attributes_tFBFBC66BBD2E9F3AD4770593880D9384A55F567C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Attributes::RoleSyntax
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RoleSyntax_0;

public:
	inline static int32_t get_offset_of_RoleSyntax_0() { return static_cast<int32_t>(offsetof(X509Attributes_tFBFBC66BBD2E9F3AD4770593880D9384A55F567C_StaticFields, ___RoleSyntax_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RoleSyntax_0() const { return ___RoleSyntax_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RoleSyntax_0() { return &___RoleSyntax_0; }
	inline void set_RoleSyntax_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RoleSyntax_0 = value;
		Il2CppCodeGenWriteBarrier((&___RoleSyntax_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509ATTRIBUTES_TFBFBC66BBD2E9F3AD4770593880D9384A55F567C_H
#ifndef X509EXTENSION_TB5ACB0FEA53B7681995185A06D4AEF29DFD72486_H
#define X509EXTENSION_TB5ACB0FEA53B7681995185A06D4AEF29DFD72486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extension
struct  X509Extension_tB5ACB0FEA53B7681995185A06D4AEF29DFD72486  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extension::critical
	bool ___critical_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extension::value
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___value_1;

public:
	inline static int32_t get_offset_of_critical_0() { return static_cast<int32_t>(offsetof(X509Extension_tB5ACB0FEA53B7681995185A06D4AEF29DFD72486, ___critical_0)); }
	inline bool get_critical_0() const { return ___critical_0; }
	inline bool* get_address_of_critical_0() { return &___critical_0; }
	inline void set_critical_0(bool value)
	{
		___critical_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(X509Extension_tB5ACB0FEA53B7681995185A06D4AEF29DFD72486, ___value_1)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_value_1() const { return ___value_1; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSION_TB5ACB0FEA53B7681995185A06D4AEF29DFD72486_H
#ifndef X509EXTENSIONSGENERATOR_T788E3FE4DF38CC565DDEE0B6D23F2A611C604E33_H
#define X509EXTENSIONSGENERATOR_T788E3FE4DF38CC565DDEE0B6D23F2A611C604E33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ExtensionsGenerator
struct  X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33  : public RuntimeObject
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ExtensionsGenerator::extensions
	RuntimeObject* ___extensions_0;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ExtensionsGenerator::extOrdering
	RuntimeObject* ___extOrdering_1;

public:
	inline static int32_t get_offset_of_extensions_0() { return static_cast<int32_t>(offsetof(X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33, ___extensions_0)); }
	inline RuntimeObject* get_extensions_0() const { return ___extensions_0; }
	inline RuntimeObject** get_address_of_extensions_0() { return &___extensions_0; }
	inline void set_extensions_0(RuntimeObject* value)
	{
		___extensions_0 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_0), value);
	}

	inline static int32_t get_offset_of_extOrdering_1() { return static_cast<int32_t>(offsetof(X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33, ___extOrdering_1)); }
	inline RuntimeObject* get_extOrdering_1() const { return ___extOrdering_1; }
	inline RuntimeObject** get_address_of_extOrdering_1() { return &___extOrdering_1; }
	inline void set_extOrdering_1(RuntimeObject* value)
	{
		___extOrdering_1 = value;
		Il2CppCodeGenWriteBarrier((&___extOrdering_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONSGENERATOR_T788E3FE4DF38CC565DDEE0B6D23F2A611C604E33_H
#ifndef X509NAMEENTRYCONVERTER_T007AA731425354FFC770B335EB86EC1126045D28_H
#define X509NAMEENTRYCONVERTER_T007AA731425354FFC770B335EB86EC1126045D28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509NameEntryConverter
struct  X509NameEntryConverter_t007AA731425354FFC770B335EB86EC1126045D28  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509NAMEENTRYCONVERTER_T007AA731425354FFC770B335EB86EC1126045D28_H
#ifndef X509NAMETOKENIZER_TB7EC8BFB137765264AA497567E0709DEDFFABE04_H
#define X509NAMETOKENIZER_TB7EC8BFB137765264AA497567E0709DEDFFABE04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509NameTokenizer
struct  X509NameTokenizer_tB7EC8BFB137765264AA497567E0709DEDFFABE04  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509NameTokenizer::value
	String_t* ___value_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509NameTokenizer::index
	int32_t ___index_1;
	// System.Char BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509NameTokenizer::separator
	Il2CppChar ___separator_2;
	// System.Text.StringBuilder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509NameTokenizer::buffer
	StringBuilder_t * ___buffer_3;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(X509NameTokenizer_tB7EC8BFB137765264AA497567E0709DEDFFABE04, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(X509NameTokenizer_tB7EC8BFB137765264AA497567E0709DEDFFABE04, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_separator_2() { return static_cast<int32_t>(offsetof(X509NameTokenizer_tB7EC8BFB137765264AA497567E0709DEDFFABE04, ___separator_2)); }
	inline Il2CppChar get_separator_2() const { return ___separator_2; }
	inline Il2CppChar* get_address_of_separator_2() { return &___separator_2; }
	inline void set_separator_2(Il2CppChar value)
	{
		___separator_2 = value;
	}

	inline static int32_t get_offset_of_buffer_3() { return static_cast<int32_t>(offsetof(X509NameTokenizer_tB7EC8BFB137765264AA497567E0709DEDFFABE04, ___buffer_3)); }
	inline StringBuilder_t * get_buffer_3() const { return ___buffer_3; }
	inline StringBuilder_t ** get_address_of_buffer_3() { return &___buffer_3; }
	inline void set_buffer_3(StringBuilder_t * value)
	{
		___buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509NAMETOKENIZER_TB7EC8BFB137765264AA497567E0709DEDFFABE04_H
#ifndef X509OBJECTIDENTIFIERS_T430395AFF8001F20CE1E7DA67F96A185EFDBA777_H
#define X509OBJECTIDENTIFIERS_T430395AFF8001F20CE1E7DA67F96A185EFDBA777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers
struct  X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777  : public RuntimeObject
{
public:

public:
};

struct X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::CommonName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CommonName_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::CountryName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CountryName_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::LocalityName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___LocalityName_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::StateOrProvinceName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___StateOrProvinceName_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::Organization
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Organization_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::OrganizationalUnitName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___OrganizationalUnitName_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::id_at_telephoneNumber
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_at_telephoneNumber_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::id_at_name
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_at_name_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::id_at_organizationIdentifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_at_organizationIdentifier_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdSha1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha1_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::RipeMD160
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RipeMD160_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::RipeMD160WithRsaEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RipeMD160WithRsaEncryption_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdEARsa
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdEARsa_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdPkix
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdPkix_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdPE
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdPE_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdAD
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAD_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdADCAIssuers
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdADCAIssuers_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdADOcsp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdADOcsp_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::OcspAccessMethod
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___OcspAccessMethod_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::CrlAccessMethod
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CrlAccessMethod_20;

public:
	inline static int32_t get_offset_of_CommonName_1() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___CommonName_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CommonName_1() const { return ___CommonName_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CommonName_1() { return &___CommonName_1; }
	inline void set_CommonName_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CommonName_1 = value;
		Il2CppCodeGenWriteBarrier((&___CommonName_1), value);
	}

	inline static int32_t get_offset_of_CountryName_2() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___CountryName_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CountryName_2() const { return ___CountryName_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CountryName_2() { return &___CountryName_2; }
	inline void set_CountryName_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CountryName_2 = value;
		Il2CppCodeGenWriteBarrier((&___CountryName_2), value);
	}

	inline static int32_t get_offset_of_LocalityName_3() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___LocalityName_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_LocalityName_3() const { return ___LocalityName_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_LocalityName_3() { return &___LocalityName_3; }
	inline void set_LocalityName_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___LocalityName_3 = value;
		Il2CppCodeGenWriteBarrier((&___LocalityName_3), value);
	}

	inline static int32_t get_offset_of_StateOrProvinceName_4() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___StateOrProvinceName_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_StateOrProvinceName_4() const { return ___StateOrProvinceName_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_StateOrProvinceName_4() { return &___StateOrProvinceName_4; }
	inline void set_StateOrProvinceName_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___StateOrProvinceName_4 = value;
		Il2CppCodeGenWriteBarrier((&___StateOrProvinceName_4), value);
	}

	inline static int32_t get_offset_of_Organization_5() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___Organization_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Organization_5() const { return ___Organization_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Organization_5() { return &___Organization_5; }
	inline void set_Organization_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Organization_5 = value;
		Il2CppCodeGenWriteBarrier((&___Organization_5), value);
	}

	inline static int32_t get_offset_of_OrganizationalUnitName_6() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___OrganizationalUnitName_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_OrganizationalUnitName_6() const { return ___OrganizationalUnitName_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_OrganizationalUnitName_6() { return &___OrganizationalUnitName_6; }
	inline void set_OrganizationalUnitName_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___OrganizationalUnitName_6 = value;
		Il2CppCodeGenWriteBarrier((&___OrganizationalUnitName_6), value);
	}

	inline static int32_t get_offset_of_id_at_telephoneNumber_7() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___id_at_telephoneNumber_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_at_telephoneNumber_7() const { return ___id_at_telephoneNumber_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_at_telephoneNumber_7() { return &___id_at_telephoneNumber_7; }
	inline void set_id_at_telephoneNumber_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_at_telephoneNumber_7 = value;
		Il2CppCodeGenWriteBarrier((&___id_at_telephoneNumber_7), value);
	}

	inline static int32_t get_offset_of_id_at_name_8() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___id_at_name_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_at_name_8() const { return ___id_at_name_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_at_name_8() { return &___id_at_name_8; }
	inline void set_id_at_name_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_at_name_8 = value;
		Il2CppCodeGenWriteBarrier((&___id_at_name_8), value);
	}

	inline static int32_t get_offset_of_id_at_organizationIdentifier_9() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___id_at_organizationIdentifier_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_at_organizationIdentifier_9() const { return ___id_at_organizationIdentifier_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_at_organizationIdentifier_9() { return &___id_at_organizationIdentifier_9; }
	inline void set_id_at_organizationIdentifier_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_at_organizationIdentifier_9 = value;
		Il2CppCodeGenWriteBarrier((&___id_at_organizationIdentifier_9), value);
	}

	inline static int32_t get_offset_of_IdSha1_10() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___IdSha1_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha1_10() const { return ___IdSha1_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha1_10() { return &___IdSha1_10; }
	inline void set_IdSha1_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha1_10 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha1_10), value);
	}

	inline static int32_t get_offset_of_RipeMD160_11() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___RipeMD160_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RipeMD160_11() const { return ___RipeMD160_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RipeMD160_11() { return &___RipeMD160_11; }
	inline void set_RipeMD160_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RipeMD160_11 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD160_11), value);
	}

	inline static int32_t get_offset_of_RipeMD160WithRsaEncryption_12() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___RipeMD160WithRsaEncryption_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RipeMD160WithRsaEncryption_12() const { return ___RipeMD160WithRsaEncryption_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RipeMD160WithRsaEncryption_12() { return &___RipeMD160WithRsaEncryption_12; }
	inline void set_RipeMD160WithRsaEncryption_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RipeMD160WithRsaEncryption_12 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD160WithRsaEncryption_12), value);
	}

	inline static int32_t get_offset_of_IdEARsa_13() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___IdEARsa_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdEARsa_13() const { return ___IdEARsa_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdEARsa_13() { return &___IdEARsa_13; }
	inline void set_IdEARsa_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdEARsa_13 = value;
		Il2CppCodeGenWriteBarrier((&___IdEARsa_13), value);
	}

	inline static int32_t get_offset_of_IdPkix_14() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___IdPkix_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdPkix_14() const { return ___IdPkix_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdPkix_14() { return &___IdPkix_14; }
	inline void set_IdPkix_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdPkix_14 = value;
		Il2CppCodeGenWriteBarrier((&___IdPkix_14), value);
	}

	inline static int32_t get_offset_of_IdPE_15() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___IdPE_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdPE_15() const { return ___IdPE_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdPE_15() { return &___IdPE_15; }
	inline void set_IdPE_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdPE_15 = value;
		Il2CppCodeGenWriteBarrier((&___IdPE_15), value);
	}

	inline static int32_t get_offset_of_IdAD_16() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___IdAD_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAD_16() const { return ___IdAD_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAD_16() { return &___IdAD_16; }
	inline void set_IdAD_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAD_16 = value;
		Il2CppCodeGenWriteBarrier((&___IdAD_16), value);
	}

	inline static int32_t get_offset_of_IdADCAIssuers_17() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___IdADCAIssuers_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdADCAIssuers_17() const { return ___IdADCAIssuers_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdADCAIssuers_17() { return &___IdADCAIssuers_17; }
	inline void set_IdADCAIssuers_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdADCAIssuers_17 = value;
		Il2CppCodeGenWriteBarrier((&___IdADCAIssuers_17), value);
	}

	inline static int32_t get_offset_of_IdADOcsp_18() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___IdADOcsp_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdADOcsp_18() const { return ___IdADOcsp_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdADOcsp_18() { return &___IdADOcsp_18; }
	inline void set_IdADOcsp_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdADOcsp_18 = value;
		Il2CppCodeGenWriteBarrier((&___IdADOcsp_18), value);
	}

	inline static int32_t get_offset_of_OcspAccessMethod_19() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___OcspAccessMethod_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_OcspAccessMethod_19() const { return ___OcspAccessMethod_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_OcspAccessMethod_19() { return &___OcspAccessMethod_19; }
	inline void set_OcspAccessMethod_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___OcspAccessMethod_19 = value;
		Il2CppCodeGenWriteBarrier((&___OcspAccessMethod_19), value);
	}

	inline static int32_t get_offset_of_CrlAccessMethod_20() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields, ___CrlAccessMethod_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CrlAccessMethod_20() const { return ___CrlAccessMethod_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CrlAccessMethod_20() { return &___CrlAccessMethod_20; }
	inline void set_CrlAccessMethod_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CrlAccessMethod_20 = value;
		Il2CppCodeGenWriteBarrier((&___CrlAccessMethod_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509OBJECTIDENTIFIERS_T430395AFF8001F20CE1E7DA67F96A185EFDBA777_H
#ifndef X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#define X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct  X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder::parameters
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * ___parameters_0;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB, ___parameters_0)); }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * get_parameters_0() const { return ___parameters_0; }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#define ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct  Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifndef ECPRIVATEKEYSTRUCTURE_T8D7DE8938D0B55174D99D7E78456E5DCAD70F55F_H
#define ECPRIVATEKEYSTRUCTURE_T8D7DE8938D0B55174D99D7E78456E5DCAD70F55F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.ECPrivateKeyStructure
struct  ECPrivateKeyStructure_t8D7DE8938D0B55174D99D7E78456E5DCAD70F55F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.ECPrivateKeyStructure::seq
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___seq_2;

public:
	inline static int32_t get_offset_of_seq_2() { return static_cast<int32_t>(offsetof(ECPrivateKeyStructure_t8D7DE8938D0B55174D99D7E78456E5DCAD70F55F, ___seq_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_seq_2() const { return ___seq_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_seq_2() { return &___seq_2; }
	inline void set_seq_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___seq_2 = value;
		Il2CppCodeGenWriteBarrier((&___seq_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPRIVATEKEYSTRUCTURE_T8D7DE8938D0B55174D99D7E78456E5DCAD70F55F_H
#ifndef SECP112R1HOLDER_T1D1D45C84C2CE8DE3CE6D0BEA0FED7E5073D3AE3_H
#define SECP112R1HOLDER_T1D1D45C84C2CE8DE3CE6D0BEA0FED7E5073D3AE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp112r1Holder
struct  Secp112r1Holder_t1D1D45C84C2CE8DE3CE6D0BEA0FED7E5073D3AE3  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp112r1Holder_t1D1D45C84C2CE8DE3CE6D0BEA0FED7E5073D3AE3_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp112r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp112r1Holder_t1D1D45C84C2CE8DE3CE6D0BEA0FED7E5073D3AE3_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP112R1HOLDER_T1D1D45C84C2CE8DE3CE6D0BEA0FED7E5073D3AE3_H
#ifndef SECP112R2HOLDER_TA6AFD383DA328CC150BDC4C7427C6E4C151F9032_H
#define SECP112R2HOLDER_TA6AFD383DA328CC150BDC4C7427C6E4C151F9032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp112r2Holder
struct  Secp112r2Holder_tA6AFD383DA328CC150BDC4C7427C6E4C151F9032  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp112r2Holder_tA6AFD383DA328CC150BDC4C7427C6E4C151F9032_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp112r2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp112r2Holder_tA6AFD383DA328CC150BDC4C7427C6E4C151F9032_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP112R2HOLDER_TA6AFD383DA328CC150BDC4C7427C6E4C151F9032_H
#ifndef SECP128R1HOLDER_T819ACD6434C7C1402A6EA3FCD06C247FEF99CA75_H
#define SECP128R1HOLDER_T819ACD6434C7C1402A6EA3FCD06C247FEF99CA75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp128r1Holder
struct  Secp128r1Holder_t819ACD6434C7C1402A6EA3FCD06C247FEF99CA75  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp128r1Holder_t819ACD6434C7C1402A6EA3FCD06C247FEF99CA75_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp128r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp128r1Holder_t819ACD6434C7C1402A6EA3FCD06C247FEF99CA75_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1HOLDER_T819ACD6434C7C1402A6EA3FCD06C247FEF99CA75_H
#ifndef SECP128R2HOLDER_T7560CEFCEB9C09E59673D46A731C09CE8C5CB198_H
#define SECP128R2HOLDER_T7560CEFCEB9C09E59673D46A731C09CE8C5CB198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp128r2Holder
struct  Secp128r2Holder_t7560CEFCEB9C09E59673D46A731C09CE8C5CB198  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp128r2Holder_t7560CEFCEB9C09E59673D46A731C09CE8C5CB198_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp128r2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp128r2Holder_t7560CEFCEB9C09E59673D46A731C09CE8C5CB198_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R2HOLDER_T7560CEFCEB9C09E59673D46A731C09CE8C5CB198_H
#ifndef SECP160K1HOLDER_TD1CF7A461D85742DF3526D2F08AB75A49F29528B_H
#define SECP160K1HOLDER_TD1CF7A461D85742DF3526D2F08AB75A49F29528B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160k1Holder
struct  Secp160k1Holder_tD1CF7A461D85742DF3526D2F08AB75A49F29528B  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp160k1Holder_tD1CF7A461D85742DF3526D2F08AB75A49F29528B_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160k1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp160k1Holder_tD1CF7A461D85742DF3526D2F08AB75A49F29528B_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160K1HOLDER_TD1CF7A461D85742DF3526D2F08AB75A49F29528B_H
#ifndef SECP160R1HOLDER_T74946AF12676BA09DA3CD2300C7861ABE3E6D9AD_H
#define SECP160R1HOLDER_T74946AF12676BA09DA3CD2300C7861ABE3E6D9AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160r1Holder
struct  Secp160r1Holder_t74946AF12676BA09DA3CD2300C7861ABE3E6D9AD  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp160r1Holder_t74946AF12676BA09DA3CD2300C7861ABE3E6D9AD_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp160r1Holder_t74946AF12676BA09DA3CD2300C7861ABE3E6D9AD_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1HOLDER_T74946AF12676BA09DA3CD2300C7861ABE3E6D9AD_H
#ifndef SECP160R2HOLDER_T16C50ADA9D787AE5A487D5AAC402C1A68BE9FD8C_H
#define SECP160R2HOLDER_T16C50ADA9D787AE5A487D5AAC402C1A68BE9FD8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160r2Holder
struct  Secp160r2Holder_t16C50ADA9D787AE5A487D5AAC402C1A68BE9FD8C  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp160r2Holder_t16C50ADA9D787AE5A487D5AAC402C1A68BE9FD8C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160r2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp160r2Holder_t16C50ADA9D787AE5A487D5AAC402C1A68BE9FD8C_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2HOLDER_T16C50ADA9D787AE5A487D5AAC402C1A68BE9FD8C_H
#ifndef SECP192K1HOLDER_T9D5464195D960C0C13CEFE8B7FE5A57B8F4BCAA0_H
#define SECP192K1HOLDER_T9D5464195D960C0C13CEFE8B7FE5A57B8F4BCAA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp192k1Holder
struct  Secp192k1Holder_t9D5464195D960C0C13CEFE8B7FE5A57B8F4BCAA0  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp192k1Holder_t9D5464195D960C0C13CEFE8B7FE5A57B8F4BCAA0_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp192k1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp192k1Holder_t9D5464195D960C0C13CEFE8B7FE5A57B8F4BCAA0_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1HOLDER_T9D5464195D960C0C13CEFE8B7FE5A57B8F4BCAA0_H
#ifndef SECP192R1HOLDER_TB40768428A806BB6E7AEE00D2B494395A41E995B_H
#define SECP192R1HOLDER_TB40768428A806BB6E7AEE00D2B494395A41E995B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp192r1Holder
struct  Secp192r1Holder_tB40768428A806BB6E7AEE00D2B494395A41E995B  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp192r1Holder_tB40768428A806BB6E7AEE00D2B494395A41E995B_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp192r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp192r1Holder_tB40768428A806BB6E7AEE00D2B494395A41E995B_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1HOLDER_TB40768428A806BB6E7AEE00D2B494395A41E995B_H
#ifndef SECP224K1HOLDER_T1675BC604667C4EDBB03CED70D742F9039EBF013_H
#define SECP224K1HOLDER_T1675BC604667C4EDBB03CED70D742F9039EBF013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp224k1Holder
struct  Secp224k1Holder_t1675BC604667C4EDBB03CED70D742F9039EBF013  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp224k1Holder_t1675BC604667C4EDBB03CED70D742F9039EBF013_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp224k1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp224k1Holder_t1675BC604667C4EDBB03CED70D742F9039EBF013_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1HOLDER_T1675BC604667C4EDBB03CED70D742F9039EBF013_H
#ifndef SECP224R1HOLDER_TDC4BE46EC2780885B913042911B169B7A79DD96D_H
#define SECP224R1HOLDER_TDC4BE46EC2780885B913042911B169B7A79DD96D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp224r1Holder
struct  Secp224r1Holder_tDC4BE46EC2780885B913042911B169B7A79DD96D  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp224r1Holder_tDC4BE46EC2780885B913042911B169B7A79DD96D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp224r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp224r1Holder_tDC4BE46EC2780885B913042911B169B7A79DD96D_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1HOLDER_TDC4BE46EC2780885B913042911B169B7A79DD96D_H
#ifndef SECP256K1HOLDER_TA264EBF6C1037E3AF7C804529474E198A069EED2_H
#define SECP256K1HOLDER_TA264EBF6C1037E3AF7C804529474E198A069EED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp256k1Holder
struct  Secp256k1Holder_tA264EBF6C1037E3AF7C804529474E198A069EED2  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp256k1Holder_tA264EBF6C1037E3AF7C804529474E198A069EED2_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp256k1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp256k1Holder_tA264EBF6C1037E3AF7C804529474E198A069EED2_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1HOLDER_TA264EBF6C1037E3AF7C804529474E198A069EED2_H
#ifndef SMIMECAPABILITIES_T20369201B014E96F67B3906E1149E9684C2B4DB4_H
#define SMIMECAPABILITIES_T20369201B014E96F67B3906E1149E9684C2B4DB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities
struct  SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::capabilities
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___capabilities_13;

public:
	inline static int32_t get_offset_of_capabilities_13() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4, ___capabilities_13)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_capabilities_13() const { return ___capabilities_13; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_capabilities_13() { return &___capabilities_13; }
	inline void set_capabilities_13(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___capabilities_13 = value;
		Il2CppCodeGenWriteBarrier((&___capabilities_13), value);
	}
};

struct SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::PreferSignedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PreferSignedData_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::CannotDecryptAny
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CannotDecryptAny_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::SmimeCapabilitesVersions
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SmimeCapabilitesVersions_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::Aes256Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Aes256Cbc_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::Aes192Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Aes192Cbc_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::Aes128Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Aes128Cbc_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::IdeaCbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdeaCbc_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::Cast5Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Cast5Cbc_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::DesCbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DesCbc_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::DesEde3Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DesEde3Cbc_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilities::RC2Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RC2Cbc_12;

public:
	inline static int32_t get_offset_of_PreferSignedData_2() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields, ___PreferSignedData_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PreferSignedData_2() const { return ___PreferSignedData_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PreferSignedData_2() { return &___PreferSignedData_2; }
	inline void set_PreferSignedData_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PreferSignedData_2 = value;
		Il2CppCodeGenWriteBarrier((&___PreferSignedData_2), value);
	}

	inline static int32_t get_offset_of_CannotDecryptAny_3() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields, ___CannotDecryptAny_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CannotDecryptAny_3() const { return ___CannotDecryptAny_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CannotDecryptAny_3() { return &___CannotDecryptAny_3; }
	inline void set_CannotDecryptAny_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CannotDecryptAny_3 = value;
		Il2CppCodeGenWriteBarrier((&___CannotDecryptAny_3), value);
	}

	inline static int32_t get_offset_of_SmimeCapabilitesVersions_4() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields, ___SmimeCapabilitesVersions_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SmimeCapabilitesVersions_4() const { return ___SmimeCapabilitesVersions_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SmimeCapabilitesVersions_4() { return &___SmimeCapabilitesVersions_4; }
	inline void set_SmimeCapabilitesVersions_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SmimeCapabilitesVersions_4 = value;
		Il2CppCodeGenWriteBarrier((&___SmimeCapabilitesVersions_4), value);
	}

	inline static int32_t get_offset_of_Aes256Cbc_5() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields, ___Aes256Cbc_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Aes256Cbc_5() const { return ___Aes256Cbc_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Aes256Cbc_5() { return &___Aes256Cbc_5; }
	inline void set_Aes256Cbc_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Aes256Cbc_5 = value;
		Il2CppCodeGenWriteBarrier((&___Aes256Cbc_5), value);
	}

	inline static int32_t get_offset_of_Aes192Cbc_6() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields, ___Aes192Cbc_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Aes192Cbc_6() const { return ___Aes192Cbc_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Aes192Cbc_6() { return &___Aes192Cbc_6; }
	inline void set_Aes192Cbc_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Aes192Cbc_6 = value;
		Il2CppCodeGenWriteBarrier((&___Aes192Cbc_6), value);
	}

	inline static int32_t get_offset_of_Aes128Cbc_7() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields, ___Aes128Cbc_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Aes128Cbc_7() const { return ___Aes128Cbc_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Aes128Cbc_7() { return &___Aes128Cbc_7; }
	inline void set_Aes128Cbc_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Aes128Cbc_7 = value;
		Il2CppCodeGenWriteBarrier((&___Aes128Cbc_7), value);
	}

	inline static int32_t get_offset_of_IdeaCbc_8() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields, ___IdeaCbc_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdeaCbc_8() const { return ___IdeaCbc_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdeaCbc_8() { return &___IdeaCbc_8; }
	inline void set_IdeaCbc_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdeaCbc_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdeaCbc_8), value);
	}

	inline static int32_t get_offset_of_Cast5Cbc_9() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields, ___Cast5Cbc_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Cast5Cbc_9() const { return ___Cast5Cbc_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Cast5Cbc_9() { return &___Cast5Cbc_9; }
	inline void set_Cast5Cbc_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Cast5Cbc_9 = value;
		Il2CppCodeGenWriteBarrier((&___Cast5Cbc_9), value);
	}

	inline static int32_t get_offset_of_DesCbc_10() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields, ___DesCbc_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DesCbc_10() const { return ___DesCbc_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DesCbc_10() { return &___DesCbc_10; }
	inline void set_DesCbc_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DesCbc_10 = value;
		Il2CppCodeGenWriteBarrier((&___DesCbc_10), value);
	}

	inline static int32_t get_offset_of_DesEde3Cbc_11() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields, ___DesEde3Cbc_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DesEde3Cbc_11() const { return ___DesEde3Cbc_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DesEde3Cbc_11() { return &___DesEde3Cbc_11; }
	inline void set_DesEde3Cbc_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DesEde3Cbc_11 = value;
		Il2CppCodeGenWriteBarrier((&___DesEde3Cbc_11), value);
	}

	inline static int32_t get_offset_of_RC2Cbc_12() { return static_cast<int32_t>(offsetof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields, ___RC2Cbc_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RC2Cbc_12() const { return ___RC2Cbc_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RC2Cbc_12() { return &___RC2Cbc_12; }
	inline void set_RC2Cbc_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RC2Cbc_12 = value;
		Il2CppCodeGenWriteBarrier((&___RC2Cbc_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMIMECAPABILITIES_T20369201B014E96F67B3906E1149E9684C2B4DB4_H
#ifndef SMIMECAPABILITY_T5449B39E48F3CA301B6772E17983E08DF7CE441B_H
#define SMIMECAPABILITY_T5449B39E48F3CA301B6772E17983E08DF7CE441B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapability
struct  SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapability::capabilityID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___capabilityID_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapability::parameters
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___parameters_9;

public:
	inline static int32_t get_offset_of_capabilityID_8() { return static_cast<int32_t>(offsetof(SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B, ___capabilityID_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_capabilityID_8() const { return ___capabilityID_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_capabilityID_8() { return &___capabilityID_8; }
	inline void set_capabilityID_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___capabilityID_8 = value;
		Il2CppCodeGenWriteBarrier((&___capabilityID_8), value);
	}

	inline static int32_t get_offset_of_parameters_9() { return static_cast<int32_t>(offsetof(SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B, ___parameters_9)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_parameters_9() const { return ___parameters_9; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_parameters_9() { return &___parameters_9; }
	inline void set_parameters_9(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___parameters_9 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_9), value);
	}
};

struct SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapability::PreferSignedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PreferSignedData_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapability::CannotDecryptAny
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CannotDecryptAny_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapability::SmimeCapabilitiesVersions
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SmimeCapabilitiesVersions_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapability::DesCbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DesCbc_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapability::DesEde3Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DesEde3Cbc_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapability::RC2Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RC2Cbc_7;

public:
	inline static int32_t get_offset_of_PreferSignedData_2() { return static_cast<int32_t>(offsetof(SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields, ___PreferSignedData_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PreferSignedData_2() const { return ___PreferSignedData_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PreferSignedData_2() { return &___PreferSignedData_2; }
	inline void set_PreferSignedData_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PreferSignedData_2 = value;
		Il2CppCodeGenWriteBarrier((&___PreferSignedData_2), value);
	}

	inline static int32_t get_offset_of_CannotDecryptAny_3() { return static_cast<int32_t>(offsetof(SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields, ___CannotDecryptAny_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CannotDecryptAny_3() const { return ___CannotDecryptAny_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CannotDecryptAny_3() { return &___CannotDecryptAny_3; }
	inline void set_CannotDecryptAny_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CannotDecryptAny_3 = value;
		Il2CppCodeGenWriteBarrier((&___CannotDecryptAny_3), value);
	}

	inline static int32_t get_offset_of_SmimeCapabilitiesVersions_4() { return static_cast<int32_t>(offsetof(SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields, ___SmimeCapabilitiesVersions_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SmimeCapabilitiesVersions_4() const { return ___SmimeCapabilitiesVersions_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SmimeCapabilitiesVersions_4() { return &___SmimeCapabilitiesVersions_4; }
	inline void set_SmimeCapabilitiesVersions_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SmimeCapabilitiesVersions_4 = value;
		Il2CppCodeGenWriteBarrier((&___SmimeCapabilitiesVersions_4), value);
	}

	inline static int32_t get_offset_of_DesCbc_5() { return static_cast<int32_t>(offsetof(SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields, ___DesCbc_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DesCbc_5() const { return ___DesCbc_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DesCbc_5() { return &___DesCbc_5; }
	inline void set_DesCbc_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DesCbc_5 = value;
		Il2CppCodeGenWriteBarrier((&___DesCbc_5), value);
	}

	inline static int32_t get_offset_of_DesEde3Cbc_6() { return static_cast<int32_t>(offsetof(SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields, ___DesEde3Cbc_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DesEde3Cbc_6() const { return ___DesEde3Cbc_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DesEde3Cbc_6() { return &___DesEde3Cbc_6; }
	inline void set_DesEde3Cbc_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DesEde3Cbc_6 = value;
		Il2CppCodeGenWriteBarrier((&___DesEde3Cbc_6), value);
	}

	inline static int32_t get_offset_of_RC2Cbc_7() { return static_cast<int32_t>(offsetof(SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields, ___RC2Cbc_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RC2Cbc_7() const { return ___RC2Cbc_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RC2Cbc_7() { return &___RC2Cbc_7; }
	inline void set_RC2Cbc_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RC2Cbc_7 = value;
		Il2CppCodeGenWriteBarrier((&___RC2Cbc_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMIMECAPABILITY_T5449B39E48F3CA301B6772E17983E08DF7CE441B_H
#ifndef BRAINPOOLP160R1HOLDER_TDC9082B944DA05E409B0AADD46151183FF6DF31C_H
#define BRAINPOOLP160R1HOLDER_TDC9082B944DA05E409B0AADD46151183FF6DF31C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP160r1Holder
struct  BrainpoolP160r1Holder_tDC9082B944DA05E409B0AADD46151183FF6DF31C  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP160r1Holder_tDC9082B944DA05E409B0AADD46151183FF6DF31C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP160r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP160r1Holder_tDC9082B944DA05E409B0AADD46151183FF6DF31C_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP160R1HOLDER_TDC9082B944DA05E409B0AADD46151183FF6DF31C_H
#ifndef BRAINPOOLP160T1HOLDER_TD2E957EB433DED5FA2076636CA42258BDA6194C0_H
#define BRAINPOOLP160T1HOLDER_TD2E957EB433DED5FA2076636CA42258BDA6194C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP160t1Holder
struct  BrainpoolP160t1Holder_tD2E957EB433DED5FA2076636CA42258BDA6194C0  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP160t1Holder_tD2E957EB433DED5FA2076636CA42258BDA6194C0_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP160t1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP160t1Holder_tD2E957EB433DED5FA2076636CA42258BDA6194C0_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP160T1HOLDER_TD2E957EB433DED5FA2076636CA42258BDA6194C0_H
#ifndef BRAINPOOLP192R1HOLDER_T49E9F282500CD2976A89540CE2927C3BB7021507_H
#define BRAINPOOLP192R1HOLDER_T49E9F282500CD2976A89540CE2927C3BB7021507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP192r1Holder
struct  BrainpoolP192r1Holder_t49E9F282500CD2976A89540CE2927C3BB7021507  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP192r1Holder_t49E9F282500CD2976A89540CE2927C3BB7021507_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP192r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP192r1Holder_t49E9F282500CD2976A89540CE2927C3BB7021507_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP192R1HOLDER_T49E9F282500CD2976A89540CE2927C3BB7021507_H
#ifndef BRAINPOOLP192T1HOLDER_T7AE6FF54924FB8AB15DDB3E9C56DBCC4234FB438_H
#define BRAINPOOLP192T1HOLDER_T7AE6FF54924FB8AB15DDB3E9C56DBCC4234FB438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP192t1Holder
struct  BrainpoolP192t1Holder_t7AE6FF54924FB8AB15DDB3E9C56DBCC4234FB438  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP192t1Holder_t7AE6FF54924FB8AB15DDB3E9C56DBCC4234FB438_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP192t1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP192t1Holder_t7AE6FF54924FB8AB15DDB3E9C56DBCC4234FB438_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP192T1HOLDER_T7AE6FF54924FB8AB15DDB3E9C56DBCC4234FB438_H
#ifndef BRAINPOOLP224R1HOLDER_T28A94F4AC836DE587696793FDB85B1398364FEBF_H
#define BRAINPOOLP224R1HOLDER_T28A94F4AC836DE587696793FDB85B1398364FEBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP224r1Holder
struct  BrainpoolP224r1Holder_t28A94F4AC836DE587696793FDB85B1398364FEBF  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP224r1Holder_t28A94F4AC836DE587696793FDB85B1398364FEBF_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP224r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP224r1Holder_t28A94F4AC836DE587696793FDB85B1398364FEBF_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP224R1HOLDER_T28A94F4AC836DE587696793FDB85B1398364FEBF_H
#ifndef BRAINPOOLP224T1HOLDER_T2764822BE418FAB50F9617C6D907E18B31FD5A2E_H
#define BRAINPOOLP224T1HOLDER_T2764822BE418FAB50F9617C6D907E18B31FD5A2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP224t1Holder
struct  BrainpoolP224t1Holder_t2764822BE418FAB50F9617C6D907E18B31FD5A2E  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP224t1Holder_t2764822BE418FAB50F9617C6D907E18B31FD5A2E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP224t1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP224t1Holder_t2764822BE418FAB50F9617C6D907E18B31FD5A2E_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP224T1HOLDER_T2764822BE418FAB50F9617C6D907E18B31FD5A2E_H
#ifndef BRAINPOOLP256R1HOLDER_TAC00F4C5B4D236BAA2A9DDC59D5B098E510B2240_H
#define BRAINPOOLP256R1HOLDER_TAC00F4C5B4D236BAA2A9DDC59D5B098E510B2240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP256r1Holder
struct  BrainpoolP256r1Holder_tAC00F4C5B4D236BAA2A9DDC59D5B098E510B2240  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP256r1Holder_tAC00F4C5B4D236BAA2A9DDC59D5B098E510B2240_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP256r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP256r1Holder_tAC00F4C5B4D236BAA2A9DDC59D5B098E510B2240_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP256R1HOLDER_TAC00F4C5B4D236BAA2A9DDC59D5B098E510B2240_H
#ifndef BRAINPOOLP256T1HOLDER_TDE62C06FCE0194F00BB8EA0D01845894657D09E6_H
#define BRAINPOOLP256T1HOLDER_TDE62C06FCE0194F00BB8EA0D01845894657D09E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP256t1Holder
struct  BrainpoolP256t1Holder_tDE62C06FCE0194F00BB8EA0D01845894657D09E6  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP256t1Holder_tDE62C06FCE0194F00BB8EA0D01845894657D09E6_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP256t1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP256t1Holder_tDE62C06FCE0194F00BB8EA0D01845894657D09E6_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP256T1HOLDER_TDE62C06FCE0194F00BB8EA0D01845894657D09E6_H
#ifndef BRAINPOOLP320R1HOLDER_T3F1DD741FBB8192C91644922A04D897DE2BF069A_H
#define BRAINPOOLP320R1HOLDER_T3F1DD741FBB8192C91644922A04D897DE2BF069A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP320r1Holder
struct  BrainpoolP320r1Holder_t3F1DD741FBB8192C91644922A04D897DE2BF069A  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP320r1Holder_t3F1DD741FBB8192C91644922A04D897DE2BF069A_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP320r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP320r1Holder_t3F1DD741FBB8192C91644922A04D897DE2BF069A_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP320R1HOLDER_T3F1DD741FBB8192C91644922A04D897DE2BF069A_H
#ifndef BRAINPOOLP320T1HOLDER_T73D87ECA0CEA861CC89B180D5ACBC1A481FA01F4_H
#define BRAINPOOLP320T1HOLDER_T73D87ECA0CEA861CC89B180D5ACBC1A481FA01F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP320t1Holder
struct  BrainpoolP320t1Holder_t73D87ECA0CEA861CC89B180D5ACBC1A481FA01F4  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP320t1Holder_t73D87ECA0CEA861CC89B180D5ACBC1A481FA01F4_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP320t1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP320t1Holder_t73D87ECA0CEA861CC89B180D5ACBC1A481FA01F4_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP320T1HOLDER_T73D87ECA0CEA861CC89B180D5ACBC1A481FA01F4_H
#ifndef BRAINPOOLP384R1HOLDER_T6D356240ABA0AE876E37788A833B0229E4E0600C_H
#define BRAINPOOLP384R1HOLDER_T6D356240ABA0AE876E37788A833B0229E4E0600C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP384r1Holder
struct  BrainpoolP384r1Holder_t6D356240ABA0AE876E37788A833B0229E4E0600C  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP384r1Holder_t6D356240ABA0AE876E37788A833B0229E4E0600C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP384r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP384r1Holder_t6D356240ABA0AE876E37788A833B0229E4E0600C_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP384R1HOLDER_T6D356240ABA0AE876E37788A833B0229E4E0600C_H
#ifndef BRAINPOOLP384T1HOLDER_T92A96BBFFE177797F8EB38A56D460A1F9E186888_H
#define BRAINPOOLP384T1HOLDER_T92A96BBFFE177797F8EB38A56D460A1F9E186888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP384t1Holder
struct  BrainpoolP384t1Holder_t92A96BBFFE177797F8EB38A56D460A1F9E186888  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP384t1Holder_t92A96BBFFE177797F8EB38A56D460A1F9E186888_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP384t1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP384t1Holder_t92A96BBFFE177797F8EB38A56D460A1F9E186888_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP384T1HOLDER_T92A96BBFFE177797F8EB38A56D460A1F9E186888_H
#ifndef BRAINPOOLP512R1HOLDER_TA09C2D1E42763C3CDDFD019A7CA15B9C48368CE9_H
#define BRAINPOOLP512R1HOLDER_TA09C2D1E42763C3CDDFD019A7CA15B9C48368CE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP512r1Holder
struct  BrainpoolP512r1Holder_tA09C2D1E42763C3CDDFD019A7CA15B9C48368CE9  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP512r1Holder_tA09C2D1E42763C3CDDFD019A7CA15B9C48368CE9_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP512r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP512r1Holder_tA09C2D1E42763C3CDDFD019A7CA15B9C48368CE9_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP512R1HOLDER_TA09C2D1E42763C3CDDFD019A7CA15B9C48368CE9_H
#ifndef BRAINPOOLP512T1HOLDER_TCAB82C1105BA7D08DE8974CE82C2B6773570EFE5_H
#define BRAINPOOLP512T1HOLDER_TCAB82C1105BA7D08DE8974CE82C2B6773570EFE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP512t1Holder
struct  BrainpoolP512t1Holder_tCAB82C1105BA7D08DE8974CE82C2B6773570EFE5  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct BrainpoolP512t1Holder_tCAB82C1105BA7D08DE8974CE82C2B6773570EFE5_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP512t1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP512t1Holder_tCAB82C1105BA7D08DE8974CE82C2B6773570EFE5_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP512T1HOLDER_TCAB82C1105BA7D08DE8974CE82C2B6773570EFE5_H
#ifndef ACCURACY_T0F0184312CD5A87E1D7E8274229F3C694D758835_H
#define ACCURACY_T0F0184312CD5A87E1D7E8274229F3C694D758835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.Accuracy
struct  Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.Accuracy::seconds
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___seconds_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.Accuracy::millis
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___millis_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.Accuracy::micros
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___micros_4;

public:
	inline static int32_t get_offset_of_seconds_2() { return static_cast<int32_t>(offsetof(Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835, ___seconds_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_seconds_2() const { return ___seconds_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_seconds_2() { return &___seconds_2; }
	inline void set_seconds_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___seconds_2 = value;
		Il2CppCodeGenWriteBarrier((&___seconds_2), value);
	}

	inline static int32_t get_offset_of_millis_3() { return static_cast<int32_t>(offsetof(Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835, ___millis_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_millis_3() const { return ___millis_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_millis_3() { return &___millis_3; }
	inline void set_millis_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___millis_3 = value;
		Il2CppCodeGenWriteBarrier((&___millis_3), value);
	}

	inline static int32_t get_offset_of_micros_4() { return static_cast<int32_t>(offsetof(Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835, ___micros_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_micros_4() const { return ___micros_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_micros_4() { return &___micros_4; }
	inline void set_micros_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___micros_4 = value;
		Il2CppCodeGenWriteBarrier((&___micros_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCURACY_T0F0184312CD5A87E1D7E8274229F3C694D758835_H
#ifndef MESSAGEIMPRINT_T45C405F0EE355FE3A42EE3B7D917C681F0136849_H
#define MESSAGEIMPRINT_T45C405F0EE355FE3A42EE3B7D917C681F0136849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.MessageImprint
struct  MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.MessageImprint::hashAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___hashAlgorithm_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.MessageImprint::hashedMessage
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___hashedMessage_3;

public:
	inline static int32_t get_offset_of_hashAlgorithm_2() { return static_cast<int32_t>(offsetof(MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849, ___hashAlgorithm_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_hashAlgorithm_2() const { return ___hashAlgorithm_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_hashAlgorithm_2() { return &___hashAlgorithm_2; }
	inline void set_hashAlgorithm_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___hashAlgorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___hashAlgorithm_2), value);
	}

	inline static int32_t get_offset_of_hashedMessage_3() { return static_cast<int32_t>(offsetof(MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849, ___hashedMessage_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_hashedMessage_3() const { return ___hashedMessage_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_hashedMessage_3() { return &___hashedMessage_3; }
	inline void set_hashedMessage_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___hashedMessage_3 = value;
		Il2CppCodeGenWriteBarrier((&___hashedMessage_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEIMPRINT_T45C405F0EE355FE3A42EE3B7D917C681F0136849_H
#ifndef TIMESTAMPREQ_TAA5C6E201885890945014C861159673C44B7C1B3_H
#define TIMESTAMPREQ_TAA5C6E201885890945014C861159673C44B7C1B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampReq
struct  TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampReq::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.MessageImprint BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampReq::messageImprint
	MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849 * ___messageImprint_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampReq::tsaPolicy
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___tsaPolicy_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampReq::nonce
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___nonce_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampReq::certReq
	DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * ___certReq_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampReq::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_7;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_messageImprint_3() { return static_cast<int32_t>(offsetof(TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3, ___messageImprint_3)); }
	inline MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849 * get_messageImprint_3() const { return ___messageImprint_3; }
	inline MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849 ** get_address_of_messageImprint_3() { return &___messageImprint_3; }
	inline void set_messageImprint_3(MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849 * value)
	{
		___messageImprint_3 = value;
		Il2CppCodeGenWriteBarrier((&___messageImprint_3), value);
	}

	inline static int32_t get_offset_of_tsaPolicy_4() { return static_cast<int32_t>(offsetof(TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3, ___tsaPolicy_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_tsaPolicy_4() const { return ___tsaPolicy_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_tsaPolicy_4() { return &___tsaPolicy_4; }
	inline void set_tsaPolicy_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___tsaPolicy_4 = value;
		Il2CppCodeGenWriteBarrier((&___tsaPolicy_4), value);
	}

	inline static int32_t get_offset_of_nonce_5() { return static_cast<int32_t>(offsetof(TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3, ___nonce_5)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_nonce_5() const { return ___nonce_5; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_nonce_5() { return &___nonce_5; }
	inline void set_nonce_5(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___nonce_5 = value;
		Il2CppCodeGenWriteBarrier((&___nonce_5), value);
	}

	inline static int32_t get_offset_of_certReq_6() { return static_cast<int32_t>(offsetof(TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3, ___certReq_6)); }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * get_certReq_6() const { return ___certReq_6; }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C ** get_address_of_certReq_6() { return &___certReq_6; }
	inline void set_certReq_6(DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * value)
	{
		___certReq_6 = value;
		Il2CppCodeGenWriteBarrier((&___certReq_6), value);
	}

	inline static int32_t get_offset_of_extensions_7() { return static_cast<int32_t>(offsetof(TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3, ___extensions_7)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_7() const { return ___extensions_7; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_7() { return &___extensions_7; }
	inline void set_extensions_7(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPREQ_TAA5C6E201885890945014C861159673C44B7C1B3_H
#ifndef TIMESTAMPRESP_TAC8AE758086B07D77018BB1BAB5E780677BD3090_H
#define TIMESTAMPRESP_TAC8AE758086B07D77018BB1BAB5E780677BD3090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampResp
struct  TimeStampResp_tAC8AE758086B07D77018BB1BAB5E780677BD3090  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatusInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampResp::pkiStatusInfo
	PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * ___pkiStatusInfo_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampResp::timeStampToken
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___timeStampToken_3;

public:
	inline static int32_t get_offset_of_pkiStatusInfo_2() { return static_cast<int32_t>(offsetof(TimeStampResp_tAC8AE758086B07D77018BB1BAB5E780677BD3090, ___pkiStatusInfo_2)); }
	inline PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * get_pkiStatusInfo_2() const { return ___pkiStatusInfo_2; }
	inline PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E ** get_address_of_pkiStatusInfo_2() { return &___pkiStatusInfo_2; }
	inline void set_pkiStatusInfo_2(PkiStatusInfo_t15BA16AB659A7A930F085748710FF141C65EB13E * value)
	{
		___pkiStatusInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___pkiStatusInfo_2), value);
	}

	inline static int32_t get_offset_of_timeStampToken_3() { return static_cast<int32_t>(offsetof(TimeStampResp_tAC8AE758086B07D77018BB1BAB5E780677BD3090, ___timeStampToken_3)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_timeStampToken_3() const { return ___timeStampToken_3; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_timeStampToken_3() { return &___timeStampToken_3; }
	inline void set_timeStampToken_3(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___timeStampToken_3 = value;
		Il2CppCodeGenWriteBarrier((&___timeStampToken_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPRESP_TAC8AE758086B07D77018BB1BAB5E780677BD3090_H
#ifndef TSTINFO_TD5FA0B62E85C480BAEAECD7C7C09806246A51E47_H
#define TSTINFO_TD5FA0B62E85C480BAEAECD7C7C09806246A51E47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo
struct  TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo::tsaPolicyId
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___tsaPolicyId_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.MessageImprint BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo::messageImprint
	MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849 * ___messageImprint_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo::serialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serialNumber_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo::genTime
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___genTime_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.Accuracy BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo::accuracy
	Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835 * ___accuracy_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo::ordering
	DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * ___ordering_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo::nonce
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___nonce_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo::tsa
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___tsa_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_11;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_tsaPolicyId_3() { return static_cast<int32_t>(offsetof(TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47, ___tsaPolicyId_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_tsaPolicyId_3() const { return ___tsaPolicyId_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_tsaPolicyId_3() { return &___tsaPolicyId_3; }
	inline void set_tsaPolicyId_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___tsaPolicyId_3 = value;
		Il2CppCodeGenWriteBarrier((&___tsaPolicyId_3), value);
	}

	inline static int32_t get_offset_of_messageImprint_4() { return static_cast<int32_t>(offsetof(TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47, ___messageImprint_4)); }
	inline MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849 * get_messageImprint_4() const { return ___messageImprint_4; }
	inline MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849 ** get_address_of_messageImprint_4() { return &___messageImprint_4; }
	inline void set_messageImprint_4(MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849 * value)
	{
		___messageImprint_4 = value;
		Il2CppCodeGenWriteBarrier((&___messageImprint_4), value);
	}

	inline static int32_t get_offset_of_serialNumber_5() { return static_cast<int32_t>(offsetof(TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47, ___serialNumber_5)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serialNumber_5() const { return ___serialNumber_5; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serialNumber_5() { return &___serialNumber_5; }
	inline void set_serialNumber_5(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serialNumber_5 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_5), value);
	}

	inline static int32_t get_offset_of_genTime_6() { return static_cast<int32_t>(offsetof(TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47, ___genTime_6)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_genTime_6() const { return ___genTime_6; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_genTime_6() { return &___genTime_6; }
	inline void set_genTime_6(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___genTime_6 = value;
		Il2CppCodeGenWriteBarrier((&___genTime_6), value);
	}

	inline static int32_t get_offset_of_accuracy_7() { return static_cast<int32_t>(offsetof(TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47, ___accuracy_7)); }
	inline Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835 * get_accuracy_7() const { return ___accuracy_7; }
	inline Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835 ** get_address_of_accuracy_7() { return &___accuracy_7; }
	inline void set_accuracy_7(Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835 * value)
	{
		___accuracy_7 = value;
		Il2CppCodeGenWriteBarrier((&___accuracy_7), value);
	}

	inline static int32_t get_offset_of_ordering_8() { return static_cast<int32_t>(offsetof(TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47, ___ordering_8)); }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * get_ordering_8() const { return ___ordering_8; }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C ** get_address_of_ordering_8() { return &___ordering_8; }
	inline void set_ordering_8(DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * value)
	{
		___ordering_8 = value;
		Il2CppCodeGenWriteBarrier((&___ordering_8), value);
	}

	inline static int32_t get_offset_of_nonce_9() { return static_cast<int32_t>(offsetof(TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47, ___nonce_9)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_nonce_9() const { return ___nonce_9; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_nonce_9() { return &___nonce_9; }
	inline void set_nonce_9(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___nonce_9 = value;
		Il2CppCodeGenWriteBarrier((&___nonce_9), value);
	}

	inline static int32_t get_offset_of_tsa_10() { return static_cast<int32_t>(offsetof(TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47, ___tsa_10)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_tsa_10() const { return ___tsa_10; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_tsa_10() { return &___tsa_10; }
	inline void set_tsa_10(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___tsa_10 = value;
		Il2CppCodeGenWriteBarrier((&___tsa_10), value);
	}

	inline static int32_t get_offset_of_extensions_11() { return static_cast<int32_t>(offsetof(TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47, ___extensions_11)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_11() const { return ___extensions_11; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_11() { return &___extensions_11; }
	inline void set_extensions_11(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_11 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSTINFO_TD5FA0B62E85C480BAEAECD7C7C09806246A51E47_H
#ifndef ATTRIBUTETYPEANDVALUE_TEDDC8E9F1BB20364D723EAA44223E76375C65A27_H
#define ATTRIBUTETYPEANDVALUE_TEDDC8E9F1BB20364D723EAA44223E76375C65A27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.AttributeTypeAndValue
struct  AttributeTypeAndValue_tEDDC8E9F1BB20364D723EAA44223E76375C65A27  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.AttributeTypeAndValue::type
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___type_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.AttributeTypeAndValue::value
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___value_3;

public:
	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(AttributeTypeAndValue_tEDDC8E9F1BB20364D723EAA44223E76375C65A27, ___type_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_type_2() const { return ___type_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier((&___type_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(AttributeTypeAndValue_tEDDC8E9F1BB20364D723EAA44223E76375C65A27, ___value_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_value_3() const { return ___value_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETYPEANDVALUE_TEDDC8E9F1BB20364D723EAA44223E76375C65A27_H
#ifndef DIRECTORYSTRING_T55BD84D10FDF8A6794760B8C6D70ADC242AC012B_H
#define DIRECTORYSTRING_T55BD84D10FDF8A6794760B8C6D70ADC242AC012B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString
struct  DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerStringBase BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString::str
	DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303 * ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B, ___str_2)); }
	inline DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303 * get_str_2() const { return ___str_2; }
	inline DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303 ** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303 * value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORYSTRING_T55BD84D10FDF8A6794760B8C6D70ADC242AC012B_H
#ifndef RDN_TA2093D2BCCD3265BBA69AB76402C884C6207F708_H
#define RDN_TA2093D2BCCD3265BBA69AB76402C884C6207F708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.Rdn
struct  Rdn_tA2093D2BCCD3265BBA69AB76402C884C6207F708  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.Rdn::values
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___values_2;

public:
	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(Rdn_tA2093D2BCCD3265BBA69AB76402C884C6207F708, ___values_2)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_values_2() const { return ___values_2; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier((&___values_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RDN_TA2093D2BCCD3265BBA69AB76402C884C6207F708_H
#ifndef ATTRIBUTEX509_T27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76_H
#define ATTRIBUTEX509_T27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeX509
struct  AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeX509::attrType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___attrType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeX509::attrValues
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___attrValues_3;

public:
	inline static int32_t get_offset_of_attrType_2() { return static_cast<int32_t>(offsetof(AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76, ___attrType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_attrType_2() const { return ___attrType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_attrType_2() { return &___attrType_2; }
	inline void set_attrType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___attrType_2 = value;
		Il2CppCodeGenWriteBarrier((&___attrType_2), value);
	}

	inline static int32_t get_offset_of_attrValues_3() { return static_cast<int32_t>(offsetof(AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76, ___attrValues_3)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_attrValues_3() const { return ___attrValues_3; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_attrValues_3() { return &___attrValues_3; }
	inline void set_attrValues_3(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___attrValues_3 = value;
		Il2CppCodeGenWriteBarrier((&___attrValues_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEX509_T27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76_H
#ifndef CRLENTRY_T6D850A322CED0E23E960207D7E59AD5493D5F63C_H
#define CRLENTRY_T6D850A322CED0E23E960207D7E59AD5493D5F63C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlEntry
struct  CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlEntry::seq
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___seq_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlEntry::userCertificate
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___userCertificate_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlEntry::revocationDate
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___revocationDate_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlEntry::crlEntryExtensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___crlEntryExtensions_5;

public:
	inline static int32_t get_offset_of_seq_2() { return static_cast<int32_t>(offsetof(CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C, ___seq_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_seq_2() const { return ___seq_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_seq_2() { return &___seq_2; }
	inline void set_seq_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___seq_2 = value;
		Il2CppCodeGenWriteBarrier((&___seq_2), value);
	}

	inline static int32_t get_offset_of_userCertificate_3() { return static_cast<int32_t>(offsetof(CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C, ___userCertificate_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_userCertificate_3() const { return ___userCertificate_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_userCertificate_3() { return &___userCertificate_3; }
	inline void set_userCertificate_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___userCertificate_3 = value;
		Il2CppCodeGenWriteBarrier((&___userCertificate_3), value);
	}

	inline static int32_t get_offset_of_revocationDate_4() { return static_cast<int32_t>(offsetof(CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C, ___revocationDate_4)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_revocationDate_4() const { return ___revocationDate_4; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_revocationDate_4() { return &___revocationDate_4; }
	inline void set_revocationDate_4(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___revocationDate_4 = value;
		Il2CppCodeGenWriteBarrier((&___revocationDate_4), value);
	}

	inline static int32_t get_offset_of_crlEntryExtensions_5() { return static_cast<int32_t>(offsetof(CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C, ___crlEntryExtensions_5)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_crlEntryExtensions_5() const { return ___crlEntryExtensions_5; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_crlEntryExtensions_5() { return &___crlEntryExtensions_5; }
	inline void set_crlEntryExtensions_5(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___crlEntryExtensions_5 = value;
		Il2CppCodeGenWriteBarrier((&___crlEntryExtensions_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRLENTRY_T6D850A322CED0E23E960207D7E59AD5493D5F63C_H
#ifndef NAMECONSTRAINTS_T32DADF4E7B769CB9582A14FA4A4FD9672A635A6C_H
#define NAMECONSTRAINTS_T32DADF4E7B769CB9582A14FA4A4FD9672A635A6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.NameConstraints
struct  NameConstraints_t32DADF4E7B769CB9582A14FA4A4FD9672A635A6C  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.NameConstraints::permitted
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___permitted_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.NameConstraints::excluded
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___excluded_3;

public:
	inline static int32_t get_offset_of_permitted_2() { return static_cast<int32_t>(offsetof(NameConstraints_t32DADF4E7B769CB9582A14FA4A4FD9672A635A6C, ___permitted_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_permitted_2() const { return ___permitted_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_permitted_2() { return &___permitted_2; }
	inline void set_permitted_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___permitted_2 = value;
		Il2CppCodeGenWriteBarrier((&___permitted_2), value);
	}

	inline static int32_t get_offset_of_excluded_3() { return static_cast<int32_t>(offsetof(NameConstraints_t32DADF4E7B769CB9582A14FA4A4FD9672A635A6C, ___excluded_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_excluded_3() const { return ___excluded_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_excluded_3() { return &___excluded_3; }
	inline void set_excluded_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___excluded_3 = value;
		Il2CppCodeGenWriteBarrier((&___excluded_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMECONSTRAINTS_T32DADF4E7B769CB9582A14FA4A4FD9672A635A6C_H
#ifndef NOTICEREFERENCE_T881A2D3508F9AA3C2EEC071137E809179E643234_H
#define NOTICEREFERENCE_T881A2D3508F9AA3C2EEC071137E809179E643234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.NoticeReference
struct  NoticeReference_t881A2D3508F9AA3C2EEC071137E809179E643234  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DisplayText BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.NoticeReference::organization
	DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB * ___organization_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.NoticeReference::noticeNumbers
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___noticeNumbers_3;

public:
	inline static int32_t get_offset_of_organization_2() { return static_cast<int32_t>(offsetof(NoticeReference_t881A2D3508F9AA3C2EEC071137E809179E643234, ___organization_2)); }
	inline DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB * get_organization_2() const { return ___organization_2; }
	inline DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB ** get_address_of_organization_2() { return &___organization_2; }
	inline void set_organization_2(DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB * value)
	{
		___organization_2 = value;
		Il2CppCodeGenWriteBarrier((&___organization_2), value);
	}

	inline static int32_t get_offset_of_noticeNumbers_3() { return static_cast<int32_t>(offsetof(NoticeReference_t881A2D3508F9AA3C2EEC071137E809179E643234, ___noticeNumbers_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_noticeNumbers_3() const { return ___noticeNumbers_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_noticeNumbers_3() { return &___noticeNumbers_3; }
	inline void set_noticeNumbers_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___noticeNumbers_3 = value;
		Il2CppCodeGenWriteBarrier((&___noticeNumbers_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTICEREFERENCE_T881A2D3508F9AA3C2EEC071137E809179E643234_H
#ifndef OBJECTDIGESTINFO_TDF6863A222D8EE0A0CB96CA58AF94A5796A2659F_H
#define OBJECTDIGESTINFO_TDF6863A222D8EE0A0CB96CA58AF94A5796A2659F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ObjectDigestInfo
struct  ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ObjectDigestInfo::digestedObjectType
	DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F * ___digestedObjectType_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ObjectDigestInfo::otherObjectTypeID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___otherObjectTypeID_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ObjectDigestInfo::digestAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___digestAlgorithm_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ObjectDigestInfo::objectDigest
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___objectDigest_8;

public:
	inline static int32_t get_offset_of_digestedObjectType_5() { return static_cast<int32_t>(offsetof(ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F, ___digestedObjectType_5)); }
	inline DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F * get_digestedObjectType_5() const { return ___digestedObjectType_5; }
	inline DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F ** get_address_of_digestedObjectType_5() { return &___digestedObjectType_5; }
	inline void set_digestedObjectType_5(DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F * value)
	{
		___digestedObjectType_5 = value;
		Il2CppCodeGenWriteBarrier((&___digestedObjectType_5), value);
	}

	inline static int32_t get_offset_of_otherObjectTypeID_6() { return static_cast<int32_t>(offsetof(ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F, ___otherObjectTypeID_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_otherObjectTypeID_6() const { return ___otherObjectTypeID_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_otherObjectTypeID_6() { return &___otherObjectTypeID_6; }
	inline void set_otherObjectTypeID_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___otherObjectTypeID_6 = value;
		Il2CppCodeGenWriteBarrier((&___otherObjectTypeID_6), value);
	}

	inline static int32_t get_offset_of_digestAlgorithm_7() { return static_cast<int32_t>(offsetof(ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F, ___digestAlgorithm_7)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_digestAlgorithm_7() const { return ___digestAlgorithm_7; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_digestAlgorithm_7() { return &___digestAlgorithm_7; }
	inline void set_digestAlgorithm_7(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___digestAlgorithm_7 = value;
		Il2CppCodeGenWriteBarrier((&___digestAlgorithm_7), value);
	}

	inline static int32_t get_offset_of_objectDigest_8() { return static_cast<int32_t>(offsetof(ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F, ___objectDigest_8)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_objectDigest_8() const { return ___objectDigest_8; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_objectDigest_8() { return &___objectDigest_8; }
	inline void set_objectDigest_8(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___objectDigest_8 = value;
		Il2CppCodeGenWriteBarrier((&___objectDigest_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDIGESTINFO_TDF6863A222D8EE0A0CB96CA58AF94A5796A2659F_H
#ifndef POLICYINFORMATION_TAA2139758A8642ED6AC8CCD1E8875E5E82066DBE_H
#define POLICYINFORMATION_TAA2139758A8642ED6AC8CCD1E8875E5E82066DBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyInformation
struct  PolicyInformation_tAA2139758A8642ED6AC8CCD1E8875E5E82066DBE  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyInformation::policyIdentifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___policyIdentifier_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyInformation::policyQualifiers
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___policyQualifiers_3;

public:
	inline static int32_t get_offset_of_policyIdentifier_2() { return static_cast<int32_t>(offsetof(PolicyInformation_tAA2139758A8642ED6AC8CCD1E8875E5E82066DBE, ___policyIdentifier_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_policyIdentifier_2() const { return ___policyIdentifier_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_policyIdentifier_2() { return &___policyIdentifier_2; }
	inline void set_policyIdentifier_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___policyIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___policyIdentifier_2), value);
	}

	inline static int32_t get_offset_of_policyQualifiers_3() { return static_cast<int32_t>(offsetof(PolicyInformation_tAA2139758A8642ED6AC8CCD1E8875E5E82066DBE, ___policyQualifiers_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_policyQualifiers_3() const { return ___policyQualifiers_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_policyQualifiers_3() { return &___policyQualifiers_3; }
	inline void set_policyQualifiers_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___policyQualifiers_3 = value;
		Il2CppCodeGenWriteBarrier((&___policyQualifiers_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLICYINFORMATION_TAA2139758A8642ED6AC8CCD1E8875E5E82066DBE_H
#ifndef POLICYMAPPINGS_T9941C3AA1FA95799D51A7A778662385DE446B11A_H
#define POLICYMAPPINGS_T9941C3AA1FA95799D51A7A778662385DE446B11A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyMappings
struct  PolicyMappings_t9941C3AA1FA95799D51A7A778662385DE446B11A  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyMappings::seq
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___seq_2;

public:
	inline static int32_t get_offset_of_seq_2() { return static_cast<int32_t>(offsetof(PolicyMappings_t9941C3AA1FA95799D51A7A778662385DE446B11A, ___seq_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_seq_2() const { return ___seq_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_seq_2() { return &___seq_2; }
	inline void set_seq_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___seq_2 = value;
		Il2CppCodeGenWriteBarrier((&___seq_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLICYMAPPINGS_T9941C3AA1FA95799D51A7A778662385DE446B11A_H
#ifndef POLICYQUALIFIERINFO_T5468F83F74055851E359E3796A1C70B50AF91C13_H
#define POLICYQUALIFIERINFO_T5468F83F74055851E359E3796A1C70B50AF91C13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyQualifierInfo
struct  PolicyQualifierInfo_t5468F83F74055851E359E3796A1C70B50AF91C13  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyQualifierInfo::policyQualifierId
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___policyQualifierId_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyQualifierInfo::qualifier
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___qualifier_3;

public:
	inline static int32_t get_offset_of_policyQualifierId_2() { return static_cast<int32_t>(offsetof(PolicyQualifierInfo_t5468F83F74055851E359E3796A1C70B50AF91C13, ___policyQualifierId_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_policyQualifierId_2() const { return ___policyQualifierId_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_policyQualifierId_2() { return &___policyQualifierId_2; }
	inline void set_policyQualifierId_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___policyQualifierId_2 = value;
		Il2CppCodeGenWriteBarrier((&___policyQualifierId_2), value);
	}

	inline static int32_t get_offset_of_qualifier_3() { return static_cast<int32_t>(offsetof(PolicyQualifierInfo_t5468F83F74055851E359E3796A1C70B50AF91C13, ___qualifier_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_qualifier_3() const { return ___qualifier_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_qualifier_3() { return &___qualifier_3; }
	inline void set_qualifier_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___qualifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___qualifier_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLICYQUALIFIERINFO_T5468F83F74055851E359E3796A1C70B50AF91C13_H
#ifndef PRIVATEKEYUSAGEPERIOD_TE025B41B5FEC729171D2FCB22F69C72984B6FD9E_H
#define PRIVATEKEYUSAGEPERIOD_TE025B41B5FEC729171D2FCB22F69C72984B6FD9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PrivateKeyUsagePeriod
struct  PrivateKeyUsagePeriod_tE025B41B5FEC729171D2FCB22F69C72984B6FD9E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PrivateKeyUsagePeriod::_notBefore
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ____notBefore_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PrivateKeyUsagePeriod::_notAfter
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ____notAfter_3;

public:
	inline static int32_t get_offset_of__notBefore_2() { return static_cast<int32_t>(offsetof(PrivateKeyUsagePeriod_tE025B41B5FEC729171D2FCB22F69C72984B6FD9E, ____notBefore_2)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get__notBefore_2() const { return ____notBefore_2; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of__notBefore_2() { return &____notBefore_2; }
	inline void set__notBefore_2(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		____notBefore_2 = value;
		Il2CppCodeGenWriteBarrier((&____notBefore_2), value);
	}

	inline static int32_t get_offset_of__notAfter_3() { return static_cast<int32_t>(offsetof(PrivateKeyUsagePeriod_tE025B41B5FEC729171D2FCB22F69C72984B6FD9E, ____notAfter_3)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get__notAfter_3() const { return ____notAfter_3; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of__notAfter_3() { return &____notAfter_3; }
	inline void set__notAfter_3(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		____notAfter_3 = value;
		Il2CppCodeGenWriteBarrier((&____notAfter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVATEKEYUSAGEPERIOD_TE025B41B5FEC729171D2FCB22F69C72984B6FD9E_H
#ifndef BIOMETRICDATA_T1CD2E01651D7A1B51ED11D258B792B754E0A2DE4_H
#define BIOMETRICDATA_T1CD2E01651D7A1B51ED11D258B792B754E0A2DE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.BiometricData
struct  BiometricData_t1CD2E01651D7A1B51ED11D258B792B754E0A2DE4  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.TypeOfBiometricData BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.BiometricData::typeOfBiometricData
	TypeOfBiometricData_t7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4 * ___typeOfBiometricData_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.BiometricData::hashAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___hashAlgorithm_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.BiometricData::biometricDataHash
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___biometricDataHash_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.BiometricData::sourceDataUri
	DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * ___sourceDataUri_5;

public:
	inline static int32_t get_offset_of_typeOfBiometricData_2() { return static_cast<int32_t>(offsetof(BiometricData_t1CD2E01651D7A1B51ED11D258B792B754E0A2DE4, ___typeOfBiometricData_2)); }
	inline TypeOfBiometricData_t7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4 * get_typeOfBiometricData_2() const { return ___typeOfBiometricData_2; }
	inline TypeOfBiometricData_t7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4 ** get_address_of_typeOfBiometricData_2() { return &___typeOfBiometricData_2; }
	inline void set_typeOfBiometricData_2(TypeOfBiometricData_t7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4 * value)
	{
		___typeOfBiometricData_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeOfBiometricData_2), value);
	}

	inline static int32_t get_offset_of_hashAlgorithm_3() { return static_cast<int32_t>(offsetof(BiometricData_t1CD2E01651D7A1B51ED11D258B792B754E0A2DE4, ___hashAlgorithm_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_hashAlgorithm_3() const { return ___hashAlgorithm_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_hashAlgorithm_3() { return &___hashAlgorithm_3; }
	inline void set_hashAlgorithm_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___hashAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___hashAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_biometricDataHash_4() { return static_cast<int32_t>(offsetof(BiometricData_t1CD2E01651D7A1B51ED11D258B792B754E0A2DE4, ___biometricDataHash_4)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_biometricDataHash_4() const { return ___biometricDataHash_4; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_biometricDataHash_4() { return &___biometricDataHash_4; }
	inline void set_biometricDataHash_4(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___biometricDataHash_4 = value;
		Il2CppCodeGenWriteBarrier((&___biometricDataHash_4), value);
	}

	inline static int32_t get_offset_of_sourceDataUri_5() { return static_cast<int32_t>(offsetof(BiometricData_t1CD2E01651D7A1B51ED11D258B792B754E0A2DE4, ___sourceDataUri_5)); }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * get_sourceDataUri_5() const { return ___sourceDataUri_5; }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE ** get_address_of_sourceDataUri_5() { return &___sourceDataUri_5; }
	inline void set_sourceDataUri_5(DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * value)
	{
		___sourceDataUri_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceDataUri_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIOMETRICDATA_T1CD2E01651D7A1B51ED11D258B792B754E0A2DE4_H
#ifndef ISO4217CURRENCYCODE_TA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E_H
#define ISO4217CURRENCYCODE_TA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.Iso4217CurrencyCode
struct  Iso4217CurrencyCode_tA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.Iso4217CurrencyCode::obj
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___obj_5;

public:
	inline static int32_t get_offset_of_obj_5() { return static_cast<int32_t>(offsetof(Iso4217CurrencyCode_tA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E, ___obj_5)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_obj_5() const { return ___obj_5; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_obj_5() { return &___obj_5; }
	inline void set_obj_5(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___obj_5 = value;
		Il2CppCodeGenWriteBarrier((&___obj_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISO4217CURRENCYCODE_TA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E_H
#ifndef MONETARYVALUE_TBE0AC4C885422EEF9CD262C5AA058E5CC935F1E0_H
#define MONETARYVALUE_TBE0AC4C885422EEF9CD262C5AA058E5CC935F1E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.MonetaryValue
struct  MonetaryValue_tBE0AC4C885422EEF9CD262C5AA058E5CC935F1E0  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.Iso4217CurrencyCode BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.MonetaryValue::currency
	Iso4217CurrencyCode_tA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E * ___currency_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.MonetaryValue::amount
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___amount_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.MonetaryValue::exponent
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___exponent_4;

public:
	inline static int32_t get_offset_of_currency_2() { return static_cast<int32_t>(offsetof(MonetaryValue_tBE0AC4C885422EEF9CD262C5AA058E5CC935F1E0, ___currency_2)); }
	inline Iso4217CurrencyCode_tA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E * get_currency_2() const { return ___currency_2; }
	inline Iso4217CurrencyCode_tA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E ** get_address_of_currency_2() { return &___currency_2; }
	inline void set_currency_2(Iso4217CurrencyCode_tA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E * value)
	{
		___currency_2 = value;
		Il2CppCodeGenWriteBarrier((&___currency_2), value);
	}

	inline static int32_t get_offset_of_amount_3() { return static_cast<int32_t>(offsetof(MonetaryValue_tBE0AC4C885422EEF9CD262C5AA058E5CC935F1E0, ___amount_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_amount_3() const { return ___amount_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_amount_3() { return &___amount_3; }
	inline void set_amount_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___amount_3 = value;
		Il2CppCodeGenWriteBarrier((&___amount_3), value);
	}

	inline static int32_t get_offset_of_exponent_4() { return static_cast<int32_t>(offsetof(MonetaryValue_tBE0AC4C885422EEF9CD262C5AA058E5CC935F1E0, ___exponent_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_exponent_4() const { return ___exponent_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_exponent_4() { return &___exponent_4; }
	inline void set_exponent_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___exponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___exponent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONETARYVALUE_TBE0AC4C885422EEF9CD262C5AA058E5CC935F1E0_H
#ifndef QCSTATEMENT_T6D61FB1103E3DDE4A73AB9062B813E46EF20F963_H
#define QCSTATEMENT_T6D61FB1103E3DDE4A73AB9062B813E46EF20F963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.QCStatement
struct  QCStatement_t6D61FB1103E3DDE4A73AB9062B813E46EF20F963  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.QCStatement::qcStatementId
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___qcStatementId_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.QCStatement::qcStatementInfo
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___qcStatementInfo_3;

public:
	inline static int32_t get_offset_of_qcStatementId_2() { return static_cast<int32_t>(offsetof(QCStatement_t6D61FB1103E3DDE4A73AB9062B813E46EF20F963, ___qcStatementId_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_qcStatementId_2() const { return ___qcStatementId_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_qcStatementId_2() { return &___qcStatementId_2; }
	inline void set_qcStatementId_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___qcStatementId_2 = value;
		Il2CppCodeGenWriteBarrier((&___qcStatementId_2), value);
	}

	inline static int32_t get_offset_of_qcStatementInfo_3() { return static_cast<int32_t>(offsetof(QCStatement_t6D61FB1103E3DDE4A73AB9062B813E46EF20F963, ___qcStatementInfo_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_qcStatementInfo_3() const { return ___qcStatementInfo_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_qcStatementInfo_3() { return &___qcStatementInfo_3; }
	inline void set_qcStatementInfo_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___qcStatementInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___qcStatementInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QCSTATEMENT_T6D61FB1103E3DDE4A73AB9062B813E46EF20F963_H
#ifndef SEMANTICSINFORMATION_T0CCBC07FAFA758E2C154DFBEAF5E05EF3C733BA9_H
#define SEMANTICSINFORMATION_T0CCBC07FAFA758E2C154DFBEAF5E05EF3C733BA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.SemanticsInformation
struct  SemanticsInformation_t0CCBC07FAFA758E2C154DFBEAF5E05EF3C733BA9  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.SemanticsInformation::semanticsIdentifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___semanticsIdentifier_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.SemanticsInformation::nameRegistrationAuthorities
	GeneralNameU5BU5D_t8C8E87C7B1F2854AD266B704A0BA432004D2DC73* ___nameRegistrationAuthorities_3;

public:
	inline static int32_t get_offset_of_semanticsIdentifier_2() { return static_cast<int32_t>(offsetof(SemanticsInformation_t0CCBC07FAFA758E2C154DFBEAF5E05EF3C733BA9, ___semanticsIdentifier_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_semanticsIdentifier_2() const { return ___semanticsIdentifier_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_semanticsIdentifier_2() { return &___semanticsIdentifier_2; }
	inline void set_semanticsIdentifier_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___semanticsIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___semanticsIdentifier_2), value);
	}

	inline static int32_t get_offset_of_nameRegistrationAuthorities_3() { return static_cast<int32_t>(offsetof(SemanticsInformation_t0CCBC07FAFA758E2C154DFBEAF5E05EF3C733BA9, ___nameRegistrationAuthorities_3)); }
	inline GeneralNameU5BU5D_t8C8E87C7B1F2854AD266B704A0BA432004D2DC73* get_nameRegistrationAuthorities_3() const { return ___nameRegistrationAuthorities_3; }
	inline GeneralNameU5BU5D_t8C8E87C7B1F2854AD266B704A0BA432004D2DC73** get_address_of_nameRegistrationAuthorities_3() { return &___nameRegistrationAuthorities_3; }
	inline void set_nameRegistrationAuthorities_3(GeneralNameU5BU5D_t8C8E87C7B1F2854AD266B704A0BA432004D2DC73* value)
	{
		___nameRegistrationAuthorities_3 = value;
		Il2CppCodeGenWriteBarrier((&___nameRegistrationAuthorities_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEMANTICSINFORMATION_T0CCBC07FAFA758E2C154DFBEAF5E05EF3C733BA9_H
#ifndef TYPEOFBIOMETRICDATA_T7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4_H
#define TYPEOFBIOMETRICDATA_T7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.TypeOfBiometricData
struct  TypeOfBiometricData_t7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Qualified.TypeOfBiometricData::obj
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___obj_4;

public:
	inline static int32_t get_offset_of_obj_4() { return static_cast<int32_t>(offsetof(TypeOfBiometricData_t7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4, ___obj_4)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_obj_4() const { return ___obj_4; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_obj_4() { return &___obj_4; }
	inline void set_obj_4(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___obj_4 = value;
		Il2CppCodeGenWriteBarrier((&___obj_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEOFBIOMETRICDATA_T7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4_H
#ifndef ROLESYNTAX_T113AB5191C6ED1A79005F820EF78505BC90A79A1_H
#define ROLESYNTAX_T113AB5191C6ED1A79005F820EF78505BC90A79A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.RoleSyntax
struct  RoleSyntax_t113AB5191C6ED1A79005F820EF78505BC90A79A1  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.RoleSyntax::roleAuthority
	GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * ___roleAuthority_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.RoleSyntax::roleName
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___roleName_3;

public:
	inline static int32_t get_offset_of_roleAuthority_2() { return static_cast<int32_t>(offsetof(RoleSyntax_t113AB5191C6ED1A79005F820EF78505BC90A79A1, ___roleAuthority_2)); }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * get_roleAuthority_2() const { return ___roleAuthority_2; }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD ** get_address_of_roleAuthority_2() { return &___roleAuthority_2; }
	inline void set_roleAuthority_2(GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * value)
	{
		___roleAuthority_2 = value;
		Il2CppCodeGenWriteBarrier((&___roleAuthority_2), value);
	}

	inline static int32_t get_offset_of_roleName_3() { return static_cast<int32_t>(offsetof(RoleSyntax_t113AB5191C6ED1A79005F820EF78505BC90A79A1, ___roleName_3)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_roleName_3() const { return ___roleName_3; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_roleName_3() { return &___roleName_3; }
	inline void set_roleName_3(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___roleName_3 = value;
		Il2CppCodeGenWriteBarrier((&___roleName_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROLESYNTAX_T113AB5191C6ED1A79005F820EF78505BC90A79A1_H
#ifndef RSAPUBLICKEYSTRUCTURE_T0E62415F89EE299A3B61B467F144887987E6B63E_H
#define RSAPUBLICKEYSTRUCTURE_T0E62415F89EE299A3B61B467F144887987E6B63E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.RsaPublicKeyStructure
struct  RsaPublicKeyStructure_t0E62415F89EE299A3B61B467F144887987E6B63E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.RsaPublicKeyStructure::modulus
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___modulus_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.RsaPublicKeyStructure::publicExponent
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___publicExponent_3;

public:
	inline static int32_t get_offset_of_modulus_2() { return static_cast<int32_t>(offsetof(RsaPublicKeyStructure_t0E62415F89EE299A3B61B467F144887987E6B63E, ___modulus_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_modulus_2() const { return ___modulus_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_modulus_2() { return &___modulus_2; }
	inline void set_modulus_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___modulus_2 = value;
		Il2CppCodeGenWriteBarrier((&___modulus_2), value);
	}

	inline static int32_t get_offset_of_publicExponent_3() { return static_cast<int32_t>(offsetof(RsaPublicKeyStructure_t0E62415F89EE299A3B61B467F144887987E6B63E, ___publicExponent_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_publicExponent_3() const { return ___publicExponent_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_publicExponent_3() { return &___publicExponent_3; }
	inline void set_publicExponent_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___publicExponent_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicExponent_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAPUBLICKEYSTRUCTURE_T0E62415F89EE299A3B61B467F144887987E6B63E_H
#ifndef NAMEORPSEUDONYM_T0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35_H
#define NAMEORPSEUDONYM_T0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.NameOrPseudonym
struct  NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.NameOrPseudonym::pseudonym
	DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * ___pseudonym_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.NameOrPseudonym::surname
	DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * ___surname_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.NameOrPseudonym::givenName
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___givenName_4;

public:
	inline static int32_t get_offset_of_pseudonym_2() { return static_cast<int32_t>(offsetof(NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35, ___pseudonym_2)); }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * get_pseudonym_2() const { return ___pseudonym_2; }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B ** get_address_of_pseudonym_2() { return &___pseudonym_2; }
	inline void set_pseudonym_2(DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * value)
	{
		___pseudonym_2 = value;
		Il2CppCodeGenWriteBarrier((&___pseudonym_2), value);
	}

	inline static int32_t get_offset_of_surname_3() { return static_cast<int32_t>(offsetof(NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35, ___surname_3)); }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * get_surname_3() const { return ___surname_3; }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B ** get_address_of_surname_3() { return &___surname_3; }
	inline void set_surname_3(DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * value)
	{
		___surname_3 = value;
		Il2CppCodeGenWriteBarrier((&___surname_3), value);
	}

	inline static int32_t get_offset_of_givenName_4() { return static_cast<int32_t>(offsetof(NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35, ___givenName_4)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_givenName_4() const { return ___givenName_4; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_givenName_4() { return &___givenName_4; }
	inline void set_givenName_4(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___givenName_4 = value;
		Il2CppCodeGenWriteBarrier((&___givenName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEORPSEUDONYM_T0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35_H
#ifndef PERSONALDATA_TEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078_H
#define PERSONALDATA_TEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.PersonalData
struct  PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.NameOrPseudonym BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.PersonalData::nameOrPseudonym
	NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35 * ___nameOrPseudonym_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.PersonalData::nameDistinguisher
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___nameDistinguisher_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.PersonalData::dateOfBirth
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___dateOfBirth_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.PersonalData::placeOfBirth
	DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * ___placeOfBirth_5;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.PersonalData::gender
	String_t* ___gender_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SigI.PersonalData::postalAddress
	DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * ___postalAddress_7;

public:
	inline static int32_t get_offset_of_nameOrPseudonym_2() { return static_cast<int32_t>(offsetof(PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078, ___nameOrPseudonym_2)); }
	inline NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35 * get_nameOrPseudonym_2() const { return ___nameOrPseudonym_2; }
	inline NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35 ** get_address_of_nameOrPseudonym_2() { return &___nameOrPseudonym_2; }
	inline void set_nameOrPseudonym_2(NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35 * value)
	{
		___nameOrPseudonym_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameOrPseudonym_2), value);
	}

	inline static int32_t get_offset_of_nameDistinguisher_3() { return static_cast<int32_t>(offsetof(PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078, ___nameDistinguisher_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_nameDistinguisher_3() const { return ___nameDistinguisher_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_nameDistinguisher_3() { return &___nameDistinguisher_3; }
	inline void set_nameDistinguisher_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___nameDistinguisher_3 = value;
		Il2CppCodeGenWriteBarrier((&___nameDistinguisher_3), value);
	}

	inline static int32_t get_offset_of_dateOfBirth_4() { return static_cast<int32_t>(offsetof(PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078, ___dateOfBirth_4)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_dateOfBirth_4() const { return ___dateOfBirth_4; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_dateOfBirth_4() { return &___dateOfBirth_4; }
	inline void set_dateOfBirth_4(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___dateOfBirth_4 = value;
		Il2CppCodeGenWriteBarrier((&___dateOfBirth_4), value);
	}

	inline static int32_t get_offset_of_placeOfBirth_5() { return static_cast<int32_t>(offsetof(PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078, ___placeOfBirth_5)); }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * get_placeOfBirth_5() const { return ___placeOfBirth_5; }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B ** get_address_of_placeOfBirth_5() { return &___placeOfBirth_5; }
	inline void set_placeOfBirth_5(DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * value)
	{
		___placeOfBirth_5 = value;
		Il2CppCodeGenWriteBarrier((&___placeOfBirth_5), value);
	}

	inline static int32_t get_offset_of_gender_6() { return static_cast<int32_t>(offsetof(PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078, ___gender_6)); }
	inline String_t* get_gender_6() const { return ___gender_6; }
	inline String_t** get_address_of_gender_6() { return &___gender_6; }
	inline void set_gender_6(String_t* value)
	{
		___gender_6 = value;
		Il2CppCodeGenWriteBarrier((&___gender_6), value);
	}

	inline static int32_t get_offset_of_postalAddress_7() { return static_cast<int32_t>(offsetof(PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078, ___postalAddress_7)); }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * get_postalAddress_7() const { return ___postalAddress_7; }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B ** get_address_of_postalAddress_7() { return &___postalAddress_7; }
	inline void set_postalAddress_7(DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * value)
	{
		___postalAddress_7 = value;
		Il2CppCodeGenWriteBarrier((&___postalAddress_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSONALDATA_TEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078_H
#ifndef SUBJECTDIRECTORYATTRIBUTES_T306D51B35F66F7C25F14845D6816FC274FFE4A9A_H
#define SUBJECTDIRECTORYATTRIBUTES_T306D51B35F66F7C25F14845D6816FC274FFE4A9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectDirectoryAttributes
struct  SubjectDirectoryAttributes_t306D51B35F66F7C25F14845D6816FC274FFE4A9A  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectDirectoryAttributes::attributes
	RuntimeObject* ___attributes_2;

public:
	inline static int32_t get_offset_of_attributes_2() { return static_cast<int32_t>(offsetof(SubjectDirectoryAttributes_t306D51B35F66F7C25F14845D6816FC274FFE4A9A, ___attributes_2)); }
	inline RuntimeObject* get_attributes_2() const { return ___attributes_2; }
	inline RuntimeObject** get_address_of_attributes_2() { return &___attributes_2; }
	inline void set_attributes_2(RuntimeObject* value)
	{
		___attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTDIRECTORYATTRIBUTES_T306D51B35F66F7C25F14845D6816FC274FFE4A9A_H
#ifndef SUBJECTKEYIDENTIFIER_TF922F6F32716AB7578A5809B185B104DD0D538D7_H
#define SUBJECTKEYIDENTIFIER_TF922F6F32716AB7578A5809B185B104DD0D538D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectKeyIdentifier
struct  SubjectKeyIdentifier_tF922F6F32716AB7578A5809B185B104DD0D538D7  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectKeyIdentifier::keyIdentifier
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___keyIdentifier_2;

public:
	inline static int32_t get_offset_of_keyIdentifier_2() { return static_cast<int32_t>(offsetof(SubjectKeyIdentifier_tF922F6F32716AB7578A5809B185B104DD0D538D7, ___keyIdentifier_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_keyIdentifier_2() const { return ___keyIdentifier_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_keyIdentifier_2() { return &___keyIdentifier_2; }
	inline void set_keyIdentifier_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___keyIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyIdentifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTKEYIDENTIFIER_TF922F6F32716AB7578A5809B185B104DD0D538D7_H
#ifndef SUBJECTPUBLICKEYINFO_T881A355EADDE5414A74A5F5D2FD4A64D21D91F90_H
#define SUBJECTPUBLICKEYINFO_T881A355EADDE5414A74A5F5D2FD4A64D21D91F90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo
struct  SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo::algID
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algID_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo::keyData
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___keyData_3;

public:
	inline static int32_t get_offset_of_algID_2() { return static_cast<int32_t>(offsetof(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90, ___algID_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algID_2() const { return ___algID_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algID_2() { return &___algID_2; }
	inline void set_algID_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algID_2 = value;
		Il2CppCodeGenWriteBarrier((&___algID_2), value);
	}

	inline static int32_t get_offset_of_keyData_3() { return static_cast<int32_t>(offsetof(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90, ___keyData_3)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_keyData_3() const { return ___keyData_3; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_keyData_3() { return &___keyData_3; }
	inline void set_keyData_3(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___keyData_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTPUBLICKEYINFO_T881A355EADDE5414A74A5F5D2FD4A64D21D91F90_H
#ifndef TARGET_TFB39652E1D3B26D5335C6681A7F22D342AE8B2C9_H
#define TARGET_TFB39652E1D3B26D5335C6681A7F22D342AE8B2C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Target
struct  Target_tFB39652E1D3B26D5335C6681A7F22D342AE8B2C9  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Target::targetName
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___targetName_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Target::targetGroup
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___targetGroup_3;

public:
	inline static int32_t get_offset_of_targetName_2() { return static_cast<int32_t>(offsetof(Target_tFB39652E1D3B26D5335C6681A7F22D342AE8B2C9, ___targetName_2)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_targetName_2() const { return ___targetName_2; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_targetName_2() { return &___targetName_2; }
	inline void set_targetName_2(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___targetName_2 = value;
		Il2CppCodeGenWriteBarrier((&___targetName_2), value);
	}

	inline static int32_t get_offset_of_targetGroup_3() { return static_cast<int32_t>(offsetof(Target_tFB39652E1D3B26D5335C6681A7F22D342AE8B2C9, ___targetGroup_3)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_targetGroup_3() const { return ___targetGroup_3; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_targetGroup_3() { return &___targetGroup_3; }
	inline void set_targetGroup_3(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___targetGroup_3 = value;
		Il2CppCodeGenWriteBarrier((&___targetGroup_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGET_TFB39652E1D3B26D5335C6681A7F22D342AE8B2C9_H
#ifndef TARGETINFORMATION_T449ACFE048A7DB35DAEEA7D349C3A12E28716918_H
#define TARGETINFORMATION_T449ACFE048A7DB35DAEEA7D349C3A12E28716918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TargetInformation
struct  TargetInformation_t449ACFE048A7DB35DAEEA7D349C3A12E28716918  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TargetInformation::targets
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___targets_2;

public:
	inline static int32_t get_offset_of_targets_2() { return static_cast<int32_t>(offsetof(TargetInformation_t449ACFE048A7DB35DAEEA7D349C3A12E28716918, ___targets_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_targets_2() const { return ___targets_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_targets_2() { return &___targets_2; }
	inline void set_targets_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___targets_2 = value;
		Il2CppCodeGenWriteBarrier((&___targets_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETINFORMATION_T449ACFE048A7DB35DAEEA7D349C3A12E28716918_H
#ifndef TARGETS_T521998AA6A08390718858AEE853968F1EE917EA2_H
#define TARGETS_T521998AA6A08390718858AEE853968F1EE917EA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Targets
struct  Targets_t521998AA6A08390718858AEE853968F1EE917EA2  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Targets::targets
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___targets_2;

public:
	inline static int32_t get_offset_of_targets_2() { return static_cast<int32_t>(offsetof(Targets_t521998AA6A08390718858AEE853968F1EE917EA2, ___targets_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_targets_2() const { return ___targets_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_targets_2() { return &___targets_2; }
	inline void set_targets_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___targets_2 = value;
		Il2CppCodeGenWriteBarrier((&___targets_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETS_T521998AA6A08390718858AEE853968F1EE917EA2_H
#ifndef TBSCERTIFICATELIST_T2AB061DB74344163C3090B032EE1E4657B9EEDBC_H
#define TBSCERTIFICATELIST_T2AB061DB74344163C3090B032EE1E4657B9EEDBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList
struct  TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList::seq
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___seq_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList::signature
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signature_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList::issuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___issuer_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList::thisUpdate
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___thisUpdate_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList::nextUpdate
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___nextUpdate_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList::revokedCertificates
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___revokedCertificates_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateList::crlExtensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___crlExtensions_9;

public:
	inline static int32_t get_offset_of_seq_2() { return static_cast<int32_t>(offsetof(TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC, ___seq_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_seq_2() const { return ___seq_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_seq_2() { return &___seq_2; }
	inline void set_seq_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___seq_2 = value;
		Il2CppCodeGenWriteBarrier((&___seq_2), value);
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC, ___version_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_3() const { return ___version_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_3 = value;
		Il2CppCodeGenWriteBarrier((&___version_3), value);
	}

	inline static int32_t get_offset_of_signature_4() { return static_cast<int32_t>(offsetof(TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC, ___signature_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signature_4() const { return ___signature_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signature_4() { return &___signature_4; }
	inline void set_signature_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signature_4 = value;
		Il2CppCodeGenWriteBarrier((&___signature_4), value);
	}

	inline static int32_t get_offset_of_issuer_5() { return static_cast<int32_t>(offsetof(TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC, ___issuer_5)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_issuer_5() const { return ___issuer_5; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_issuer_5() { return &___issuer_5; }
	inline void set_issuer_5(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___issuer_5 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_5), value);
	}

	inline static int32_t get_offset_of_thisUpdate_6() { return static_cast<int32_t>(offsetof(TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC, ___thisUpdate_6)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_thisUpdate_6() const { return ___thisUpdate_6; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_thisUpdate_6() { return &___thisUpdate_6; }
	inline void set_thisUpdate_6(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___thisUpdate_6 = value;
		Il2CppCodeGenWriteBarrier((&___thisUpdate_6), value);
	}

	inline static int32_t get_offset_of_nextUpdate_7() { return static_cast<int32_t>(offsetof(TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC, ___nextUpdate_7)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_nextUpdate_7() const { return ___nextUpdate_7; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_nextUpdate_7() { return &___nextUpdate_7; }
	inline void set_nextUpdate_7(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___nextUpdate_7 = value;
		Il2CppCodeGenWriteBarrier((&___nextUpdate_7), value);
	}

	inline static int32_t get_offset_of_revokedCertificates_8() { return static_cast<int32_t>(offsetof(TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC, ___revokedCertificates_8)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_revokedCertificates_8() const { return ___revokedCertificates_8; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_revokedCertificates_8() { return &___revokedCertificates_8; }
	inline void set_revokedCertificates_8(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___revokedCertificates_8 = value;
		Il2CppCodeGenWriteBarrier((&___revokedCertificates_8), value);
	}

	inline static int32_t get_offset_of_crlExtensions_9() { return static_cast<int32_t>(offsetof(TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC, ___crlExtensions_9)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_crlExtensions_9() const { return ___crlExtensions_9; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_crlExtensions_9() { return &___crlExtensions_9; }
	inline void set_crlExtensions_9(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___crlExtensions_9 = value;
		Il2CppCodeGenWriteBarrier((&___crlExtensions_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TBSCERTIFICATELIST_T2AB061DB74344163C3090B032EE1E4657B9EEDBC_H
#ifndef TBSCERTIFICATESTRUCTURE_T3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39_H
#define TBSCERTIFICATESTRUCTURE_T3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure
struct  TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::seq
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___seq_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::serialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serialNumber_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::signature
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signature_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::issuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___issuer_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::startDate
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___startDate_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::endDate
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___endDate_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::subject
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___subject_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::subjectPublicKeyInfo
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___subjectPublicKeyInfo_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::issuerUniqueID
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___issuerUniqueID_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::subjectUniqueID
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___subjectUniqueID_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_13;

public:
	inline static int32_t get_offset_of_seq_2() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___seq_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_seq_2() const { return ___seq_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_seq_2() { return &___seq_2; }
	inline void set_seq_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___seq_2 = value;
		Il2CppCodeGenWriteBarrier((&___seq_2), value);
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___version_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_3() const { return ___version_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_3 = value;
		Il2CppCodeGenWriteBarrier((&___version_3), value);
	}

	inline static int32_t get_offset_of_serialNumber_4() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___serialNumber_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serialNumber_4() const { return ___serialNumber_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serialNumber_4() { return &___serialNumber_4; }
	inline void set_serialNumber_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serialNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_4), value);
	}

	inline static int32_t get_offset_of_signature_5() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___signature_5)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signature_5() const { return ___signature_5; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signature_5() { return &___signature_5; }
	inline void set_signature_5(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signature_5 = value;
		Il2CppCodeGenWriteBarrier((&___signature_5), value);
	}

	inline static int32_t get_offset_of_issuer_6() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___issuer_6)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_issuer_6() const { return ___issuer_6; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_issuer_6() { return &___issuer_6; }
	inline void set_issuer_6(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___issuer_6 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_6), value);
	}

	inline static int32_t get_offset_of_startDate_7() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___startDate_7)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_startDate_7() const { return ___startDate_7; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_startDate_7() { return &___startDate_7; }
	inline void set_startDate_7(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___startDate_7 = value;
		Il2CppCodeGenWriteBarrier((&___startDate_7), value);
	}

	inline static int32_t get_offset_of_endDate_8() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___endDate_8)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_endDate_8() const { return ___endDate_8; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_endDate_8() { return &___endDate_8; }
	inline void set_endDate_8(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___endDate_8 = value;
		Il2CppCodeGenWriteBarrier((&___endDate_8), value);
	}

	inline static int32_t get_offset_of_subject_9() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___subject_9)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_subject_9() const { return ___subject_9; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_subject_9() { return &___subject_9; }
	inline void set_subject_9(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___subject_9 = value;
		Il2CppCodeGenWriteBarrier((&___subject_9), value);
	}

	inline static int32_t get_offset_of_subjectPublicKeyInfo_10() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___subjectPublicKeyInfo_10)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_subjectPublicKeyInfo_10() const { return ___subjectPublicKeyInfo_10; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_subjectPublicKeyInfo_10() { return &___subjectPublicKeyInfo_10; }
	inline void set_subjectPublicKeyInfo_10(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___subjectPublicKeyInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___subjectPublicKeyInfo_10), value);
	}

	inline static int32_t get_offset_of_issuerUniqueID_11() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___issuerUniqueID_11)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_issuerUniqueID_11() const { return ___issuerUniqueID_11; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_issuerUniqueID_11() { return &___issuerUniqueID_11; }
	inline void set_issuerUniqueID_11(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___issuerUniqueID_11 = value;
		Il2CppCodeGenWriteBarrier((&___issuerUniqueID_11), value);
	}

	inline static int32_t get_offset_of_subjectUniqueID_12() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___subjectUniqueID_12)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_subjectUniqueID_12() const { return ___subjectUniqueID_12; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_subjectUniqueID_12() { return &___subjectUniqueID_12; }
	inline void set_subjectUniqueID_12(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___subjectUniqueID_12 = value;
		Il2CppCodeGenWriteBarrier((&___subjectUniqueID_12), value);
	}

	inline static int32_t get_offset_of_extensions_13() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39, ___extensions_13)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_13() const { return ___extensions_13; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_13() { return &___extensions_13; }
	inline void set_extensions_13(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_13 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TBSCERTIFICATESTRUCTURE_T3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39_H
#ifndef TIME_T74086E2BF904AA38D0EBD7CD379053B6F33C011A_H
#define TIME_T74086E2BF904AA38D0EBD7CD379053B6F33C011A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time
struct  Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time::time
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___time_2;

public:
	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A, ___time_2)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_time_2() const { return ___time_2; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___time_2 = value;
		Il2CppCodeGenWriteBarrier((&___time_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIME_T74086E2BF904AA38D0EBD7CD379053B6F33C011A_H
#ifndef USERNOTICE_T44B6430B6956C4427A107CF515B8AA4DA15D7310_H
#define USERNOTICE_T44B6430B6956C4427A107CF515B8AA4DA15D7310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.UserNotice
struct  UserNotice_t44B6430B6956C4427A107CF515B8AA4DA15D7310  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.NoticeReference BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.UserNotice::noticeRef
	NoticeReference_t881A2D3508F9AA3C2EEC071137E809179E643234 * ___noticeRef_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DisplayText BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.UserNotice::explicitText
	DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB * ___explicitText_3;

public:
	inline static int32_t get_offset_of_noticeRef_2() { return static_cast<int32_t>(offsetof(UserNotice_t44B6430B6956C4427A107CF515B8AA4DA15D7310, ___noticeRef_2)); }
	inline NoticeReference_t881A2D3508F9AA3C2EEC071137E809179E643234 * get_noticeRef_2() const { return ___noticeRef_2; }
	inline NoticeReference_t881A2D3508F9AA3C2EEC071137E809179E643234 ** get_address_of_noticeRef_2() { return &___noticeRef_2; }
	inline void set_noticeRef_2(NoticeReference_t881A2D3508F9AA3C2EEC071137E809179E643234 * value)
	{
		___noticeRef_2 = value;
		Il2CppCodeGenWriteBarrier((&___noticeRef_2), value);
	}

	inline static int32_t get_offset_of_explicitText_3() { return static_cast<int32_t>(offsetof(UserNotice_t44B6430B6956C4427A107CF515B8AA4DA15D7310, ___explicitText_3)); }
	inline DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB * get_explicitText_3() const { return ___explicitText_3; }
	inline DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB ** get_address_of_explicitText_3() { return &___explicitText_3; }
	inline void set_explicitText_3(DisplayText_t35D77F340949841C4236212DDA1957F0BC4BD4CB * value)
	{
		___explicitText_3 = value;
		Il2CppCodeGenWriteBarrier((&___explicitText_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERNOTICE_T44B6430B6956C4427A107CF515B8AA4DA15D7310_H
#ifndef V2FORM_TD57A9480E55B3CC489C97D55EED79C0B27DF6496_H
#define V2FORM_TD57A9480E55B3CC489C97D55EED79C0B27DF6496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2Form
struct  V2Form_tD57A9480E55B3CC489C97D55EED79C0B27DF6496  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2Form::issuerName
	GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * ___issuerName_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2Form::baseCertificateID
	IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * ___baseCertificateID_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ObjectDigestInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2Form::objectDigestInfo
	ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F * ___objectDigestInfo_4;

public:
	inline static int32_t get_offset_of_issuerName_2() { return static_cast<int32_t>(offsetof(V2Form_tD57A9480E55B3CC489C97D55EED79C0B27DF6496, ___issuerName_2)); }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * get_issuerName_2() const { return ___issuerName_2; }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD ** get_address_of_issuerName_2() { return &___issuerName_2; }
	inline void set_issuerName_2(GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * value)
	{
		___issuerName_2 = value;
		Il2CppCodeGenWriteBarrier((&___issuerName_2), value);
	}

	inline static int32_t get_offset_of_baseCertificateID_3() { return static_cast<int32_t>(offsetof(V2Form_tD57A9480E55B3CC489C97D55EED79C0B27DF6496, ___baseCertificateID_3)); }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * get_baseCertificateID_3() const { return ___baseCertificateID_3; }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C ** get_address_of_baseCertificateID_3() { return &___baseCertificateID_3; }
	inline void set_baseCertificateID_3(IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * value)
	{
		___baseCertificateID_3 = value;
		Il2CppCodeGenWriteBarrier((&___baseCertificateID_3), value);
	}

	inline static int32_t get_offset_of_objectDigestInfo_4() { return static_cast<int32_t>(offsetof(V2Form_tD57A9480E55B3CC489C97D55EED79C0B27DF6496, ___objectDigestInfo_4)); }
	inline ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F * get_objectDigestInfo_4() const { return ___objectDigestInfo_4; }
	inline ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F ** get_address_of_objectDigestInfo_4() { return &___objectDigestInfo_4; }
	inline void set_objectDigestInfo_4(ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F * value)
	{
		___objectDigestInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___objectDigestInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // V2FORM_TD57A9480E55B3CC489C97D55EED79C0B27DF6496_H
#ifndef X509CERTIFICATESTRUCTURE_T76D9AE98B45AAF1A06FE9E795E97503122F17242_H
#define X509CERTIFICATESTRUCTURE_T76D9AE98B45AAF1A06FE9E795E97503122F17242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure
struct  X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure::tbsCert
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 * ___tbsCert_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure::sigAlgID
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___sigAlgID_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure::sig
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___sig_4;

public:
	inline static int32_t get_offset_of_tbsCert_2() { return static_cast<int32_t>(offsetof(X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242, ___tbsCert_2)); }
	inline TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 * get_tbsCert_2() const { return ___tbsCert_2; }
	inline TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 ** get_address_of_tbsCert_2() { return &___tbsCert_2; }
	inline void set_tbsCert_2(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 * value)
	{
		___tbsCert_2 = value;
		Il2CppCodeGenWriteBarrier((&___tbsCert_2), value);
	}

	inline static int32_t get_offset_of_sigAlgID_3() { return static_cast<int32_t>(offsetof(X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242, ___sigAlgID_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_sigAlgID_3() const { return ___sigAlgID_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_sigAlgID_3() { return &___sigAlgID_3; }
	inline void set_sigAlgID_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___sigAlgID_3 = value;
		Il2CppCodeGenWriteBarrier((&___sigAlgID_3), value);
	}

	inline static int32_t get_offset_of_sig_4() { return static_cast<int32_t>(offsetof(X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242, ___sig_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_sig_4() const { return ___sig_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_sig_4() { return &___sig_4; }
	inline void set_sig_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___sig_4 = value;
		Il2CppCodeGenWriteBarrier((&___sig_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATESTRUCTURE_T76D9AE98B45AAF1A06FE9E795E97503122F17242_H
#ifndef X509DEFAULTENTRYCONVERTER_T7BA3BF838D6E3FD8EFC8A041F70D7A487C97CFBA_H
#define X509DEFAULTENTRYCONVERTER_T7BA3BF838D6E3FD8EFC8A041F70D7A487C97CFBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509DefaultEntryConverter
struct  X509DefaultEntryConverter_t7BA3BF838D6E3FD8EFC8A041F70D7A487C97CFBA  : public X509NameEntryConverter_t007AA731425354FFC770B335EB86EC1126045D28
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509DEFAULTENTRYCONVERTER_T7BA3BF838D6E3FD8EFC8A041F70D7A487C97CFBA_H
#ifndef X509EXTENSIONS_TFBB7387546053A219E86A448C813BF7042984B02_H
#define X509EXTENSIONS_TFBB7387546053A219E86A448C813BF7042984B02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions
struct  X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::extensions
	RuntimeObject* ___extensions_34;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::ordering
	RuntimeObject* ___ordering_35;

public:
	inline static int32_t get_offset_of_extensions_34() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02, ___extensions_34)); }
	inline RuntimeObject* get_extensions_34() const { return ___extensions_34; }
	inline RuntimeObject** get_address_of_extensions_34() { return &___extensions_34; }
	inline void set_extensions_34(RuntimeObject* value)
	{
		___extensions_34 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_34), value);
	}

	inline static int32_t get_offset_of_ordering_35() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02, ___ordering_35)); }
	inline RuntimeObject* get_ordering_35() const { return ___ordering_35; }
	inline RuntimeObject** get_address_of_ordering_35() { return &___ordering_35; }
	inline void set_ordering_35(RuntimeObject* value)
	{
		___ordering_35 = value;
		Il2CppCodeGenWriteBarrier((&___ordering_35), value);
	}
};

struct X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::SubjectDirectoryAttributes
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SubjectDirectoryAttributes_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::SubjectKeyIdentifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SubjectKeyIdentifier_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::KeyUsage
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___KeyUsage_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::PrivateKeyUsagePeriod
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PrivateKeyUsagePeriod_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::SubjectAlternativeName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SubjectAlternativeName_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::IssuerAlternativeName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IssuerAlternativeName_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::BasicConstraints
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BasicConstraints_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::CrlNumber
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CrlNumber_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::ReasonCode
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ReasonCode_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::InstructionCode
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___InstructionCode_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::InvalidityDate
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___InvalidityDate_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::DeltaCrlIndicator
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DeltaCrlIndicator_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::IssuingDistributionPoint
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IssuingDistributionPoint_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::CertificateIssuer
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CertificateIssuer_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::NameConstraints
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NameConstraints_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::CrlDistributionPoints
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CrlDistributionPoints_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::CertificatePolicies
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CertificatePolicies_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::PolicyMappings
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PolicyMappings_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::AuthorityKeyIdentifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___AuthorityKeyIdentifier_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::PolicyConstraints
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PolicyConstraints_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::ExtendedKeyUsage
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ExtendedKeyUsage_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::FreshestCrl
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___FreshestCrl_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::InhibitAnyPolicy
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___InhibitAnyPolicy_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::AuthorityInfoAccess
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___AuthorityInfoAccess_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::SubjectInfoAccess
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SubjectInfoAccess_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::LogoType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___LogoType_27;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::BiometricInfo
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BiometricInfo_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::QCStatements
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___QCStatements_29;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::AuditIdentity
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___AuditIdentity_30;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::NoRevAvail
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NoRevAvail_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::TargetInformation
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___TargetInformation_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions::ExpiredCertsOnCrl
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ExpiredCertsOnCrl_33;

public:
	inline static int32_t get_offset_of_SubjectDirectoryAttributes_2() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___SubjectDirectoryAttributes_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SubjectDirectoryAttributes_2() const { return ___SubjectDirectoryAttributes_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SubjectDirectoryAttributes_2() { return &___SubjectDirectoryAttributes_2; }
	inline void set_SubjectDirectoryAttributes_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SubjectDirectoryAttributes_2 = value;
		Il2CppCodeGenWriteBarrier((&___SubjectDirectoryAttributes_2), value);
	}

	inline static int32_t get_offset_of_SubjectKeyIdentifier_3() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___SubjectKeyIdentifier_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SubjectKeyIdentifier_3() const { return ___SubjectKeyIdentifier_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SubjectKeyIdentifier_3() { return &___SubjectKeyIdentifier_3; }
	inline void set_SubjectKeyIdentifier_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SubjectKeyIdentifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___SubjectKeyIdentifier_3), value);
	}

	inline static int32_t get_offset_of_KeyUsage_4() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___KeyUsage_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_KeyUsage_4() const { return ___KeyUsage_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_KeyUsage_4() { return &___KeyUsage_4; }
	inline void set_KeyUsage_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___KeyUsage_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyUsage_4), value);
	}

	inline static int32_t get_offset_of_PrivateKeyUsagePeriod_5() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___PrivateKeyUsagePeriod_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PrivateKeyUsagePeriod_5() const { return ___PrivateKeyUsagePeriod_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PrivateKeyUsagePeriod_5() { return &___PrivateKeyUsagePeriod_5; }
	inline void set_PrivateKeyUsagePeriod_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PrivateKeyUsagePeriod_5 = value;
		Il2CppCodeGenWriteBarrier((&___PrivateKeyUsagePeriod_5), value);
	}

	inline static int32_t get_offset_of_SubjectAlternativeName_6() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___SubjectAlternativeName_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SubjectAlternativeName_6() const { return ___SubjectAlternativeName_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SubjectAlternativeName_6() { return &___SubjectAlternativeName_6; }
	inline void set_SubjectAlternativeName_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SubjectAlternativeName_6 = value;
		Il2CppCodeGenWriteBarrier((&___SubjectAlternativeName_6), value);
	}

	inline static int32_t get_offset_of_IssuerAlternativeName_7() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___IssuerAlternativeName_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IssuerAlternativeName_7() const { return ___IssuerAlternativeName_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IssuerAlternativeName_7() { return &___IssuerAlternativeName_7; }
	inline void set_IssuerAlternativeName_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IssuerAlternativeName_7 = value;
		Il2CppCodeGenWriteBarrier((&___IssuerAlternativeName_7), value);
	}

	inline static int32_t get_offset_of_BasicConstraints_8() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___BasicConstraints_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BasicConstraints_8() const { return ___BasicConstraints_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BasicConstraints_8() { return &___BasicConstraints_8; }
	inline void set_BasicConstraints_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BasicConstraints_8 = value;
		Il2CppCodeGenWriteBarrier((&___BasicConstraints_8), value);
	}

	inline static int32_t get_offset_of_CrlNumber_9() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___CrlNumber_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CrlNumber_9() const { return ___CrlNumber_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CrlNumber_9() { return &___CrlNumber_9; }
	inline void set_CrlNumber_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CrlNumber_9 = value;
		Il2CppCodeGenWriteBarrier((&___CrlNumber_9), value);
	}

	inline static int32_t get_offset_of_ReasonCode_10() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___ReasonCode_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ReasonCode_10() const { return ___ReasonCode_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ReasonCode_10() { return &___ReasonCode_10; }
	inline void set_ReasonCode_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ReasonCode_10 = value;
		Il2CppCodeGenWriteBarrier((&___ReasonCode_10), value);
	}

	inline static int32_t get_offset_of_InstructionCode_11() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___InstructionCode_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_InstructionCode_11() const { return ___InstructionCode_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_InstructionCode_11() { return &___InstructionCode_11; }
	inline void set_InstructionCode_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___InstructionCode_11 = value;
		Il2CppCodeGenWriteBarrier((&___InstructionCode_11), value);
	}

	inline static int32_t get_offset_of_InvalidityDate_12() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___InvalidityDate_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_InvalidityDate_12() const { return ___InvalidityDate_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_InvalidityDate_12() { return &___InvalidityDate_12; }
	inline void set_InvalidityDate_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___InvalidityDate_12 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidityDate_12), value);
	}

	inline static int32_t get_offset_of_DeltaCrlIndicator_13() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___DeltaCrlIndicator_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DeltaCrlIndicator_13() const { return ___DeltaCrlIndicator_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DeltaCrlIndicator_13() { return &___DeltaCrlIndicator_13; }
	inline void set_DeltaCrlIndicator_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DeltaCrlIndicator_13 = value;
		Il2CppCodeGenWriteBarrier((&___DeltaCrlIndicator_13), value);
	}

	inline static int32_t get_offset_of_IssuingDistributionPoint_14() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___IssuingDistributionPoint_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IssuingDistributionPoint_14() const { return ___IssuingDistributionPoint_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IssuingDistributionPoint_14() { return &___IssuingDistributionPoint_14; }
	inline void set_IssuingDistributionPoint_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IssuingDistributionPoint_14 = value;
		Il2CppCodeGenWriteBarrier((&___IssuingDistributionPoint_14), value);
	}

	inline static int32_t get_offset_of_CertificateIssuer_15() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___CertificateIssuer_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CertificateIssuer_15() const { return ___CertificateIssuer_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CertificateIssuer_15() { return &___CertificateIssuer_15; }
	inline void set_CertificateIssuer_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CertificateIssuer_15 = value;
		Il2CppCodeGenWriteBarrier((&___CertificateIssuer_15), value);
	}

	inline static int32_t get_offset_of_NameConstraints_16() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___NameConstraints_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NameConstraints_16() const { return ___NameConstraints_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NameConstraints_16() { return &___NameConstraints_16; }
	inline void set_NameConstraints_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NameConstraints_16 = value;
		Il2CppCodeGenWriteBarrier((&___NameConstraints_16), value);
	}

	inline static int32_t get_offset_of_CrlDistributionPoints_17() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___CrlDistributionPoints_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CrlDistributionPoints_17() const { return ___CrlDistributionPoints_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CrlDistributionPoints_17() { return &___CrlDistributionPoints_17; }
	inline void set_CrlDistributionPoints_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CrlDistributionPoints_17 = value;
		Il2CppCodeGenWriteBarrier((&___CrlDistributionPoints_17), value);
	}

	inline static int32_t get_offset_of_CertificatePolicies_18() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___CertificatePolicies_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CertificatePolicies_18() const { return ___CertificatePolicies_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CertificatePolicies_18() { return &___CertificatePolicies_18; }
	inline void set_CertificatePolicies_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CertificatePolicies_18 = value;
		Il2CppCodeGenWriteBarrier((&___CertificatePolicies_18), value);
	}

	inline static int32_t get_offset_of_PolicyMappings_19() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___PolicyMappings_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PolicyMappings_19() const { return ___PolicyMappings_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PolicyMappings_19() { return &___PolicyMappings_19; }
	inline void set_PolicyMappings_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PolicyMappings_19 = value;
		Il2CppCodeGenWriteBarrier((&___PolicyMappings_19), value);
	}

	inline static int32_t get_offset_of_AuthorityKeyIdentifier_20() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___AuthorityKeyIdentifier_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_AuthorityKeyIdentifier_20() const { return ___AuthorityKeyIdentifier_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_AuthorityKeyIdentifier_20() { return &___AuthorityKeyIdentifier_20; }
	inline void set_AuthorityKeyIdentifier_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___AuthorityKeyIdentifier_20 = value;
		Il2CppCodeGenWriteBarrier((&___AuthorityKeyIdentifier_20), value);
	}

	inline static int32_t get_offset_of_PolicyConstraints_21() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___PolicyConstraints_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PolicyConstraints_21() const { return ___PolicyConstraints_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PolicyConstraints_21() { return &___PolicyConstraints_21; }
	inline void set_PolicyConstraints_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PolicyConstraints_21 = value;
		Il2CppCodeGenWriteBarrier((&___PolicyConstraints_21), value);
	}

	inline static int32_t get_offset_of_ExtendedKeyUsage_22() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___ExtendedKeyUsage_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ExtendedKeyUsage_22() const { return ___ExtendedKeyUsage_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ExtendedKeyUsage_22() { return &___ExtendedKeyUsage_22; }
	inline void set_ExtendedKeyUsage_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ExtendedKeyUsage_22 = value;
		Il2CppCodeGenWriteBarrier((&___ExtendedKeyUsage_22), value);
	}

	inline static int32_t get_offset_of_FreshestCrl_23() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___FreshestCrl_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_FreshestCrl_23() const { return ___FreshestCrl_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_FreshestCrl_23() { return &___FreshestCrl_23; }
	inline void set_FreshestCrl_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___FreshestCrl_23 = value;
		Il2CppCodeGenWriteBarrier((&___FreshestCrl_23), value);
	}

	inline static int32_t get_offset_of_InhibitAnyPolicy_24() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___InhibitAnyPolicy_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_InhibitAnyPolicy_24() const { return ___InhibitAnyPolicy_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_InhibitAnyPolicy_24() { return &___InhibitAnyPolicy_24; }
	inline void set_InhibitAnyPolicy_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___InhibitAnyPolicy_24 = value;
		Il2CppCodeGenWriteBarrier((&___InhibitAnyPolicy_24), value);
	}

	inline static int32_t get_offset_of_AuthorityInfoAccess_25() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___AuthorityInfoAccess_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_AuthorityInfoAccess_25() const { return ___AuthorityInfoAccess_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_AuthorityInfoAccess_25() { return &___AuthorityInfoAccess_25; }
	inline void set_AuthorityInfoAccess_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___AuthorityInfoAccess_25 = value;
		Il2CppCodeGenWriteBarrier((&___AuthorityInfoAccess_25), value);
	}

	inline static int32_t get_offset_of_SubjectInfoAccess_26() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___SubjectInfoAccess_26)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SubjectInfoAccess_26() const { return ___SubjectInfoAccess_26; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SubjectInfoAccess_26() { return &___SubjectInfoAccess_26; }
	inline void set_SubjectInfoAccess_26(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SubjectInfoAccess_26 = value;
		Il2CppCodeGenWriteBarrier((&___SubjectInfoAccess_26), value);
	}

	inline static int32_t get_offset_of_LogoType_27() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___LogoType_27)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_LogoType_27() const { return ___LogoType_27; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_LogoType_27() { return &___LogoType_27; }
	inline void set_LogoType_27(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___LogoType_27 = value;
		Il2CppCodeGenWriteBarrier((&___LogoType_27), value);
	}

	inline static int32_t get_offset_of_BiometricInfo_28() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___BiometricInfo_28)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BiometricInfo_28() const { return ___BiometricInfo_28; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BiometricInfo_28() { return &___BiometricInfo_28; }
	inline void set_BiometricInfo_28(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BiometricInfo_28 = value;
		Il2CppCodeGenWriteBarrier((&___BiometricInfo_28), value);
	}

	inline static int32_t get_offset_of_QCStatements_29() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___QCStatements_29)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_QCStatements_29() const { return ___QCStatements_29; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_QCStatements_29() { return &___QCStatements_29; }
	inline void set_QCStatements_29(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___QCStatements_29 = value;
		Il2CppCodeGenWriteBarrier((&___QCStatements_29), value);
	}

	inline static int32_t get_offset_of_AuditIdentity_30() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___AuditIdentity_30)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_AuditIdentity_30() const { return ___AuditIdentity_30; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_AuditIdentity_30() { return &___AuditIdentity_30; }
	inline void set_AuditIdentity_30(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___AuditIdentity_30 = value;
		Il2CppCodeGenWriteBarrier((&___AuditIdentity_30), value);
	}

	inline static int32_t get_offset_of_NoRevAvail_31() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___NoRevAvail_31)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NoRevAvail_31() const { return ___NoRevAvail_31; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NoRevAvail_31() { return &___NoRevAvail_31; }
	inline void set_NoRevAvail_31(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NoRevAvail_31 = value;
		Il2CppCodeGenWriteBarrier((&___NoRevAvail_31), value);
	}

	inline static int32_t get_offset_of_TargetInformation_32() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___TargetInformation_32)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_TargetInformation_32() const { return ___TargetInformation_32; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_TargetInformation_32() { return &___TargetInformation_32; }
	inline void set_TargetInformation_32(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___TargetInformation_32 = value;
		Il2CppCodeGenWriteBarrier((&___TargetInformation_32), value);
	}

	inline static int32_t get_offset_of_ExpiredCertsOnCrl_33() { return static_cast<int32_t>(offsetof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields, ___ExpiredCertsOnCrl_33)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ExpiredCertsOnCrl_33() const { return ___ExpiredCertsOnCrl_33; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ExpiredCertsOnCrl_33() { return &___ExpiredCertsOnCrl_33; }
	inline void set_ExpiredCertsOnCrl_33(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ExpiredCertsOnCrl_33 = value;
		Il2CppCodeGenWriteBarrier((&___ExpiredCertsOnCrl_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONS_TFBB7387546053A219E86A448C813BF7042984B02_H
#ifndef X509NAME_T0D6EDC5B877B327C75FEDE783010E4C3843534D4_H
#define X509NAME_T0D6EDC5B877B327C75FEDE783010E4C3843534D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name
struct  X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::ordering
	RuntimeObject* ___ordering_42;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509NameEntryConverter BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::converter
	X509NameEntryConverter_t007AA731425354FFC770B335EB86EC1126045D28 * ___converter_43;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::values
	RuntimeObject* ___values_44;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::added
	RuntimeObject* ___added_45;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::seq
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___seq_46;

public:
	inline static int32_t get_offset_of_ordering_42() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4, ___ordering_42)); }
	inline RuntimeObject* get_ordering_42() const { return ___ordering_42; }
	inline RuntimeObject** get_address_of_ordering_42() { return &___ordering_42; }
	inline void set_ordering_42(RuntimeObject* value)
	{
		___ordering_42 = value;
		Il2CppCodeGenWriteBarrier((&___ordering_42), value);
	}

	inline static int32_t get_offset_of_converter_43() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4, ___converter_43)); }
	inline X509NameEntryConverter_t007AA731425354FFC770B335EB86EC1126045D28 * get_converter_43() const { return ___converter_43; }
	inline X509NameEntryConverter_t007AA731425354FFC770B335EB86EC1126045D28 ** get_address_of_converter_43() { return &___converter_43; }
	inline void set_converter_43(X509NameEntryConverter_t007AA731425354FFC770B335EB86EC1126045D28 * value)
	{
		___converter_43 = value;
		Il2CppCodeGenWriteBarrier((&___converter_43), value);
	}

	inline static int32_t get_offset_of_values_44() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4, ___values_44)); }
	inline RuntimeObject* get_values_44() const { return ___values_44; }
	inline RuntimeObject** get_address_of_values_44() { return &___values_44; }
	inline void set_values_44(RuntimeObject* value)
	{
		___values_44 = value;
		Il2CppCodeGenWriteBarrier((&___values_44), value);
	}

	inline static int32_t get_offset_of_added_45() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4, ___added_45)); }
	inline RuntimeObject* get_added_45() const { return ___added_45; }
	inline RuntimeObject** get_address_of_added_45() { return &___added_45; }
	inline void set_added_45(RuntimeObject* value)
	{
		___added_45 = value;
		Il2CppCodeGenWriteBarrier((&___added_45), value);
	}

	inline static int32_t get_offset_of_seq_46() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4, ___seq_46)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_seq_46() const { return ___seq_46; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_seq_46() { return &___seq_46; }
	inline void set_seq_46(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___seq_46 = value;
		Il2CppCodeGenWriteBarrier((&___seq_46), value);
	}
};

struct X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::C
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___C_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::O
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___O_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::OU
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___OU_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::T
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___T_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::CN
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CN_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::Street
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Street_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::SerialNumber
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SerialNumber_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::L
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___L_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::ST
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ST_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::Surname
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Surname_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::GivenName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GivenName_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::Initials
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Initials_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::Generation
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Generation_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::UniqueIdentifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___UniqueIdentifier_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::BusinessCategory
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___BusinessCategory_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::PostalCode
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PostalCode_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::DnQualifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DnQualifier_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::Pseudonym
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pseudonym_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::DateOfBirth
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DateOfBirth_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::PlaceOfBirth
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PlaceOfBirth_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::Gender
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Gender_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::CountryOfCitizenship
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CountryOfCitizenship_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::CountryOfResidence
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CountryOfResidence_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::NameAtBirth
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NameAtBirth_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::PostalAddress
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PostalAddress_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::DmdName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DmdName_27;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::TelephoneNumber
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___TelephoneNumber_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::OrganizationIdentifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___OrganizationIdentifier_29;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::Name
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Name_30;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::EmailAddress
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EmailAddress_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::UnstructuredName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___UnstructuredName_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::UnstructuredAddress
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___UnstructuredAddress_33;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::E
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___E_34;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::DC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DC_35;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::UID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___UID_36;
	// System.Boolean[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::defaultReverse
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___defaultReverse_37;
	// System.Collections.Hashtable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::DefaultSymbols
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___DefaultSymbols_38;
	// System.Collections.Hashtable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::RFC2253Symbols
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___RFC2253Symbols_39;
	// System.Collections.Hashtable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::RFC1779Symbols
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___RFC1779Symbols_40;
	// System.Collections.Hashtable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name::DefaultLookup
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___DefaultLookup_41;

public:
	inline static int32_t get_offset_of_C_2() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___C_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_C_2() const { return ___C_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_C_2() { return &___C_2; }
	inline void set_C_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___C_2 = value;
		Il2CppCodeGenWriteBarrier((&___C_2), value);
	}

	inline static int32_t get_offset_of_O_3() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___O_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_O_3() const { return ___O_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_O_3() { return &___O_3; }
	inline void set_O_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___O_3 = value;
		Il2CppCodeGenWriteBarrier((&___O_3), value);
	}

	inline static int32_t get_offset_of_OU_4() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___OU_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_OU_4() const { return ___OU_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_OU_4() { return &___OU_4; }
	inline void set_OU_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___OU_4 = value;
		Il2CppCodeGenWriteBarrier((&___OU_4), value);
	}

	inline static int32_t get_offset_of_T_5() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___T_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_T_5() const { return ___T_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_T_5() { return &___T_5; }
	inline void set_T_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___T_5 = value;
		Il2CppCodeGenWriteBarrier((&___T_5), value);
	}

	inline static int32_t get_offset_of_CN_6() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___CN_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CN_6() const { return ___CN_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CN_6() { return &___CN_6; }
	inline void set_CN_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CN_6 = value;
		Il2CppCodeGenWriteBarrier((&___CN_6), value);
	}

	inline static int32_t get_offset_of_Street_7() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___Street_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Street_7() const { return ___Street_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Street_7() { return &___Street_7; }
	inline void set_Street_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Street_7 = value;
		Il2CppCodeGenWriteBarrier((&___Street_7), value);
	}

	inline static int32_t get_offset_of_SerialNumber_8() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___SerialNumber_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SerialNumber_8() const { return ___SerialNumber_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SerialNumber_8() { return &___SerialNumber_8; }
	inline void set_SerialNumber_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SerialNumber_8 = value;
		Il2CppCodeGenWriteBarrier((&___SerialNumber_8), value);
	}

	inline static int32_t get_offset_of_L_9() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___L_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_L_9() const { return ___L_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_L_9() { return &___L_9; }
	inline void set_L_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___L_9 = value;
		Il2CppCodeGenWriteBarrier((&___L_9), value);
	}

	inline static int32_t get_offset_of_ST_10() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___ST_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ST_10() const { return ___ST_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ST_10() { return &___ST_10; }
	inline void set_ST_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ST_10 = value;
		Il2CppCodeGenWriteBarrier((&___ST_10), value);
	}

	inline static int32_t get_offset_of_Surname_11() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___Surname_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Surname_11() const { return ___Surname_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Surname_11() { return &___Surname_11; }
	inline void set_Surname_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Surname_11 = value;
		Il2CppCodeGenWriteBarrier((&___Surname_11), value);
	}

	inline static int32_t get_offset_of_GivenName_12() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___GivenName_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GivenName_12() const { return ___GivenName_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GivenName_12() { return &___GivenName_12; }
	inline void set_GivenName_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GivenName_12 = value;
		Il2CppCodeGenWriteBarrier((&___GivenName_12), value);
	}

	inline static int32_t get_offset_of_Initials_13() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___Initials_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Initials_13() const { return ___Initials_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Initials_13() { return &___Initials_13; }
	inline void set_Initials_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Initials_13 = value;
		Il2CppCodeGenWriteBarrier((&___Initials_13), value);
	}

	inline static int32_t get_offset_of_Generation_14() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___Generation_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Generation_14() const { return ___Generation_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Generation_14() { return &___Generation_14; }
	inline void set_Generation_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Generation_14 = value;
		Il2CppCodeGenWriteBarrier((&___Generation_14), value);
	}

	inline static int32_t get_offset_of_UniqueIdentifier_15() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___UniqueIdentifier_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_UniqueIdentifier_15() const { return ___UniqueIdentifier_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_UniqueIdentifier_15() { return &___UniqueIdentifier_15; }
	inline void set_UniqueIdentifier_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___UniqueIdentifier_15 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueIdentifier_15), value);
	}

	inline static int32_t get_offset_of_BusinessCategory_16() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___BusinessCategory_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_BusinessCategory_16() const { return ___BusinessCategory_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_BusinessCategory_16() { return &___BusinessCategory_16; }
	inline void set_BusinessCategory_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___BusinessCategory_16 = value;
		Il2CppCodeGenWriteBarrier((&___BusinessCategory_16), value);
	}

	inline static int32_t get_offset_of_PostalCode_17() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___PostalCode_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PostalCode_17() const { return ___PostalCode_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PostalCode_17() { return &___PostalCode_17; }
	inline void set_PostalCode_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PostalCode_17 = value;
		Il2CppCodeGenWriteBarrier((&___PostalCode_17), value);
	}

	inline static int32_t get_offset_of_DnQualifier_18() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___DnQualifier_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DnQualifier_18() const { return ___DnQualifier_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DnQualifier_18() { return &___DnQualifier_18; }
	inline void set_DnQualifier_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DnQualifier_18 = value;
		Il2CppCodeGenWriteBarrier((&___DnQualifier_18), value);
	}

	inline static int32_t get_offset_of_Pseudonym_19() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___Pseudonym_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pseudonym_19() const { return ___Pseudonym_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pseudonym_19() { return &___Pseudonym_19; }
	inline void set_Pseudonym_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pseudonym_19 = value;
		Il2CppCodeGenWriteBarrier((&___Pseudonym_19), value);
	}

	inline static int32_t get_offset_of_DateOfBirth_20() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___DateOfBirth_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DateOfBirth_20() const { return ___DateOfBirth_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DateOfBirth_20() { return &___DateOfBirth_20; }
	inline void set_DateOfBirth_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DateOfBirth_20 = value;
		Il2CppCodeGenWriteBarrier((&___DateOfBirth_20), value);
	}

	inline static int32_t get_offset_of_PlaceOfBirth_21() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___PlaceOfBirth_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PlaceOfBirth_21() const { return ___PlaceOfBirth_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PlaceOfBirth_21() { return &___PlaceOfBirth_21; }
	inline void set_PlaceOfBirth_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PlaceOfBirth_21 = value;
		Il2CppCodeGenWriteBarrier((&___PlaceOfBirth_21), value);
	}

	inline static int32_t get_offset_of_Gender_22() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___Gender_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Gender_22() const { return ___Gender_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Gender_22() { return &___Gender_22; }
	inline void set_Gender_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Gender_22 = value;
		Il2CppCodeGenWriteBarrier((&___Gender_22), value);
	}

	inline static int32_t get_offset_of_CountryOfCitizenship_23() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___CountryOfCitizenship_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CountryOfCitizenship_23() const { return ___CountryOfCitizenship_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CountryOfCitizenship_23() { return &___CountryOfCitizenship_23; }
	inline void set_CountryOfCitizenship_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CountryOfCitizenship_23 = value;
		Il2CppCodeGenWriteBarrier((&___CountryOfCitizenship_23), value);
	}

	inline static int32_t get_offset_of_CountryOfResidence_24() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___CountryOfResidence_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CountryOfResidence_24() const { return ___CountryOfResidence_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CountryOfResidence_24() { return &___CountryOfResidence_24; }
	inline void set_CountryOfResidence_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CountryOfResidence_24 = value;
		Il2CppCodeGenWriteBarrier((&___CountryOfResidence_24), value);
	}

	inline static int32_t get_offset_of_NameAtBirth_25() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___NameAtBirth_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NameAtBirth_25() const { return ___NameAtBirth_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NameAtBirth_25() { return &___NameAtBirth_25; }
	inline void set_NameAtBirth_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NameAtBirth_25 = value;
		Il2CppCodeGenWriteBarrier((&___NameAtBirth_25), value);
	}

	inline static int32_t get_offset_of_PostalAddress_26() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___PostalAddress_26)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PostalAddress_26() const { return ___PostalAddress_26; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PostalAddress_26() { return &___PostalAddress_26; }
	inline void set_PostalAddress_26(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PostalAddress_26 = value;
		Il2CppCodeGenWriteBarrier((&___PostalAddress_26), value);
	}

	inline static int32_t get_offset_of_DmdName_27() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___DmdName_27)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DmdName_27() const { return ___DmdName_27; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DmdName_27() { return &___DmdName_27; }
	inline void set_DmdName_27(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DmdName_27 = value;
		Il2CppCodeGenWriteBarrier((&___DmdName_27), value);
	}

	inline static int32_t get_offset_of_TelephoneNumber_28() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___TelephoneNumber_28)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_TelephoneNumber_28() const { return ___TelephoneNumber_28; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_TelephoneNumber_28() { return &___TelephoneNumber_28; }
	inline void set_TelephoneNumber_28(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___TelephoneNumber_28 = value;
		Il2CppCodeGenWriteBarrier((&___TelephoneNumber_28), value);
	}

	inline static int32_t get_offset_of_OrganizationIdentifier_29() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___OrganizationIdentifier_29)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_OrganizationIdentifier_29() const { return ___OrganizationIdentifier_29; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_OrganizationIdentifier_29() { return &___OrganizationIdentifier_29; }
	inline void set_OrganizationIdentifier_29(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___OrganizationIdentifier_29 = value;
		Il2CppCodeGenWriteBarrier((&___OrganizationIdentifier_29), value);
	}

	inline static int32_t get_offset_of_Name_30() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___Name_30)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Name_30() const { return ___Name_30; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Name_30() { return &___Name_30; }
	inline void set_Name_30(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Name_30 = value;
		Il2CppCodeGenWriteBarrier((&___Name_30), value);
	}

	inline static int32_t get_offset_of_EmailAddress_31() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___EmailAddress_31)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EmailAddress_31() const { return ___EmailAddress_31; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EmailAddress_31() { return &___EmailAddress_31; }
	inline void set_EmailAddress_31(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EmailAddress_31 = value;
		Il2CppCodeGenWriteBarrier((&___EmailAddress_31), value);
	}

	inline static int32_t get_offset_of_UnstructuredName_32() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___UnstructuredName_32)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_UnstructuredName_32() const { return ___UnstructuredName_32; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_UnstructuredName_32() { return &___UnstructuredName_32; }
	inline void set_UnstructuredName_32(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___UnstructuredName_32 = value;
		Il2CppCodeGenWriteBarrier((&___UnstructuredName_32), value);
	}

	inline static int32_t get_offset_of_UnstructuredAddress_33() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___UnstructuredAddress_33)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_UnstructuredAddress_33() const { return ___UnstructuredAddress_33; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_UnstructuredAddress_33() { return &___UnstructuredAddress_33; }
	inline void set_UnstructuredAddress_33(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___UnstructuredAddress_33 = value;
		Il2CppCodeGenWriteBarrier((&___UnstructuredAddress_33), value);
	}

	inline static int32_t get_offset_of_E_34() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___E_34)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_E_34() const { return ___E_34; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_E_34() { return &___E_34; }
	inline void set_E_34(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___E_34 = value;
		Il2CppCodeGenWriteBarrier((&___E_34), value);
	}

	inline static int32_t get_offset_of_DC_35() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___DC_35)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DC_35() const { return ___DC_35; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DC_35() { return &___DC_35; }
	inline void set_DC_35(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DC_35 = value;
		Il2CppCodeGenWriteBarrier((&___DC_35), value);
	}

	inline static int32_t get_offset_of_UID_36() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___UID_36)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_UID_36() const { return ___UID_36; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_UID_36() { return &___UID_36; }
	inline void set_UID_36(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___UID_36 = value;
		Il2CppCodeGenWriteBarrier((&___UID_36), value);
	}

	inline static int32_t get_offset_of_defaultReverse_37() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___defaultReverse_37)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_defaultReverse_37() const { return ___defaultReverse_37; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_defaultReverse_37() { return &___defaultReverse_37; }
	inline void set_defaultReverse_37(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___defaultReverse_37 = value;
		Il2CppCodeGenWriteBarrier((&___defaultReverse_37), value);
	}

	inline static int32_t get_offset_of_DefaultSymbols_38() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___DefaultSymbols_38)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_DefaultSymbols_38() const { return ___DefaultSymbols_38; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_DefaultSymbols_38() { return &___DefaultSymbols_38; }
	inline void set_DefaultSymbols_38(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___DefaultSymbols_38 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultSymbols_38), value);
	}

	inline static int32_t get_offset_of_RFC2253Symbols_39() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___RFC2253Symbols_39)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_RFC2253Symbols_39() const { return ___RFC2253Symbols_39; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_RFC2253Symbols_39() { return &___RFC2253Symbols_39; }
	inline void set_RFC2253Symbols_39(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___RFC2253Symbols_39 = value;
		Il2CppCodeGenWriteBarrier((&___RFC2253Symbols_39), value);
	}

	inline static int32_t get_offset_of_RFC1779Symbols_40() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___RFC1779Symbols_40)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_RFC1779Symbols_40() const { return ___RFC1779Symbols_40; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_RFC1779Symbols_40() { return &___RFC1779Symbols_40; }
	inline void set_RFC1779Symbols_40(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___RFC1779Symbols_40 = value;
		Il2CppCodeGenWriteBarrier((&___RFC1779Symbols_40), value);
	}

	inline static int32_t get_offset_of_DefaultLookup_41() { return static_cast<int32_t>(offsetof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields, ___DefaultLookup_41)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_DefaultLookup_41() const { return ___DefaultLookup_41; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_DefaultLookup_41() { return &___DefaultLookup_41; }
	inline void set_DefaultLookup_41(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___DefaultLookup_41 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultLookup_41), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509NAME_T0D6EDC5B877B327C75FEDE783010E4C3843534D4_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef DEROBJECTIDENTIFIER_T6D50C7785FE0773A3C6CFF7A550A85885C2D2676_H
#define DEROBJECTIDENTIFIER_T6D50C7785FE0773A3C6CFF7A550A85885C2D2676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct  DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier::identifier
	String_t* ___identifier_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier::body
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___body_3;

public:
	inline static int32_t get_offset_of_identifier_2() { return static_cast<int32_t>(offsetof(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676, ___identifier_2)); }
	inline String_t* get_identifier_2() const { return ___identifier_2; }
	inline String_t** get_address_of_identifier_2() { return &___identifier_2; }
	inline void set_identifier_2(String_t* value)
	{
		___identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_2), value);
	}

	inline static int32_t get_offset_of_body_3() { return static_cast<int32_t>(offsetof(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676, ___body_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_body_3() const { return ___body_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_body_3() { return &___body_3; }
	inline void set_body_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___body_3 = value;
		Il2CppCodeGenWriteBarrier((&___body_3), value);
	}
};

struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier::cache
	DerObjectIdentifierU5BU5D_tC595A7256A12DE98032B8267A8795C3EC8F3D6B9* ___cache_5;

public:
	inline static int32_t get_offset_of_cache_5() { return static_cast<int32_t>(offsetof(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676_StaticFields, ___cache_5)); }
	inline DerObjectIdentifierU5BU5D_tC595A7256A12DE98032B8267A8795C3EC8F3D6B9* get_cache_5() const { return ___cache_5; }
	inline DerObjectIdentifierU5BU5D_tC595A7256A12DE98032B8267A8795C3EC8F3D6B9** get_address_of_cache_5() { return &___cache_5; }
	inline void set_cache_5(DerObjectIdentifierU5BU5D_tC595A7256A12DE98032B8267A8795C3EC8F3D6B9* value)
	{
		___cache_5 = value;
		Il2CppCodeGenWriteBarrier((&___cache_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEROBJECTIDENTIFIER_T6D50C7785FE0773A3C6CFF7A550A85885C2D2676_H
#ifndef DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#define DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerStringBase
struct  DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifndef SMIMECAPABILITIESATTRIBUTE_T75BFBECF6B8AC5FF12341D604E15E5FB414DFA49_H
#define SMIMECAPABILITIESATTRIBUTE_T75BFBECF6B8AC5FF12341D604E15E5FB414DFA49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeCapabilitiesAttribute
struct  SmimeCapabilitiesAttribute_t75BFBECF6B8AC5FF12341D604E15E5FB414DFA49  : public AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMIMECAPABILITIESATTRIBUTE_T75BFBECF6B8AC5FF12341D604E15E5FB414DFA49_H
#ifndef SMIMEENCRYPTIONKEYPREFERENCEATTRIBUTE_T9D36A4A270572BCF24E7C95AF25653F7BF5802D9_H
#define SMIMEENCRYPTIONKEYPREFERENCEATTRIBUTE_T9D36A4A270572BCF24E7C95AF25653F7BF5802D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Smime.SmimeEncryptionKeyPreferenceAttribute
struct  SmimeEncryptionKeyPreferenceAttribute_t9D36A4A270572BCF24E7C95AF25653F7BF5802D9  : public AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMIMEENCRYPTIONKEYPREFERENCEATTRIBUTE_T9D36A4A270572BCF24E7C95AF25653F7BF5802D9_H
#ifndef FILTERSTREAM_TF308252D96A498705BC2A63B721A04E367BE6706_H
#define FILTERSTREAM_TF308252D96A498705BC2A63B721A04E367BE6706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Utilities.FilterStream
struct  FilterStream_tF308252D96A498705BC2A63B721A04E367BE6706  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Utilities.FilterStream::s
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___s_5;

public:
	inline static int32_t get_offset_of_s_5() { return static_cast<int32_t>(offsetof(FilterStream_tF308252D96A498705BC2A63B721A04E367BE6706, ___s_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_s_5() const { return ___s_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_s_5() { return &___s_5; }
	inline void set_s_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___s_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERSTREAM_TF308252D96A498705BC2A63B721A04E367BE6706_H
#ifndef CHOICE_T5C34E6C38903E7449EA5258A51CCDF908856E696_H
#define CHOICE_T5C34E6C38903E7449EA5258A51CCDF908856E696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Target_Choice
struct  Choice_t5C34E6C38903E7449EA5258A51CCDF908856E696 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Target_Choice::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Choice_t5C34E6C38903E7449EA5258A51CCDF908856E696, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHOICE_T5C34E6C38903E7449EA5258A51CCDF908856E696_H
#ifndef DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#define DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString
struct  DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::mData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mData_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::mPadBits
	int32_t ___mPadBits_4;

public:
	inline static int32_t get_offset_of_mData_3() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374, ___mData_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mData_3() const { return ___mData_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mData_3() { return &___mData_3; }
	inline void set_mData_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mData_3 = value;
		Il2CppCodeGenWriteBarrier((&___mData_3), value);
	}

	inline static int32_t get_offset_of_mPadBits_4() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374, ___mPadBits_4)); }
	inline int32_t get_mPadBits_4() const { return ___mPadBits_4; }
	inline int32_t* get_address_of_mPadBits_4() { return &___mPadBits_4; }
	inline void set_mPadBits_4(int32_t value)
	{
		___mPadBits_4 = value;
	}
};

struct DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields
{
public:
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::table
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___table_2;

public:
	inline static int32_t get_offset_of_table_2() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields, ___table_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_table_2() const { return ___table_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_table_2() { return &___table_2; }
	inline void set_table_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___table_2 = value;
		Il2CppCodeGenWriteBarrier((&___table_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#ifndef POLICYQUALIFIERID_TFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866_H
#define POLICYQUALIFIERID_TFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyQualifierID
struct  PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866  : public DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676
{
public:

public:
};

struct PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyQualifierID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyQualifierID::IdQtCps
	PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866 * ___IdQtCps_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyQualifierID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.PolicyQualifierID::IdQtUnotice
	PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866 * ___IdQtUnotice_8;

public:
	inline static int32_t get_offset_of_IdQtCps_7() { return static_cast<int32_t>(offsetof(PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866_StaticFields, ___IdQtCps_7)); }
	inline PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866 * get_IdQtCps_7() const { return ___IdQtCps_7; }
	inline PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866 ** get_address_of_IdQtCps_7() { return &___IdQtCps_7; }
	inline void set_IdQtCps_7(PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866 * value)
	{
		___IdQtCps_7 = value;
		Il2CppCodeGenWriteBarrier((&___IdQtCps_7), value);
	}

	inline static int32_t get_offset_of_IdQtUnotice_8() { return static_cast<int32_t>(offsetof(PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866_StaticFields, ___IdQtUnotice_8)); }
	inline PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866 * get_IdQtUnotice_8() const { return ___IdQtUnotice_8; }
	inline PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866 ** get_address_of_IdQtUnotice_8() { return &___IdQtUnotice_8; }
	inline void set_IdQtUnotice_8(PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866 * value)
	{
		___IdQtUnotice_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdQtUnotice_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLICYQUALIFIERID_TFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866_H
#ifndef KEYUSAGE_T05EEF3A686765AE32A749B3D717EBA68DDFAEDB9_H
#define KEYUSAGE_T05EEF3A686765AE32A749B3D717EBA68DDFAEDB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.KeyUsage
struct  KeyUsage_t05EEF3A686765AE32A749B3D717EBA68DDFAEDB9  : public DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYUSAGE_T05EEF3A686765AE32A749B3D717EBA68DDFAEDB9_H
#ifndef REASONFLAGS_TBB7FD42D692CC7182B9F721F5558208A7EBF1C3A_H
#define REASONFLAGS_TBB7FD42D692CC7182B9F721F5558208A7EBF1C3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.ReasonFlags
struct  ReasonFlags_tBB7FD42D692CC7182B9F721F5558208A7EBF1C3A  : public DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REASONFLAGS_TBB7FD42D692CC7182B9F721F5558208A7EBF1C3A_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5300 = { sizeof (KeyUsage_t05EEF3A686765AE32A749B3D717EBA68DDFAEDB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5300[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5301 = { sizeof (NameConstraints_t32DADF4E7B769CB9582A14FA4A4FD9672A635A6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5301[2] = 
{
	NameConstraints_t32DADF4E7B769CB9582A14FA4A4FD9672A635A6C::get_offset_of_permitted_2(),
	NameConstraints_t32DADF4E7B769CB9582A14FA4A4FD9672A635A6C::get_offset_of_excluded_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5302 = { sizeof (NoticeReference_t881A2D3508F9AA3C2EEC071137E809179E643234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5302[2] = 
{
	NoticeReference_t881A2D3508F9AA3C2EEC071137E809179E643234::get_offset_of_organization_2(),
	NoticeReference_t881A2D3508F9AA3C2EEC071137E809179E643234::get_offset_of_noticeNumbers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5303 = { sizeof (ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5303[7] = 
{
	0,
	0,
	0,
	ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F::get_offset_of_digestedObjectType_5(),
	ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F::get_offset_of_otherObjectTypeID_6(),
	ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F::get_offset_of_digestAlgorithm_7(),
	ObjectDigestInfo_tDF6863A222D8EE0A0CB96CA58AF94A5796A2659F::get_offset_of_objectDigest_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5304 = { sizeof (PolicyInformation_tAA2139758A8642ED6AC8CCD1E8875E5E82066DBE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5304[2] = 
{
	PolicyInformation_tAA2139758A8642ED6AC8CCD1E8875E5E82066DBE::get_offset_of_policyIdentifier_2(),
	PolicyInformation_tAA2139758A8642ED6AC8CCD1E8875E5E82066DBE::get_offset_of_policyQualifiers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5305 = { sizeof (PolicyMappings_t9941C3AA1FA95799D51A7A778662385DE446B11A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5305[1] = 
{
	PolicyMappings_t9941C3AA1FA95799D51A7A778662385DE446B11A::get_offset_of_seq_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5306 = { sizeof (PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866), -1, sizeof(PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5306[3] = 
{
	0,
	PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866_StaticFields::get_offset_of_IdQtCps_7(),
	PolicyQualifierID_tFB5B6E62FC4159F4CF846E3D0E5C46C89FE81866_StaticFields::get_offset_of_IdQtUnotice_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5307 = { sizeof (PolicyQualifierInfo_t5468F83F74055851E359E3796A1C70B50AF91C13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5307[2] = 
{
	PolicyQualifierInfo_t5468F83F74055851E359E3796A1C70B50AF91C13::get_offset_of_policyQualifierId_2(),
	PolicyQualifierInfo_t5468F83F74055851E359E3796A1C70B50AF91C13::get_offset_of_qualifier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5308 = { sizeof (PrivateKeyUsagePeriod_tE025B41B5FEC729171D2FCB22F69C72984B6FD9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5308[2] = 
{
	PrivateKeyUsagePeriod_tE025B41B5FEC729171D2FCB22F69C72984B6FD9E::get_offset_of__notBefore_2(),
	PrivateKeyUsagePeriod_tE025B41B5FEC729171D2FCB22F69C72984B6FD9E::get_offset_of__notAfter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5309 = { sizeof (ReasonFlags_tBB7FD42D692CC7182B9F721F5558208A7EBF1C3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5309[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5310 = { sizeof (RoleSyntax_t113AB5191C6ED1A79005F820EF78505BC90A79A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5310[2] = 
{
	RoleSyntax_t113AB5191C6ED1A79005F820EF78505BC90A79A1::get_offset_of_roleAuthority_2(),
	RoleSyntax_t113AB5191C6ED1A79005F820EF78505BC90A79A1::get_offset_of_roleName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5311 = { sizeof (RsaPublicKeyStructure_t0E62415F89EE299A3B61B467F144887987E6B63E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5311[2] = 
{
	RsaPublicKeyStructure_t0E62415F89EE299A3B61B467F144887987E6B63E::get_offset_of_modulus_2(),
	RsaPublicKeyStructure_t0E62415F89EE299A3B61B467F144887987E6B63E::get_offset_of_publicExponent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5312 = { sizeof (SubjectDirectoryAttributes_t306D51B35F66F7C25F14845D6816FC274FFE4A9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5312[1] = 
{
	SubjectDirectoryAttributes_t306D51B35F66F7C25F14845D6816FC274FFE4A9A::get_offset_of_attributes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5313 = { sizeof (SubjectKeyIdentifier_tF922F6F32716AB7578A5809B185B104DD0D538D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5313[1] = 
{
	SubjectKeyIdentifier_tF922F6F32716AB7578A5809B185B104DD0D538D7::get_offset_of_keyIdentifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5314 = { sizeof (SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5314[2] = 
{
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90::get_offset_of_algID_2(),
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90::get_offset_of_keyData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5315 = { sizeof (Target_tFB39652E1D3B26D5335C6681A7F22D342AE8B2C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5315[2] = 
{
	Target_tFB39652E1D3B26D5335C6681A7F22D342AE8B2C9::get_offset_of_targetName_2(),
	Target_tFB39652E1D3B26D5335C6681A7F22D342AE8B2C9::get_offset_of_targetGroup_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5316 = { sizeof (Choice_t5C34E6C38903E7449EA5258A51CCDF908856E696)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5316[3] = 
{
	Choice_t5C34E6C38903E7449EA5258A51CCDF908856E696::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5317 = { sizeof (TargetInformation_t449ACFE048A7DB35DAEEA7D349C3A12E28716918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5317[1] = 
{
	TargetInformation_t449ACFE048A7DB35DAEEA7D349C3A12E28716918::get_offset_of_targets_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5318 = { sizeof (Targets_t521998AA6A08390718858AEE853968F1EE917EA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5318[1] = 
{
	Targets_t521998AA6A08390718858AEE853968F1EE917EA2::get_offset_of_targets_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5319 = { sizeof (TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5319[12] = 
{
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_seq_2(),
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_version_3(),
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_serialNumber_4(),
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_signature_5(),
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_issuer_6(),
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_startDate_7(),
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_endDate_8(),
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_subject_9(),
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_subjectPublicKeyInfo_10(),
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_issuerUniqueID_11(),
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_subjectUniqueID_12(),
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39::get_offset_of_extensions_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5320 = { sizeof (CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5320[4] = 
{
	CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C::get_offset_of_seq_2(),
	CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C::get_offset_of_userCertificate_3(),
	CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C::get_offset_of_revocationDate_4(),
	CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C::get_offset_of_crlEntryExtensions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5321 = { sizeof (TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5321[8] = 
{
	TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC::get_offset_of_seq_2(),
	TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC::get_offset_of_version_3(),
	TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC::get_offset_of_signature_4(),
	TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC::get_offset_of_issuer_5(),
	TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC::get_offset_of_thisUpdate_6(),
	TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC::get_offset_of_nextUpdate_7(),
	TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC::get_offset_of_revokedCertificates_8(),
	TbsCertificateList_t2AB061DB74344163C3090B032EE1E4657B9EEDBC::get_offset_of_crlExtensions_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5322 = { sizeof (RevokedCertificatesEnumeration_tA5745D9F80919FD73DE390F7A4986054A21C92CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5322[1] = 
{
	RevokedCertificatesEnumeration_tA5745D9F80919FD73DE390F7A4986054A21C92CC::get_offset_of_en_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5323 = { sizeof (RevokedCertificatesEnumerator_t5E2BD09913613D13773B1EFC061FE5EBCA89041A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5323[1] = 
{
	RevokedCertificatesEnumerator_t5E2BD09913613D13773B1EFC061FE5EBCA89041A::get_offset_of_e_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5324 = { sizeof (Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5324[1] = 
{
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5325 = { sizeof (UserNotice_t44B6430B6956C4427A107CF515B8AA4DA15D7310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5325[2] = 
{
	UserNotice_t44B6430B6956C4427A107CF515B8AA4DA15D7310::get_offset_of_noticeRef_2(),
	UserNotice_t44B6430B6956C4427A107CF515B8AA4DA15D7310::get_offset_of_explicitText_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5326 = { sizeof (V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5326[8] = 
{
	V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53::get_offset_of_version_0(),
	V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53::get_offset_of_serialNumber_1(),
	V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53::get_offset_of_signature_2(),
	V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53::get_offset_of_issuer_3(),
	V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53::get_offset_of_startDate_4(),
	V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53::get_offset_of_endDate_5(),
	V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53::get_offset_of_subject_6(),
	V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53::get_offset_of_subjectPublicKeyInfo_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5327 = { sizeof (V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5327[10] = 
{
	V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7::get_offset_of_version_0(),
	V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7::get_offset_of_holder_1(),
	V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7::get_offset_of_issuer_2(),
	V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7::get_offset_of_signature_3(),
	V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7::get_offset_of_serialNumber_4(),
	V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7::get_offset_of_attributes_5(),
	V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7::get_offset_of_issuerUniqueID_6(),
	V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7::get_offset_of_extensions_7(),
	V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7::get_offset_of_startDate_8(),
	V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7::get_offset_of_endDate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5328 = { sizeof (V2Form_tD57A9480E55B3CC489C97D55EED79C0B27DF6496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5328[3] = 
{
	V2Form_tD57A9480E55B3CC489C97D55EED79C0B27DF6496::get_offset_of_issuerName_2(),
	V2Form_tD57A9480E55B3CC489C97D55EED79C0B27DF6496::get_offset_of_baseCertificateID_3(),
	V2Form_tD57A9480E55B3CC489C97D55EED79C0B27DF6496::get_offset_of_objectDigestInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5329 = { sizeof (V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5329[7] = 
{
	V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D::get_offset_of_version_0(),
	V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D::get_offset_of_signature_1(),
	V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D::get_offset_of_issuer_2(),
	V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D::get_offset_of_thisUpdate_3(),
	V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D::get_offset_of_nextUpdate_4(),
	V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D::get_offset_of_extensions_5(),
	V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D::get_offset_of_crlEntries_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5330 = { sizeof (V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5330[12] = 
{
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_version_0(),
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_serialNumber_1(),
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_signature_2(),
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_issuer_3(),
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_startDate_4(),
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_endDate_5(),
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_subject_6(),
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_subjectPublicKeyInfo_7(),
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_extensions_8(),
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_altNamePresentAndCritical_9(),
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_issuerUniqueID_10(),
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16::get_offset_of_subjectUniqueID_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5331 = { sizeof (X509Attributes_tFBFBC66BBD2E9F3AD4770593880D9384A55F567C), -1, sizeof(X509Attributes_tFBFBC66BBD2E9F3AD4770593880D9384A55F567C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5331[1] = 
{
	X509Attributes_tFBFBC66BBD2E9F3AD4770593880D9384A55F567C_StaticFields::get_offset_of_RoleSyntax_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5332 = { sizeof (X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5332[3] = 
{
	X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242::get_offset_of_tbsCert_2(),
	X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242::get_offset_of_sigAlgID_3(),
	X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242::get_offset_of_sig_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5333 = { sizeof (X509DefaultEntryConverter_t7BA3BF838D6E3FD8EFC8A041F70D7A487C97CFBA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5334 = { sizeof (X509Extension_tB5ACB0FEA53B7681995185A06D4AEF29DFD72486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5334[2] = 
{
	X509Extension_tB5ACB0FEA53B7681995185A06D4AEF29DFD72486::get_offset_of_critical_0(),
	X509Extension_tB5ACB0FEA53B7681995185A06D4AEF29DFD72486::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5335 = { sizeof (X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02), -1, sizeof(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5335[34] = 
{
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_SubjectDirectoryAttributes_2(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_SubjectKeyIdentifier_3(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_KeyUsage_4(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_PrivateKeyUsagePeriod_5(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_SubjectAlternativeName_6(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_IssuerAlternativeName_7(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_BasicConstraints_8(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_CrlNumber_9(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_ReasonCode_10(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_InstructionCode_11(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_InvalidityDate_12(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_DeltaCrlIndicator_13(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_IssuingDistributionPoint_14(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_CertificateIssuer_15(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_NameConstraints_16(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_CrlDistributionPoints_17(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_CertificatePolicies_18(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_PolicyMappings_19(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_AuthorityKeyIdentifier_20(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_PolicyConstraints_21(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_ExtendedKeyUsage_22(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_FreshestCrl_23(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_InhibitAnyPolicy_24(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_AuthorityInfoAccess_25(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_SubjectInfoAccess_26(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_LogoType_27(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_BiometricInfo_28(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_QCStatements_29(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_AuditIdentity_30(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_NoRevAvail_31(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_TargetInformation_32(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02_StaticFields::get_offset_of_ExpiredCertsOnCrl_33(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02::get_offset_of_extensions_34(),
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02::get_offset_of_ordering_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5336 = { sizeof (X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5336[2] = 
{
	X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33::get_offset_of_extensions_0(),
	X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33::get_offset_of_extOrdering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5337 = { sizeof (X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4), -1, sizeof(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5337[45] = 
{
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_C_2(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_O_3(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_OU_4(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_T_5(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_CN_6(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_Street_7(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_SerialNumber_8(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_L_9(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_ST_10(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_Surname_11(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_GivenName_12(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_Initials_13(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_Generation_14(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_UniqueIdentifier_15(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_BusinessCategory_16(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_PostalCode_17(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_DnQualifier_18(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_Pseudonym_19(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_DateOfBirth_20(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_PlaceOfBirth_21(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_Gender_22(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_CountryOfCitizenship_23(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_CountryOfResidence_24(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_NameAtBirth_25(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_PostalAddress_26(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_DmdName_27(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_TelephoneNumber_28(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_OrganizationIdentifier_29(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_Name_30(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_EmailAddress_31(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_UnstructuredName_32(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_UnstructuredAddress_33(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_E_34(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_DC_35(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_UID_36(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_defaultReverse_37(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_DefaultSymbols_38(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_RFC2253Symbols_39(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_RFC1779Symbols_40(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4_StaticFields::get_offset_of_DefaultLookup_41(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4::get_offset_of_ordering_42(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4::get_offset_of_converter_43(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4::get_offset_of_values_44(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4::get_offset_of_added_45(),
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4::get_offset_of_seq_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5338 = { sizeof (X509NameEntryConverter_t007AA731425354FFC770B335EB86EC1126045D28), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5339 = { sizeof (X509NameTokenizer_tB7EC8BFB137765264AA497567E0709DEDFFABE04), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5339[4] = 
{
	X509NameTokenizer_tB7EC8BFB137765264AA497567E0709DEDFFABE04::get_offset_of_value_0(),
	X509NameTokenizer_tB7EC8BFB137765264AA497567E0709DEDFFABE04::get_offset_of_index_1(),
	X509NameTokenizer_tB7EC8BFB137765264AA497567E0709DEDFFABE04::get_offset_of_separator_2(),
	X509NameTokenizer_tB7EC8BFB137765264AA497567E0709DEDFFABE04::get_offset_of_buffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5340 = { sizeof (X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777), -1, sizeof(X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5340[21] = 
{
	0,
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_CommonName_1(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_CountryName_2(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_LocalityName_3(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_StateOrProvinceName_4(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_Organization_5(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_OrganizationalUnitName_6(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_id_at_telephoneNumber_7(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_id_at_name_8(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_id_at_organizationIdentifier_9(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_IdSha1_10(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_RipeMD160_11(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_RipeMD160WithRsaEncryption_12(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_IdEARsa_13(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_IdPkix_14(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_IdPE_15(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_IdAD_16(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_IdADCAIssuers_17(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_IdADOcsp_18(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_OcspAccessMethod_19(),
	X509ObjectIdentifiers_t430395AFF8001F20CE1E7DA67F96A185EFDBA777_StaticFields::get_offset_of_CrlAccessMethod_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5341 = { sizeof (NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5341[3] = 
{
	NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35::get_offset_of_pseudonym_2(),
	NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35::get_offset_of_surname_3(),
	NameOrPseudonym_t0BE7B3DAE513BFD2146E7E23FEFE7508E28EBC35::get_offset_of_givenName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5342 = { sizeof (PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5342[6] = 
{
	PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078::get_offset_of_nameOrPseudonym_2(),
	PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078::get_offset_of_nameDistinguisher_3(),
	PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078::get_offset_of_dateOfBirth_4(),
	PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078::get_offset_of_placeOfBirth_5(),
	PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078::get_offset_of_gender_6(),
	PersonalData_tEE35DE1A9FCD4AC8ACF3D773E30EF18628D66078::get_offset_of_postalAddress_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5343 = { sizeof (SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259), -1, sizeof(SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5343[7] = 
{
	SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields::get_offset_of_IdSigI_0(),
	SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields::get_offset_of_IdSigIKP_1(),
	SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields::get_offset_of_IdSigICP_2(),
	SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields::get_offset_of_IdSigION_3(),
	SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields::get_offset_of_IdSigIKPDirectoryService_4(),
	SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields::get_offset_of_IdSigIONPersonalData_5(),
	SigIObjectIdentifiers_tD8C77F569D3BFE49D00C9BBA3DBCAD98E0E8E259_StaticFields::get_offset_of_IdSigICPSigConform_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5344 = { sizeof (BiometricData_t1CD2E01651D7A1B51ED11D258B792B754E0A2DE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5344[4] = 
{
	BiometricData_t1CD2E01651D7A1B51ED11D258B792B754E0A2DE4::get_offset_of_typeOfBiometricData_2(),
	BiometricData_t1CD2E01651D7A1B51ED11D258B792B754E0A2DE4::get_offset_of_hashAlgorithm_3(),
	BiometricData_t1CD2E01651D7A1B51ED11D258B792B754E0A2DE4::get_offset_of_biometricDataHash_4(),
	BiometricData_t1CD2E01651D7A1B51ED11D258B792B754E0A2DE4::get_offset_of_sourceDataUri_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5345 = { sizeof (EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019), -1, sizeof(EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5345[5] = 
{
	EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields::get_offset_of_IdEtsiQcs_0(),
	EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields::get_offset_of_IdEtsiQcsQcCompliance_1(),
	EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields::get_offset_of_IdEtsiQcsLimitValue_2(),
	EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields::get_offset_of_IdEtsiQcsRetentionPeriod_3(),
	EtsiQCObjectIdentifiers_t2272D860F101FE2235B85C342C710FA512AC6019_StaticFields::get_offset_of_IdEtsiQcsQcSscd_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5346 = { sizeof (Iso4217CurrencyCode_tA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5346[4] = 
{
	0,
	0,
	0,
	Iso4217CurrencyCode_tA7E4123F97AED5357BAF7EF0DF9D92FF95EA6A8E::get_offset_of_obj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5347 = { sizeof (MonetaryValue_tBE0AC4C885422EEF9CD262C5AA058E5CC935F1E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5347[3] = 
{
	MonetaryValue_tBE0AC4C885422EEF9CD262C5AA058E5CC935F1E0::get_offset_of_currency_2(),
	MonetaryValue_tBE0AC4C885422EEF9CD262C5AA058E5CC935F1E0::get_offset_of_amount_3(),
	MonetaryValue_tBE0AC4C885422EEF9CD262C5AA058E5CC935F1E0::get_offset_of_exponent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5348 = { sizeof (QCStatement_t6D61FB1103E3DDE4A73AB9062B813E46EF20F963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5348[2] = 
{
	QCStatement_t6D61FB1103E3DDE4A73AB9062B813E46EF20F963::get_offset_of_qcStatementId_2(),
	QCStatement_t6D61FB1103E3DDE4A73AB9062B813E46EF20F963::get_offset_of_qcStatementInfo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5349 = { sizeof (Rfc3739QCObjectIdentifiers_t6F603C5186BA153D221E0FCDD504D37B6DDB0682), -1, sizeof(Rfc3739QCObjectIdentifiers_t6F603C5186BA153D221E0FCDD504D37B6DDB0682_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5349[3] = 
{
	Rfc3739QCObjectIdentifiers_t6F603C5186BA153D221E0FCDD504D37B6DDB0682_StaticFields::get_offset_of_IdQcs_0(),
	Rfc3739QCObjectIdentifiers_t6F603C5186BA153D221E0FCDD504D37B6DDB0682_StaticFields::get_offset_of_IdQcsPkixQCSyntaxV1_1(),
	Rfc3739QCObjectIdentifiers_t6F603C5186BA153D221E0FCDD504D37B6DDB0682_StaticFields::get_offset_of_IdQcsPkixQCSyntaxV2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5350 = { sizeof (SemanticsInformation_t0CCBC07FAFA758E2C154DFBEAF5E05EF3C733BA9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5350[2] = 
{
	SemanticsInformation_t0CCBC07FAFA758E2C154DFBEAF5E05EF3C733BA9::get_offset_of_semanticsIdentifier_2(),
	SemanticsInformation_t0CCBC07FAFA758E2C154DFBEAF5E05EF3C733BA9::get_offset_of_nameRegistrationAuthorities_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5351 = { sizeof (TypeOfBiometricData_t7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5351[3] = 
{
	0,
	0,
	TypeOfBiometricData_t7CC8DC39F5CA02108B63A99BAAB2E15984C9EAF4::get_offset_of_obj_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5352 = { sizeof (AttributeTypeAndValue_tEDDC8E9F1BB20364D723EAA44223E76375C65A27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5352[2] = 
{
	AttributeTypeAndValue_tEDDC8E9F1BB20364D723EAA44223E76375C65A27::get_offset_of_type_2(),
	AttributeTypeAndValue_tEDDC8E9F1BB20364D723EAA44223E76375C65A27::get_offset_of_value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5353 = { sizeof (DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5353[1] = 
{
	DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5354 = { sizeof (Rdn_tA2093D2BCCD3265BBA69AB76402C884C6207F708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5354[1] = 
{
	Rdn_tA2093D2BCCD3265BBA69AB76402C884C6207F708::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5355 = { sizeof (IetfUtilities_t04748B3229C3A70F5662483B0121EEE3DF709499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5356 = { sizeof (Asn1Dump_tFCA8D611C62D064BC367E124B616021CC3AD6CEA), -1, sizeof(Asn1Dump_tFCA8D611C62D064BC367E124B616021CC3AD6CEA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5356[3] = 
{
	Asn1Dump_tFCA8D611C62D064BC367E124B616021CC3AD6CEA_StaticFields::get_offset_of_NewLine_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5357 = { sizeof (FilterStream_tF308252D96A498705BC2A63B721A04E367BE6706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5357[1] = 
{
	FilterStream_tF308252D96A498705BC2A63B721A04E367BE6706::get_offset_of_s_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5358 = { sizeof (UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74), -1, sizeof(UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5358[39] = 
{
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_UaOid_0(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu4145le_1(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu4145be_2(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7564digest_256_3(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7564digest_384_4(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7564digest_512_5(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7564mac_256_6(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7564mac_384_7(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7564mac_512_8(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ecb_128_9(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ecb_256_10(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ecb_512_11(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ctr_128_12(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ctr_256_13(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ctr_512_14(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624cfb_128_15(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624cfb_256_16(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624cfb_512_17(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624cmac_128_18(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624cmac_256_19(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624cmac_512_20(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624cbc_128_21(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624cbc_256_22(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624cbc_512_23(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ofb_128_24(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ofb_256_25(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ofb_512_26(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624gmac_128_27(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624gmac_256_28(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624gmac_512_29(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ccm_128_30(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ccm_256_31(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624ccm_512_32(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624xts_128_33(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624xts_256_34(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624xts_512_35(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624kw_128_36(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624kw_256_37(),
	UAObjectIdentifiers_tC751B1A89C8018A7B3BD16115AE04FFE6BC8CB74_StaticFields::get_offset_of_dstu7624kw_512_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5359 = { sizeof (Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5359[7] = 
{
	Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835::get_offset_of_seconds_2(),
	Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835::get_offset_of_millis_3(),
	Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835::get_offset_of_micros_4(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5360 = { sizeof (MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5360[2] = 
{
	MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849::get_offset_of_hashAlgorithm_2(),
	MessageImprint_t45C405F0EE355FE3A42EE3B7D917C681F0136849::get_offset_of_hashedMessage_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5361 = { sizeof (TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5361[6] = 
{
	TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3::get_offset_of_version_2(),
	TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3::get_offset_of_messageImprint_3(),
	TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3::get_offset_of_tsaPolicy_4(),
	TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3::get_offset_of_nonce_5(),
	TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3::get_offset_of_certReq_6(),
	TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3::get_offset_of_extensions_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5362 = { sizeof (TimeStampResp_tAC8AE758086B07D77018BB1BAB5E780677BD3090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5362[2] = 
{
	TimeStampResp_tAC8AE758086B07D77018BB1BAB5E780677BD3090::get_offset_of_pkiStatusInfo_2(),
	TimeStampResp_tAC8AE758086B07D77018BB1BAB5E780677BD3090::get_offset_of_timeStampToken_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5363 = { sizeof (TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5363[10] = 
{
	TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47::get_offset_of_version_2(),
	TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47::get_offset_of_tsaPolicyId_3(),
	TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47::get_offset_of_messageImprint_4(),
	TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47::get_offset_of_serialNumber_5(),
	TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47::get_offset_of_genTime_6(),
	TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47::get_offset_of_accuracy_7(),
	TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47::get_offset_of_ordering_8(),
	TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47::get_offset_of_nonce_9(),
	TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47::get_offset_of_tsa_10(),
	TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47::get_offset_of_extensions_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5364 = { sizeof (TeleTrusTNamedCurves_tA499005BAC3A263142091AB9C1809ACEEF485277), -1, sizeof(TeleTrusTNamedCurves_tA499005BAC3A263142091AB9C1809ACEEF485277_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5364[3] = 
{
	TeleTrusTNamedCurves_tA499005BAC3A263142091AB9C1809ACEEF485277_StaticFields::get_offset_of_objIds_0(),
	TeleTrusTNamedCurves_tA499005BAC3A263142091AB9C1809ACEEF485277_StaticFields::get_offset_of_curves_1(),
	TeleTrusTNamedCurves_tA499005BAC3A263142091AB9C1809ACEEF485277_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5365 = { sizeof (BrainpoolP160r1Holder_tDC9082B944DA05E409B0AADD46151183FF6DF31C), -1, sizeof(BrainpoolP160r1Holder_tDC9082B944DA05E409B0AADD46151183FF6DF31C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5365[1] = 
{
	BrainpoolP160r1Holder_tDC9082B944DA05E409B0AADD46151183FF6DF31C_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5366 = { sizeof (BrainpoolP160t1Holder_tD2E957EB433DED5FA2076636CA42258BDA6194C0), -1, sizeof(BrainpoolP160t1Holder_tD2E957EB433DED5FA2076636CA42258BDA6194C0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5366[1] = 
{
	BrainpoolP160t1Holder_tD2E957EB433DED5FA2076636CA42258BDA6194C0_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5367 = { sizeof (BrainpoolP192r1Holder_t49E9F282500CD2976A89540CE2927C3BB7021507), -1, sizeof(BrainpoolP192r1Holder_t49E9F282500CD2976A89540CE2927C3BB7021507_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5367[1] = 
{
	BrainpoolP192r1Holder_t49E9F282500CD2976A89540CE2927C3BB7021507_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5368 = { sizeof (BrainpoolP192t1Holder_t7AE6FF54924FB8AB15DDB3E9C56DBCC4234FB438), -1, sizeof(BrainpoolP192t1Holder_t7AE6FF54924FB8AB15DDB3E9C56DBCC4234FB438_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5368[1] = 
{
	BrainpoolP192t1Holder_t7AE6FF54924FB8AB15DDB3E9C56DBCC4234FB438_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5369 = { sizeof (BrainpoolP224r1Holder_t28A94F4AC836DE587696793FDB85B1398364FEBF), -1, sizeof(BrainpoolP224r1Holder_t28A94F4AC836DE587696793FDB85B1398364FEBF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5369[1] = 
{
	BrainpoolP224r1Holder_t28A94F4AC836DE587696793FDB85B1398364FEBF_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5370 = { sizeof (BrainpoolP224t1Holder_t2764822BE418FAB50F9617C6D907E18B31FD5A2E), -1, sizeof(BrainpoolP224t1Holder_t2764822BE418FAB50F9617C6D907E18B31FD5A2E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5370[1] = 
{
	BrainpoolP224t1Holder_t2764822BE418FAB50F9617C6D907E18B31FD5A2E_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5371 = { sizeof (BrainpoolP256r1Holder_tAC00F4C5B4D236BAA2A9DDC59D5B098E510B2240), -1, sizeof(BrainpoolP256r1Holder_tAC00F4C5B4D236BAA2A9DDC59D5B098E510B2240_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5371[1] = 
{
	BrainpoolP256r1Holder_tAC00F4C5B4D236BAA2A9DDC59D5B098E510B2240_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5372 = { sizeof (BrainpoolP256t1Holder_tDE62C06FCE0194F00BB8EA0D01845894657D09E6), -1, sizeof(BrainpoolP256t1Holder_tDE62C06FCE0194F00BB8EA0D01845894657D09E6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5372[1] = 
{
	BrainpoolP256t1Holder_tDE62C06FCE0194F00BB8EA0D01845894657D09E6_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5373 = { sizeof (BrainpoolP320r1Holder_t3F1DD741FBB8192C91644922A04D897DE2BF069A), -1, sizeof(BrainpoolP320r1Holder_t3F1DD741FBB8192C91644922A04D897DE2BF069A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5373[1] = 
{
	BrainpoolP320r1Holder_t3F1DD741FBB8192C91644922A04D897DE2BF069A_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5374 = { sizeof (BrainpoolP320t1Holder_t73D87ECA0CEA861CC89B180D5ACBC1A481FA01F4), -1, sizeof(BrainpoolP320t1Holder_t73D87ECA0CEA861CC89B180D5ACBC1A481FA01F4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5374[1] = 
{
	BrainpoolP320t1Holder_t73D87ECA0CEA861CC89B180D5ACBC1A481FA01F4_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5375 = { sizeof (BrainpoolP384r1Holder_t6D356240ABA0AE876E37788A833B0229E4E0600C), -1, sizeof(BrainpoolP384r1Holder_t6D356240ABA0AE876E37788A833B0229E4E0600C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5375[1] = 
{
	BrainpoolP384r1Holder_t6D356240ABA0AE876E37788A833B0229E4E0600C_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5376 = { sizeof (BrainpoolP384t1Holder_t92A96BBFFE177797F8EB38A56D460A1F9E186888), -1, sizeof(BrainpoolP384t1Holder_t92A96BBFFE177797F8EB38A56D460A1F9E186888_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5376[1] = 
{
	BrainpoolP384t1Holder_t92A96BBFFE177797F8EB38A56D460A1F9E186888_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5377 = { sizeof (BrainpoolP512r1Holder_tA09C2D1E42763C3CDDFD019A7CA15B9C48368CE9), -1, sizeof(BrainpoolP512r1Holder_tA09C2D1E42763C3CDDFD019A7CA15B9C48368CE9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5377[1] = 
{
	BrainpoolP512r1Holder_tA09C2D1E42763C3CDDFD019A7CA15B9C48368CE9_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5378 = { sizeof (BrainpoolP512t1Holder_tCAB82C1105BA7D08DE8974CE82C2B6773570EFE5), -1, sizeof(BrainpoolP512t1Holder_tCAB82C1105BA7D08DE8974CE82C2B6773570EFE5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5378[1] = 
{
	BrainpoolP512t1Holder_tCAB82C1105BA7D08DE8974CE82C2B6773570EFE5_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5379 = { sizeof (TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599), -1, sizeof(TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5379[28] = 
{
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_TeleTrusTAlgorithm_0(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_RipeMD160_1(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_RipeMD128_2(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_RipeMD256_3(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_TeleTrusTRsaSignatureAlgorithm_4(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_RsaSignatureWithRipeMD160_5(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_RsaSignatureWithRipeMD128_6(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_RsaSignatureWithRipeMD256_7(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_ECSign_8(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_ECSignWithSha1_9(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_ECSignWithRipeMD160_10(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_EccBrainpool_11(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_EllipticCurve_12(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_VersionOne_13(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP160R1_14(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP160T1_15(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP192R1_16(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP192T1_17(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP224R1_18(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP224T1_19(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP256R1_20(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP256T1_21(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP320R1_22(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP320T1_23(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP384R1_24(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP384T1_25(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP512R1_26(),
	TeleTrusTObjectIdentifiers_tA82CC0DFAECBEB246464026892FDBC42DA792599_StaticFields::get_offset_of_BrainpoolP512T1_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5380 = { sizeof (SmimeAttributes_t6A797E12F55684FDF7459D0E6093CE62B34CAE7E), -1, sizeof(SmimeAttributes_t6A797E12F55684FDF7459D0E6093CE62B34CAE7E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5380[2] = 
{
	SmimeAttributes_t6A797E12F55684FDF7459D0E6093CE62B34CAE7E_StaticFields::get_offset_of_SmimeCapabilities_0(),
	SmimeAttributes_t6A797E12F55684FDF7459D0E6093CE62B34CAE7E_StaticFields::get_offset_of_EncrypKeyPref_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5381 = { sizeof (SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4), -1, sizeof(SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5381[12] = 
{
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields::get_offset_of_PreferSignedData_2(),
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields::get_offset_of_CannotDecryptAny_3(),
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields::get_offset_of_SmimeCapabilitesVersions_4(),
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields::get_offset_of_Aes256Cbc_5(),
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields::get_offset_of_Aes192Cbc_6(),
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields::get_offset_of_Aes128Cbc_7(),
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields::get_offset_of_IdeaCbc_8(),
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields::get_offset_of_Cast5Cbc_9(),
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields::get_offset_of_DesCbc_10(),
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields::get_offset_of_DesEde3Cbc_11(),
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4_StaticFields::get_offset_of_RC2Cbc_12(),
	SmimeCapabilities_t20369201B014E96F67B3906E1149E9684C2B4DB4::get_offset_of_capabilities_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5382 = { sizeof (SmimeCapabilitiesAttribute_t75BFBECF6B8AC5FF12341D604E15E5FB414DFA49), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5383 = { sizeof (SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B), -1, sizeof(SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5383[8] = 
{
	SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields::get_offset_of_PreferSignedData_2(),
	SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields::get_offset_of_CannotDecryptAny_3(),
	SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields::get_offset_of_SmimeCapabilitiesVersions_4(),
	SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields::get_offset_of_DesCbc_5(),
	SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields::get_offset_of_DesEde3Cbc_6(),
	SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B_StaticFields::get_offset_of_RC2Cbc_7(),
	SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B::get_offset_of_capabilityID_8(),
	SmimeCapability_t5449B39E48F3CA301B6772E17983E08DF7CE441B::get_offset_of_parameters_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5384 = { sizeof (SmimeCapabilityVector_t1D336FCCD77F16F27122120C87F4258A7AC96F67), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5384[1] = 
{
	SmimeCapabilityVector_t1D336FCCD77F16F27122120C87F4258A7AC96F67::get_offset_of_capabilities_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5385 = { sizeof (SmimeEncryptionKeyPreferenceAttribute_t9D36A4A270572BCF24E7C95AF25653F7BF5802D9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5386 = { sizeof (ECPrivateKeyStructure_t8D7DE8938D0B55174D99D7E78456E5DCAD70F55F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5386[1] = 
{
	ECPrivateKeyStructure_t8D7DE8938D0B55174D99D7E78456E5DCAD70F55F::get_offset_of_seq_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5387 = { sizeof (SecNamedCurves_t21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A), -1, sizeof(SecNamedCurves_t21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5387[3] = 
{
	SecNamedCurves_t21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A_StaticFields::get_offset_of_objIds_0(),
	SecNamedCurves_t21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A_StaticFields::get_offset_of_curves_1(),
	SecNamedCurves_t21E27B2218E6D7746D3EA1ADE1FA6AA333FC0D7A_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5388 = { sizeof (Secp112r1Holder_t1D1D45C84C2CE8DE3CE6D0BEA0FED7E5073D3AE3), -1, sizeof(Secp112r1Holder_t1D1D45C84C2CE8DE3CE6D0BEA0FED7E5073D3AE3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5388[1] = 
{
	Secp112r1Holder_t1D1D45C84C2CE8DE3CE6D0BEA0FED7E5073D3AE3_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5389 = { sizeof (Secp112r2Holder_tA6AFD383DA328CC150BDC4C7427C6E4C151F9032), -1, sizeof(Secp112r2Holder_tA6AFD383DA328CC150BDC4C7427C6E4C151F9032_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5389[1] = 
{
	Secp112r2Holder_tA6AFD383DA328CC150BDC4C7427C6E4C151F9032_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5390 = { sizeof (Secp128r1Holder_t819ACD6434C7C1402A6EA3FCD06C247FEF99CA75), -1, sizeof(Secp128r1Holder_t819ACD6434C7C1402A6EA3FCD06C247FEF99CA75_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5390[1] = 
{
	Secp128r1Holder_t819ACD6434C7C1402A6EA3FCD06C247FEF99CA75_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5391 = { sizeof (Secp128r2Holder_t7560CEFCEB9C09E59673D46A731C09CE8C5CB198), -1, sizeof(Secp128r2Holder_t7560CEFCEB9C09E59673D46A731C09CE8C5CB198_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5391[1] = 
{
	Secp128r2Holder_t7560CEFCEB9C09E59673D46A731C09CE8C5CB198_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5392 = { sizeof (Secp160k1Holder_tD1CF7A461D85742DF3526D2F08AB75A49F29528B), -1, sizeof(Secp160k1Holder_tD1CF7A461D85742DF3526D2F08AB75A49F29528B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5392[1] = 
{
	Secp160k1Holder_tD1CF7A461D85742DF3526D2F08AB75A49F29528B_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5393 = { sizeof (Secp160r1Holder_t74946AF12676BA09DA3CD2300C7861ABE3E6D9AD), -1, sizeof(Secp160r1Holder_t74946AF12676BA09DA3CD2300C7861ABE3E6D9AD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5393[1] = 
{
	Secp160r1Holder_t74946AF12676BA09DA3CD2300C7861ABE3E6D9AD_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5394 = { sizeof (Secp160r2Holder_t16C50ADA9D787AE5A487D5AAC402C1A68BE9FD8C), -1, sizeof(Secp160r2Holder_t16C50ADA9D787AE5A487D5AAC402C1A68BE9FD8C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5394[1] = 
{
	Secp160r2Holder_t16C50ADA9D787AE5A487D5AAC402C1A68BE9FD8C_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5395 = { sizeof (Secp192k1Holder_t9D5464195D960C0C13CEFE8B7FE5A57B8F4BCAA0), -1, sizeof(Secp192k1Holder_t9D5464195D960C0C13CEFE8B7FE5A57B8F4BCAA0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5395[1] = 
{
	Secp192k1Holder_t9D5464195D960C0C13CEFE8B7FE5A57B8F4BCAA0_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5396 = { sizeof (Secp192r1Holder_tB40768428A806BB6E7AEE00D2B494395A41E995B), -1, sizeof(Secp192r1Holder_tB40768428A806BB6E7AEE00D2B494395A41E995B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5396[1] = 
{
	Secp192r1Holder_tB40768428A806BB6E7AEE00D2B494395A41E995B_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5397 = { sizeof (Secp224k1Holder_t1675BC604667C4EDBB03CED70D742F9039EBF013), -1, sizeof(Secp224k1Holder_t1675BC604667C4EDBB03CED70D742F9039EBF013_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5397[1] = 
{
	Secp224k1Holder_t1675BC604667C4EDBB03CED70D742F9039EBF013_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5398 = { sizeof (Secp224r1Holder_tDC4BE46EC2780885B913042911B169B7A79DD96D), -1, sizeof(Secp224r1Holder_tDC4BE46EC2780885B913042911B169B7A79DD96D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5398[1] = 
{
	Secp224r1Holder_tDC4BE46EC2780885B913042911B169B7A79DD96D_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5399 = { sizeof (Secp256k1Holder_tA264EBF6C1037E3AF7C804529474E198A069EED2), -1, sizeof(Secp256k1Holder_tA264EBF6C1037E3AF7C804529474E198A069EED2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5399[1] = 
{
	Secp256k1Holder_tA264EBF6C1037E3AF7C804529474E198A069EED2_StaticFields::get_offset_of_Instance_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
