None for the second waited nor the third.

Meantime as on I pass'd, one met my sight
Whom soon as view'd; "Of him," cried I, "not yet
Mine eye hath had his fill."  With fixed gaze
I therefore scann'd him.  Straight the teacher kind
Paus'd with me, and consented I should walk
Backward a space, and the tormented spirit,
Who thought to hide him, bent his visage down.
But it avail'd him nought; for I exclaim'd:
"Thou who dost cast thy eye upon the ground,
Unless thy features do belie thee much,
Venedico art thou.  But what brings thee
Into this bitter seas'ning?"  He replied:
"Unwillingly I answer to thy words.
But thy clear speech, that to my mind recalls
The world I once inhabited, constrains me.
Know then 'twas I who led fair Ghisola
To do the Marquis' will, however fame
The shameful tale have bruited.  Nor alone
Bologna hither sendeth me to mourn
Rather with us the place is so o'erthrong'd
That not so many tongues this day are taught,
Betwixt the Reno and Savena's stream,
To answer SIPA in their country's phrase.
And if of that securer proof thou need,
Remember but our craving thirst for gold."

Him speaking thus, a demon with his thong
Struck, and exclaim'd, "Away! corrupter! here
Women are none for sale."  Forthwith I join'd
My escort, and few paces thence we came
To where a rock forth issued from the bank.
That easily ascended, to the right
Upon its splinter turning, we depart
From those eternal barriers. When arriv'd,
Where underneath the gaping arch lets pass
The scourged souls: "Pause here," the teacher said,
"And let these others miserable, now
Strike on thy ken, faces not yet beheld,
For that together they with us have walk'd."

From the old bridge we ey'd the pack, who came
From th' other side towards us, like the rest,
Excoriate from the lash.  My gentle guide,
By me unquestion'd, thus his speech resum'd:
"Behold that lofty shade, who this way tends,
And seems too woe-begone to drop a tear.
How yet the regal aspect he retains!
Jason is he, whose skill and prowess won
The ram from Colchos. To the Lemnian isle
His passage thither led him, when those bold
And pitiless women had slain all their males.
There he with tokens and fair witching words
Hypsipyle beguil'd, a virgin young,
Who first had all the rest herself beguil'd.
Impregnated he left her there forlorn.
Such is the guilt condemns him to this pain.
Here too Medea's inj'ries are avenged.
All bear him company, who like deceit
To his have practis'd.  And thus much to know
Of the first vale suffice thee, and of those
Whom its keen torments urge."  Now had we come
Where, crossing the next pier, the straighten'd path
Bestrides its shoulders to another arch.

Hence in the second chasm we heard the ghosts,
Who jibber in low melancholy sounds,
With wide-stretch'd nostrils snort, and on themselves
Smite with their palms.  Upon the banks a scurf
From the foul steam condens'd, encrusting hung,
That held sharp combat with the sight and smell.

So hollow is the depth, that from no part,
Save on the summit of the rocky span,
Could I distinguish aught.  Thus far we came;
And thence I saw, within the foss below,
A crowd immers'd in ordure, that appear'd
Draff of the human body.  There beneath
Searching with eye inquisitive, I mark'd
One with his head so grim'd, 't were hard to deem,
If he were clerk or layman.  Loud he cried:
"Why greedily thus bendest more on me,
Than on these other filthy ones, thy ken?"