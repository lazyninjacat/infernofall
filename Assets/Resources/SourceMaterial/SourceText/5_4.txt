"Then by that love which carries them along,
Entreat; and they will come."  Soon as the wind
Sway'd them toward us, I thus fram'd my speech:
"O wearied spirits! come, and hold discourse
With us, if by none else restrain'd."  As doves
By fond desire invited, on wide wings
And firm, to their sweet nest returning home,
Cleave the air, wafted by their will along;
Thus issu'd from that troop, where Dido ranks,
They through the ill air speeding; with such force
My cry prevail'd by strong affection urg'd.

"O gracious creature and benign! who go'st
Visiting, through this element obscure,
Us, who the world with bloody stain imbru'd;
If for a friend the King of all we own'd,
Our pray'r to him should for thy peace arise,
Since thou hast pity on our evil plight.
Of whatsoe'er to hear or to discourse
It pleases thee, that will we hear, of that
Freely with thee discourse, while e'er the wind,
As now, is mute.  The land, that gave me birth,
Is situate on the coast, where Po descends
To rest in ocean with his sequent streams.

"Love, that in gentle heart is quickly learnt,
Entangled him by that fair form, from me
Ta'en in such cruel sort, as grieves me still:
Love, that denial takes from none belov'd,
Caught me with pleasing him so passing well,
That, as thou see'st, he yet deserts me not.