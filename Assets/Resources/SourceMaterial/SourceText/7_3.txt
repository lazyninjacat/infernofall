"My guide! of thee this also would I learn;
This fortune, that thou speak'st of, what it is,
Whose talons grasp the blessings of the world?"

He thus: "O beings blind! what ignorance
Besets you?  Now my judgment hear and mark.
He, whose transcendent wisdom passes all,
The heavens creating, gave them ruling powers
To guide them, so that each part shines to each,
Their light in equal distribution pour'd.
By similar appointment he ordain'd
Over the world's bright images to rule
Superintendence of a guiding hand
And general minister, which at due time
May change the empty vantages of life
From race to race, from one to other's blood,
Beyond prevention of man's wisest care:
Wherefore one nation rises into sway,
Another languishes, e'en as her will
Decrees, from us conceal'd, as in the grass
The serpent train.  Against her nought avails
Your utmost wisdom.  She with foresight plans,
Judges, and carries on her reign, as theirs
The other powers divine.  Her changes know
None intermission: by necessity
She is made swift, so frequent come who claim
Succession in her favours.  This is she,
So execrated e'en by those, whose debt
To her is rather praise; they wrongfully
With blame requite her, and with evil word;
But she is blessed, and for that recks not:
Amidst the other primal beings glad
Rolls on her sphere, and in her bliss exults.
Now on our way pass we, to heavier woe
Descending: for each star is falling now,
That mounted at our entrance, and forbids
Too long our tarrying."  We the circle cross'd
To the next steep, arriving at a well,
That boiling pours itself down to a foss
Sluic'd from its source.  Far murkier was the wave
Than sablest grain: and we in company
Of the inky waters, journeying by their side,
Enter'd, though by a different track, beneath.
Into a lake, the Stygian nam'd, expands
The dismal stream, when it hath reach'd the foot
Of the grey wither'd cliffs.  Intent I stood
To gaze, and in the marish sunk descried
A miry tribe, all naked, and with looks
Betok'ning rage.  They with their hands alone
Struck not, but with the head, the breast, the feet,
Cutting each other piecemeal with their fangs.

The good instructor spake; "Now seest thou, son!
The souls of those, whom anger overcame.
This too for certain know, that underneath
The water dwells a multitude, whose sighs
Into these bubbles make the surface heave,
As thine eye tells thee wheresoe'er it turn.
Fix'd in the slime they say: 'Sad once were we
In the sweet air made gladsome by the sun,
Carrying a foul and lazy mist within:
Now in these murky settlings are we sad.'
Such dolorous strain they gurgle in their throats.
But word distinct can utter none."  Our route
Thus compass'd we, a segment widely stretch'd
Between the dry embankment, and the core
Of the loath'd pool, turning meanwhile our eyes
Downward on those who gulp'd its muddy lees;
Nor stopp'd, till to a tower's low base we came.